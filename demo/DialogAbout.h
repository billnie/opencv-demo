#pragma once
#include "afxcmn.h"


// CDialogAbout 对话框
namespace aui {
	class CDialogAbout : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogAbout)

	public:
		CDialogAbout(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogAbout();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_ABOUT };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		afx_msg void OnSize(UINT nType, int cx, int cy);
		virtual BOOL OnInitDialog();
		CRichEditCtrl m_richEdit;
		afx_msg void OnBnClickedBtnHlp();
		afx_msg void OnBnClickedBtnDownVs();

		int addrichLog(LPCTSTR title, LPCTSTR message);
		afx_msg void OnBnClickedBtnClear();
		afx_msg void OnBnClickedBtnGpath();
	};
}