#pragma once
#include "afxcmn.h"


// CDialogLog 对话框
namespace aui {
	class CDialogLog : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogLog)

	public:
		CDialogLog(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogLog();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_LOGIN };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		BOOL bAutoScroll;
		CRichEditCtrl m_richEdit;
		CRichEditCtrl m_richEditLog;

		int addrichLog(LPTSTR title, LPTSTR message);
		int addrichLog2(LPTSTR title, LPTSTR message);
		virtual BOOL OnInitDialog();
		afx_msg void OnBnClickedBtnClear();
		afx_msg void OnBnClickedBtnClear2();
		afx_msg void OnSize(UINT nType, int cx, int cy);
		afx_msg void OnDestroy();
		virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
		afx_msg void OnBnClickedBtnScroll();
	};
}