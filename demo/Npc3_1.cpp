#include "Npc3_1.h"

namespace anpc {

	CNpc3_1::CNpc3_1()
	{
	}


	CNpc3_1::~CNpc3_1()
	{
	}
	int CNpc3_1::fighturn(int turn, int sub) {
		return CNpcBase::fighturn(turn, sub);
	}
	//加状态处理，有一些可能 不一样的
	int CNpc3_1::addfightState(){
		return CNpcComm::addfightState();
	}
	int CNpc3_1::updateturn(string info) {		//提示轮到我了
		return CNpcBase::updateturn(info);
	}
}