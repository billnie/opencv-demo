

#include	 "stlstr.h"
#include "StdAfx.h"
#include <io.h>  
#include <fcntl.h> 
#include<fstream>
#include <iostream>
#include <istream>
#include <ostream>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/common.hpp>
   
#include <boost/xpressive/xpressive_dynamic.hpp>  
#include "log_config.h"
#include "log_producer_client.h"
#include "apr_iconv.h"
#include "nb_osx_win.h"
using namespace boost::xpressive;
namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;
using namespace std;
using std::string;
// 注释：多字节包括GBK和UTF-8
int GBK2UTF8(char *szGbk, char *szUtf8, int Len)
{
	// 先将多字节GBK（CP_ACP或ANSI）转换成宽字符UTF-16
	// 得到转换后，所需要的内存字符数
	int n = MultiByteToWideChar(CP_ACP, 0, szGbk, -1, NULL, 0);
	// 字符数乘以 sizeof(WCHAR) 得到字节数
	WCHAR *str1 = new WCHAR[sizeof(WCHAR) * n];
	// 转换
	MultiByteToWideChar(CP_ACP,  // MultiByte的代码页Code Page
		0,            //附加标志，与音标有关
		szGbk,        // 输入的GBK字符串
		-1,           // 输入字符串长度，-1表示由函数内部计算
		str1,         // 输出
		n             // 输出所需分配的内存
		);

	// 再将宽字符（UTF-16）转换多字节（UTF-8）
	n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);
	if (n > Len)
	{
		delete[]str1;
		return -1;
	}
	WideCharToMultiByte(CP_UTF8, 0, str1, -1, szUtf8, n, NULL, NULL);
	delete[]str1;
	str1 = NULL;
	return 0;
}
int iconvs(char *ins, char *outs) {
	char *out, *buffer;

	apr_pool_t* pool = NULL;


	const char *in = "软件开始启动";
	const char *to = "UTF-8";
	const char *from = "GBK";
	apr_size_t buffer_len;
	apr_size_t in_len = strlen(in);
	apr_size_t out_len = in_len * 3;
	apr_size_t converted_len = 2;
	apr_iconv_t handle;
	apr_status_t cd;
	buffer_len = out_len;
	buffer = (char*)malloc(sizeof(char)*out_len);
	buffer_len--;
	cd = apr_iconv_open(to, from, pool, &handle);
	if (cd != 0) {
		return cd;
	}
	apr_iconv(handle, &in, &in_len, &out, &out_len, &converted_len);
	cout << in << endl;
	apr_iconv_close(handle, pool);

}
void DT(const char * fmt, ...)
{
#ifdef NDEBUG
	return;
#endif
	std::string strResult = "";
	if (NULL != fmt)
	{
		va_list marker = NULL;
		va_start(marker, fmt);                            //初始化变量参数 
		size_t nLength = _vscprintf(fmt, marker) + 1;    //获取格式化字符串长度
		std::vector<char> vBuffer(nLength, '\0');        //创建用于存储格式化字符串的字符数组
		int nWritten = _vsnprintf_s(&vBuffer[0], vBuffer.size(), nLength, fmt, marker);
		if (nWritten > 0)
		{
			strResult = &vBuffer[0];
			log_producer_client *client = get_log_producer_client(g_producer, NULL);
			char buf[1024];
			GBK2UTF8((char*)strResult.c_str(), buf, 1024);
			log_producer_client_add_log(client, 2, "log", buf);//			log_producer_client_add_logfmt(get_log_producer_client(g_producer, NULL), 3, "", "", strResult.c_str());
		}
		va_end(marker);                                    //重置变量参数
	}

	SYSTEMTIME sTime;
	GetLocalTime(&sTime);
	TCHAR chBuf[100] = { 0 };
	//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
	//	sTime.wMilliseconds,sTime.wDayOfWeek);
	wsprintf(chBuf, _T("%02u:%02u:%02u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
		sTime.wMilliseconds);
	OutputDebugStringA(chBuf);
//#ifdef _DEBUG
//	BOOST_LOG_TRIVIAL(debug) << strBuffer;
//#endif
	BOOST_LOG_TRIVIAL(debug) << strResult;
	OutputDebugStringA(strResult.c_str());
	OutputDebugStringA("\r\n");
}

namespace str
{

	void InitConsoleWindow(void)
	{
		int hCrt;
		FILE *hf;
		AllocConsole();
		hCrt = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
		hf = _fdopen(hCrt, "w");
		*stdout = *hf;
		setvbuf(stdout, NULL, _IONBF, 0);
	}
	int read_file(std::vector<std::string> &data, std::string szFile)
	{
		data.clear();
		//
		std::ifstream file_(szFile.c_str(), std::ios::in);
		//std::wifstream file_;
		//file_.open(szFile.c_str(), std::ios::in);
		if (!file_) return -1;
		const int bufsize = 512;
		char strbuf[bufsize];
		//
		while (!file_.eof())
		{
			file_.getline(strbuf, bufsize);
			string sentence = strbuf;
			if (sentence.empty())
			{
				continue;
			}
			data.push_back(sentence);
		}
		file_.close();
		//
		if (data.size() < 1)
		{
			return -1;
		}
		return 0;
	}
	int initlog(std::string file) {
		// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
		//  执行此操作
		char tempbuf[128];
		char strtm[32] = { 0 };

		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);

		timeinfo = localtime(&rawtime);

		strftime(tempbuf, 128, ".\\log\\%Y%m%d.log", timeinfo);
		boost::log::add_file_log(
			keywords::auto_flush = true,
			keywords::open_mode = std::ios::app,//追加写入  
			keywords::file_name = tempbuf,// "log_%N.log",// AppHolder::Instance().config().log_folder + "/sign_%Y-%m-%d_%H-%M-%S.%N.log",
			keywords::rotation_size = 10 * 1024 * 1024,
			keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
			keywords::min_free_space = 3 * 1024 * 1024,
			keywords::format = "[%TimeStamp%]: %Message%"
			);

		boost::log::add_common_attributes();
		string ss;
		ss = getManufactureID() + "," + getCpuType();
		initaliyunproducerlog("log_config.json", getOsInfo().c_str(),ss.c_str());
		BOOST_LOG_TRIVIAL(info) << "---欢迎使用天堂M辅助!---";
		return 0;
	}
	
	int log(int lvl, char *fmt, ...) {
		std::string strResult = "";
		if (NULL != fmt)
		{
			va_list marker = NULL;
			va_start(marker, fmt);                            //初始化变量参数 
			size_t nLength = _vscprintf(fmt, marker) + 1;    //获取格式化字符串长度
			std::vector<char> vBuffer(nLength, '\0');        //创建用于存储格式化字符串的字符数组
			int nWritten = _vsnprintf_s(&vBuffer[0], vBuffer.size(), nLength, fmt, marker);
			if (nWritten > 0)
			{
				strResult = &vBuffer[0];
			}
			va_end(marker);             
			log_producer_client *client = get_log_producer_client(g_producer, NULL);
			char buf[1024];
			GBK2UTF8((char*)strResult.c_str(), buf, 1024);
			log_producer_client_add_log(client, lvl, "log", buf);
			//log_producer_client_logfmt(client, 3,"", "",strResult.c_str());
			//重置变量参数
			switch (lvl) {
			case 0:
				BOOST_LOG_TRIVIAL(trace) << strResult;
				break;
			case 1:
				BOOST_LOG_TRIVIAL(debug) << strResult;
				break;
			case 2:
				BOOST_LOG_TRIVIAL(info) << strResult;
				break;
			case 3:
				BOOST_LOG_TRIVIAL(warning) << strResult;
				break;
			default:
				BOOST_LOG_TRIVIAL(trace) << strResult;
			}
		}
		return 0;
	}
	std::string format(const char *fmt, ...)
	{
		std::string strResult = "";
		if (NULL != fmt)
		{
			va_list marker = NULL;
			va_start(marker, fmt);                            //初始化变量参数 
			size_t nLength = _vscprintf(fmt, marker) + 1;    //获取格式化字符串长度
			std::vector<char> vBuffer(nLength, '\0');        //创建用于存储格式化字符串的字符数组
			int nWritten = _vsnprintf_s(&vBuffer[0], vBuffer.size(), nLength, fmt, marker);
			if (nWritten > 0)
			{
				strResult = &vBuffer[0];
			}
			va_end(marker);                                    //重置变量参数
		}
		return strResult;
	}
	//字符串格式化函数
	std::wstring format(const wchar_t *fmt, ...)
	{
		std::wstring strResult = L"";
		if (NULL != fmt)
		{
			va_list marker = NULL;
			va_start(marker, fmt);                            //初始化变量参数
			size_t nLength = _vscwprintf(fmt, marker) + 1;    //获取格式化字符串长度
			std::vector<wchar_t> vBuffer(nLength, L'\0');    //创建用于存储格式化字符串的字符数组
			int nWritten = _vsnwprintf_s(&vBuffer[0], vBuffer.size(), nLength, fmt, marker);
			if (nWritten > 0)
			{
				strResult = &vBuffer[0];
			}
			va_end(marker);                                    //重置变量参数
		}
		return strResult;
	}

	bool isregxip(std::string str) {
		cregex reg_ip = cregex::compile("(25[0-4]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[1-9])[.](25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])[.](25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])[.](25[0-4]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[1-9])");
		return  regex_match(str.c_str(), reg_ip);
	}
}