// FormControl.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "FormControl.h"


// CFormControl

IMPLEMENT_DYNCREATE(CFormControl, CFormView)

CFormControl::CFormControl()
	: CFormView(IDD_DLG_FORM)
{

}

CFormControl::~CFormControl()
{
}

void CFormControl::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_tabCtrl);
}

BEGIN_MESSAGE_MAP(CFormControl, CFormView)
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CFormControl::OnSelchangeTab1)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CFormControl 诊断

#ifdef _DEBUG
void CFormControl::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CFormControl::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CFormControl 消息处理程序


void CFormControl::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	m_tabCtrl.InsertItem(0, TEXT("    游戏    "));  //添加参数一选项卡
	m_tabCtrl.InsertItem(1, TEXT("    测试数据    "));  //添加参数一选项卡

	m_dlg.Create(IDD_DEMO_DIALOG, this);

	//		m_dlg5.Create(IDD_DLG_TRIG,this);
	//获得IDC_TABTEST客户区大小
	CRect rs;
	m_tabCtrl.GetClientRect(&rs);
	//调整子对话框在父窗口中的位置
	rs.top += 40;
	rs.bottom -= 44;
	rs.left += 4;
	rs.right -= 4;

	//设置子对话框尺寸并移动到指定位置
	m_dlg.MoveWindow(&rs);

	//分别设置隐藏和显示
	m_dlg.ShowWindow(true);

	// TODO: 在此添加专用代码和/或调用基类
}


void CFormControl::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	CSize sizeTotal;
	sizeTotal.cx = cx;
	sizeTotal.cy = cy;
	SetScrollSizes(MM_TEXT, sizeTotal);

	if (m_tabCtrl.GetSafeHwnd() != NULL) {
		CRect rect;
		m_tabCtrl.GetWindowRect(rect);
		ScreenToClient(rect);
		rect.right = cx - rect.left;
		rect.bottom = cy - rect.left;
		m_tabCtrl.MoveWindow(rect);
		rect.top = 40;
		m_dlg.MoveWindow(rect);

	}
}


void CFormControl::OnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


void CFormControl::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CFormView::OnTimer(nIDEvent);
}
