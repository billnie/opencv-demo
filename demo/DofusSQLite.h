#pragma once
#include "sqlite3.h"
#include	<map>
#include	<string>
using namespace std;
enum kiVarType{
	kiVarTypeInt =1,
	kiVarTypeFloat,
	kiVarTypeInt64,
	kiVarTypeString,
};

union kiVar{
	int ival;
	float fval;
	__int64 hVal;
	char* sVal;
};

struct kiVal{
	unsigned char iT;
	kiVar var;
};

typedef map<string, kiVal> mapSQL;
typedef vector<mapSQL> vecSQL;
typedef vector<mapSQL>:: iterator vecSQLIterator;
typedef map<string, kiVal>:: iterator mapSQLIterator;

class CDofusSQLite
{
	static CDofusSQLite *g_dofusSQL;
	int commupdatesql(const char *sql, sqlite3 *pdb){
		int ret = -1;
		if (sql && pdb) {
			char *errmsg = NULL;
			ret = sqlite3_exec(pdb,sql,NULL,NULL,&errmsg);
			if(ret != SQLITE_OK){
				sqlite3_free(errmsg);
			}
		}
		return ret ;
	}
	int commcountsql(const char *sql, sqlite3 *pdb){
		int ret = -1;
		if (sql && pdb) {
			char *errmsg = NULL;
			sqlite3_stmt *stmt = NULL   ;
			sqlite3_prepare(pdb,sql,-1,&stmt,0);
			while(sqlite3_step(stmt)==SQLITE_ROW)
			{
				ret = sqlite3_column_int(stmt,0);
				break;
			}
			sqlite3_finalize(stmt);
		}
		return ret ;
	}
	mapSQL commrowmap(sqlite3_stmt *stmt ){
		int ret = -1;
		mapSQL mp;
		if (stmt) {
			int i;
			int type;
			kiVal kv;
			const char *col_0_name;
			int cnt = sqlite3_column_count(stmt); // 结果集中列的数量
			for (i = 0; i < cnt; i++) {
				type = sqlite3_column_type(stmt, i);
				col_0_name = sqlite3_column_name(stmt, i); // 获取列名
				switch (type) {
					case SQLITE_INTEGER:
						if (strcmp(col_0_name, "dH") == 0) {
							kv.iT = kiVarTypeInt64;
							kv.var.hVal = sqlite3_column_int64(stmt, i);
						}
						else {
							kv.iT = kiVarTypeInt;
							kv.var.ival = sqlite3_column_int(stmt, i);
						}
						mp[col_0_name] = kv;
						break;
					case SQLITE3_TEXT: {
						const char *str;
						kv.iT = kiVarTypeString;
						str = (const char *)sqlite3_column_text(stmt, i);
						if (str && strlen(str)>0) {
							kv.var.sVal = strdup(str);
							mp[col_0_name] = kv;
						}
					}
						break;
				//		case SLQITE3_INT
					case SQLITE_FLOAT:
						kv.iT = kiVarTypeFloat;
						kv.var.fval = sqlite3_column_double(stmt,i);
						mp[col_0_name] = kv;
						break;
					default:
						type++;
						break;
				}
			}
			ret = cnt;
		}
		return mp;
	}
public:
	static CDofusSQLite * instance(){
		if(g_dofusSQL == NULL){
			g_dofusSQL = new CDofusSQLite();
		}
		return g_dofusSQL;
	}
	CDofusSQLite(void);
	~CDofusSQLite(void);

	int ExecuteSQL(LPCTSTR sql = NULL, sqlite3_stmt **stat = NULL);
	void CloseDB();
private: 
	bool OpenDB();

	HANDLE m_hRead;
	sqlite3 *db;
	LPCTSTR dbName;
	LPCTSTR sqlTemp;
	sqlite3_stmt **ssTemp;
	int nResult;
public:
	vecSQL getAllFrame(void);
	//返回1为插入，返回2为更新
	int addFrame(mapSQL mp, int *fsid);
	//获取地图，根据编号来获取
	mapSQL getFsidFrame(int fsid);
	//获取地图信息，根据坐标来获取
	mapSQL getxyzFrame(int x, int y, int sub);
	mapSQL getmidFrame(string mid);
	//取不可走的地图信息
	int getdisalbeFrame(int x, int y, int sub, unsigned char** buf);
	//更新不可走的地图信息
	int adddisalbeFrame(int x, int y, int sub, unsigned char* buf,int len);
	//判断当前图是否可以战斗
	mapSQL getmapfight(int x,int y,int sub);
	//增加可战斗的图
	int addmapfight(mapSQL mp);
	//增加用户到数据库
	int addchact(mapSQL mp);
	//增加战斗到数据库
	int addfight(mapSQL mp);
};

