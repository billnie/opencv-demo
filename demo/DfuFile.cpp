// DfuFile.cpp: implementation of the CDfuFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DfuFile.h"
#include "resource.h"

#include <fstream>// std::ifstream
#include <iostream>// std::wcout
#include <vector>
#include <string>
#include <Tlhelp32.h> 
#include <psapi.h>
using std::vector;
using std::string;
//#pragma comment("zlibstat.lib")
#define CSIDL_LOCAL_APPDATA 0x001c        // <user name>\Local Settings\Applicaiton 
//#include "afxconv.h"



#define DT	DebugTrace
//uncomment following line to disable DT
//#undef DT
//#define DT

BOOL DebugTrace(TCHAR *lpszFormat,...)
{
	static HWND hwnd = ::FindWindow(_T("Fhua-DbgView"),	_T("DbgView"));
	if(!IsWindow(hwnd))
		hwnd = ::FindWindow(NULL, _T("DbgView"));
	
	{
		static TCHAR szMsg[512];
		va_list argList;
		va_start(argList, lpszFormat);
		try
		{
			_vstprintf(szMsg,lpszFormat, argList);
		}
		catch(...)
		{
			lstrcpy(szMsg ,_T("DebugHelper:Invalid string format!"));
		}
		va_end(argList);
		DWORD dwId = GetCurrentProcessId();
		if(hwnd)
			::SendMessage(hwnd,WM_SETTEXT,dwId,(LPARAM)(LPCTSTR)szMsg);
		else
			OutputDebugString(szMsg);
	}
	return TRUE;
}

int read_file(vector<string> &data, string szFile)
{
	data.clear();
	//
	std::ifstream file_(szFile.c_str(), std::ios::in);
	//std::wifstream file_;
	//file_.open(szFile.c_str(), std::ios::in);
	if (!file_) return -1;
	const int bufsize = 512;
	char strbuf[bufsize];
	//
	while (!file_.eof())
	{
		file_.getline(strbuf, bufsize);
		string sentence = strbuf;
		if (sentence.empty())
		{
			continue;
		}
		data.push_back(sentence);
	}
	file_.close();
	//
	if (data.size() < 1)
	{
		return -1;
	}
	return 0;
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDfuFile::CDfuFile()
{

}

CDfuFile::~CDfuFile()
{

}
BOOL CDfuFile::RegistryDll(CString& szDllPath)
{

	LRESULT(CALLBACK* lpDllEntryPoint)();
	HINSTANCE hLib = LoadLibrary(szDllPath);
	if (hLib < (HINSTANCE)HINSTANCE_ERROR)
	{
		return FALSE;
	}
	(FARPROC&)lpDllEntryPoint = GetProcAddress(hLib, "DllRegisterServer");
	BOOL bRet = FALSE;
	if (lpDllEntryPoint != NULL)
	{
		HRESULT hr = (*lpDllEntryPoint)();
		bRet = SUCCEEDED(hr);
		if (FAILED(hr))
		{
			AfxMessageBox("注册失败");
		}
	}
	FreeLibrary(hLib);
	return bRet;
}
long CDfuFile::GetFileSize(LPCTSTR str) {
	long lret = -1;
	if (str && _tcslen(str) > 0) {
		HANDLE handle = CreateFile(str, FILE_READ_EA, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
		if (handle != INVALID_HANDLE_VALUE)
		{
			lret = ::GetFileSize(handle, NULL);
			CloseHandle(handle);
		}
	}
	return lret;
}
int CDfuFile::RunApp(LPCTSTR path) {
	int ret = -1;
	CString str;
	PROCESS_INFORMATION ProcessInfo;
	STARTUPINFO StartupInfo; //入口参数
	ZeroMemory(&ProcessInfo, sizeof(ProcessInfo));
	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof(StartupInfo);

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	str = path;
	DT(str.LockBuffer());
	if (CreateProcess(str, NULL,
		NULL, NULL, FALSE, 0, NULL, NULL, &StartupInfo, &ProcessInfo)) {
		//WaitForSingleObject(ProcessInfo.hProcess, 10);
		//CloseHandle(ProcessInfo.hThread);
		//CloseHandle(ProcessInfo.hProcess);
		ret = 1;
	}
	return ret;
}
BOOL CDfuFile::EnumProcessbyName(DWORD   dwPID, LPCTSTR   ExeName, LONG type)
{
	int IsEuemprosuccess = 0;
	if (IsEuemprosuccess == 0)
	{
		int nItem = 0;	// 项计数
		PROCESSENTRY32 pe32 = { sizeof(PROCESSENTRY32) };
		HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hProcessSnap == INVALID_HANDLE_VALUE)
			return FALSE;
		if (::Process32First(hProcessSnap, &pe32))
		{
			do
			{
				if (type == 1)
				{
					if (strstr(pe32.szExeFile, ExeName) != NULL) //模糊匹配
					{
						DT(pe32.szExeFile);
						//		npid[nItem] = pe32.th32ProcessID;
						IsEuemprosuccess++;
						nItem++;
					}
				}
				else
				{
					if (!strstr(pe32.szExeFile, ExeName))
					{
						DT(pe32.szExeFile);
//						addrichLog(NULL, pe32.szExeFile);
						//		npid[nItem] = pe32.th32ProcessID;
						IsEuemprosuccess++;
						nItem++;
					}
				}

			} while (::Process32Next(hProcessSnap, &pe32));
		}
		::CloseHandle(hProcessSnap);
		if (IsEuemprosuccess>0)
			return TRUE;
	}
	else
	{
		for (int i = 0; i<IsEuemprosuccess; i++)
		{
			//if (dwPID == npid[i])
			//	return TRUE;
		}
	}

	return   FALSE;
}

long CDfuFile::GetResourceSize(HRSRC hInst) {
	long lret = -1;
	if (hInst !=NULL) {
		lret = SizeofResource(AfxGetResourceHandle(), hInst);
	}
	return lret;
}
CString CDfuFile::GetAppPath() {
	CString m_FilePath;
	GetModuleFileName(NULL, m_FilePath.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	m_FilePath.ReleaseBuffer();
	int m_iPosIndex;
	m_iPosIndex = m_FilePath.ReverseFind('\\');
	m_FilePath = m_FilePath.Left(m_iPosIndex);
	return m_FilePath;
}
int CDfuFile::IsFolderExist(LPCTSTR path) {
	int ret = -1;
	if (path && _tcslen(path) > 0) {
		//判断是否存在
		if (!PathFileExists(path))
			return false;
		//判断是否为目录
		DWORD attributes = ::GetFileAttributes(path);
		attributes &= FILE_ATTRIBUTE_DIRECTORY;
		return attributes == FILE_ATTRIBUTE_DIRECTORY;
	}
	return ret;
}int CDfuFile::IsFileExist(LPCTSTR csPath) {
	int ret = -1;
	HANDLE hFile = CreateFile(
		csPath,                                                 // 要判断的文件或文件夹
		0,                                                      // 我们只需要最低的权限即可
		FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, // 我们也不会对文件有任何占用
		NULL,                                                   // 安全属性，我们不关心
		OPEN_EXISTING,                                          // 只打开已存在的文件，这也是我们的目标
		FILE_ATTRIBUTE_NORMAL,                                  // 默认
		NULL                                                    // 无
		);
	if (INVALID_HANDLE_VALUE == hFile && (ERROR_ACCESS_DENIED != GetLastError()))
	{
		return FALSE;
	}

	if (INVALID_HANDLE_VALUE != hFile)
	{
		CloseHandle(hFile);       ret = 1;                              // 这里一定要关闭文件句柄，不然会导致文件占用和资源泄漏。
		hFile = INVALID_HANDLE_VALUE;
	}
	return ret;
}
int CDfuFile::OpenPath(LPCTSTR path) {
	int ret = 0;
	if (path && _tcslen(path) > 0 && IsFolderExist(path)) {
		TCHAR xpath[MAX_PATH] = { 0 };
		_tcscpy(xpath, _T("/open, "));
		_tcscat(xpath, path);
		ShellExecute(NULL, NULL, _T("explorer"),  xpath, NULL, SW_SHOW);
		ret = 1;
	}
	return ret;
}
CString CDfuFile::GetExtraPath(){
	CString str;
	DWORD dwret;
	TCHAR bufApplicateData[_MAX_PATH] = {0};
	SHGetSpecialFolderPath(NULL,bufApplicateData,/*CSIDL_PERSONAL*/CSIDL_LOCAL_APPDATA   ,NULL);	
	//wstrcat(bufApplicateData, "\\");
	str = bufApplicateData;
	str += _T("\\dfu\\");
	DT((LPTSTR)(LPCTSTR)str);
	dwret = CreateDirectory(str, NULL);
	return str;
}
int CDfuFile::ExportHEXFile(int hex, CString strExportPath)
{
	int ret=-1;
	long lret1, lret2;
	DWORD dwret;
    // 导出system.zip
	//	CString strExportPath;
	//	strExportPath = "d:\\";
    CString strSysDir; 
    HRSRC hrSrcSys =NULL;
	if(hex == 1){
		hrSrcSys = FindResource( AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_HEX1), _T("HEX") );
		strSysDir = strExportPath + _T("script.js");
	}
	else if (hex == 3) {
		hrSrcSys = FindResource(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_HEX2), _T("HEX"));
		strSysDir = strExportPath + _T("game.component.js");
	}
	else if (hex == 2) {
		hrSrcSys = FindResource(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_HEX3), _T("HEX"));
		strSysDir = strExportPath + _T("main.component.js");
	}
	DT((LPTSTR)(LPCTSTR)strSysDir);
	//先判断大小是否相等，相等就不替换了
	lret1 = GetFileSize(strSysDir);
	lret2 = GetResourceSize(hrSrcSys);
	if (lret1 != lret2 && lret2>0) {
		//	strcpy(hexfile, strSysDir.LockBuffer());
		dwret = GetFileAttributes(strSysDir);
		if (dwret == -1) {
			//	dwret = CreateDirectory(strSysDir, NULL);
			dwret = GetLastError();
		}
		else {
			DeleteFile(strSysDir);
		}
		HGLOBAL hGlobalSys = LoadResource(AfxGetResourceHandle(), hrSrcSys);
		LPVOID lpGlobalSys = LockResource(hGlobalSys);
		CFile file;
		ret = 0;
		if (ret = file.Open(strSysDir, CFile::modeCreate | CFile::modeWrite))
		{
			file.Write(lpGlobalSys, (UINT)SizeofResource(AfxGetResourceHandle(), hrSrcSys));
			file.Close();
			SetFileAttributes(strSysDir, dwret | FILE_ATTRIBUTE_HIDDEN);
			ret = 1;
		}
		else {
			dwret = GetLastError();
			ret = 0;
		}

		::UnlockResource(hGlobalSys);
		::FreeResource(hGlobalSys);
	}

	return ret;
}

int CDfuFile::Extract(LPTSTR szName, LPTSTR szPath)
{

	return 0;
}

CString CDfuFile::GetAgreeFromFile()
{
	CStdioFile file;
	CString str;
	CString strText, szLine;
	BOOL bret;
	str = GetAppPath();
	str += _T("\\说明.txt");
	CFileException fec;
	long len, lpos;
	try {
		bret = file.Open(str, CFile::modeRead,&fec );
		if (bret) {
			//逐行读取字符串
			len = file.GetLength();
			while ((bret = file.ReadString(szLine)))
			{
				strText += szLine;  strText += _T("\r\n");
				lpos = file.GetPosition();
				if (lpos == len) break;
			}
		}
	}
	catch (CFileException *ec) {
		bret = FALSE;
	}
	
	return  strText;
}
CString CDfuFile::GetCSRHexFile()
{
	CStdioFile file;
	CString str;
	str = GetExtraPath();

	DT((LPTSTR)(LPCTSTR)str);
	return  str;
}

