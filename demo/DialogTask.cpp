// DialogTask.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogTask.h"
#include "afxdialogex.h"
#include "DofusSQLite.h"

namespace aui {
	// CDialogTask 对话框

	IMPLEMENT_DYNAMIC(CDialogTask, CDialogEx)

		CDialogTask::CDialogTask(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_TASK, pParent)
	{

	}

	CDialogTask::~CDialogTask()
	{
	}

	void CDialogTask::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_LIST1, m_listCtrl);
		DDX_Control(pDX, IDC_LIST3, m_listCtrlTsk);
		DDX_Control(pDX, IDC_LIST_TSK, m_listTask);
	}
	int CDialogTask::initListCtrl(void)
	{
		m_listCtrl.SetExtendedStyle(m_listCtrl.GetExtendedStyle()
			| LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);	//设置扩展风格

		m_listCtrl.ModifyStyle(0, LVS_SINGLESEL);
		//设置列表头
		m_listCtrl.InsertColumn(0, _T("x,y,sub"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(1, _T("mapid"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(2, _T("L2"), LVCFMT_CENTER);

		//设置各列的宽度
		CRect rect;
		m_listCtrl.GetClientRect(&rect);					//获取客户区宽度
		int nWidth = rect.Width();
		m_listCtrl.SetColumnWidth(0, 50);	//测试条件1 
		m_listCtrl.SetColumnWidth(1, 86);	//测试条件1 
		m_listCtrl.SetColumnWidth(2, 56);	//测试条件2 

		m_listCtrlTsk.SetExtendedStyle(m_listCtrlTsk.GetExtendedStyle()
			| LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);	//设置扩展风格

		m_listCtrlTsk.ModifyStyle(0, LVS_SINGLESEL);
		//设置列表头
		m_listCtrlTsk.InsertColumn(0, _T("x,y,sub"), LVCFMT_CENTER);
		m_listCtrlTsk.InsertColumn(1, _T("mapid"), LVCFMT_CENTER);
		m_listCtrlTsk.InsertColumn(2, _T("L2"), LVCFMT_CENTER);

		//设置各列的宽度

		m_listCtrlTsk.GetClientRect(&rect);					//获取客户区宽度
		nWidth = rect.Width();
		m_listCtrlTsk.SetColumnWidth(0, 50);	//测试条件1 
		m_listCtrlTsk.SetColumnWidth(1, 86);	//测试条件1 
		m_listCtrlTsk.SetColumnWidth(2, 56);	//测试条件2 
		return 0;
	}
	int CDialogTask::updateTskListCtrl(void) {
	
		return 0;
	}
	int CDialogTask::updateSQListCtrl(void)
	{
		CDofusSQLite *dofusDB;
		vecSQL vec;
		dofusDB = CDofusSQLite::instance();
		vec = dofusDB->getAllFrame();

		mapSQL mp;
		CString s1, s2, s3;
		int cnt;
		for (vecSQLIterator it = vec.begin(); it != vec.end(); ++it) {
			mp = *it;
			if (mp.size()>3) {
				s1.Format(_T("%d,%d,%d"), mp[_T("x")].var.ival
					, mp[_T("y")].var.ival
					, mp[_T("sub")].var.ival);
				cnt = m_listCtrl.InsertItem(0, s1);
				if (mp[_T("mid")].var.sVal) {
					m_listCtrl.SetItemText(cnt, 1, mp[_T("mid")].var.sVal);
					free(mp[_T("mid")].var.sVal);
				}
				s1.Format(_T("%.2f"), mp[_T("light2")].var.fval);
				m_listCtrl.SetItemText(cnt, 2, s1);
				m_listCtrl.SetItemData(cnt, (DWORD)mp[_T("fsid")].var.ival);
			}
		}
		return 0;
	}

	BEGIN_MESSAGE_MAP(CDialogTask, CDialogEx)
		ON_WM_SIZE()
		ON_BN_CLICKED(IDC_BTN_TASK, &CDialogTask::OnBnClickedBtnTask)
	END_MESSAGE_MAP()

	void CDialogTask::loadTaskFromFile(LPCTSTR file) {
		
	}
	// CDialogTask 消息处理程序
}

BOOL aui::CDialogTask::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	initListCtrl();
	// TODO:  在此添加额外的初始化
	updateSQListCtrl();
	updateTskListCtrl();

	CString str, s;
	TCHAR buf[MAX_PATH] = { 0 };
	::GetPrivateProfileString("Settings",
		"task",
		"",
		buf,
		MAX_PATH, ".\\Settings.ini");
	str = buf;
	SetDlgItemText(IDC_STA_FILE, buf);
	if(str.GetLength()>0)
		loadTaskFromFile(buf);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
void aui::CDialogTask::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	CRect r, rc, rd, r1;
	GetClientRect(rc);

	if ( m_listCtrl.GetSafeHwnd()) {
		m_listCtrl.MoveWindow(rc.left, rc.top + 30, rc.Width()/2-40, rc.Height() -30);
	}
	if (m_listCtrlTsk.GetSafeHwnd()) {
		m_listCtrlTsk.MoveWindow(rc.left+ rc.Width() / 2-30, rc.top + 30, rc.Width()/2-80, rc.Height()  - 30);
	}
	if (m_listTask.GetSafeHwnd()) {
		m_listTask.MoveWindow(rc.left + rc.Width()  - 100, rc.top + 30, 96, rc.Height() - 30);
	}
	// TODO: 在此处添加消息处理程序代码
}
void aui::CDialogTask::OnBnClickedBtnTask()
{
	TCHAR szFilters[] = _T("TxT Files (*.txt)|*.dat|All Files (*.*)|*.*||");
	CFileDialog dlg(TRUE, _T("All Files(*.*)"), _T("*.*"), OFN_OVERWRITEPROMPT, szFilters);
	if (dlg.DoModal()==IDOK)
	{
		m_listTask.ResetContent();
		
	}
}
