#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CDialogTask 对话框
namespace aui {
	class CDialogTask : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogTask)

	public:
		CDialogTask(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogTask();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_TASK };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		virtual BOOL OnInitDialog();
		CListCtrl m_listCtrl;
		CListCtrl m_listCtrlTsk;
		int initListCtrl(void);
		int updateSQListCtrl(void);
		int updateTskListCtrl(void);
		afx_msg void OnSize(UINT nType, int cx, int cy);
		afx_msg void OnBnClickedBtnTask();
		//从文件中加载任务
		void loadTaskFromFile(LPCTSTR file);
		CListBox m_listTask;
	};

}