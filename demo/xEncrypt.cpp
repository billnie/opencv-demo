#include "stdafx.h"
#include "xEncrypt.h"

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wincrypt.h>

#define BLOCK_SIZE            1000
#define BUFFER_SIZE           1008

// 加密文件
BOOL xEncrypt(LPCTSTR lpszSource, LPCTSTR lpszDestination, LPCTSTR lpszPassword)
{
	FILE *hSrcFile = NULL, *hDestFile = NULL;

	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HCRYPTKEY hKey = 0, hXchgKey = 0;
	PBYTE pbBuffer = NULL, pbKeyBlob = NULL;
	BOOL bEOF = FALSE, bReturn = FALSE;
	DWORD dwCount = 0, dwKeyBlobLen = 0;

	if (_tfopen_s(&hSrcFile, lpszSource, TEXT("rb")) != 0)
	{
		goto exit;
	}

	if (_tfopen_s(&hDestFile, lpszDestination, TEXT("wb")) != 0)
	{
		goto exit;
	}

	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, 0))
	{
		DWORD ret = GetLastError();
		if (ret != NTE_BAD_KEYSET)
		{
			goto exit;
		}
		else
		{
			if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET))
			{
				goto exit;
			}
		}
	}

	if (lpszPassword == NULL)
	{
		if (!CryptGenKey(hProv, CALG_RC2, CRYPT_EXPORTABLE, &hKey))
		{
			goto exit;
		}

		if (!CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hXchgKey))
		{
			DWORD ret = GetLastError();
			if (ret != NTE_NO_KEY)
			{
				goto exit;
			}
			if (!CryptGenKey(hProv, AT_KEYEXCHANGE, NULL, &hXchgKey))
			{
				goto exit;
			}
		}

		if (!CryptExportKey(hKey, hXchgKey, SIMPLEBLOB, 0, NULL, &dwKeyBlobLen))
		{
			goto exit;
		}

		if ((pbKeyBlob = (PBYTE)malloc(dwKeyBlobLen)) == NULL)
		{
			goto exit;
		}

		if (!CryptExportKey(hKey, hXchgKey, SIMPLEBLOB, 0, pbKeyBlob, &dwKeyBlobLen))
		{
			goto exit;
		}

		fwrite(&dwKeyBlobLen, sizeof(DWORD), 1, hDestFile);

		if (ferror(hDestFile))
		{
			goto exit;
		}

		fwrite(pbKeyBlob, 1, dwKeyBlobLen, hDestFile);

		if (ferror(hDestFile))
		{
			goto exit;
		}
	}
	else
	{
		if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
		{
			goto exit;
		}

		if (!CryptHashData(hHash, (PBYTE)lpszPassword, _tcslen(lpszPassword), 0))
		{
			goto exit;
		}

		if (!CryptDeriveKey(hProv, CALG_RC2, hHash, 0, &hKey))
		{
			goto exit;
		}
	}

	if ((pbBuffer = (PBYTE)malloc(BUFFER_SIZE)) == NULL)
	{
		goto exit;
	}

	do
	{
		dwCount = fread(pbBuffer, 1, BLOCK_SIZE, hSrcFile);

		if (ferror(hSrcFile))
		{
			goto exit;
		}
		bEOF = feof(hSrcFile);

		if (!CryptEncrypt(hKey, 0, bEOF, 0, pbBuffer, &dwCount, BUFFER_SIZE))
		{
			goto exit;
		}

		fwrite(pbBuffer, 1, dwCount, hDestFile);
		if (ferror(hDestFile))
		{
			goto exit;
		}

	} while (!bEOF);

	bReturn = TRUE;

exit:
	if (hSrcFile) fclose(hSrcFile);
	if (hDestFile) fclose(hDestFile);
	if (pbKeyBlob) free(pbKeyBlob);
	if (pbBuffer) free(pbBuffer);
	if (hKey) CryptDestroyKey(hKey);
	if (hXchgKey) CryptDestroyKey(hXchgKey);
	if (hHash) CryptDestroyHash(hHash);
	if (hProv) CryptReleaseContext(hProv, 0);

	return bReturn;
}

// 解密文件
BOOL xDecrypt(LPCTSTR lpszSource, LPCTSTR lpszDestination, LPCTSTR lpszPassword)
{
	FILE *hSrcFile = NULL, *hDestFile = NULL;

	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HCRYPTKEY hKey = 0;

	PBYTE pbBuffer = NULL, pbKeyBlob = NULL;
	BOOL bEOF = 0, bReturn = FALSE;
	DWORD dwCount = 0, dwKeyBlobLen = 0;

	if (_tfopen_s(&hSrcFile, lpszSource, TEXT("rb")) != 0)
	{
		goto exit;
	}

	if (_tfopen_s(&hDestFile, lpszDestination, TEXT("wb")) != 0)
	{
		goto exit;
	}

	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, 0))
	{
		DWORD ret = GetLastError();
		if (ret != NTE_BAD_KEYSET)
		{
			goto exit;
		}
		else
		{
			if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET))
			{
				goto exit;
			}
		}
	}

	if (lpszPassword == NULL)
	{
		fread(&dwKeyBlobLen, sizeof(DWORD), 1, hSrcFile);
		if (ferror(hSrcFile) || feof(hSrcFile))
		{
			goto exit;
		}

		if ((pbKeyBlob = (PBYTE)malloc(dwKeyBlobLen)) == NULL)
		{
			goto exit;
		}

		fread(pbKeyBlob, 1, dwKeyBlobLen, hSrcFile);

		if (ferror(hSrcFile) || feof(hSrcFile))
		{
			goto exit;
		}

		if (!CryptImportKey(hProv, pbKeyBlob, dwKeyBlobLen, 0, 0, &hKey))
		{
			goto exit;
		}
	}
	else
	{
		if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
		{
			goto exit;
		}

		if (!CryptHashData(hHash, (PBYTE)lpszPassword, _tcslen(lpszPassword), 0))
		{
			goto exit;
		}

		if (!CryptDeriveKey(hProv, CALG_RC2, hHash, 0, &hKey))
		{
			goto exit;
		}
	}

	if ((pbBuffer = (PBYTE)malloc(BUFFER_SIZE)) == NULL)
	{
		goto exit;
	}

	do
	{
		dwCount = fread(pbBuffer, 1, BLOCK_SIZE, hSrcFile);

		if (ferror(hSrcFile))
		{
			goto exit;
		}

		bEOF = feof(hSrcFile);

		if (!CryptDecrypt(hKey, 0, bEOF, 0, pbBuffer, &dwCount))
		{
			goto exit;
		}

		fwrite(pbBuffer, 1, dwCount, hDestFile);
		if (ferror(hDestFile))
		{
			goto exit;
		}

	} while (!bEOF);

	bReturn = TRUE;
exit:
	if (hSrcFile) fclose(hSrcFile);
	if (hDestFile) fclose(hDestFile);
	if (pbKeyBlob) free(pbKeyBlob);
	if (pbBuffer) free(pbBuffer);
	if (hKey) CryptDestroyKey(hKey);
	if (hHash) CryptDestroyHash(hHash);
	if (hProv) CryptReleaseContext(hProv, 0);

	return bReturn;

}