#pragma once
#include "afxwin.h"

#include "afxcmn.h"

// CDialogEdit 对话框
namespace aui {
	class CDialogEdit : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogEdit)

	public:
		CDialogEdit(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogEdit();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_EDIT };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		afx_msg void OnBnClickedBtnCap();
		afx_msg void OnBnClickedBtnDofus();
		afx_msg void OnSize(UINT nType, int cx, int cy);

		afx_msg void OnBnClickedBtnCv();
		afx_msg void OnBnClickedBtnFight();
		afx_msg void OnBnClickedBtnClr();
		afx_msg void OnBnClickedBtnDofdir();
		CListBox m_listBox;
		afx_msg void OnBnClickedBtnCid();
		afx_msg void OnSelchangeListCell();

		int initHideButton();
		afx_msg void OnBnClickedBtnDwon2();
		afx_msg void OnBnClickedBtnRight2();
		afx_msg void OnBnClickedBtnUp2();
		afx_msg void OnBnClickedBtnLeft2();
		virtual BOOL OnInitDialog();
		CRichEditCtrl m_richEdit;
		int addrichLog(LPCTSTR title, LPCTSTR message);
		void OnBnClickedBtnClear();
		afx_msg void OnBnClickedBtnCheck();
		afx_msg void OnBnClickedBtnGpath();
		int InitTM();
	};
}