#include "StdAfx.h"
#include "DofusSQLite.h"

CDofusSQLite *CDofusSQLite::g_dofusSQL = NULL;

CDofusSQLite::CDofusSQLite(void)
{
	db= NULL;
	OpenDB();
}


CDofusSQLite::~CDofusSQLite(void)
{
	CloseDB();
}
bool CDofusSQLite::OpenDB()
{
 CloseDB();

 int rc = sqlite3_open(_T("dofus.db3"),&db);
 if(rc != SQLITE_OK)
 {
  return false;
 }

 //#if USE_CODE                                                           //这个是数据加密的，暂时没用
 // rc = sqlite3_key(m_hDB,"mcnair2011",10);
 // if(rc != SQLITE_OK)
 // {
 //  TRACE("ERROR:%s\n",sqlite3_errmsg(m_hDB));
 //  return false;
 // }
 //#endif

 return true;
}


void CDofusSQLite::CloseDB()
{
 if(db != NULL)
 {
  if(sqlite3_close(db) == SQLITE_OK)
  {
   db = NULL;
  }
 }
}

int CDofusSQLite::ExecuteSQL(LPCTSTR sql, sqlite3_stmt **stat)
{
	// WaitForSingleObject(m_hRead, INFINITE);
	 nResult = 0;
	 sqlTemp = sql;
	 ssTemp = stat;

	 //HANDLE handle;
	 //handle = (HANDLE)_beginthread(ThreadFunc, 0, this);
	 //WaitForSingleObject(handle,INFINITE);
	 return nResult;
}

vecSQL CDofusSQLite::getAllFrame(void)
{
	int rc;
	int numCols;
	vecSQL vec;
    sqlite3_stmt *stmt;
    if(db==NULL);
    else{
        char sql[255] = {0};
        sprintf(sql,"select * from dofus");
        sqlite3_prepare(db,sql,-1,&stmt,0);           
        while(sqlite3_step(stmt)==SQLITE_ROW)
        {
			vec.push_back(commrowmap( stmt));
        }
        sqlite3_finalize(stmt);
	}
	return vec;
}


int CDofusSQLite::addFrame(mapSQL mp, int *fsid)
{
	int x, y, sub;
	int ret = -1;
	if(fsid) *fsid =0;
	if(mp.size()>=3){
		kiVal kv;
		string s;
		x = 9999;
		y = 9999;
		sub = 9999;
		mapSQLIterator it;
		for(it=mp.begin();it!=mp.end();){
			s = it->first;	kv = it->second;
			if(s.compare("x")==0){
				x = kv.var.ival;
				mp.erase(it++);
			}else if(s.compare("y")==0){
				y = kv.var.ival;
				mp.erase(it++);
			}else if(s.compare("sub")==0){
				sub = kv.var.ival;
				mp.erase(it++);
			}else{
				it++;
			}
		}
		if(x!=9999 && y !=9999 && sub!=9999){
			int cx;
			//关键字段存在
			//判断是否存在此记录，不存在的话，创建
            char *errmsg=0;
            int rc,i;
            
            char sql[256];
			//判断当前画面是否存在
			sprintf(sql, "select count (*) from dofus where x = %d and y=%d and sub = %d;", x,y ,sub);
            if (commcountsql(sql, db)==0) {
				//不存在，插入画面信息
				ret = 1;
                sprintf(sql, "insert into dofus (x, y, sub) values(%d, %d, %d); ",x,y,sub);
                rc=commupdatesql(sql, db);
            }else{	//更新数据
				ret = 2;
			}
			{
				//找到fsid
                sprintf(sql, "select fsid from dofus where x = %d and y=%d and sub = %d;", x,y ,sub);
				cx = commcountsql(sql,db);
                if (cx<1) {
                   //插入数据失败了
                }else{
					if(fsid) *fsid = cx;
					//插入数据
					for(it=mp.begin();it!=mp.end();it++){
						s = it->first;	kv = it->second;
						switch(kv.iT){
						case kiVarTypeInt:
							sprintf(sql,"update  dofus set %s = %d where fsid = %d;",s.c_str(), kv.var.ival ,cx);
							break;
						case kiVarTypeFloat:
							sprintf(sql,"update  dofus set %s = %.2f where fsid = %d;",s.c_str(), kv.var.fval ,cx);
							break;
						case kiVarTypeInt64:
							sprintf(sql, "update  dofus set %s = %I64d where fsid = %d;", s.c_str(), kv.var.hVal, cx);
							break;
						case kiVarTypeString:
							sprintf(sql, "update  dofus set %s = %s where fsid = %d;", s.c_str(), kv.var.sVal, cx);
							break;
						}
						rc=commupdatesql(sql, db);
					}
				   
                }
            }
		}
	}
	return ret;
}


mapSQL CDofusSQLite::getFsidFrame(int fsid)
{
	int rc;
	int numCols;
	mapSQL mp;
    sqlite3_stmt *stmt;
    if(db==NULL);
    else{
        char sql[255] = {0};
        sprintf(sql,"select * from dofus where fsid = %d", fsid);
        sqlite3_prepare(db,sql,-1,&stmt,0);           
        while(sqlite3_step(stmt)==SQLITE_ROW)
        {
			mp = commrowmap( stmt);
			break;
        }
        sqlite3_finalize(stmt);
	}
	return mp;
}
mapSQL CDofusSQLite::getmidFrame(string mid) {
	int rc;
	int numCols;
	mapSQL mp;
	sqlite3_stmt *stmt;
	if (db == NULL);
	else {
		char sql[255] = { 0 };
		sprintf(sql, "select * from dofus where mid=%s; ", mid.c_str());
		sqlite3_prepare(db, sql, -1, &stmt, 0);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			mp = commrowmap(stmt);
			break;
		}
		sqlite3_finalize(stmt);
	}
	return mp;
}
mapSQL CDofusSQLite::getxyzFrame(int x, int y, int sub) {
	int rc;
	int numCols;
	mapSQL mp;
	sqlite3_stmt *stmt;
	if (db == NULL);
	else {
		char sql[255] = { 0 };
		sprintf(sql, "select * from dofus where x = %d and y = %d and sub = %d; ", x, y, sub);
		sqlite3_prepare(db, sql, -1, &stmt, 0);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			mp = commrowmap(stmt);
			break;
		}
		sqlite3_finalize(stmt);
	}
	return mp;
}
int CDofusSQLite::getdisalbeFrame(int x, int y, int sub, unsigned char** buf)
{
	int ret = 0;
	int rc;
	int numCols;
	mapSQL mp;
	sqlite3_stmt *stmt;
	if (db == NULL);
	else {
		char sql[255] = { 0 };
		sprintf(sql, "select bmp from dofus where x = %d and y=%d and sub = %d;", x, y, sub);
		sqlite3_prepare(db, sql, -1, &stmt, 0);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			const void * test = sqlite3_column_blob(stmt, 0);
			ret = sqlite3_column_bytes(stmt, 0);
			if (ret >0 ) {
				if (buf) {
					*buf = (unsigned char*)malloc(ret);
					memcpy(*buf, test, ret);
				}
			}
			break;
		}
		sqlite3_finalize(stmt);
	}
	return ret;
}
int CDofusSQLite::adddisalbeFrame(int x, int y, int sub, unsigned char* buf,int len)
{
	int ret = 0;
	int rc;
	int numCols;
	mapSQL mp;
	sqlite3_stmt *stmt;
	if (db == NULL);
	else {
		char sql[255] = { 0 };
		sprintf(sql, "update  dofus set bmp = ? where x = %d and y=%d and sub = %d;", x, y, sub);
		sqlite3_prepare(db, sql, -1, &stmt, 0);
		sqlite3_bind_blob(stmt, 1, buf, len, NULL);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			ret = 1;
			break;
		}
		sqlite3_finalize(stmt);
	}
	return ret;
}
//判断当前图是否可以战斗
mapSQL  CDofusSQLite::getmapfight(int x, int y, int sub) {
	int rc;
	int numCols;
	mapSQL mp;
	sqlite3_stmt *stmt;
	if (db == NULL);
	else {
		char sql[255] = { 0 };
		sprintf(sql, "select * from fightmap where x = %d and y = %d and sub = %d; ", x, y, sub);
		sqlite3_prepare(db, sql, -1, &stmt, 0);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			mp = commrowmap(stmt);
			break;
		}
		sqlite3_finalize(stmt);
	}
	return mp;
}
//增加可战斗的图
int  CDofusSQLite::addmapfight(mapSQL mp) {
	int x, y, sub;
	int ret = -1;
	if (mp.size() >= 3) {
		kiVal kv;
		string s;
		x = 9999;
		y = 9999;
		sub = 9999;
		mapSQLIterator it;
		for (it = mp.begin(); it != mp.end();) {
			s = it->first;	kv = it->second;
			if (s.compare("x") == 0) {
				x = kv.var.ival;
				mp.erase(it++);
			}
			else if (s.compare("y") == 0) {
				y = kv.var.ival;
				mp.erase(it++);
			}
			else if (s.compare("sub") == 0) {
				sub = kv.var.ival;
				mp.erase(it++);
			}
			else {
				it++;
			}
		}
		if (x != 9999 && y != 9999 && sub != 9999) {
			int cx;
			//关键字段存在
			//判断是否存在此记录，不存在的话，创建
			char *errmsg = 0;
			int rc, i;

			char sql[256];
			//判断当前画面是否存在
			sprintf(sql, "select count (*) from fightmap where x = %d and y=%d and sub = %d;", x, y, sub);
			if (commcountsql(sql, db) == 0) {
				//不存在，插入画面信息
				ret = 1;
				sprintf(sql, "insert into fightmap (x, y, sub) values(%d, %d, %d); ", x, y, sub);
				rc = commupdatesql(sql, db);
			}
			else {	//更新数据
				ret = 2;
			}
			{
				//找到fsid
				sprintf(sql, "select fsid from fightmap where x = %d and y=%d and sub = %d;", x, y, sub);
				cx = commcountsql(sql, db);
				if (cx<1) {
					//插入数据失败了
				}
				else {
					//插入数据
					for (it = mp.begin(); it != mp.end(); it++) {
						s = it->first;	kv = it->second;
						switch (kv.iT) {
						case kiVarTypeInt:
							sprintf(sql, "update  fightmap set %s = %d where fsid = %d;", s.c_str(), kv.var.ival, cx);
							break;
						case kiVarTypeFloat:
							sprintf(sql, "update  fightmap set %s = %.2f where fsid = %d;", s.c_str(), kv.var.fval, cx);
							break;
						case kiVarTypeInt64:
							sprintf(sql, "update  fightmap set %s = %I64d where fsid = %d;", s.c_str(), kv.var.hVal, cx);
							break;
						case kiVarTypeString:
							sprintf(sql, "update  fightmap set %s = %s where fsid = %d;", s.c_str(), kv.var.sVal, cx);
							break;
						}
						rc = commupdatesql(sql, db);
					}
				}
			}
		}
	}
	return 1;
}
int CDofusSQLite::addchact(mapSQL mp) {
	int x, y, sub;
	int ret = -1;
	if (mp.size() >= 1) {
		kiVal kv;
		string s,name;
		x = 9999;
		y = 9999;
		sub = 9999;
		mapSQLIterator it;
		for (it = mp.begin(); it != mp.end();) {
			s = it->first;	kv = it->second;
			if (s.compare("uname") == 0) {
				name = kv.var.sVal;
				mp.erase(it++); break;
			}
			else {
				it++;
			}
		}
		if (name.length()>0) {
			int cx;
			//关键字段存在
			//判断是否存在此记录，不存在的话，创建
			char *errmsg = 0;
			int rc, i;

			char sql[256];
			//判断当前画面是否存在
			sprintf(sql, "select count (*) from chact where uname = \'%s\'", name.c_str());
			if (commcountsql(sql, db) <1) {
				//不存在，插入画面信息
				ret = 1;
				sprintf(sql, "insert into chact (uname) values(\'%s\'); ", name.c_str());
				rc = commupdatesql(sql, db);
			}
			else {	//更新数据
				ret = 2;
			}
			{
				//找到fsid
				sprintf(sql, "select csid from chact where uname = \'%s\' ;", name.c_str());
				cx = commcountsql(sql, db);
				if (cx<1) {
					//插入数据失败了
				}
				else {
					//插入数据
					sprintf(sql, "update  chact set tm = datetime(\'now\', \'localtime\') where csid = %d;", cx);
					rc = commupdatesql(sql, db);
					for (it = mp.begin(); it != mp.end(); it++) {
						s = it->first;	kv = it->second;
						switch (kv.iT) {
						case kiVarTypeInt:
							sprintf(sql, "update  chact set %s = %d where csid = %d;", s.c_str(), kv.var.ival, cx);
							break;
						case kiVarTypeFloat:
							sprintf(sql, "update  chact set %s = %.2f where csid = %d;", s.c_str(), kv.var.fval, cx);
							break;
						case kiVarTypeInt64:
							sprintf(sql, "update  chact set %s = %I64d where csid = %d;", s.c_str(), kv.var.hVal, cx);
							break;
						case kiVarTypeString: 
						
							sprintf(sql, "update  chact set %s = \'%s\' where csid = %d;", s.c_str(), kv.var.sVal, cx);
							break;
						}
						rc = commupdatesql(sql, db);
					}
				}
			}
		}
	}
	return 1;
}
int CDofusSQLite::addfight(mapSQL mp) {
	int x, y, sub;
	int ret = -1;
	if (mp.size() >= 3) {
		kiVal kv;
		string s, name;
		x = 9999;
		y = 9999;
		sub = 9999;
		mapSQLIterator it;
		for (it = mp.begin(); it != mp.end();) {
			s = it->first;	kv = it->second;
			if (s.compare("uname") == 0) {
				name = kv.var.sVal;
				mp.erase(it++); break;
			}else {
				it++;
			}
		}
		if (name.length()>0) {
			int cx;
			//关键字段存在
			//判断是否存在此记录，不存在的话，创建
			char *errmsg = 0;
			int rc, i;

			char sql[1024];
			{
				//不存在，插入画面信息
				ret = 1;
				sprintf(sql, "insert into fight (uname,tm) values(\'%s\',datetime(\'now\', \'localtime\'));", name.c_str());
				rc = commupdatesql(sql, db);
				sprintf(sql, "select last_insert_rowid();");
				rc = commcountsql(sql, db);
				cx = rc;
			}
			{
				if (cx<1) {
					//插入数据失败了
				}
				else {
					//插入数据
					for (it = mp.begin(); it != mp.end(); it++) {
						s = it->first;	kv = it->second;
						switch (kv.iT) {
						case kiVarTypeInt:
							sprintf(sql, "update  fight set %s = %d where fid = %d;", s.c_str(), kv.var.ival, cx);
							break;
						case kiVarTypeFloat:
							sprintf(sql, "update  fight set %s = %.2f where fid = %d;", s.c_str(), kv.var.fval, cx);
							break;
						case kiVarTypeInt64:
							sprintf(sql, "update  fight set %s = %I64d where fid = %d;", s.c_str(), kv.var.hVal, cx);
							break;
						case kiVarTypeString:
							if (kv.var.sVal && strlen(kv.var.sVal)>10) {
								sqlite3_stmt *stmt;
								sprintf(sql, "update  chact set set %s = ? where csid = %d;", s.c_str(), cx);
								sqlite3_prepare(db, sql, -1, &stmt, 0);
								sqlite3_bind_blob(stmt, 1, kv.var.sVal, strlen(kv.var.sVal) + 1, NULL);
								while (sqlite3_step(stmt) == SQLITE_ROW)
								{
									sqlite3_finalize(stmt);
									continue;
								}
								sqlite3_finalize(stmt);
							}
							sprintf(sql, "update  fight set %s = \'%s\' where fid = %d;", s.c_str(), kv.var.sVal, cx);
							break;
						}
						rc = commupdatesql(sql, db);
					}
				}
			}
		}
	}
	return 1;
}