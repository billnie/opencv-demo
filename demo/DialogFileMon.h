#pragma once
#include "afxcmn.h"


// CDialogFileMon 对话框
namespace aui {
	class CDialogFileMon : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogFileMon)

	public:
		CDialogFileMon(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogFileMon();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_MONFILE };
#endif

	protected:
		CString m_strWatchedDir;
		HANDLE m_hThread;
		HANDLE hDir;
		int m_i;
		BOOL	m_bAddNew0;
		BOOL	m_bDel0;
		BOOL	m_bRename0;
		BOOL	m_bModify0;
		BOOL	m_bOther0;

		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		virtual BOOL OnInitDialog();
		afx_msg void OnDestroy();
		afx_msg void OnBnClickedBtnStart();
		afx_msg void OnBnClickedBtnClear();
		CRichEditCtrl m_richEdit;tm
		afx_msg void OnSize(UINT nType, int cx, int cy);

		int addrichLog(LPTSTR title, LPTSTR message);

		afx_msg void OnStart();

		afx_msg void OnSelectDir();

		afx_msg void OnStop();
		BOOL StartWatch(CString path);
		static DWORD WINAPI ThreadProc(LPVOID lParam); //线程函数，用来监视

	};
}