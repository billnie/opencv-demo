#include	"stdafx.h"
#include "tmaccount.h"
#include <iostream>
#include <istream>
#include <ostream>
#include < boost/assign.hpp >
#include < boost/typeof/typeof.hpp >
#include <boost/property_tree/ptree.hpp>    
#include <boost/property_tree/ini_parser.hpp>  
#include <boost/property_tree/json_parser.hpp>  

tmaccount::tmaccount()
{
}
int tmaccount::getallacount(string path, vector<accountitem>&acs) {
	int ret = 0;
	//读取参数
	string sip;
	boost::property_tree::ptree pt;
	try
	{
		boost::property_tree::ini_parser::read_ini(path, pt);  // 打开读文件  
		string section, s1, s2, s3;
		BOOST_AUTO(path, pt.get_child(""));    //""表示所有  
		for (BOOST_AUTO(pos_paths, path.begin()); pos_paths != path.end(); ++pos_paths)
		{
			accountitem itm;
			boost::property_tree::ptree ptx;
			ptx = pos_paths->second;
			section = pos_paths->first;
			if (section.length() > 4) {
				s3 = section.substr(4);
				s1 = "账号"; s1 += s3;
				itm.name = ptx.get<std::string>(s1);
				s2 = "密码"; s2 += s3;
				itm.password = ptx.get<std::string>(s2);
			}
			acs.push_back(itm);
		}
	}
	catch (std::exception e)
	{
		cout << e.what();
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	return ret;
}
int tmaccount::saveallacount(string path, vector<accountitem>&acs) {
	int ret = 0,i;
	boost::property_tree::ptree pt;
	try {
//		boost::property_tree::ini_parser::read_ini(path, pt);  // 打开读文件  
		accountitem itm;
		i = 1;
		for_each(acs.begin(), acs.end(), [&pt,&i,&itm](accountitem x) {
			string s1, s2;
			s1 = str::format("帐号%d.账号%d", i, i);
			pt.put<std::string>(s1, x.name);
			s1 = str::format("帐号%d.密码%d", i, i);
			pt.put<std::string>(s1, x.password);
			i++;
		});
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (std::exception e) {
		cout << e.what();
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	return ret;
}
tmaccount::~tmaccount()
{
}
