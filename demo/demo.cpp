
// demo.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "demo.h"

#include "DialogLogin.h"
#include "FormControl.h"
#include "DialogForm.h"

#include	"dfufile.h"
#include	 "stlstr.h"
#include "lua-hlp.h"
#include <lua.hpp>
#include "Utlis.hpp"
#include "LuaAimal.hpp"
#include "Student.hpp"
#include	"lfs.h"
#include "log_config.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace aui;

// CdemoApp

BEGIN_MESSAGE_MAP(CdemoApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CdemoApp 构造

CdemoApp::CdemoApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CdemoApp 对象

CdemoApp theApp;
HANDLE g_handle = NULL;

BOOL CdemoApp::InitInstance()
{
//	HANDLE g_handle = NULL;
	SECURITY_ATTRIBUTES sa;
	SECURITY_DESCRIPTOR sd;
	str::initlog("log");
	InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
	SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);
	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = &sd;
	sa.bInheritHandle = TRUE;
	str::log(1, "开始启动应用");
//	alimain(1, NULL);
//	Qmain(1, NULL);

	g_handle = CreateMutex(&sa, true, "dofus-demo");
	if ((g_handle) && (GetLastError() == ERROR_ALREADY_EXISTS))
	{
		str::log(1, "应该已经打开了");
		CloseHandle(g_handle); g_handle = NULL;
		return FALSE;
	}
	str::log(1, "启动应用");
//TODO: call AfxInitRichEdit2() to initialize richedit2 library.
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	
	str::log(1, "启动应用1");
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CoInitialize(NULL);
	str::log(1, "启动应用2");

	AfxEnableControlContainer();
	AfxInitRichEdit(); 
	CWinApp::InitInstance();
	str::log(1, "启动应用3");
	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
	{

	}
	str::log(1, "即将启动登录窗口");
	CDialogLogin login;
	if (login.DoModal() == IDOK) {
		str::log(1, "登录成功，即将进入辅助主控");

		aui::CDialogForm fdlg;
		m_pMainWnd = &fdlg;
		fdlg.DoModal();
	
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}



int CdemoApp::ExitInstance()
{
	// TODO: 在此添加专用代码和/或调用基类
	if (g_handle != NULL) {
		CloseHandle(g_handle);
		g_handle = NULL;
	}
	return CWinApp::ExitInstance();
}
