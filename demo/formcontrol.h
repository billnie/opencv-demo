#pragma once

#include "afxcmn.h"

// CFormControl 窗体视图

class CFormControl : public CFormView
{
	DECLARE_DYNCREATE(CFormControl)

protected:
	CFormControl();           // 动态创建所使用的受保护的构造函数
	virtual ~CFormControl();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_FORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CTabCtrl m_tabCtrl;
	afx_msg void OnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};


