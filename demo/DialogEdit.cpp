// DialogEdit.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogEdit.h"
#include "afxdialogex.h"
#include	"DialogForm.h"

#include "DfuFile.h"
#include "tsplug.h"
#include	"dm.h"
#include "BrowseFolder.h"
#include	"dfufile.h"
#include "MouseKey.h"
#include "DMMouseKey.h"
#include "DlgDm.h"
#include	"FindProc.h"

using namespace akb;

// CDialogEdit 对话框


HWND GetSubWindowClass(HWND hWnd, LPCTSTR cls) {
	HWND h=NULL;
	if (::IsWindow(hWnd) && cls) {
		h = GetWindow(hWnd,GW_CHILD);
		TCHAR buf[255] = { 0 };
		DWORD dwret;
		while (h != NULL) {
			dwret = ::GetClassName(h, buf,255);
			if (_tcscmp(buf, cls) == 0)
				break;
		}
	}
	return h;
}

namespace aui {
	IMPLEMENT_DYNAMIC(CDialogEdit, CDialogEx)

		CDialogEdit::CDialogEdit(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_EDIT, pParent)
	{

	}

	CDialogEdit::~CDialogEdit()
	{
	}

	void CDialogEdit::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_LIST_CELL, m_listBox);
		DDX_Control(pDX, IDC_RICHEDIT22, m_richEdit);
	}


	BEGIN_MESSAGE_MAP(CDialogEdit, CDialogEx)
		ON_BN_CLICKED(IDC_BTN_CAP, &CDialogEdit::OnBnClickedBtnCap)
		ON_BN_CLICKED(IDC_BTN_DOFUS, &CDialogEdit::OnBnClickedBtnDofus)
		ON_BN_CLICKED(IDC_BTN_FIGHT, &CDialogEdit::OnBnClickedBtnFight)
		ON_BN_CLICKED(IDC_BTN_CV, &CDialogEdit::OnBnClickedBtnCv)
		ON_BN_CLICKED(IDC_BTN_CLR, &CDialogEdit::OnBnClickedBtnClr)
		ON_BN_CLICKED(IDC_BTN_DOFDIR, &CDialogEdit::OnBnClickedBtnDofdir)
		ON_BN_CLICKED(IDC_BTN_CID, &CDialogEdit::OnBnClickedBtnCid)
		ON_LBN_SELCHANGE(IDC_LIST_CELL, &CDialogEdit::OnSelchangeListCell)
		ON_BN_CLICKED(IDC_BTN_UP2, &CDialogEdit::OnBnClickedBtnUp2)
		ON_BN_CLICKED(IDC_BTN_DWON2, &CDialogEdit::OnBnClickedBtnDwon2)
		ON_BN_CLICKED(IDC_BTN_RIGHT2, &CDialogEdit::OnBnClickedBtnRight2)
		ON_BN_CLICKED(IDC_BTN_LEFT2, &CDialogEdit::OnBnClickedBtnLeft2)
		ON_BN_CLICKED(IDC_BTN_CHECK, &CDialogEdit::OnBnClickedBtnCheck)
		ON_BN_CLICKED(IDC_BTN_GPATH, &CDialogEdit::OnBnClickedBtnGpath)
		ON_WM_SIZE()
	END_MESSAGE_MAP()


	// CDialogEdit 消息处理程序


	void CDialogEdit::OnBnClickedBtnCap()
	{
		CButton *btn;
		UINT uState;
		btn = (CButton*)GetDlgItem(IDC_BTN_CAP);
		CDialogForm *form;
		form = (CDialogForm *)GetParent()->GetParent();
		if (btn) {
			CString str;
			uState = btn->GetState();
			btn->GetWindowText(str);
			if (str.CompareNoCase(_T("stop")) == 0)
				//	if(uState)
			{
				form->KillTimer(99);
				//			btn->SetState(0);
				btn->SetWindowText(_T("cap"));
				form->addrichLog(0, NULL, _T("结束分析!"));
			}
			else {
				form->addrichLog(0, NULL, _T("开始分析!"));
				form->SetTimer(99, 400, NULL);
				//			btn->SetState(1);
				btn->SetWindowText(_T("stop"));
			}
		}
	}
	void CDialogEdit::OnBnClickedBtnDofus()
	{
//		 InitTM();
		 return;
		static DWORD dw =0;
		HWND h=NULL;
		CRect r1;
		BOOL bret;
		CDfuFile file;
		bret = file.EnumProcessbyName(0, "DofusTouchNE", 1);

		if (bret == true) {
			CString str, s;
			TCHAR buf[MAX_PATH] = { 0 };
			::GetPrivateProfileString("Settings",
				"dofus",
				"",
				buf,
				MAX_PATH, ".\\Settings.ini");
			str = buf; str += "\\DofusTouchNE.exe";
			if (str.GetLength() > 0) {
				//上次已经点过了，就不让再点了
				if(GetTickCount()- dw>30000 || dw ==0)
					file.RunApp(str);
			}
			return;
		}
		if (h == NULL) {
			ITSPlugInterFace ts;
			ts.CreateDispatch("Ts.TsSoft");

			CString x, y;//字符串声明
			CString strTmp;//字符串声明
			char s[250];//字符串声明
			long xx = 0;
			long yy = 0;
			long ret;
		//	HWND hwnd;
			CString s1, s2;
			s1 = DOFUSCLASS;
			s2 = "";
			h = (HWND)ts.FindWindow(s1, s2);
		}
		//CMouseKey *kb = CMouseKey::KMInstance();
		//CExecKeyMouse *exe = CExecKeyMouse::ExeInstance();
		//if (kb->IsValiedHWnd()==0) {
		//	kb->BindWindow((long)h, "gdi2", "dx", "dx", 0);
		//}
		::GetWindowRect(h, &r1);
		SetTimer(2, 1200, NULL);
		CDialogForm *form;
		form = (CDialogForm *)GetParent()->GetParent();
		if (form) {
			CString str;
			BOOL bret;
			str.Format("dofus = %08x, (%d,%d,%d,%d)", h, r1.left, r1.top,r1.right, r1.bottom);
			form->addrichLog(NULL, str.LockBuffer());
		}
		if (r1.right - r1.left != 815 && h != NULL) {
			::SetWindowPos(h, NULL, 0, 0, 815, 718, SWP_NOZORDER);
			if (form) {
				CString str;
				str.Format("dofus = %08x, (%d,%d,%d,%d)", h,2, 0,815, 718);
				form->addrichLog(NULL, str.LockBuffer());
			}
		}
			
	}
	
	void CDialogEdit::OnSize(UINT nType, int cx, int cy)
	{
		CDialogEx::OnSize(nType, cx, cy);
		CRect r, rc, rd, r1;
		GetClientRect(r);
		CWnd *pwnd;

		pwnd = GetDlgItem(IDC_PIC_2);
		if (pwnd->GetSafeHwnd() != NULL) {
			HWND h=NULL;

			if (h) {
				int dwidth, dheight;
				float ratioDof;		//窗口比例，用来计算抓图坐标和实际游戏坐标比例

				::GetWindowRect(h, &r1);		//
				int hig;
				pwnd->GetWindowRect(rc);
				if (1) {
					dwidth = r1.right - r1.left;
					dheight = r1.bottom - r1.top;
					hig = rc.right - rc.left;
					ratioDof = hig *1.0 / dwidth;
					//ratioDof = 1.0 / ratioDof;
					hig = (dheight*hig / dwidth);
				}
			}
		}
	}
}

void aui::CDialogEdit::OnBnClickedBtnCv()
{

}



void aui::CDialogEdit::OnBnClickedBtnFight()
{

}

void aui::CDialogEdit::OnBnClickedBtnClr()
{
	char tempbuf[128];
	char strtm[32] = { 0 };

	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);

	timeinfo = localtime(&rawtime);

	strftime(tempbuf, 128, "d:\\testgame\\data_%Y%m%d_%H%M%S_1.bmp", timeinfo);
	HBITMAP hbmp;
	CMouseKey *kb = CMouseKey::Instance();
	kb->Capture(-1, 0, 0, 0, "1.bmp");
	kb->moveTo({ 200, 300 });
	kb->mouseClick();
	
}

void aui::CDialogEdit::OnBnClickedBtnDofdir()
{
	// TODO: 在此添加控件通知处理程序代码
	CDfuFile file;
	CBrowseFolder Dlg;
	int flag = 0;
	CString str, s;
	TCHAR buf[MAX_PATH] = { 0 };
	::GetPrivateProfileString("Settings",
		"dofus",
		"",
		buf,
		MAX_PATH, ".\\Settings.ini");
	str = buf;
	if (Dlg.DoModal(this, NULL) == IDOK)
	{
		str = Dlg.GetDirPath();
		::WritePrivateProfileString("Settings",
			_T("dofus"),
			str,
			".\\Settings.ini");

	}
	SetDlgItemText(IDC_STA_PATH2, str);
	if (str.GetLength() > 0 && flag ==1) {
		s = str; s += "\\resources\\app\\out\\browser\\app\\main\\";
		file.ExportHEXFile(2, s);
		s = str; s += "\\resources\\app\\out\\browser\\app\\main\\game\\";
		file.ExportHEXFile(3, s);
	}
	memset(buf, 0, MAX_PATH);
	::SHGetSpecialFolderPathA(NULL, buf, CSIDL_APPDATA, FALSE);
	s = buf; s += _T("\\DofusTouchNE\\game\\");
//	file.ExportHEXFile(1, "C:\\Users\\Administrator\\AppData\\Roaming\\DofusTouchNE\\game\\");
	file.ExportHEXFile(1, s);
}



void aui::CDialogEdit::OnBnClickedBtnCid()
{
	// TODO: 在此添加控件通知处理程序代码
	CString s;
	GetDlgItemText(IDC_EDIT_CID,s);
}

int aui::CDialogEdit::initHideButton()
{
#ifndef DEBUG
	GetDlgItem(IDC_CHECK_V1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_V2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_V3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_V4)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_CHECK_UP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_DOWN)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_LEFT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHECK_RIGHT)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_CHECK_L2)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_BTN_UP2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_DWON2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_LEFT2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_RIGHT2)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_BTN_DEL)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_BTN_CALC)->ShowWindow(SW_HIDE);
	////	GetDlgItem(IDC_PIC_2)->ShowWindow(SW_HIDE);

	////右边部分的当时为调试代码用的地方 
	GetDlgItem(IDC_BTN_STARTF0)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_STARTF1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_FIGHT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_CV)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_PIC_1)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_BTN_CLR)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_CAP)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_BTN_PIC_SEARCH)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_BTN_PIC_SEARCH2)->ShowWindow(SW_HIDE);

	//GetDlgItem(IDC_BUTTON2)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDC_BUTTON1)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
	//GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
#endif
	return 0;
	}
void aui::CDialogEdit::OnSelchangeListCell()
{
	// TODO: 在此添加控件通知处理程序代码
	int ret;
	ret = m_listBox.GetCurSel();
	if (ret >= 0) {
		CString s;
		m_listBox.GetText(ret,s);
		SetDlgItemText(IDC_EDIT_CID, s);
		OnBnClickedBtnCid();
	}
}


void aui::CDialogEdit::OnBnClickedBtnDwon2()
{
	CDialogForm *form;
	form = (CDialogForm *)GetParent()->GetParent();
	if (form)
		form->moveNearFrame(4);//8
}

void aui::CDialogEdit::OnBnClickedBtnRight2()
{
	CDialogForm *form;
	form = (CDialogForm *)GetParent()->GetParent();
	if (form)
		form->moveNearFrame(3);	//3
}
int aui::CDialogEdit::addrichLog(LPCTSTR title, LPCTSTR message)
{
	m_richEdit.ReplaceSel(_T("\r\n"));
	SYSTEMTIME sTime;
	GetLocalTime(&sTime);
	TCHAR chBuf[100];
	//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
	//	sTime.wMilliseconds,sTime.wDayOfWeek);
	wsprintf(chBuf, _T("%02u:%02u:%02u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
		sTime.wMilliseconds);
//	m_richEdit.ReplaceSel((LPCTSTR)chBuf);
	if (title && strlen(title) > 0)
	{
		m_richEdit.ReplaceSel((LPCTSTR)title);
		m_richEdit.ReplaceSel((LPCTSTR)_T(" :"));
	}
	if (message && strlen(message) > 0)
		m_richEdit.ReplaceSel((LPCTSTR)message);
	m_richEdit.PostMessage(WM_VSCROLL, SB_BOTTOM, 0);
	return 0;
}
void aui::CDialogEdit::OnBnClickedBtnUp2()
{
	CDialogForm *form;
	form = (CDialogForm *)GetParent()->GetParent();
	if (form)
		form->moveNearFrame(2);//1
}

void aui::CDialogEdit::OnBnClickedBtnLeft2()
{
	CDialogForm *form;
	form = (CDialogForm *)GetParent()->GetParent();
	if (form)
		form->moveNearFrame(1);//4
}


BOOL aui::CDialogEdit::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	akb::CMouseKey::KMInstance();

	CString str, s;
	TCHAR buf[MAX_PATH] = { 0 };
	::GetPrivateProfileString("Settings",
		"dofus",
		"",
		buf,
		MAX_PATH, ".\\Settings.ini");
	SetDlgItemText(IDC_STA_PATH2, buf);
	initHideButton();
	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
void aui::CDialogEdit::OnBnClickedBtnClear()
{
	m_richEdit.SetFocus();
	m_richEdit.SetSel(0, -1);
	m_richEdit.ReplaceSel("");
	m_richEdit.Clear();
}
void aui::CDialogEdit::OnBnClickedBtnCheck()
{
	CDfuFile dfu;
	CString str,s,ss;
	GetDlgItemText(IDC_STA_PATH2,str);
	ss = str;
	OnBnClickedBtnClear();
	if (str.GetLength() > 0) {
		if (dfu.IsFolderExist(str)) {
			str += _T("\\DofusTouchNE.exe");
			s += _T("模拟器目录存在，");
			if (dfu.IsFileExist(str))
			{
				s += _T("模拟器存在");
				addrichLog(NULL, s);
				addrichLog(NULL, _T("您的环境正常！"));
			}
		}
	}
	else {
		addrichLog(NULL, _T("请设置好游戏目录！"));
	}
endXX:
	return;
}
void aui::CDialogEdit::OnBnClickedBtnGpath()
{
	CString s;
	CDfuFile dfu;
	TCHAR buf[1024] = { 0 };
	CString str,  ss;
	GetDlgItemText(IDC_STA_PATH2, str);
	if (str.GetLength() > 8) {
		dfu.OpenPath(str);
	}
	else 
	{
		//CDlgDm dlg;
		//dlg.DoModal();
	}
}


int aui::CDialogEdit::InitTM()
{
	static DWORD dw = 0;
	HWND h = NULL;
	CRect r1;
	BOOL bret= FALSE;
	CDfuFile file;
	bret = file.EnumProcessbyName(0, "DofusTouchNE", 1);
	h = ::FindWindow(_T("LDPlayerMainFrame"), NULL);
	
	::GetWindowRect(h, &r1);
	SetTimer(2, 1200, NULL);
	CDialogForm *form;
	form = (CDialogForm *)GetParent()->GetParent();
	if (form) {
		CString str;
		BOOL bret;
		str.Format("dofus = %08x, (%d,%d,%d,%d)", h, r1.left, r1.top, r1.right, r1.bottom);
		form->addrichLog(NULL, str.LockBuffer());
	}
	 {
//		 ::MoveWindow(h, 0, 0, r1.Width(), r1.Height(), false);
//		::SetWindowPos(h, NULL, 0, 0, r1.Width(), r1.Height(), SWP_NOZORDER);
		if (form) {
			CString str;
			str.Format("dofus = %08x, (%d,%d,%d,%d)", h, 2, 0, 815, 718);
			form->addrichLog(NULL, str.LockBuffer());
		}
	}

	h = GetSubWindowClass(h, _T("RenderWindow"));

	CMouseKey *kb = CMouseKey::KMInstance();

	if (kb->IsValiedHWnd() == 0) {
		kb->BindWindow((long)h, "gdi2", "windows", "windows", 0);
	}
	return 0;
}
