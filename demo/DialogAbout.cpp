// DialogAbout.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogAbout.h"
#include "afxdialogex.h"
#include	"DfuFile.h"

#include <string>
#include <iostream>
#include <curl/curl.h>
#include "qiniu.h"
#include "boost/thread.hpp"
#include "iostream"
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <iostream>
//#include <sys/time.h>
//#include <unistd.h>

namespace aui {
	// CDialogAbout 对话框

	//这个用来保存对话框的窗口句柄，因为后面要向这个窗口发消息，必须知道其窗口句柄
	HWND	g_hDlgWnd = NULL;
	//提供给CURL下载进度回调的函数，用于保存下载的数据到文件
	static size_t	DownloadCallback(void* pBuffer, size_t nSize, size_t nMemByte, void* pParam);
	//提供给CURL下载进度回调的函数，用于计算下载进度通知界面
	static int ProgressCallback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);
	//这是对话框的消息循环，在控制台程序里面创建GUI，仅仅是为了更好地展现下载回调这个功能
	INT_PTR CALLBACK DialogProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	//这个是下载的线程函数，为了不把对话框主界面卡死，肯定是要自己开线程来下载的了
	DWORD WINAPI DownloadThread(LPVOID lpParam);

	int _atmain(int argc, _TCHAR* argv[],HWND hWnd)
	{
		//弹出对话框，知道对话框关闭才会执行退出
		DialogBox(NULL, MAKEINTRESOURCE(IDD_DLG_DOWN), hWnd, DialogProc);
		return 0;
	}

	static size_t DownloadCallback(void* pBuffer, size_t nSize, size_t nMemByte, void* pParam)
	{
		//把下载到的数据以追加的方式写入文件(一定要有a，否则前面写入的内容就会被覆盖了)
		FILE* fp = NULL;
		fopen_s(&fp, "c:\\test.zip", "ab+");
//		fopen_s(&fp, "c:\\test.zip", "wb+");
		size_t nWrite = fwrite(pBuffer, nSize, nMemByte, fp);
		fclose(fp);
		return nWrite;
	}

	static int ProgressCallback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
	{
		if (dltotal > -0.1 && dltotal < 0.1)
			return 0;
		int nPos = (int)((dlnow / dltotal) * 100);
		//通知进度条更新下载进度
		::PostMessage(g_hDlgWnd, WM_USER + 110, nPos, 0);
		//::Sleep(10);
		return 0;
	}

	INT_PTR CALLBACK DialogProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_INITDIALOG:
		{
			g_hDlgWnd = hWnd;
			HWND hProgress = GetDlgItem(hWnd, IDC_PROGRESS1);
			SendMessage(hProgress, PBM_SETRANGE32, (WPARAM)0, (LPARAM)100);
			//对话框初始化时创建下载线程
			HANDLE hThread = CreateThread(NULL, 0, DownloadThread, 0, 0, NULL);
			CloseHandle(hThread);
			::SetWindowText(hWnd, _T("文件下载"));
			return TRUE;
		}
		case WM_COMMAND:
		{
			WORD  msg = HIWORD(wParam);
			WORD  id = LOWORD(wParam);
			if (id == IDOK || id == IDCANCEL)
				EndDialog(hWnd, id);
			break;
		}
		//case WM_ERASEBKGND:
		//	return TRUE;
		//case WM_CTLCOLORSTATIC:
		//	return (INT_PTR)(HBRUSH)::GetStockObject(WHITE_BRUSH);
		case WM_USER + 110:
		{//接收到设置进度的消息
			HWND	hProgress = GetDlgItem(hWnd, IDC_PROGRESS1);
			HWND	hStatus = GetDlgItem(hWnd, IDC_STATUS);
			if (hProgress)
				SendMessage(hProgress, PBM_SETPOS, wParam, 0L);
			if (hStatus)
			{
				TCHAR szBuffer[100] = { 0 };
				if (wParam < 100)
					_stprintf(szBuffer, _T("正在下载文件，进度：%d%%"), wParam);
				else
					_stprintf(szBuffer, _T("文件下载完毕！"));
				::SetWindowText(hStatus, szBuffer);
			}
			return 0;
		}
		default:
			break;
		}
		return FALSE;
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	DWORD WINAPI DownloadThread(LPVOID lpParam)
	{
		
		//初始化curl，这个是必须的
		CURL* curl = curl_easy_init();
		curl_easy_setopt(curl, CURLOPT_URL, "http://api.no-emu.com/game.zip");
		//设置接收数据的回调
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, DownloadCallback);
		//curl_easy_setopt(curl, CURLOPT_INFILESIZE, lFileSize);
		//curl_easy_setopt(curl, CURLOPT_HEADER, 1);
		//curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
		//curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
		// 设置重定向的最大次数
		curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 5);
		// 设置301、302跳转跟随location
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
		//设置进度回调函数
		curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, ProgressCallback);
		//curl_easy_getinfo(curl,  CURLINFO_CONTENT_LENGTH_DOWNLOAD, &lFileSize);
		//curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, g_hDlgWnd);
		//开始执行请求
		CURLcode retcCode = curl_easy_perform(curl);
		//查看是否有出错信息
		const char* pError = curl_easy_strerror(retcCode);
		//清理curl，和前面的初始化匹配
		curl_easy_cleanup(curl);
		return 0;
	}
	IMPLEMENT_DYNAMIC(CDialogAbout, CDialogEx)

		CDialogAbout::CDialogAbout(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_ABOUT, pParent)
	{

	}

	CDialogAbout::~CDialogAbout()
	{
	}

	void CDialogAbout::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_RICHEDIT21, m_richEdit);
	}


	BEGIN_MESSAGE_MAP(CDialogAbout, CDialogEx)
		ON_WM_SIZE()
		ON_BN_CLICKED(IDC_BTN_HLP, &CDialogAbout::OnBnClickedBtnHlp)
		ON_BN_CLICKED(IDC_BTN_DOWN_VS, &CDialogAbout::OnBnClickedBtnDownVs)
		ON_BN_CLICKED(IDC_BTN_GPATH, &CDialogAbout::OnBnClickedBtnGpath)
	END_MESSAGE_MAP()


	// CDialogAbout 消息处理程序


	void CDialogAbout::OnSize(UINT nType, int cx, int cy)
	{
		CDialogEx::OnSize(nType, cx, cy);
		CRect r, rc, rd, r1;
		GetClientRect(rc);

		if (m_richEdit.GetSafeHwnd()) {
			m_richEdit.MoveWindow(rc.left, rc.top + 36, rc.Width(), rc.Height()-38);
		}
	}


	BOOL CDialogAbout::OnInitDialog()
	{
		CDialogEx::OnInitDialog();
		CString str;
		str = "build at ";
		str += __DATE__;
		str += _T("  by 706919534@qq.com");
		SetDlgItemText(IDC_STA_VER, str);
		qiniumain();
#ifdef _DEBUG
		GetDlgItem(IDC_BTN_DOWN_VS)->ShowWindow(SW_SHOW);
#endif
		// TODO:  在此添加额外的初始化
		OnBnClickedBtnHlp();
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}


	void CDialogAbout::OnBnClickedBtnHlp()
	{
		// TODO: 在此添加控件通知处理程序代码
		CString s;
		CDfuFile dfu;
		TCHAR buf[1024] = { 0 };
		s = dfu.GetAgreeFromFile();
		if (s.GetLength() > 0) {
			OnBnClickedBtnClear();
			addrichLog(NULL, s);
		}
	}
	void downthread()
	{
		//_atmain(1, NULL); return;
	}
	void CDialogAbout::OnBnClickedBtnDownVs()
	{
		//boost::function<void()> func(downthread);
		//boost::thread t(func);
		//t.timed_join(boost::posix_time::seconds(1));
		_atmain(1, NULL, GetSafeHwnd()); return;
		//CString s;
		//CDfuFile dfu;
		//TCHAR buf[1024] = { 0 };
		//s = dfu.GetAppPath();
		//s += _T("\\1.txt");
		//qiniu_uploadfile("aa.txt",s.GetBuffer() );
		//https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x86.exe
		// TODO: 在此添加控件通知处理程序代码
	}
	// CDialogLog 消息处理程序
	int CDialogAbout::addrichLog(LPCTSTR title, LPCTSTR message)
	{
		m_richEdit.ReplaceSel(_T("\r\n"));
		SYSTEMTIME sTime;
		GetLocalTime(&sTime);
		TCHAR chBuf[100];
		//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
		//	sTime.wMilliseconds,sTime.wDayOfWeek);
		wsprintf(chBuf, _T("%02u:%02u:%02u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
			sTime.wMilliseconds);
//		m_richEdit.ReplaceSel((LPCTSTR)chBuf);
		if (title && strlen(title) > 0)
		{
			m_richEdit.ReplaceSel((LPCTSTR)title);
			m_richEdit.ReplaceSel((LPCTSTR)_T(" :"));
		}
		if (message && strlen(message) > 0)
			m_richEdit.ReplaceSel((LPCTSTR)message);
		m_richEdit.PostMessage(WM_VSCROLL, SB_BOTTOM, 0);
		return 0;
	}
	void CDialogAbout::OnBnClickedBtnClear()
	{
		m_richEdit.SetFocus();
		m_richEdit.SetSel(0, -1);
		m_richEdit.ReplaceSel("");
		m_richEdit.Clear();
	}
	void CDialogAbout::OnBnClickedBtnGpath()
	{
		CString s;
		CDfuFile dfu;
		TCHAR buf[1024] = { 0 };
		::SHGetSpecialFolderPathA(NULL, buf, CSIDL_APPDATA, FALSE);
		s = buf; s += _T("\\DofusTouchNE\\");
		dfu.OpenPath(s);
	}

}
