#ifndef LOG_TEST_CONFIG_H
#define LOG_TEST_CONFIG_H

#include "log_define.h"

LOG_CPP_START
int initaliyunproducerlog(const char *fileName, const char *machine, const char *dfa);
int releasealiyunproducerlog();
int tmain(int argc, char *argv[]);
int qmain(int argc, char *arqgv[]);
int alimain(int argc, char *argv[]);
extern const char LOG_ENDPOINT[];
extern const char ACCESS_KEY_ID[];
extern const char ACCESS_KEY_SECRET[];
extern const char PROJECT_NAME[];
extern const char LOGSTORE_NAME[];

LOG_CPP_END

#endif
