
#ifndef LIBAOS_OSVER_H
#define LIBAOS_OSVER_H

#include "aos_define.h"
#include <iostream>   
#include <string>  

AOS_CPP_START

#pragma warning(disable: 4996) // avoid GetVersionEx to be warned  

std::string getOsInfo();
// init cpu in assembly language  
void initCpu(DWORD veax);

long getCpuFreq();
std::string getManufactureID();

std::string getCpuType();
void getCpuInfo();

// ---- get memory info ---- //  
void getMemoryInfo();

int xxxmain(int argc, char *argv[]);

AOS_CPP_END

#endif
