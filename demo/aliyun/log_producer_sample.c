

//
// Created by ZhangCheng on 24/11/2017.
//

#include "log_producer_config.h"
#include "log_producer_client.h"
#include "log_config.h"
log_producer * g_producer =NULL;
// 数据发送的回调函数
void on_log_send_done(const char * config_name, log_producer_result result, size_t log_bytes, size_t compressed_bytes, const char * req_id, const char * message)
{
    if (result == LOG_PRODUCER_OK)
    {
        printf("send done, config : %s, result : %s, log bytes : %d, compressed bytes : %d, request id : %s\n",
               config_name, get_log_producer_result_string(result),
               (int)log_bytes, (int)compressed_bytes, req_id);
        return;
    }
    printf("send error, config : %s, result : %s, log bytes : %d, compressed bytes : %d, request id : %s, error message : %s\n",
           config_name, get_log_producer_result_string(result),
           (int)log_bytes, (int)compressed_bytes, req_id, message);
    return;
}



void log_producer_post_logs(const char * fileName, int logs)
{
    // 初始化producer相关依赖环境
    if (log_producer_env_init() != LOG_PRODUCER_OK) {
        exit(1);
    }

    // 从指定配置文件创建producer实例，on_log_send_done 可以填NULL
    // 用户也可以手动创建config，使用create_log_producer接口创建producer
    log_producer * producer ;
//    producer = create_log_producer_by_config_file(fileName, on_log_send_done);
    log_producer_config *config = load_log_producer_config_file(fileName);
    producer = create_log_producer(config, on_log_send_done);
    if (producer == NULL)
    {
        printf("create log producer by config file fail \n");
        exit(1);
    }

    // 获取默认的client
    log_producer_client * client = get_log_producer_client(producer, NULL);
    if (client == NULL)
    {
        printf("get root client fail \n");
        exit(1);
    }

    int32_t i = 0;
    for (i = 0; i < logs; ++i)
    {
        LOG_ALIYUNFMT_DEBUG(client, "test","error_id=%d", i);
    }

    destroy_log_producer(producer);

    log_producer_env_destroy();
}
int releasealiyunproducerlog(){
    destroy_log_producer(g_producer);
    log_producer_env_destroy();
    g_producer =NULL;
    return 1;
}
int initaliyunproducerlog(const char *fileName,const char *machine, const char*dfa){
//    apr_initialize();
    if (log_producer_env_init() != LOG_PRODUCER_OK) {
        exit(1);
    }
    
    // 从指定配置文件创建producer实例，on_log_send_done 可以填NULL
    // 用户也可以手动创建config，使用create_log_producer接口创建producer
    //log_producer * producer ;
    //    producer = create_log_producer_by_config_file(fileName, on_log_send_done);
    log_producer_config *config = load_log_producer_config_file(fileName);
    if(config ){
        if( machine && strlen(machine))
           config->configName = strdup(machine);
       if( dfa && strlen(dfa))
           config->topic = strdup(dfa);
	   config->endpoint = strdup(LOG_ENDPOINT);
	   config->accessKeyId = strdup(ACCESS_KEY_ID);
	   config->accessKey = strdup(ACCESS_KEY_SECRET);
    }
    g_producer = create_log_producer(config, on_log_send_done);
    if (g_producer == NULL)
    {
        printf("create log producer by config file fail \n");
        return 0;
    }
    
    // 获取默认的client
    log_producer_client * client = get_log_producer_client(g_producer, NULL);
    if (client == NULL)
    {
        printf("get root client fail \n");
        return 0;
    }
    return 1;
}
int qmain(int argc, char *argv[])
{
    const char * filePath = "G:\\desi\\opencv-demo\\demo\\aliyun\\log_config.json";
    int logs = 3;
    if (argc == 3)
    {
        filePath = argv[1];
        logs = atoi(argv[2]);
    }
    log_producer_post_logs(filePath, logs);
    return 0;
}


