#pragma once

#include	"dm.h"
#include "MouseKey.h"
#include	<string>

/*
大漠按键消息处理
*/
#include <lua.hpp>
namespace akb {
	class CDMMouseKey :
		public CMouseKey
	{
	public:
		Idmsoft dm;
		CDMMouseKey();
		~CDMMouseKey();
		static CMouseKey * Instance() {
			if (g_kb == NULL) {
				g_kb = new CDMMouseKey();
			}
			return g_kb;
		}
		int isLoadDm();
		int asyn_execLua(LPCTSTR function, LPCTSTR file, LPTSTR result);
		int execLua(LPCTSTR function, LPCTSTR file, LPTSTR result);
		virtual long BindWindow(long hwnd, LPCTSTR display, LPCTSTR mouse, LPCTSTR keypad, long mode);
		virtual long UnBindWindow();
		long IsBind();
		virtual int moveTo(POINT pt);
		virtual int mouseClick();
		virtual int LeftDown();
		virtual int LeftUp();
		virtual int mousedoubleDown(POINT pt);
		virtual int dragmousemoveDown(POINT pt1, POINT pt2);
		virtual int keyDown(unsigned char key);
		virtual int Capture(int x, int y, int w, int h, LPCTSTR filename);

		static int s_execLua(LPCTSTR function, std::string file, LPTSTR result, CDMMouseKey*mk);
	};

}