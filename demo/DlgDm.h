#pragma once

#include	"dm.h"
// CDlgDm 对话框

class CDlgDm : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgDm)

public:
	Idmsoft dm;
	CDlgDm(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgDm();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLGDM };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnFont();
	afx_msg void OnBnClickedBtnOcr();
	virtual BOOL OnInitDialog();

	int execLua(LPCTSTR function, LPCTSTR file);
	afx_msg void OnBnClickedBtnLvl();
	afx_msg void OnBnClickedBtnCap();
	afx_msg void OnBnClickedBtnScrol();
	afx_msg void OnBnClickedBtnBmpdia();
	afx_msg void OnBnClickedBtnGame();
	afx_msg void OnBnClickedBtnAuto();
};
