// DialogLogin.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogLogin.h"
#include "afxdialogex.h"
#include "..\include\hdinfo.h"
#include "boost/thread.hpp"
#include "iostream"
using namespace std;

#pragma comment(lib, "..\\lib\\hdinfo.lib")
#pragma comment(lib,"Netapi32.lib")
// CDialogLogin 对话框
using namespace std;
int _xtmain(int argc, char* argv[]);
#include <iostream>

#include	"md5.h"
#include "lua-hlp.h"
#include	"posenc.h"
#include "qiniu.h"
#include <curl/curl.h>
#include <io.h>
#include <fcntl.h> 
extern int userlvl;
static const WORD MAX_CONSOLE_LINES = 500;
extern int getxxtm();

//
time_t convert(int year, int month, int day)
{
	tm info = { 0 };
	info.tm_year = year - 1900;
	info.tm_mon = month - 1;
	info.tm_mday = day;
	return mktime(&info);
}
int  get_days(const char* from, const char* to)
{
	int year, month, day;
	sscanf(from, "%d-%d-%d", &year, &month, &day);
	int fromSecond = (int)convert(year, month, day);
	sscanf(to, "%d-%d-%d", &year, &month, &day);
	int toSecond = (int)convert(year, month, day);

	return (toSecond - fromSecond) / 24 / 3600;
}
int xtdmain()
{
	const char* from = "2017-8-15";
	const char* to = "2017-8-14";
	int days = get_days(from, to);
	printf("From:%s\nTo:%s\n", from, to);
	printf("%d\n", days);
//	system("pause");
	return 0;
}
//时间和功能是明文，
int gensn(LPCTSTR sn, int mode, LPCTSTR tm, LPTSTR buf) {
	int ret = -1;
	if (sn && tm && mode > 0 && mode < 12) {
		TCHAR xx[256] = { 0 };
		char *sa,*sb;
		sprintf(xx, "%s#%s#%d", sn, tm, mode);
		sa = base64_encode(xx, strlen(xx));
		sb = md5Str(sa);
		if (sb) {
			strcpy(buf, sb); ret = 1; free(sb);
		}
		free(sa);
	}
	return ret;
}

int gensn2(LPCTSTR sn, int mode,  LPTSTR buf) {
	int ret = -1;
	CString str;
	//获取系统时间
	CTime tm;
	tm = CTime::GetCurrentTime();//获取系统日期
	str = tm.Format("%Y-%m-%d");
	if (sn  && mode > 0 && mode < 12) {
		TCHAR xx[256] = { 0 };
		char *sa, *sb;
		sprintf(xx, "%s#%s#%d", sn, str, mode);
		sa = base64_encode(xx, strlen(xx));
		sb = md5Str(sa);
		if (sb) {
			strcpy(buf, sb); ret = 1; free(sb);
		}
		free(sa);
	}
	return ret;
}
/*
sn硬件序列号， md返回功能信息,返回为1表法成功了，否则退出程序
*/
int checkvaliduser(LPCTSTR sn, int *md) {
	int ret = -1;
	//通过lua读取信息,base64及md5后的信息, 日期信息，功能信息
	//根据此生成一个加密信息
	//比较读取的加密信息和生成的加密信息，如果两者相等，再进行一步的往下
	if (sn && md) {
		int mode; long lret;
		char buf[128] = { 0 };	//密钥
		char xx[1024] = { 0 };
		char strtime[32] = { 0 };
		execlua4(22, 22, "xcode", "show.lua", buf);
		//序列号格式是(序列号-时间-功能),base64,md5一下
		execlua4(22, 22, "xtime", "show.lua", strtime);
		execlua2(22, 22, "xmode", "show.lua", &lret);
		mode = lret;
		gensn(sn, mode, strtime, xx);
		if (strcmp(xx, buf) == 0) {
			ret = 1; *md = mode;
		}
	}
	return ret;
}

void threadFunc()
{
	cout << "This is a thread function" << endl;
}
int xmain()
{
	boost::function<void()> func(threadFunc);
	boost::thread t(func);
	t.join();
//	bst::test();
	return 0;
}

namespace aui {
	IMPLEMENT_DYNAMIC(CDialogLogin, CDialogEx)

		CDialogLogin::CDialogLogin(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DIALOG, pParent)
	{

	}

	CDialogLogin::~CDialogLogin()
	{
	}

	void CDialogLogin::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
	}


	BEGIN_MESSAGE_MAP(CDialogLogin, CDialogEx)
		ON_BN_CLICKED(IDOK, &CDialogLogin::OnBnClickedOk)
	//	ON_BN_CLICKED(IDCANCEL, &CDialogLogin::OnBnClickedOk)
	END_MESSAGE_MAP()


	// CDialogLogin 消息处理程序

	void CDialogLogin::OnBnClickedOk()
	{
		// TODO: 在此添加控件通知处理程序代码
//		if (getxxtm() < 0|| lret <2) return;
		CString str, str1;
		char buf[1024] = { 0 };
		execluaocr("lotion", "ocr.lua", buf);
		char sn[256] = { 0 };
		GetDlgItemText(IDC_EDIT1, str);
		str1 = GetHardInfo();
		//if (str.Compare(str) != 0) {	//显示的信息必须和硬件的信息一致
		//	return;
		//}
		//用户没有注册
		char *ss,*sa;
		int ret,md;
		//execlua4(22, 22, "xcode", "show.lua", buf);
		////序列号格式是(序列号-时间-功能),base64,md5一下
		//ss = base64_encode(buf, strlen(buf));
		////sa = base64_decode(ss, strlen(ss), &ret);
		////free(sa);
		//execlua4(22, 22, "xmode", "show.lua", buf);
		//gensn2(str, 3, buf);
		//95b425238fddda45f544710a10deaf20 家里的台试机
//		gensn("95b425238fddda45f544710a10deaf20", 7, "2017-03-09", sn);
//		gensn("6VMVY5TJ", 7, "2017-03-09", sn);
//		gensn("6VMVY5TJ", 7, "2017-03-21", sn); //深圳网友
		gensn("S6712708", 7, "2017-03-09", sn); //国兴
		gensn("7679757b36087ec414641e23f9bbc22d", 7, "2017-03-09", sn);	//国兴
		DT(sn);
		//if (strcmp(buf, sn) == 0) {
		//	ret = 1;
		//}
		
		ret = checkvaliduser(str,&md);
		if (str.Compare("A74PZY8L-12") == 0) {//开发笔记本
			userlvl = 3;
			CDialogEx::OnOK();
		}
		else if (str.Compare("5VMVLS6Z") == 0) {
			CDialogEx::OnOK();
		}
		else if (str.Compare("9VM1WCA5") == 0) {
			CDialogEx::OnOK();
		}
		//else if (str.Compare("19801221") == 0) {
		//	CDialogEx::OnOK();
		//}
		else if (str.Compare("6QZ3RH1A") == 0) {//黑龙江
			CDialogEx::OnOK();
		}
		else if (str.Compare("09268B04") == 0) {//黄武权
			CDialogEx::OnOK();
		}//09268B04
		else if (str.Compare("90Derler-C WD5000") == 0) {//河北 ，只打npc
			userlvl = 3;
			CDialogEx::OnOK();
		}
		//else if (str.Compare("5VMK2VBQ") == 0) {//深圳文燕，只升级2017,2,28
		//	userlvl = 1;
		//	CDialogEx::OnOK();
		//}
		//else if (str.Compare("9F04597 -C1S67127-9F04597 -C1S67127") == 0) {//河北全功能2017,2,28
		//	userlvl = 3;
		//	CDialogEx::OnOK();	//已经转成了lua的方式来验证了，能多开
		//}//7679757b36087ec414641e23f9bbc22d
		//else if (str.Compare("7679757b36087ec414641e23f9bbc22d") == 0) {//河北2017,3,2
		//	userlvl = 1;			//已经转成了lua的方式来验证了，能多开
		//	CDialogEx::OnOK();
		//}//7679757b36087ec414641e23f9bbc22delse
		else if (str.Compare("C982KING") == 0) {//河北2017,3,2
			userlvl = 1;
			CDialogEx::OnOK();
		}
		else if (str.Compare("OSTAR S1-") == 0) {//河北2017,3,2
			userlvl = 1;
			CDialogEx::OnOK();
		}
		else if (str.Compare("6VM1EAAA") == 0) {//河北2017,3,2
			userlvl = 1;
			CDialogEx::OnOK();
		}
		else {
			if (ret == 1) {
				userlvl = md;
				CDialogEx::OnOK();
			}
			else {
				userlvl = md;
				CDialogEx::OnOK();
				str::log(1, "登录失败，未注册，请联系作者");
			}
		}
		//		if (userlvl < 1) return;		
		//90Derler-C WD5000
		//else if (str.GetLength() == 0) {
		//	CDialogEx::OnOK();
		//}
	}
	CString CDialogLogin::GetHardInfo() {
		CString str;
		BYTE byte[1024];
		char bufs[256] = { 0 };
		int nret;
		char buf[1024];
		memset(byte, 0, sizeof(BYTE) * 1024);
		nret = sizeof(BYTE);
		nret = GetDiskSerial(byte, 1024);
		str::log(1, "启动登录窗口初始化-sn, %d", nret);
		if (nret ==1) {
			CString s;
			int i;
			memset(buf, 0, sizeof(char) * 1024);
			str::log(1, "启动登录窗口初始化-解析hd");
			for (i = 0; i < nret; i++)
			{
				memcpy(buf + i * 9, &byte[i * 0x20], 8);
				buf[(i + 1) * 9 - 1] = '-';
			}
			buf[i * 9 - 1] = '\0';
		}
		else {
			str::log(1, "启动登录窗口初始化-解析另外的hd");
			nret = GetLogicDisk(bufs);
			char *ptr;
			ptr = strtok(bufs, "-");
			GetMacAddr(buf);
			memset(buf, 0, 1024);
			int i =0;
			while (ptr != NULL) {
				//i++;
				//ptr = strtok(NULL, "-");
				//if (i == 1) continue;
				GetXXXLogicDiskSerial(ptr, buf); 
				unsigned char decrypt[16];
				MD5_CTX md5;
				MD5Init(&md5);
				MD5Update(&md5,(unsigned char*) buf, strlen((char *)buf));
				MD5Final(&md5, decrypt);
				memset(bufs, 0, 200);
				char sz[8] = { 0 };
				for (i = 0; i<16; i++)
				{
					sprintf(sz, "%02x", decrypt[i]);
					strcat(bufs, sz);
				}
				strcpy(buf, bufs);
				//md5一下
				break;
			}
		}

#ifdef _DEBUG
		AllocConsole();
		freopen("CON", "r", stdin);
		freopen("CON", "w", stdout);
		freopen("CON", "w", stderr);
#endif
		//_asm int 3;
		//	xmain();
		//	_xtmain(0, 0);
		str::log(1, "启动登录窗口初始化-显示完成");
		str = buf;
		return str;
	}
	static size_t DownloadCallback2(void* pBuffer, size_t nSize, size_t nMemByte, void* pParam)
	{
		//把下载到的数据以追加的方式写入文件(一定要有a，否则前面写入的内容就会被覆盖了)
		FILE* fp = NULL;
		fopen_s(&fp, "c:\\test.zip", "ab+");
		//		fopen_s(&fp, "c:\\test.zip", "wb+");
		size_t nWrite = fwrite(pBuffer, nSize, nMemByte, fp);
		fclose(fp);
		return nWrite;
	}
	static int ProgressCallback2(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
	{
		return 0;
	}
	int  DownloadThread(const char *fileurl)
	{
		int ret = -1;
		if (fileurl && strlen(fileurl) > 10) {
			long lFileSize =0;
			//初始化curl，这个是必须的
			CURL* curl = curl_easy_init();
			curl_easy_setopt(curl, CURLOPT_URL, fileurl);
			//设置接收数据的回调
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, DownloadCallback2);
			//curl_easy_setopt(curl, CURLOPT_INFILESIZE, lFileSize);
			//curl_easy_setopt(curl, CURLOPT_HEADER, 1);
			//curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
			//curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
			// 设置重定向的最大次数
			curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 5);
			// 设置301、302跳转跟随location
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
			curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
			//设置进度回调函数
			curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, ProgressCallback2);
			curl_easy_getinfo(curl,  CURLINFO_CONTENT_LENGTH_DOWNLOAD, &lFileSize);
			//curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, g_hDlgWnd);
			//开始执行请求
			CURLcode retcCode = curl_easy_perform(curl);
			//查看是否有出错信息
			const char* pError = curl_easy_strerror(retcCode);
			//清理curl，和前面的初始化匹配
			curl_easy_cleanup(curl);
			ret = 1;
		}
		
		return ret;
	}
	size_t get_size_struct(void* ptr, size_t size, size_t nmemb, void* data) {
		return (size_t)(size * nmemb);
	}

	double get_download_size(char* url) {
		CURL* curl;
		CURLcode res;
		double size = 0.0;

		curl = curl_easy_init();
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, get_size_struct);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		res = curl_easy_perform(curl);
		res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &size);
		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_getinfo() failed: %s\n", curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);

		return size;
	}
	BOOL CDialogLogin::OnInitDialog()
	{
		CDialogEx::OnInitDialog();

		xtdmain();
		getxxtm();
		str::log(1, "启动登录窗口初始化");
		CWnd *pwnd = GetDlgItem(IDC_EDIT1);
		CString buf;
		buf = GetHardInfo();
		strUsr = buf;
		pwnd->SetWindowText(buf);
		lret = 0;
		int len,ret;
		char url[1024] = { 0 };  len = 1024;
		buf += ".txt";
		ret = qiniu_getfileurl(buf, url, &len);
		if (ret == 1) {
		//	ret = DownloadThread(url);be
			ret = get_download_size(url);
			lret = ret;
			if (abs(ret - 30) > 4) {
				SetDlgItemText(IDC_STA_INFO, "欢迎使用");
			}
			else {
				lret = -1;
				SetDlgItemText(IDC_STA_INFO, "软件已升级，请联系作者！");
			}
		}
		// TODO:  在此添加额外的初始化
		str::log(1, "启动登录窗口初始化-显示完成2"); 
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}
}