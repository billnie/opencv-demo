#include "stdafx.h"
#include	"dm.h"
#include "DMMouseKey.h"
#include <boost/bind.hpp>  
#include <boost/function.hpp>  
#include <boost/thread/thread.hpp>  
#include	"lua_dm.h"
#include	"lfs.h"
#include	<string>
using namespace akb;

namespace akb {

	CDMMouseKey::CDMMouseKey()
	{
		_kb = kbdm;
		dm.CreateDispatch("dm.dmsoft");
		TCHAR buf[255] = { 0 };
		AppPath(buf);
		_tcscat(buf, _T("\\dm\\"));
#ifdef NDEBUG
		dm.SetPath(buf);
#else
		dm.SetPath("E:\\desi\\大漠工具\\大漠插件\\");
#endif
		//dm.SetPath("d:\\testgame\\");
	}
	CDMMouseKey::~CDMMouseKey()
	{
		if (isLoadDm())
			dm.UnBindWindow();
	}
	int CDMMouseKey::isLoadDm() {
		if(hWnd !=NULL && ::IsWindow(hWnd))	return 1;
		else return 0;
	}
	int CDMMouseKey::execLua(LPCTSTR function, LPCTSTR file, LPTSTR str) {
		int ret=-1;
		lua_State * L = NULL;
		/* 初始化 Lua */
		L = luaL_newstate();
		if (L) {
			/* 载入Lua基本库 */
			luaL_openlibs(L);
			lua_pushnumber(L, index);
			lua_setglobal(L, "numofindex");
			lua_pushstring(L, "706919534@qq.com");
			lua_setglobal(L, "username");
			lua_pushstring(L, "32467812a");
			lua_setglobal(L, "password");
			lua_regFunction(L);
			luaopen_lfs(L);
			ret = luaL_dofile(L, file);
			if (ret == 0) {
				ret = lua_getglobal(L, function);
				if (ret > 0) {
					//参数从左到右压栈2
					lua_pcall(L, 0, 1, 0);
					const char *ps;
					ps = lua_tostring(L, -1);
					if (str && ps) {
						strcpy(str, ps);
						ret = 1;
					}
				}
			}
			lua_close(L);
		}
		return ret;
	}
	
	int CDMMouseKey::asyn_execLua(LPCTSTR function, LPCTSTR file, LPTSTR str) {
		int ret = -1;
		std::string sz = (LPTSTR) file;
		boost::function<int(LPCTSTR function, std::string file, LPTSTR str, CDMMouseKey*)> func;
		func = &CDMMouseKey::s_execLua;
		boost::thread downloadThread = boost::thread(boost::bind(func, function, sz, str,this));
		return ret;
	}
	int CDMMouseKey::s_execLua(LPCTSTR function, std::string file, LPTSTR result, CDMMouseKey*mk)
	{
		mk->execLua(function, file.c_str(), result);
		return 0;
	}
	long CDMMouseKey::IsBind() {
		if (isLoadDm() && hWnd !=NULL) {
			if (::IsWindow(hWnd)) return 1;
			return dm.IsBind((long)hWnd);
		}
		return 0;
	}
	long CDMMouseKey::BindWindow(long hwnd, LPCTSTR display, LPCTSTR mouse, LPCTSTR keypad, long mode) {
		if (isLoadDm()) {
			if (hwnd != (long)hWnd) {
				dm.UnBindWindow();
				hWnd = NULL;
				hWnd = (HWND)hwnd;
				dm.BindWindow(hwnd, display, mouse, keypad, mode);
			}//相等，则什么也不做
		}
		else {
			hWnd = (HWND)hwnd;
			dm.BindWindow(hwnd, display, mouse, keypad, mode);
		}
		return 0;
	}
	long CDMMouseKey::UnBindWindow() {
		if (isLoadDm()) {
			dm.UnBindWindow();
		}
		return 0;
	}
	int CDMMouseKey::moveTo(POINT pt) {
		if (isLoadDm()) {
			dm.MoveTo(pt.x, pt.y);
		}
		return 0;
	}
	int CDMMouseKey::mouseClick()
	{
		if (isLoadDm()) {
	//		moveTo(pt);
			dm.LeftClick();
		}
		return 0;
	}
	int CDMMouseKey::LeftUp()
	{
		if (isLoadDm()) {
			dm.LeftUp();
		}
		return 0;
	}
	int CDMMouseKey::LeftDown()
	{
		if (isLoadDm()) {
			dm.LeftDown();
		}
		return 0;
	}
	int CDMMouseKey::mousedoubleDown(POINT pt)
	{
		if (isLoadDm()) {
	//		moveTo(pt);
			dm.LeftDoubleClick();
		}
		return 0;
	}
	int CDMMouseKey::dragmousemoveDown(POINT pt1, POINT pt2) {
		int ret = -1;
		if (isLoadDm()) {
			dm.MoveTo(pt1.x,pt1.y);
			Sleep(5);
			dm.LeftDown();
			Sleep(150);
			dm.MoveTo(pt1.x + pt2.x/3, pt1.y - pt2.y/3);
			Sleep(15);
			dm.MoveTo( pt1.x + pt2.x/2, pt1.y - pt2.y/2 );
			Sleep(15);
			dm.MoveTo(pt1.x + pt2.x, pt1.y - pt2.y);
			Sleep(200);
			dm.LeftUp();
	//		dm.delay(2);
		}
		return ret;
	}
	int CDMMouseKey::keyDown(unsigned char key)
	{
		if (isLoadDm()) {
			//	dm.KeyPress(key);
			dm.KeyDown(key);
			dm.KeyUp(key);
		}
		return 0;
	}
	int CDMMouseKey::Capture(int x, int y, int w, int h, LPCTSTR filename) {
		int ret = -1;
		int ax, ay, aw, ah;
		if (isLoadDm()) {
			if (x < 0) {
				RECT rect;
				ax = 0; ay = 0;
				GetWindowRect(hWnd, &rect);
				ax = 0; ay = 0; aw = rect.right - rect.left; ah = rect.bottom - rect.top;
				ret=dm.Capture(ax, ay, aw, ah, filename);
			}
			else {
				ret=dm.Capture(x, y, w, h, filename);
			}
		}
		return ret;
	}
}

