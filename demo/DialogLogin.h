#pragma once

#include	"DialogMulti.h"
// CDialogLogin 对话框
namespace aui {
	class CDialogLogin : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogLogin)

	public:
		CDialogLogin(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogLogin();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DIALOG };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		CString strUsr;
		long lret;
		afx_msg void OnBnClickedOk();
		virtual BOOL OnInitDialog();
		CString GetHardInfo();
	};
}