// DlgDm.cpp : 实现文件
//

#include "stdafx.h"
#include "DlgDm.h"
#include "afxdialogex.h"
#include	"resource.h"
#include "lua-hlp.h"
#include	 "MouseKey.h"
#include	"DMMouseKey.h"
#include	"lua_dm.h"
#include "tmfactory.h"

using namespace akb;
// CDlgDm 对话框

IMPLEMENT_DYNAMIC(CDlgDm, CDialogEx)

CDlgDm::CDlgDm(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLGDM, pParent)
{

}

CDlgDm::~CDlgDm()
{
}

void CDlgDm::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgDm, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_FONT, &CDlgDm::OnBnClickedBtnFont)
	ON_BN_CLICKED(IDC_BTN_OCR, &CDlgDm::OnBnClickedBtnOcr)
	ON_BN_CLICKED(IDC_BTN_LVL, &CDlgDm::OnBnClickedBtnLvl)
	ON_BN_CLICKED(IDC_BTN_CAP, &CDlgDm::OnBnClickedBtnCap)
	ON_BN_CLICKED(IDC_BTN_SCROL, &CDlgDm::OnBnClickedBtnScrol)
	ON_BN_CLICKED(IDC_BTN_BMPDIA, &CDlgDm::OnBnClickedBtnBmpdia)
	ON_BN_CLICKED(IDC_BTN_GAME, &CDlgDm::OnBnClickedBtnGame)
	ON_BN_CLICKED(IDC_BTN_AUTO, &CDlgDm::OnBnClickedBtnAuto)
END_MESSAGE_MAP()


// CDlgDm 消息处理程序


void CDlgDm::OnBnClickedBtnFont()
{
	execLua("buyarrow", "ocr.lua");
}

void CDlgDm::OnBnClickedBtnAuto()
{
	execLua("lvlfight", "ocr.lua");
}


void CDlgDm::OnBnClickedBtnOcr()
{
	// TODO: 在此添加控件通知处理程序代码
	CString str;
	int ret;
	execLua("xlotion", "ocr.lua");
}

BOOL CDlgDm::OnInitDialog()
{
	CDialogEx::OnInitDialog();
//	dm.CreateDispatch("dm.dmsoft");
	// TODO:  在此添加额外的初始化
	CString str, s;
	TCHAR buf[MAX_PATH] = { 0 };
	::GetPrivateProfileString("Settings",
		"path",
		"",
		buf,
		MAX_PATH, "E:\\desi\\大漠工具\\大漠插件\\");
	if (_tcslen(buf) < 4) {
		_tcscpy(buf, _T("E:\\desi\\大漠工具\\大漠插件\\"));
	}
	SetDlgItemText(IDC_STA_FONT, buf);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


int CDlgDm::execLua(LPCTSTR function, LPCTSTR file)
{
	CString str;
	TCHAR ss[MAX_PATH] = { 0 };
	TCHAR sz[MAX_PATH] = { 0 },*cc;
	
	int ret;
	char buf[1024] = { 0 };

	if (0) {
		tmfactory *fac; fac = tmfactory::Instance();
		tmVirMachine *mac; mac = fac->getMachine(1);
		mac->callluaFunction(function);
		return 0;
	}

	akb::CDMMouseKey *mk;
	mk = (akb::CDMMouseKey*)akb::CDMMouseKey::Instance();
	AppPath(sz);
#ifdef _DEBUG
	cc = _tcsrchr(sz, '\\');
	if (cc) {
		*cc = 0;
		_tcscat(sz, _T("\\demo"));
	}
	sprintf(ss, _T("%s\\tm\\lua\\%s"), sz, file);
#else
	_tcscpy(ss, file);
#endif
	
	ret = mk->asyn_execLua(function, ss, buf);
	return 0;
}	


void CDlgDm::OnBnClickedBtnLvl()
{
	execLua("xlvl", "ocr.lua");
}


void CDlgDm::OnBnClickedBtnCap()
{
	char tempbuf[128];
	char strtm[32] = { 0 };

	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);

	timeinfo = localtime(&rawtime);

	strftime(tempbuf, 128, "d:\\testgame\\tm\\data_%Y%m%d_%H%M%S_1.bmp", timeinfo);
	HBITMAP hbmp;
	if (0) {
		tmfactory *fac; fac = tmfactory::Instance();
		tmVirMachine *mac; mac = fac->getMachine(1);
		mac->mk->Capture(-1, 0, 0, 0, tempbuf);
	}else{
		akb::CMouseKey *kb = akb::CMouseKey::Instance();
		//	kb->Capture(-1, 0, 0, 0, "1.bmp");
		kb->Capture(-1, 0, 0, 0, tempbuf);
	}
}


void CDlgDm::OnBnClickedBtnScrol()
{
	execLua("xlscrolls", "ocr.lua");
}


void CDlgDm::OnBnClickedBtnBmpdia()
{
	execLua("xcheckdialog", "ocr.lua");
}


void CDlgDm::OnBnClickedBtnGame()
{
	execLua("xstartgame", "ocr.lua");
}

