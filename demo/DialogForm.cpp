// DialogForm.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogForm.h"
#include "afxdialogex.h"


#include "boost/thread.hpp"
#include "iostream"
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <iostream>
#include <Tlhelp32.h> 
#include <psapi.h>
#include "MouseKey.h"
#include "FindProc.h"
using namespace akb;

extern int g_state=1;		//游戏状态，是否为暂停，继续状态
unsigned char xtwsm=0;	//状态，包括wssocket 1,和sharemem bit2的状态
int _xtmain(int argc, char* argv[]);
int getxxtm() {
	time_t timep;
	time_t xt;
	double dt;
	int xx;
	struct tm xtm = { 0 };
	xtm.tm_year = 117;
	xtm.tm_mon = 10;
	xtm.tm_mday = 6;
	xt = mktime(&xtm);
	time(&timep); /*获取time_t类型的当前时间*/
				  /*用gmtime将time_t类型的时间转换为struct tm类型的时间，按没有经过时区转换的UTC时间
				  然后再用asctime转换为我们常见的格式 Fri Jan 11 17:25:24 2008
				  */
	dt = difftime(xt,timep);
	xx = dt / (60 * 60 * 24);
	printf("%s", asctime(gmtime(&timep)));
	return xx;
}
#include <process.h>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

// CDialogForm 对话框
aui::CDialogForm *gform = NULL;



namespace aui {
	IMPLEMENT_DYNAMIC(CDialogForm, CDialogEx)

		CDialogForm::CDialogForm(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DIALOG4, pParent)
	{
	}

	CDialogForm::~CDialogForm()
	{
	}

	void CDialogForm::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_TAB1, m_tabCtrl);
	}


	BEGIN_MESSAGE_MAP(CDialogForm, CDialogEx)
		ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CDialogForm::OnTcnSelchangeTab1)
		ON_WM_SIZE()
		ON_WM_TIMER()
		ON_WM_DESTROY()
	END_MESSAGE_MAP()
	int CDialogForm::addrichLog(LPTSTR title, LPTSTR message)
	{
		addrichLog(0, title, message);
		return 0;
	}
	int CDialogForm::addrichLog2(LPTSTR title, LPTSTR message)
	{
		addrichLog(1, title, message);
		return 0;
	}
	// CDialogForm 消息处理程序
	int CDialogForm::addrichLog(int t, LPTSTR title, LPTSTR message)
	{
		DT(message);
		if (t == 0)
			m_dlgLog.addrichLog(title, message);
		else if (t == 1) {
			m_dlgLog.addrichLog2(title, message);
		}
		return 0;
	}

	void CDialogForm::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
	{
		int cur_sel = m_tabCtrl.GetCurSel();

		if (m_cur_mode_sel != cur_sel) {
			m_pPage[m_cur_mode_sel]->ShowWindow(SW_HIDE);
			m_pPage[cur_sel]->ShowWindow(SW_SHOW);
			m_cur_mode_sel = cur_sel;
		}
		*pResult = 0;
	}

	int CDialogForm::loadIni() {
		CString str, s;
		TCHAR buf[MAX_PATH] = { 0 };
		::GetPrivateProfileString("Settings",
			"task",
			"",
			buf,
			MAX_PATH, ".\\Settings.ini");

		return 0;
	}
	BOOL CDialogForm::OnInitDialog()
	{
		CDialogEx::OnInitDialog();
		CString s;
		s = _T("天堂M-辅助");
		s += _T(" - build at ");
		s += __DATE__;
		s += _T(" @706919534 ");
	
		SetWindowText(s);
		str::log(1, "启动游戏窗口初始化");

		m_evtTurn = CreateEvent(NULL,   //默认安全级别  
			TRUE,   //人工重置  
			FALSE,  //初始为无信号  
			NULL); //
		gform = this;
		tskSta = tskStateNone;
		m_dlgMult.Create(IDD_DLG_MULTI, &m_tabCtrl);
		CRect rs;
		m_tabCtrl.GetClientRect(&rs);
		//调整子对话框在父窗口中的位置
		rs.top += 40;
		rs.bottom -= 44;
		rs.left += 4;
		rs.right -= 4;

		//设置子对话框尺寸并移动到指定位置
		m_dlgMult.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgMult.ShowWindow(true);
#ifdef DG
		m_dlgNpc.Create(IDD_DLG_NPC, &m_tabCtrl);
		m_dlgNpc.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgNpc.ShowWindow(false);
#endif
		m_dlgTask.Create(IDD_DLG_TASK, &m_tabCtrl);
		m_dlgTask.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgTask.ShowWindow(false);

		m_dlgEdit.Create(IDD_DLG_EDIT, &m_tabCtrl);
		m_dlgEdit.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgEdit.ShowWindow(false);

		m_dlgLog.Create(IDD_DLG_LOGIN, &m_tabCtrl);
		m_dlgLog.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgLog.ShowWindow(false);

#ifdef DG
		m_dlgmon.Create(IDD_DLG_MONFILE, &m_tabCtrl);
		m_dlgmon.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgmon.ShowWindow(false);
#endif
		m_dlgAbout.Create(IDD_DLG_ABOUT, &m_tabCtrl);
		m_dlgAbout.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgAbout.ShowWindow(false);
#ifdef DEBUG
		m_dlgTm.Create(IDD_DLGDM, &m_tabCtrl);
		m_dlgTm.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_dlgTm.ShowWindow(false);
#endif
		int cx = GetSystemMetrics(SM_CXSCREEN);
		int cy = GetSystemMetrics(SM_CYSCREEN);
		CRect rc, rt;
		GetClientRect(&rc);
		//rc.top = cy - rc.left + rc.bottom;
		SystemParametersInfo(SPI_GETWORKAREA, 0, &rt, 0);
		SetWindowPos(NULL, cx - 340, 0,340, rt.Height(), SWP_NOZORDER);
		//SetWindowPos(NULL, cx - (rc.right - rc.left) - 80, 0, rc.right - rc.left + 80, rt.Height(), SWP_NOZORDER);
		int i = 0;
		m_tabCtrl.InsertItem(i++, TEXT(" 游戏 "));  //添加参数一选项卡
#ifdef DG
		m_tabCtrl.InsertItem(i++, TEXT(" npc "));  //添加参数一选项卡
#endif
		m_tabCtrl.InsertItem(2, TEXT(" edit "));  //添加参数一选项卡
		m_tabCtrl.InsertItem(3, TEXT(" log "));  //添加参数一选项卡
#ifdef DG	
		m_tabCtrl.InsertItem(4, TEXT(" task "));  //添加参数一选项卡	
		m_tabCtrl.InsertItem(5, TEXT(" mon "));  //添加参数一选项卡
#endif
		m_tabCtrl.InsertItem(6, TEXT(" 关于 "));  //添加参数一选项卡
#ifdef DEBUG	
		m_tabCtrl.InsertItem(6, TEXT(" 天堂M "));
#endif
		i = 0;
		m_cur_mode_sel = 0;
		m_pPage[i++] = &m_dlgMult;
#ifdef DG
		m_pPage[i++] = &m_dlgNpc;
#endif
		m_pPage[i++] = &m_dlgEdit;
		m_pPage[i++] = &m_dlgLog;
#ifdef DG	
		m_pPage[i++] = &m_dlgTask;

		m_pPage[i++] = &m_dlgmon;
#endif
		m_pPage[i++] = &m_dlgAbout;
#ifdef DEBUG	
		m_pPage[i++] = &m_dlgTm;
#endif
		//读取配置文件，看是否上次有自动挂机任务，如果有，就加入
		str::log(1, "启动游戏窗口初始化，加载配置");
		loadIni();

		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}
	void CDialogForm::OnSize(UINT nType, int cx, int cy)
	{
		CDialogEx::OnSize(nType, cx, cy);
		CSize sizeTotal;
		sizeTotal.cx = cx;
		sizeTotal.cy = cy;

		if (m_tabCtrl.GetSafeHwnd() != NULL) {
			CRect rect;
			GetClientRect(rect);
			m_tabCtrl.MoveWindow(rect);
			m_tabCtrl.GetWindowRect(rect);
			ScreenToClient(rect);
			rect.right = cx - rect.left;
			rect.bottom = cy - rect.left;
			m_tabCtrl.MoveWindow(rect);
			rect.top = 20;
			m_dlgMult.MoveWindow(rect);
#ifdef DG
			m_dlgNpc.MoveWindow(rect);
			m_dlgTask.MoveWindow(rect);
			m_dlgmon.MoveWindow(rect);
#endif			
#ifdef _DEBUG
			m_dlgTm.MoveWindow(rect);
#endif
			m_dlgEdit.MoveWindow(rect);
			m_dlgLog.MoveWindow(rect);
			m_dlgAbout.MoveWindow(rect);

		}
	}

	void CDialogForm::OnTimer(UINT_PTR nIDEvent)
	{
		switch (nIDEvent) {
		case 1:
		{
			KillTimer(1);
		}
		break;
		case 2:	//延时点击任务
			mouseDown(pt);
			KillTimer(nIDEvent);
			break;
		case 3:	//打完npc后走图
		{

			KillTimer(nIDEvent);
		}
		break;
		case 97:		//自动打怪时钟
			break;
		case 99:			//自动截图, 200ms一次
			updateDofusImage(IDC_PIC_2);
			break;
		}
		CDialogEx::OnTimer(nIDEvent);
	}

	int CDialogForm::updateDofusImage(UINT uID)
	{
		HBITMAP hbitmap;
		HWND h;
		CRect r1;
		LPRECT lpRect;

		hbitmap = capXXXWindow(DOFUSCLASS, DOFUSTITLE);
		if (hbitmap) {
			
			DeleteObject(hbitmap);
		}
		return 0;
	}
	
	int CDialogForm::mousedoubleDown(POINT pt)
	{
		HWND h;
		CRect r1;
		int dwidth, dheight;
		POINT p;

		if (h == NULL) //return 0;
			return 0;
		::GetWindowRect(h, &r1);
		dwidth = r1.right - r1.left;
		dheight = r1.bottom - r1.top;
		//保存当前鼠标指针
		//取得当前鼠标位置
		GetCursorPos(&p);
		//设置鼠标指针位置  取坐标:x=273;y=273 //lparam 0x01110111
		SetCursorPos(r1.left + pt.x, r1.top + pt.y);
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
		Sleep(1);
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
		DT("doublemouseDown (%d,%d)", pt.x, pt.y);
		SetCursorPos(p.x, p.y);
		return 0;
	}
	int CDialogForm::dragmousemoveDown(POINT pt1, POINT pt2) {
		CRect r1;
		int dwidth, dheight;
		POINT p;
		HWND h;

		if (0) {
			::GetWindowRect(h, &r1);
			dwidth = r1.right - r1.left;
			dheight = r1.bottom - r1.top;

			if (pt1.x < 1 || pt1.y < 1) return 0;
			//保存当前鼠标指针
			//取得当前鼠标位置
			GetCursorPos(&p);
			//设置鼠标指针位置  取坐标:x=273;y=273 //lparam 0x01110111
			if (0) {
				::PostMessage(h, WM_LBUTTONDOWN, 0, MAKELPARAM(pt1.x, pt1.y));
				//		Sleep(1);
				::PostMessage(h, WM_MOUSEMOVE, 0, MAKELPARAM(pt2.x, pt2.y));
				::PostMessage(h, MOUSEEVENTF_LEFTUP, 0, MAKELPARAM(pt2.x, pt2.y));
			}
			else {
				SetCursorPos(r1.left + pt1.x, r1.top + pt1.y);
				//按下
				mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				Sleep(2);
				mouse_event(MOUSEEVENTF_MOVE, pt2.x, pt2.y, 0, 0);
				//抬起
				Sleep(2);
				mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

			}
			DT("mousemoveDown (%d,%d)", pt1.x, pt1.y);
			SetCursorPos(p.x, p.y);
		}
		else {
			CMouseKey *kb = CMouseKey::Instance();
			kb->dragmousemoveDown(pt1, pt2);
		}
		return 0;
	}
	int CDialogForm::moveNearFrame(int direction) {
		POINT m_pt, pt2;
		pt2 = { 0,0 };
		int width, height;
		int dx, dy;
		dy = 130; dx = 130;
		width = 799; height = 540;
		switch (direction) {
		case 2://1:	//up
			m_pt.y = 110;
			m_pt.x = width / 2;
			pt2.y = -dy;
			break;
		case 3://2:	//right
			m_pt.x = width - 220;
			m_pt.y = height / 2;
			pt2.x = 0 - dx;
			break;
		case 1://4:		//left
			m_pt.x = 220;
			m_pt.y = height / 2;
			pt2.x = 0 + dx;
			break;
		case 4:// 8:		//down
			m_pt.y = height - 95;
			m_pt.x = width / 2;
			pt2.y = 0 + dy;
			break;
		}
		dragmousemoveDown(m_pt, pt2);
		return 0;
	}
	int CDialogForm::mouseDown(POINT pt)
	{
		CRect r1;
		int dwidth, dheight;
		POINT p;
		HWND h;

		::GetWindowRect(h, &r1);
		dwidth = r1.right - r1.left;
		dheight = r1.bottom - r1.top;

		if (pt.x < 1 || pt.y < 1) return 0;
		//保存当前鼠标指针
		//取得当前鼠标位置
		GetCursorPos(&p);
		//设置鼠标指针位置  取坐标:x=273;y=273 //lparam 0x01110111
		SetCursorPos(r1.left + pt.x, r1.top + pt.y);
		//按下
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		Sleep(1);
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
		//抬起
		//mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
		DT("mouseDown (%d,%d)(%d,%d)", pt.x, pt.y, r1.left + pt.x, r1.top + pt.y);
		SetCursorPos(p.x, p.y);
		return 0;
	}
	int CDialogForm::addwebsocketDofus(LPTSTR message, int &evt) {
		
		return 0;
	}

	int aui::CDialogForm::setTskStat(int state)
	{
		tskSta = (tskState)state;
		g_state = state;

		return 0;
	}


	void aui::CDialogForm::OnDestroy()
	{
		gform = NULL;
		if (m_evtTurn != INVALID_HANDLE_VALUE)
			CloseHandle(m_evtTurn);
		m_evtTurn = INVALID_HANDLE_VALUE;
		__super::OnDestroy();

		// TODO: 在此处添加消息处理程序代码
	}
}