#pragma once
#include "NpcComm.h"
namespace anpc {
	class CNpc3_1 :
		public CNpcComm
	{
	public:
		CNpc3_1();
		~CNpc3_1();
		virtual int fighturn(int turn, int sub);
		//加状态处理，有一些可能 不一样的
		virtual int addfightState();
		virtual int updateturn(string info);		//提示轮到我了
	};
}