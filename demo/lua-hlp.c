#include <memory.h>
#include "lua-hlp.h"

#ifdef __cplusplus
extern "C" {
#include "lua.h"  
#include <lauxlib.h>   
#include <lualib.h>   
}
#else
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#endif

#include	"windows.h"
//
////待注册的C函数，该函数的声明形式在上面的例子中已经给出。
////需要说明的是，该函数必须以C的形式被导出，因此extern "C"是必须的。
////函数代码和上例相同，这里不再赘述。
//int dm_ocr(lua_State* L)
//{
//	double op1 = luaL_checknumber(L, 1);
//	double op2 = luaL_checknumber(L, 2);
//	lua_pushnumber(L, op1 + op2);
//	return 1;
//}
//
//int dm_findpic(lua_State* L)
//{
//	double op1 = luaL_checknumber(L, 1);
//	double op2 = luaL_checknumber(L, 2);
//	lua_pushnumber(L, op1 - op2);
//	return 1;
//}
//
////luaL_Reg结构体的第一个字段为字符串，在注册时用于通知Lua该函数的名字。
////第一个字段为C函数指针。
////结构体数组中的最后一个元素的两个字段均为NULL，用于提示Lua注册函数已经到达数组的末尾。
//  luaL_Reg mylibs[] = {
//	{ "ocr", dm_ocr },
//	{ "findpic", dm_findpic },
//	{ NULL, NULL }
//};
//
//int luaopen_mytestlib(lua_State* L)
//{
//	const char* libName = "mytestlib";
//	luaL_register(L, libName, mylibs);
//	return 1;
//}
//#include "stlstr.h"
int execlua2(int v1, int v2, LPCTSTR func, LPCTSTR file ,long *lret)
{
	int ret;
	lua_State * L = NULL;
	/* 初始化 Lua */
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
//		str::log(1, "运行脚本 %s, %s", file, func);
		/* 运行脚本 */
		ret = luaL_dofile(L, file);
		if (ret==0) {
			ret = lua_getglobal(L, func);
			if (ret> 0) {
				//参数从左到右压栈2
				lua_pushinteger(L, v1);
				lua_pushinteger(L, v2);
				lua_pcall(L, 2, 1, 0);
				if (lret)
					*lret = lua_tointeger(L, -1);
				printf("lua add function return val is %d \n", lua_tointeger(L, -1));
			}
		}

		//	std::cout << "lua add function return val is " << lua_tointeger(L, -1) << endl;

		/* 清除Lua */
		lua_close(L);
	}

	return 1;
}
int execlua3(LPCTSTR s1, int v2, LPCTSTR func, LPCTSTR file, long *lret) {
	int ret;
	lua_State * L = NULL;
	/* 初始化 Lua */
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
		//		str::log(1, "运行脚本 %s, %s", file, func);
		/* 运行脚本 */
		ret = luaL_dofile(L, file);
		if (ret == 0) {
			ret = lua_getglobal(L, func);
			if (ret> 0) {
				//参数从左到右压栈2
				lua_pushstring(L, s1);
				lua_pushinteger(L, v2);
				lua_pcall(L, 2, 2, 0);
				if (lret)
					*lret = lua_tointeger(L, -2);
				printf("lua add function return val is %d \n", lua_tointeger(L, -1));
			}
		}
		lua_close(L);
	}

	return 1;
}
int execlua4(int v1, int v2, LPCTSTR func, LPCTSTR file, LPTSTR str) {
	int ret;
	lua_State * L = NULL;
	/* 初始化 Lua */
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
		//		str::log(1, "运行脚本 %s, %s", file, func);
		/* 运行脚本 */
		ret = luaL_dofile(L, file);
		if (ret == 0) {
			ret = lua_getglobal(L, func);
			if (ret > 0) {
				//参数从左到右压栈2
				lua_pushinteger(L, v1);
				lua_pushinteger(L, v2);
				lua_pcall(L, 2, 1, 0);
				char *ps;
				ps = lua_tostring(L, -1);
				if (str && ps) {
					strcpy(str, ps);
				}
			}
		}
		lua_close(L);
	}
	return 1;
}
int execlua5(LPCTSTR s1, int v1, int v2, LPCTSTR func, LPCTSTR file,long *lret)
{
	int ret;
	lua_State * L = NULL;
	/* 初始化 Lua */
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
		//		str::log(1, "运行脚本 %s, %s", file, func);
		/* 运行脚本 */
		ret = luaL_dofile(L, file);
		if (ret == 0) {
			ret = lua_getglobal(L, func);
			if (ret> 0) {
				//参数从左到右压栈2
				lua_pushstring(L, s1);
				lua_pushinteger(L, v1);
				lua_pushinteger(L, v2);
				lua_pcall(L, 3, 1, 0);
				if (lret)
					*lret = lua_tointeger(L, -1);
				printf("lua add function return val is %d \n", lua_tointeger(L, -1));
			}
		}
		lua_close(L);
	}

	return 1;
}
int execluaocr( LPCTSTR func, LPCTSTR file, LPTSTR str) {
	int ret;
	lua_State * L = NULL;
	/* 初始化 Lua */
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
		//		str::log(1, "运行脚本 %s, %s", file, func);
		/* 运行脚本 */
		ret = luaL_dofile(L, file);
		if (ret == 0) {
			ret = lua_getglobal(L, func);
			if (ret > 0) {
				//参数从左到右压栈2
				lua_pcall(L, 0, 1, 0);
				char *ps;
				ps = lua_tostring(L, -1);
				if (str && ps) {
					strcpy(str, ps);
				}
			}
		}
		lua_close(L);
	}
	return 1;
}

int parseocrstring(LPCTSTR str, int *x, int *y, int *w, int *h, LPTSTR color, float *val){
	int ret = -1;
	if (str&& _tcslen(str)>10) {
		char*temp = strtok(str,(const char*) ",");
		int i=0;
		while (temp)
		{
			switch (i) {
			case 0:
				*x = atoi(temp);
				break;
			case 1:
				*y = atoi(temp);
				break;
			case 2:
				*w = atoi(temp);
				break;
			case 3:
				*h = atoi(temp);
				break;
			case 4:
				strcpy(color, temp);
				break;
			case 5:
				*val = atof(temp);
				break;
			}
			i++;
//			printf("%s ", temp);
			temp = strtok(NULL, ",");
		}
		if (i >= 5)
			ret = 1;
	}
	return ret;
}