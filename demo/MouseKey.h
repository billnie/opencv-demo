#pragma once
/*
键盘鼠标基本操作类
*/
#include	"windows.h"
#define WM_LOG_LUA WM_USER+300  
namespace akb {
	enum kbType {
		taskNone,
		kbNormal,
		kbdm,	//大漠后台
		kbts,	//天使后台
	};
	class CMouseKey
	{
	public:
		int index;	//当前编号，多开时用
		HWND hWnd;	//绑定的窗口
		kbType _kb;
		int IsValiedHWnd();
		static HWND  loghWnd;
		static CMouseKey *g_kb;
		static CMouseKey * KMInstance(kbType = kbdm);
		static CMouseKey * Instance() {
			if (g_kb == NULL) {
				g_kb = new CMouseKey();
			}
			return g_kb;
		}

		CMouseKey();
		~CMouseKey();
		virtual long BindWindow(long hwnd, LPCTSTR display, LPCTSTR mouse, LPCTSTR keypad, long mode) { return 0; };
		virtual long UnBindWindow() { return 0; };
		virtual long IsBind() { return 0; }
		virtual int moveTo(POINT pt) { return 0; };
		virtual int mouseClick() { return 0; };
		virtual int LeftDown() { return 0; };
		virtual int LeftUp() { return 0; };
		virtual int mousedoubleDown(POINT pt) { return 0; };
		virtual int keyDown(unsigned char key) { return 0; }
		virtual int dragmousemoveDown(POINT pt1, POINT pt2) { return 0; };
		virtual int Capture(int x, int y, int w, int h, LPCTSTR filename) { return 0; }
	};
}
