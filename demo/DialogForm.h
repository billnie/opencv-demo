#pragma once

#include "DialogMulti.h"
#include "DialogNpc.h"
#include	"DialogLog.h"
#include	"DialogTask.h"
#include	"DialogEdit.h"
#include "DialogFileMon.h"
#include "afxcmn.h"

#include "DialogAbout.h"
#include "DlgDm.h"

// CDialogForm 对话框
namespace aui {
	class CDialogForm : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogForm)

		CDialogMulti	m_dlgMult;
		CDialogNpc		m_dlgNpc;
		CDialogEdit		m_dlgEdit;
		CDialogLog		m_dlgLog;
		CDialogTask		m_dlgTask;
		CDialogAbout	m_dlgAbout;
		CDialogFileMon m_dlgmon;
		CDlgDm			m_dlgTm;

		int m_cur_mode_sel;
		CDialog *m_pPage[7];
		POINT pt;
	public:
		enum tskState {
			tskStateNone,
			tskStateRunning,
			tskStatePause,
		};
		tskState tskSta;
		HANDLE m_evtTurn;
		CDialogForm(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogForm();
		virtual int mouseDown(POINT pt);
		virtual int mousedoubleDown(POINT pt) ;
		virtual int addrichLog(LPTSTR title, LPTSTR message) ;
		virtual int addrichLog2(LPTSTR title, LPTSTR message) ;

		virtual int dragmousemoveDown(POINT pt1, POINT pt2);
		//dir 1 up  2right 3left, 4down
		virtual int moveNearFrame( int dir);		//移动到相邻的地图
		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DIALOG4 };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		DECLARE_MESSAGE_MAP()
	public:
		afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
		CTabCtrl m_tabCtrl;
		virtual BOOL OnInitDialog();
		afx_msg void OnSize(UINT nType, int cx, int cy);
		
		afx_msg void OnTimer(UINT_PTR nIDEvent);
		int updateDofusImage(UINT uID);
		int setTskStat(int state);
		int addrichLog(int t, LPTSTR title, LPTSTR message);
		int addwebsocketDofus(LPTSTR message, int &evt);

			//加载配置文件
		int loadIni();

		afx_msg void OnDestroy();

	};
}