#pragma once
#include	<vector>
#include	<string>
#include	<map>
#include "MouseKey.h"
#include <lua.hpp>
#include <boost/thread/thread.hpp>  
#include <boost/thread/mutex.hpp>  
#include "tmbststate.h"

using namespace std;
//using namespace akb;
class CMouseKey;
class tmbststate;
//虚拟机
class tmVirMachine {
public:
	enum {
		tmInit,
		tmIdle,
		tmRunning,
		tmPasue,
		tmStop,
		tmRestart,	//重新加载脚本，方便调试
	};
	~tmVirMachine() {
		if (L) { lua_close(L); L = NULL; }
		if (mk != NULL) {
			delete mk; mk = NULL;
		}
		if (bstState != NULL) {
			delete bstState;  bstState = NULL;
		}
	}
	tmVirMachine(int inx) {
		mk = NULL;	level = 0; lmotion = 0;	index = inx;	L = NULL; vmState = tmInit; larrow = 0;
		bstState = NULL;
		initlua();
		initMk(inx);
	}
	void initlua();
	void release();
	void initMk(int inx);	//创建指定编号的lua，大漠对象，及状态机等，指定lua 日志输出窗口
	int  initluaFile(TCHAR *filename);	//创建虚拟机，加载lua脚本文件
	int	 initAccount(LPCTSTR name, LPCTSTR password);	//虚拟处理线程起来后，自动登录功能
	int  callluaFunction(const TCHAR *function);
	//带参和返回参数的执行脚本
	int  callluavmFunction(const TCHAR *function, const char *fmt, ...);
	int  getLevel();
	int  getLmotion();
	int	 getlarrow();
	int  process_Event();
	akb::CMouseKey *mk;		//键盘鼠标执行，及抓图执行体
	int level;				//游戏级别
	int lmotion;			//小瓶数量
	int larrow;				//弓箭数量
	int index;
	lua_State * L;			//
	int vmState;
	tmbststate *bstState;
protected:
	boost::mutex lock;
};
//工厂
class tmfactory
{
public:
	map<int, tmVirMachine *>mpMachine;
	static tmfactory *g_fac;
	vector<tmVirMachine> vecMac;
	static tmfactory * Instance() {
		if (g_fac == NULL) {
			g_fac = new tmfactory();
		}
		return (tmfactory *)g_fac;
	}
	tmVirMachine * getMachine(int index);
	int removeMachine(int index);
	int getLuaPath(TCHAR *buf);
	tmfactory();
	~tmfactory();
};

