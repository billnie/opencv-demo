// DialogMulti.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogMulti.h"
#include "afxdialogex.h"

#include	"DialogForm.h"
#include	"DialogLog.h"
#include "tmaccount.h"
#include "tmfactory.h"

#include "DialogAccount.h"
#include <boost/bind.hpp>  
#include <boost/function.hpp>  
#include <boost/thread/thread.hpp>  

//用户功能选项，与下面是相同的
int userTask =0;	//用户的任务类型
int userlvl = 0;
#define NPC_ONLY 2
#define	UP_ONLY 1
#define	MULT_UP	4
#define USER_MODE 3

//返回当前选择项
int GetListCtrlSelectedIndex(CListCtrl &ctrl) {
	int ret = -1;
	if (ctrl.GetSelectedCount() > 0) {
		POSITION pos;
		pos = ctrl.GetFirstSelectedItemPosition();
		while (pos) {
			ret = ctrl.GetNextSelectedItem(pos);
			break;
		}
	}
	return ret;
}
// CDialogMulti 对话框
namespace aui {
	IMPLEMENT_DYNAMIC(CDialogMulti, CDialogEx)

		CDialogMulti::CDialogMulti(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_MULTI, pParent)
	{

	}

	CDialogMulti::~CDialogMulti()
	{
	}

	void CDialogMulti::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_LIST1, m_listCtrl);
		DDX_Control(pDX, IDC_COMBO2, m_comBoxtask);
		DDX_Control(pDX, IDC_COMBO1, m_comBoxlvl);
	}


	BEGIN_MESSAGE_MAP(CDialogMulti, CDialogEx)
		ON_BN_CLICKED(IDC_BTN_ADD, &CDialogMulti::OnBnClickedBtnAdd)
		ON_BN_CLICKED(IDC_BTN_EDIT, &CDialogMulti::OnBnClickedBtnEdit)
		ON_BN_CLICKED(IDC_BTN_TAB, &CDialogMulti::OnBnClickedBtnTab)
		ON_BN_CLICKED(IDC_BTN_BEGIN, &CDialogMulti::OnBnClickedBtnBegin)
		ON_BN_CLICKED(IDC_BTN_PAUSE, &CDialogMulti::OnBnClickedBtnPause)
		ON_BN_CLICKED(IDC_BTN_LEADER, &CDialogMulti::OnBnClickedBtnLeader)
		ON_BN_CLICKED(IDC_BTN_SET, &CDialogMulti::OnBnClickedBtnSet)
		ON_COMMAND_RANGE(IDC_CHECK_BIRD, IDC_CHECK_MULTF, OnCommandRangeTask)
		ON_NOTIFY(NM_RCLICK, IDC_LIST1, &CDialogMulti::OnRclickList1)
		ON_COMMAND(ID_MENU_DELETE, &CDialogMulti::OnMenuDelete)
		ON_COMMAND(IDR_MENU_START, &CDialogMulti::OnIdrMenuStart)
		ON_COMMAND(IDR_MENU_STOP, &CDialogMulti::OnIdrMenuStop)
		ON_COMMAND(ID_MENU_PAUSE, &CDialogMulti::OnIdrMenuPause)
		ON_COMMAND(ID_MENU_RESTART, &CDialogMulti::OnMenuRestart)
		ON_WM_TIMER()
	END_MESSAGE_MAP()


	// CDialogMulti 消息处理程序


	BOOL CDialogMulti::OnInitDialog()
	{
		CDialogEx::OnInitDialog();
#ifndef _DEBUG
		GetDlgItem(IDC_CHECK_TEAM)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_MULTF)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_MULTNPC)->ShowWindow(SW_HIDE);
#endif
		lastIndex = 0;
		CString str;
		int i;

		for (i = 8; i < 120; i++) {
			str.Format("%d", i);
			m_comBoxlvl.AddString(str);
		}
		m_comBoxlvl.SetCurSel(0);
		m_listCtrl.SetExtendedStyle(m_listCtrl.GetExtendedStyle()
			| LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);	//设置扩展风格

		m_listCtrl.ModifyStyle(0, LVS_SINGLESEL);
		//设置列表头
		m_listCtrl.InsertColumn(0, _T("编号"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(1, _T("登录名"), LVCFMT_LEFT);
		m_listCtrl.InsertColumn(2, _T("级别"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(3, _T("小瓶/弓箭"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(4, _T("密码"), LVCFMT_CENTER);
		m_listCtrl.InsertColumn(5, _T("desc"), LVCFMT_CENTER);
		//	m_listCtrl.InsertColumn(2, _T("direct"), LVCFMT_CENTER);
		//设置各列的宽度
		CRect rect;
		m_listCtrl.GetClientRect(&rect);					//获取客户区宽度
		int nWidth = rect.Width();
		m_listCtrl.SetColumnWidth(0, 50);	//编号
		m_listCtrl.SetColumnWidth(1, 100);	//帐号
		m_listCtrl.SetColumnWidth(2, 48);	//级别
		m_listCtrl.SetColumnWidth(3, 88);	//小瓶
		m_listCtrl.SetColumnWidth(4, 98);	//测试条件2
		m_listCtrl.SetColumnWidth(5, 198);	//测试条件2

		int md;
		md = ::GetPrivateProfileInt("Selections", "mode", 1, ".\\Settings.ini");

#if (USER_MODE == UP_ONLY)
		md = 1;
#endif
#if (USER_MODE == NPC_ONLY)
		md = 2;
#endif
#if (USER_MODE == NPC_ONLY|UP_ONLY)
		//		md = 3;
#endif
		if ((userlvl == (NPC_ONLY | UP_ONLY)) || (userlvl & 4));
		//		fac->tskType = (taskType)md;
		else if (userlvl == 0) {
			str::log(1, "未注册，请联系作者");
			::PostQuitMessage(0);
		}
		else
			md = userlvl;
		userTask = md;
	
		char buf[MAX_PATH];
		::GetPrivateProfileString("Settings", "teamleader", "", buf,
			MAX_PATH, ".\\Settings.ini");
		SetDlgItemText(IDC_STA_LEADER, buf);

		str::log(1, "用户级别, %d", md);
		str::log(1, "上次功能, %d（1，升级，2npc， 3全功能）", userlvl);
		switch (md) {
		case 2:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_NPC); break;
		case 1:CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_BIRD); break;
		case 3: CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_MULTNPC); break;
		case 4: CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_MULTF); break;
		case 5: CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_TEAM); break;
		case 6: CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_MULTNPC); break;
		default:
			break;
		}

		tmaccount ac;
		vector<accountitem>vs;
		ac.getallacount("账号.ini", vs);
		vector<accountitem>::iterator it;
		int cnt = m_listCtrl.GetItemCount();
		for (it = vs.begin(); it != vs.end(); it++) {
			cnt = m_listCtrl.InsertItem(cnt, str::format("%d", cnt + 1).c_str());
			m_listCtrl.SetItemText(cnt, 1, (*it).name.c_str());
			m_listCtrl.SetItemText(cnt, 4, (*it).password.c_str());
			cnt++;
		}
		SetTimer(1, 3000, NULL);
		//		GetDlgItem(IDC_BTN_PAUSE)->ShowWindow(SW_SHOW);
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}
	void CDialogMulti::OnBnClickedBtnAdd()
	{
		int cnt;
		int ret;
		CString s;
		cnt = m_listCtrl.GetItemCount();
		CDialogAccount dlg;
		ret = dlg.DoModal();
		if (ret == IDOK) {//插入
			cnt = m_listCtrl.InsertItem(cnt, str::format("%d", cnt + 1).c_str());
			m_listCtrl.SetItemText(cnt, 1, dlg.m_strName);
			m_listCtrl.SetItemText(cnt, 4, dlg.m_strPasw);
		}
		saveClient();
	}

	void CDialogMulti::OnBnClickedBtnEdit()
	{
		// TODO: 在此添加控件通知处理程序代码
	}


	void CDialogMulti::OnBnClickedBtnBegin()
	{
		// TODO: 在此添加控件通知处理程序代码
		CString str;
		GetDlgItem(IDC_BTN_BEGIN)->GetWindowText(str);

		CDialogForm *form;
		form = (CDialogForm *)GetParent()->GetParent();
		if (str.Compare("start") == 0) {
			GetDlgItem(IDC_BTN_BEGIN)->SetWindowText("stop");
			GetDlgItem(IDC_BTN_PAUSE)->ShowWindow(SW_SHOW);
			form->setTskStat(1);
			//		form->SetTimer(99, 400, NULL);
		}
		else {
			GetDlgItem(IDC_BTN_BEGIN)->SetWindowText("start");
			GetDlgItem(IDC_BTN_PAUSE)->ShowWindow(SW_HIDE);
			form->setTskStat(0);
			
			m_listCtrl.DeleteAllItems();
			GetDlgItem(IDC_BTN_BEGIN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BTN_PAUSE)->ShowWindow(SW_HIDE);
		}
	}

	void CDialogMulti::OnBnClickedBtnPause()
	{
		CString str;
		GetDlgItem(IDC_BTN_PAUSE)->GetWindowText(str);

		CDialogForm *form;
		form = (CDialogForm *)GetParent()->GetParent();
		if (str.Compare("pause") == 0) {
			GetDlgItem(IDC_BTN_PAUSE)->SetWindowText("contine");
			form->setTskStat(2);
		}
		else {
			GetDlgItem(IDC_BTN_PAUSE)->SetWindowText("pause");
			//			GetDlgItem(IDC_BTN_PAUSE)->ShowWindow(SW_HIDE);
			form->setTskStat(1);
		}
	}
	int  CDialogMulti::findTaskItem(LPTSTR name) {
		int ret = -1;
		int cnt;
		CString str;
		cnt = m_listCtrl.GetItemCount();
		if (cnt == 0) {
			ret = -1;
		}
		else {
			int i, xx; xx = -1;
			for (i = 0; i < cnt; i++) {
				str = m_listCtrl.GetItemText(i, 1);
				xx = strncmp(str.LockBuffer(), name, strlen(name));
				if (str.Compare(name) == 0 || xx == 0) {
					ret = i;
					break;
				}
			}
		}
		return ret;
	}
	
	void CDialogMulti::OnBnClickedBtnTab()
	{
		CDialogForm *form;
		form = (CDialogForm *)GetParent()->GetParent();
		//		m_bsttab.start()
		form->SetTimer(1, 200, NULL);
	}
	void CDialogMulti::OnCommandRangeTask(UINT uId) {
		switch (uId) {
		case IDC_CHECK_BIRD:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_BIRD);
			break;
		case IDC_CHECK_ZHIFU:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_ZHIFU);
			break;
		case IDC_CHECK_NPC:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_NPC);
			break;
		case IDC_CHECK_MULTF:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_MULTF);
			break;
		case IDC_CHECK_MULTNPC:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_MULTNPC);
			break;
		case IDC_CHECK_TEAM:
			CheckRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF, IDC_CHECK_TEAM);
			break;
		}
		OnBnClickedBtnSet();
	}
	void CDialogMulti::OnRclickList1(NMHDR *pNMHDR, LRESULT *pResult)
	{
		LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
		//防止在空白区点击弹出菜单
		if (m_listCtrl.GetSelectedCount() <= 0)return;
		//下面的这段代码, 不单单适应于ListCtrl
		CMenu menu, *pPopup;
		menu.LoadMenu(IDR_MENU_LSMULT);
		pPopup = menu.GetSubMenu(0);
		CPoint myPoint;
		ClientToScreen(&myPoint);
		GetCursorPos(&myPoint); //鼠标位置
		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, myPoint.x, myPoint.y, this);
		*pResult = 0;
	}

	void CDialogMulti::OnMenuDelete()
	{
		if (m_listCtrl.GetSelectedCount() <= 0)return;
		POSITION pos;
		pos = m_listCtrl.GetFirstSelectedItemPosition();
		while (pos) {
			int i;
			i = m_listCtrl.GetNextSelectedItem(pos);
			m_listCtrl.DeleteItem(i);
			saveClient();
		}
	}
	void CDialogMulti::OnBnClickedBtnLeader()
	{
		CString s;
		int ret;
		UINT uSelectedCount = m_listCtrl.GetSelectedCount();
		// Update all of the selected items.
		if (uSelectedCount > 0) {
			int nItem = -1;
			nItem = m_listCtrl.GetNextItem(nItem, LVNI_SELECTED);
			int nSelected = m_listCtrl.GetSelectionMark();
			nSelected = nItem;
			int x, y, sub;
			s = m_listCtrl.GetItemText(nSelected, 1);
			
		}
	}

	void CDialogMulti::OnBnClickedBtnSet()
	{
		int md;
		md = GetCheckedRadioButton(IDC_CHECK_BIRD, IDC_CHECK_MULTF);
		switch (md) {
		case IDC_CHECK_BIRD:
			md = 1;
			break;
		case IDC_CHECK_NPC:
			md = 2;
			break;
		case IDC_CHECK_ZHIFU:
			md = 3;
		case IDC_CHECK_MULTF:
			md = 4;
			break;
		case IDC_CHECK_MULTNPC:
			md = 6;
			break;
		case IDC_CHECK_TEAM:
			md = 5;
			break;
		}
#if (USER_MODE == UP_ONLY)
		md = 1;
#endif
#if (USER_MODE == NPC_ONLY)
		md = 2;
#endif
#if (USER_MODE ==( NPC_ONLY|UP_ONLY))
		//		md = 3;
#endif
		if ((userlvl != (NPC_ONLY | UP_ONLY)) && !(userlvl & 4))	//不是全功能
			md = userlvl;
		userTask = md;
		TCHAR   ValBuf[16];
		CString s;
		sprintf(ValBuf, "%d", md);
		str::log(1, "设置的功能, %d", md);
		WritePrivateProfileString("Selections", "mode", ValBuf, ".\\Settings.ini");
		GetDlgItemText(IDC_STA_LEADER, s);

	}
	int CDialogMulti::saveClient()
	{
		int cnt = m_listCtrl.GetItemCount();
		vector<accountitem>vs;
		CString s;
		for (int i = 0; i < cnt; i++) {
			s = m_listCtrl.GetItemText(i, 1);
			accountitem itm;
			itm.name = (LPCTSTR)s;
			s = m_listCtrl.GetItemText(i, 4);
			itm.password = (LPCTSTR)s;
			vs.push_back(itm);
		}
		tmaccount ac;
		ac.saveallacount("账号.ini", vs);
		return 0;
	}

	void CDialogMulti::OnIdrMenuStart()
	{
		boost::thread([this]() {
			int index,ret;
			index = GetListCtrlSelectedIndex(this->m_listCtrl);  index++;
			tmfactory *fac;
			tmVirMachine *mac;
			fac = tmfactory::Instance();
			mac = fac->getMachine(index);
			if (mac) {
				CString name, password;
				name = this->m_listCtrl.GetItemText(index - 1, 1);
				password = this->m_listCtrl.GetItemText(index - 1, 4);
				mac->initAccount(name, password);
				//先判断是否已经已经登录了游戏，如果登录了，则跳过
				//启动模拟器
				mac->vmState = tmVirMachine::tmRunning;
				mac->callluaFunction("xstartgame");
				
				//进入关卡
//				mac->callluavmFunction("xadd", "ii>i",2,3,&ret);
				mac->callluavmFunction("getlvl", ">i",  &ret);
				mac->callluaFunction("autobegin");
				while (1) {
//					if (mac->mk->IsBind() == 0) break;
					if (mac->vmState == tmVirMachine::tmRunning) {
						Sleep(2000);
						mac->getLevel();	//取游戏的级别
//						mac->callluavmFunction("getlvl", ">i", &ret);
						Sleep(200);
						mac->getLmotion();	//取药水的数量
						Sleep(200);
						mac->getlarrow();	//取弓箭的数量
						mac->callluaFunction("xcheckdialog");	//综合处理
						mac->process_Event();	//状态机处理
					}else if (mac->vmState == tmVirMachine::tmStop){
						break;
					}else if (mac->vmState == tmVirMachine::tmRestart) {
						mac->release();
						mac->initlua();
						mac->initluaFile("ocr.lua");
						mac->vmState = tmVirMachine::tmRunning;
					}
					else {
						Sleep(1000);
					}
				}
				mac->vmState = tmVirMachine::tmStop;
				//自主处理
			}
		});
	}
	void CDialogMulti::OnIdrMenuStop()
	{
		int index;
		index = GetListCtrlSelectedIndex(this->m_listCtrl);  index++;
		tmfactory *fac;
		tmVirMachine *mac;
		fac = tmfactory::Instance();
		mac = fac->getMachine(index);
		if (mac) {
				mac->vmState = tmVirMachine::tmStop;
		}
	}
	void CDialogMulti::OnIdrMenuPause()
	{
		int index;
		index = GetListCtrlSelectedIndex(this->m_listCtrl);  index++;
		tmfactory *fac;
		tmVirMachine *mac;
		fac = tmfactory::Instance();
		mac = fac->getMachine(index);
		if (mac) {
			if (mac->vmState == tmVirMachine::tmRunning)
				mac->vmState = tmVirMachine::tmPasue;
			else if (mac->vmState == tmVirMachine::tmPasue) {
				mac->vmState = tmVirMachine::tmRunning;
			}
		}
	}

	void CDialogMulti::OnMenuRestart()
	{
		int index;
		index = GetListCtrlSelectedIndex(this->m_listCtrl);  index++;
		tmfactory *fac;
		tmVirMachine *mac;
		fac = tmfactory::Instance();
		mac = fac->getMachine(index);
		if (mac) {
			mac->vmState = tmVirMachine::tmRestart;
		}
	}

	void CDialogMulti::OnTimer(UINT_PTR nIDEvent)
	{
		// TODO: 在此添加消息处理程序代码和/或调用默认值
		if (m_listCtrl.GetItemCount() > 0) {
			tmfactory *fac;
			fac = tmfactory::Instance();
			tmVirMachine *mac;
			mac = fac->getMachine(1);
			if (mac) {
				switch (mac->vmState) {
				case tmVirMachine::tmPasue:
					m_listCtrl.SetItemText(0, 0, str::format("%d-暂停", mac->index).c_str());
					break;
				case tmVirMachine::tmRunning:
					m_listCtrl.SetItemText(0, 0, str::format("%d-运行", mac->index).c_str());
					break;
				}
				m_listCtrl.SetItemText(0, 2, str::format("%d", mac->level).c_str());
				m_listCtrl.SetItemText(0, 3, str::format("%d/%d", mac->lmotion, mac->larrow).c_str());
			}
		}
		CDialogEx::OnTimer(nIDEvent);
	}
}

