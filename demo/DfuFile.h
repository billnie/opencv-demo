// DfuFile.h: interface for the CDfuFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DFUFILE_H__1D9BF540_7874_48B8_AECC_0B2B0E0ED764__INCLUDED_)
#define AFX_DFUFILE_H__1D9BF540_7874_48B8_AECC_0B2B0E0ED764__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>

using std::vector;
using namespace std;
int read_file(vector<string> &data, string szFile);

class CDfuFile  
{
	
public:
	CString GetAgreeFromFile();
	CString GetCSRHexFile();
	wstring zipFile;
	int Extract( LPTSTR szName, LPTSTR szPath );
	int ExportHEXFile(int id, CString fileName);
	long GetFileSize(LPCTSTR str);
	long GetResourceSize(HRSRC hInst);
	CString GetExtraPath();
	int IsFolderExist(LPCTSTR path);
	int IsFileExist(LPCTSTR path);
	int OpenPath(LPCTSTR path);
	BOOL RegistryDll(CString& szDllPath);
	////写ini值
	//int WriteProfileString(LPCTSTR file, LPCTSTR section, LPCTSTR key, LPCTSTR defval);
	////读ini值
	//int ReadProfileString(LPCTSTR file, LPCTSTR section, LPCTSTR key, LPTSTR val);
	//启动一个应用
	int RunApp(LPCTSTR path);
	//查找进程
	BOOL   EnumProcessbyName(DWORD   dwPID, LPCTSTR   ExeName, LONG type = 0);
	CString GetAppPath();

	CDfuFile();
	virtual ~CDfuFile();
	wstring ANSIToUnicode( const string& str )
	{
		int  len = 0;
		len = str.length();
		int  unicodeLen = ::MultiByteToWideChar( CP_ACP,
            0,
            str.c_str(),
            -1,
            NULL,
            0 );  
		wchar_t *  pUnicode;  
		pUnicode = new  wchar_t[unicodeLen+1];  
		memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t));  
		::MultiByteToWideChar( CP_ACP,
			0,
			str.c_str(),
			-1,
			(LPWSTR)pUnicode,
			unicodeLen );  
		wstring  rt;  
		rt = ( wchar_t* )pUnicode;
		delete  pUnicode; 
		
		return  rt;  
	}
	string UnicodeToANSI( const wstring& str )
	{
		char*     pElementText;
		int    iTextLen;
		// wide char to multi char
		iTextLen = WideCharToMultiByte( CP_ACP,
			0,
			str.c_str(),
			-1,
			NULL,
			0,
			NULL,
			NULL );
		pElementText = new char[iTextLen + 1];
		memset( ( void* )pElementText, 0, sizeof( char ) * ( iTextLen + 1 ) );
		::WideCharToMultiByte( CP_ACP,
			0,
			str.c_str(),
			-1,
			pElementText,
			iTextLen,
			NULL,
			NULL );
		string strText;
		strText = pElementText;
		delete[] pElementText;
		return strText;
	}
};

#endif // !defined(AFX_DFUFILE_H__1D9BF540_7874_48B8_AECC_0B2B0E0ED764__INCLUDED_)
