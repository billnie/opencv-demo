// DialogAccount.cpp : 实现文件
//

#include "stdafx.h"
#include "DialogAccount.h"
#include "afxdialogex.h"
#include	"resource.h"

// CDialogAccount 对话框

IMPLEMENT_DYNAMIC(CDialogAccount, CDialogEx)

CDialogAccount::CDialogAccount(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_ADD, pParent)
	, m_strName(_T(""))
	, m_strPasw(_T(""))
{

}

CDialogAccount::~CDialogAccount()
{
}

void CDialogAccount::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_PASW, m_strPasw);
}


BEGIN_MESSAGE_MAP(CDialogAccount, CDialogEx)
	ON_BN_CLICKED(IDOK, &CDialogAccount::OnBnClickedOk)
END_MESSAGE_MAP()


// CDialogAccount 消息处理程序


void CDialogAccount::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}
