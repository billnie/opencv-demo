#pragma once
#include "afxcmn.h"
#include "afxwin.h"

// CDialogMulti 对话框
namespace aui {
	class CDialogMulti : public CDialogEx
	{
		DECLARE_DYNAMIC(CDialogMulti)

	public:
		CDialogMulti(CWnd* pParent = NULL);   // 标准构造函数
		virtual ~CDialogMulti();

		// 对话框数据
#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_DLG_MULTI };
#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

		
	public:
		CListCtrl m_listCtrl;
		int lastIndex;

		CComboBox m_comBoxtask;
		CComboBox m_comBoxlvl;
		//查找用户名
		int findTaskItem(LPTSTR str);
		int saveClient();
		virtual BOOL OnInitDialog();

		DECLARE_MESSAGE_MAP()
		afx_msg void OnBnClickedBtnAdd();
		afx_msg void OnBnClickedBtnEdit();

		afx_msg void OnBnClickedBtnBegin();
		afx_msg void OnRclickList1(NMHDR *pNMHDR, LRESULT *pResult);
		afx_msg void OnBnClickedBtnPause();

		afx_msg void OnBnClickedBtnTab();
		afx_msg void OnBnClickedBtnLeader();
		afx_msg void OnBnClickedBtnSet();
		afx_msg void OnCommandRangeTask(UINT uId);

		afx_msg void OnMenuDelete();
		afx_msg void OnIdrMenuStart();
		afx_msg void OnIdrMenuPause();
		afx_msg void OnIdrMenuStop();
		afx_msg void OnTimer(UINT_PTR nIDEvent);
		afx_msg void OnMenuRestart();
	};
}