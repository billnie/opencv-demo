/*
  This program detects image features using SIFT keypoints. For more info,
  refer to:
  
  Lowe, D. Distinctive image features from scale-invariant keypoints.
  International Journal of Computer Vision, 60, 2 (2004), pp.91--110.
  
  Copyright (C) 2006-2012  Rob Hess <rob@iqengines.com>

  Note: The SIFT algorithm is patented in the United States and cannot be
  used in commercial products without a license from the University of
  British Columbia.  For more information, refer to the file LICENSE.ubc
  that accompanied this distribution.

  Version: 1.1.2-20100521
*/

#include "sift.h"
#include "imgfeatures.h"
#include "utils.h"

#include <highgui.h>

//using namespace cv;
//using namespace std;
//#include <unistd.h>//linux中的操作

//#define OPTIONS ":o:m:i:s:c:r:n:b:dxh"

/*************************** Function Prototypes *****************************/




/******************************** Globals ************************************/

char* pname;
char* img_file_name;
//char* out_file_name = "D:\\MyDataSet\\vehicle_samples\\CarWindowDatafeat\\feat (2).txt";
//char* out_img_name = "D:\\MyDataSet\\vehicle_samples\\CarWindowDatafeatImg\\0.jpg";
char out_file_name[100];
char out_img_name[100];
int intvls = SIFT_INTVLS;//每个尺度空间的采样间隔数 3
double sigma = SIFT_SIGMA;//高斯平滑的数目 1.6
double contr_thr = SIFT_CONTR_THR;//阈值点 小于则被提出值越大被提出的特征点越多
int curv_thr = SIFT_CURV_THR;//
int img_dbl = SIFT_IMG_DBL;//金字塔构造 1表示在建立尺度空间之前将原图放大一倍
int descr_width = SIFT_DESCR_WIDTH;//4
int descr_hist_bins = SIFT_DESCR_HIST_BINS;//8 4*4*8=128维
int display = 1;

/* the maximum number of keypoint NN candidates to check during BBF search */
#define KDTREE_BBF_MAX_NN_CHKS 200

/* threshold on squared ratio of distances between NN and 2nd NN 最近点与次最近点距离之比*/
#define NN_SQ_DIST_RATIO_THR 0.49
/********************************** Main *************************************/
int mainx(int argc, char** argv)
{
	IplImage* img1, *img2, *stacked;
	struct feature* feat1, *feat2, *feat;
	struct feature** nbrs;
	struct kd_node* kd_root;
	CvPoint pt1, pt2;
	double d0, d1;
	int n1, n2, k, i, m = 0;

	/*
	if( argc != 3 )
	fatal_error( "usage: %s <img1> <img2>", argv[0] );
	*/

	//argv[1] = "beaver.png";
	//argv[2] = "beaver_xform.png";

	argv[1] = "img1.jpg";
	argv[2] = "img5.jpg";

	img1 = cvLoadImage(argv[1], 1);
	if (!img1)
		fatal_error("unable to load image from %s", argv[1]);
	img2 = cvLoadImage(argv[2], 1);
	if (!img2)
		fatal_error("unable to load image from %s", argv[2]);
	stacked = stack_imgs(img1, img2);

	fprintf(stderr, "Finding features in %s...\n", argv[1]);
	n1 = sift_features(img1, &feat1);	//计算图像特征点
	if (display)	//显示？
	{
		draw_features(img1, feat1, n1);
//		display_big_img(img1, argv[1]);
		cvWaitKey(0);
	}
	fprintf(stderr, "Finding features in %s...\n", argv[2]);
	n2 = sift_features(img2, &feat2);
	if (display)
	{
		draw_features(img2, feat2, n2);
	//	display_big_img(img2, argv[2]);
		cvWaitKey(0);
	}
	fprintf(stderr, "Building kd tree...\n");
	kd_root = kdtree_build(feat2, n2);
	for (i = 0; i < n1; i++)	//逐点匹配
	{
		feat = feat1 + i;
		k = kdtree_bbf_knn(kd_root, feat, 2, &nbrs, KDTREE_BBF_MAX_NN_CHKS);	//找2个最近点
		if (k == 2)
		{
			d0 = descr_dist_sq(feat, nbrs[0]);
			d1 = descr_dist_sq(feat, nbrs[1]);
			if (d0 < d1 * NN_SQ_DIST_RATIO_THR)	//最近点与次最近点距离之比要小才当做正确匹配，然后画一条线
			{
				pt1 = cvPoint(cvRound(feat->x), cvRound(feat->y));
				pt2 = cvPoint(cvRound(nbrs[0]->x), cvRound(nbrs[0]->y));
				pt2.y += img1->height;
				cvLine(stacked, pt1, pt2, CV_RGB(255, 0, 255), 1, 8, 0);
				m++;
				feat1[i].fwd_match = nbrs[0];
			}
		}
		free(nbrs);
	}

	fprintf(stderr, "Found %d total matches\n", m);
	//display_big_img(stacked, "Matches");
	cvWaitKey(0);

	///*
	//UNCOMMENT BELOW TO SEE HOW RANSAC FUNCTION WORKS

	//Note that this line above:

	//feat1[i].fwd_match = nbrs[0];

	//is important for the RANSAC function to work.
	//*/
	////计算变换参数
	//{
	//	CvMat* H;
	//	IplImage* xformed;
	//	H = ransac_xform(feat1, n1, FEATURE_FWD_MATCH, lsq_homog, 4, 0.01,
	//		homog_xfer_err, 3.0, NULL, NULL);
	//	if (H)
	//	{
	//		xformed = cvCreateImage(cvGetSize(img2), IPL_DEPTH_8U, 3);
	//		cvWarpPerspective(img1, xformed, H,
	//			CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS,
	//			cvScalarAll(0));
	//		cvNamedWindow("Xformed", 1);
	//		cvShowImage("Xformed", xformed);
	//		cvWaitKey(0);
	//		cvReleaseImage(&xformed);
	//		cvReleaseMat(&H);
	//	}
	//}


	cvReleaseImage(&stacked);
	cvReleaseImage(&img1);
	cvReleaseImage(&img2);
	kdtree_release(kd_root);
	free(feat1);
	free(feat2);
	return 0;
}

int main()
{
  
  struct feature* features;
  int n = 0;

  char cc[3][255];
  IplImage* img;
  mainx(0,cc);
//  img=cvLoadImage( "0.jpg", 1 );
  img = cvLoadImage("D:\\testgame\\元素\\资源\\弓0.bmp", 1);

	  if( ! img )
		  //fatal_error( "unable to load image from %s", img_file_name );
		  printf("NULL pointer error!");
	  fprintf( stderr, "Finding SIFT features...\n" );
	  n = _sift_features( img, &features, intvls, sigma, contr_thr, curv_thr,
		  img_dbl, descr_width, descr_hist_bins );

	  fprintf( stderr, "Found %d features.\n", n );
	 


	  if( display )
	  {
		  draw_features( img, features, n );
		  // display_big_img( img, img_file_name );
		  cvNamedWindow("img",1);
		  cvShowImage("img",img);
		  cvWaitKey( 0 );
	  }
	 
	  	 
	  export_features( "feat.txt", features, n );
	   

		 
  
  return 0;
}

