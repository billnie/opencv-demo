#pragma once


// CDialogAccount 对话框

class CDialogAccount : public CDialogEx
{
	DECLARE_DYNAMIC(CDialogAccount)

public:
	CDialogAccount(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDialogAccount();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_ADD };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CString m_strName;
	CString m_strPasw;
};
