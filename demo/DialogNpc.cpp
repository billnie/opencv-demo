// DialogNpc.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogNpc.h"
#include "afxdialogex.h"
#include "DofusSQLite.h"

// CDialogNpc 对话框
namespace aui {
	IMPLEMENT_DYNAMIC(CDialogNpc, CDialogEx)

		CDialogNpc::CDialogNpc(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_NPC, pParent)
	{

	}

	CDialogNpc::~CDialogNpc()
	{
	}

	void CDialogNpc::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_LIST1, m_listNpc);
	}


	BEGIN_MESSAGE_MAP(CDialogNpc, CDialogEx)
		ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CDialogNpc::OnLvnItemchangedList1)
	END_MESSAGE_MAP()

	// CDialogNpc 消息处理程序

	BOOL CDialogNpc::OnInitDialog()
	{
		CDialogEx::OnInitDialog();
		m_listNpc.SetExtendedStyle(m_listNpc.GetExtendedStyle()
			| LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);	//设置扩展风格

		m_listNpc.ModifyStyle(0, LVS_SINGLESEL);
		//设置列表头
		int i = 2;
		m_listNpc.InsertColumn(0, _T("x,y,sub"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(1, _T("state"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u1"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u2"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u3"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u4"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u5"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u6"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u7"), LVCFMT_CENTER);
		m_listNpc.InsertColumn(i++, _T("u8"), LVCFMT_CENTER);
		//设置各列的宽度
		CRect rect;
		int nWidth;
		m_listNpc.GetClientRect(&rect);					//获取客户区宽度
		nWidth = rect.Width();
		m_listNpc.SetColumnWidth(0, 60);	//测试条件1 
		m_listNpc.SetColumnWidth(1, 58);	//测试条件1 
		nWidth = 40;
		i = 2;
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		m_listNpc.SetColumnWidth(i++, nWidth);	//测试条件1 
		// TODO:  在此添加额外的初始化
		addNpcList();
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}


	int CDialogNpc::addNpcList()
	{
		m_listNpc.DeleteAllItems();

		return 0;
	}


	void CDialogNpc::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
	{
		LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
		// TODO: 在此添加控件通知处理程序代码
		*pResult = 0;
	}
}