#include "stdafx.h"
#include "DofusFightBase.h"


CDofusFightBase::CDofusFightBase()
{
	ox = 5;
	oy = 21;
	bh = 28;
	bw = 56;
	init();
}


CDofusFightBase::~CDofusFightBase()
{
}


int CDofusFightBase::init()
{
	memset(xxxP, XV*YV, 0);

	objGrid gr(193,49,35);	//红色
	objGrid gb(0, 0, 0);	//黑色
	objGrid gd(91, 83, 61);//砖的颜色
	objGrid gbb(29, 130,206);	//蓝色
	objGrid gbc(190, 82, 59);
	objGrid gbd(89, 125, 99);
	objGrid gbe(73, 67, 178);
	objGrid gbf(103, 163, 70);
	vecgrid.push_back(gr);
	vecgrid.push_back(gb);
	vecgrid.push_back(gd);
	vecgrid.push_back(gbb);
	vecgrid.push_back(gbc);
	vecgrid.push_back(gbd);
	vecgrid.push_back(gbe);
	vecgrid.push_back(gbf);
	return 0;
}


int CDofusFightBase::cusorPos(int x, int y)
{
	if (x >= ox && y >= oy) {
		row = (x - ox) / bw;
		col = (y - oy) / bh;
		return 1;
	}
	return 0;
}

//查询格子中的对象
int CDofusFightBase::findObjGrid(int type)
{
	int ret;
	int cnt = 0;
	ret = gettypeObjRGB(type);
	if (ret > -1 && m_bmp ) {
		if (!m_bmp->image)
			return -1;
		int i, j, k;
		unsigned char r, g, b;
		//先找中心点，是否是存在
		for (i = ox; i < w; i += bw) {
			for (j = oy; j < h; j += bh) {
				//取中心点的坐标
				m_bmp->image.get_pixel(i, j,r,g,b );
				if (grobj.isequal(r, g, b))
					cnt++;
			}
		}
		//再找一遍
		for (i = ox+bw/2; i < w; i += bw) {
			for (j = oy-bh/2; j < h; j += bh) {
				//取中心点的坐标
				m_bmp->image.get_pixel(i, j, r, g, b);
				if (grobj.isequal(r, g, b))
					cnt++;
			}
		}
	}
	return cnt;
}


int CDofusFightBase::gettypeObjRGB(int type)
{
//	objGrid gr;
	int ret = type;
	switch (type) {
	case 0:
		grobj.init(193, 49, 35);
		break;
	case 1:
		break;
	case 2:
		break;
	default:
		ret = -1;
	}
	return ret;
}


int CDofusFightBase::findgridObj(objGrid grd)
{
	int ret = -1;
	int i = 0;
	vector<objGrid> ::iterator it;
	it = vecgrid.begin();
	for (; it != vecgrid.end(); ++it) {
		if (grd.isequal2(*it)) {
			ret = i;
			break;
		}
		i++;
	}
	return ret;
}


int CDofusFightBase::findObjGrid2(int _r, int _g, int _b)
{
	int ret;
	int cnt = 0;
	grobj.init(_r, _g, _b);
	if ( m_bmp) {
		if (!m_bmp->image)
			return -1;
		int i, j, k;
		unsigned char r, g, b;
		//先找中心点，是否是存在
		for (i = ox; i < w; i += bw) {
			for (j = oy; j < h; j += bh) {
				//取中心点的坐标
				m_bmp->image.get_pixel(i, j, r, g, b);
				if (grobj.isequal(r, g, b))
					cnt++;
			}
		}
		//再找一遍
		for (i = ox + bw / 2; i < w; i += bw) {
			for (j = oy - bh / 2; j < h; j += bh) {
				//取中心点的坐标
				m_bmp->image.get_pixel(i, j, r, g, b);
				if (grobj.isequal(r, g, b))
					cnt++;
			}
		}
	}
	return cnt;
}
