// DialogLog.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogLog.h"
#include "afxdialogex.h"
#include	"MouseKey.h"

// CDialogLog 对话框
namespace aui {
	IMPLEMENT_DYNAMIC(CDialogLog, CDialogEx)

		CDialogLog::CDialogLog(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_LOGIN, pParent)
	{
		bAutoScroll = TRUE;
	}

	CDialogLog::~CDialogLog()
	{
	}

	void CDialogLog::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_RICHEDIT21, m_richEdit);
		DDX_Control(pDX, IDC_RICHEDIT22, m_richEditLog);
	}


	BEGIN_MESSAGE_MAP(CDialogLog, CDialogEx)
		ON_WM_SIZE()
		ON_WM_DESTROY()
		ON_BN_CLICKED(IDC_BTN_SCROLL, &CDialogLog::OnBnClickedBtnScroll)
		ON_BN_CLICKED(IDC_BTN_CLEAR, &CDialogLog::OnBnClickedBtnClear)
		ON_BN_CLICKED(IDC_BTN_CLEAR2, &CDialogLog::OnBnClickedBtnClear2)
	END_MESSAGE_MAP()

	// CDialogLog 消息处理程序
	int CDialogLog::addrichLog(LPTSTR title, LPTSTR message)
	{
		if (bAutoScroll) {
			m_richEdit.ReplaceSel(_T("\r\n"));
			SYSTEMTIME sTime;
			GetLocalTime(&sTime);
			TCHAR chBuf[100];
			//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
			//	sTime.wMilliseconds,sTime.wDayOfWeek);
			wsprintf(chBuf, _T("%02u:%02u:%02u:%03u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
				sTime.wMilliseconds);
			m_richEdit.ReplaceSel((LPCTSTR)chBuf);
			if (title && strlen(title) > 0)
			{
				m_richEdit.ReplaceSel((LPCTSTR)title);
				m_richEdit.ReplaceSel((LPCTSTR)_T(" :"));
			}
			if (message && strlen(message) > 0)
				m_richEdit.ReplaceSel((LPCTSTR)message);

			m_richEdit.PostMessage(WM_VSCROLL, SB_BOTTOM, 0);
		}
		return 0;
	}
	int CDialogLog::addrichLog2(LPTSTR title, LPTSTR message)
	{
#ifdef _DEBUG
		m_richEditLog.ReplaceSel(_T("\r\n"));
		SYSTEMTIME sTime;
		GetLocalTime(&sTime);
		TCHAR chBuf[100];
		//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
		//	sTime.wMilliseconds,sTime.wDayOfWeek);
		wsprintf(chBuf, _T("%02u:%02u:%02u:%03u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
			sTime.wMilliseconds);
		//wsprintf(chBuf, _T("%02u/%02u %02u:%02u:%02u:%03u -->"), sTime.wMonth, sTime.wDay, sTime.wHour, sTime.wMinute, sTime.wSecond,
		//	sTime.wMilliseconds);
		m_richEditLog.ReplaceSel((LPCTSTR)chBuf);
		if (title && strlen(title) > 0)
		{
			m_richEditLog.ReplaceSel((LPCTSTR)title);
			m_richEditLog.ReplaceSel((LPCTSTR)_T(" : "));
		}
		if (message && strlen(message) > 0)
			m_richEditLog.ReplaceSel((LPCTSTR)message);
		m_richEditLog.PostMessage(WM_VSCROLL, SB_BOTTOM, 0);
#endif
		return 0;
	}


	BOOL CDialogLog::OnInitDialog()
	{
		akb::CMouseKey::loghWnd = this->GetSafeHwnd();
		CDialogEx::OnInitDialog();

		addrichLog(NULL, "init app");
		// TODO:  在此添加额外的初始化
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}


	void CDialogLog::OnBnClickedBtnClear()
	{
		m_richEdit.SetFocus();
		m_richEdit.SetSel(0, -1);
		m_richEdit.ReplaceSel("");
		m_richEdit.Clear();
	}


	void CDialogLog::OnBnClickedBtnClear2()
	{
		m_richEditLog.SetFocus();
		m_richEditLog.SetSel(0, -1);
		m_richEditLog.ReplaceSel("");
		m_richEditLog.Clear();
	}
	void CDialogLog::OnSize(UINT nType, int cx, int cy)
	{
		CDialogEx::OnSize(nType, cx, cy);
		CRect r, rc, rd, r1;
		GetClientRect(rc);
	
		if (m_richEdit.GetSafeHwnd()) {
			m_richEdit.MoveWindow(rc.left, rc.top+60, rc.Width(), rc.Height()-64);
		}
		if (m_richEditLog.GetSafeHwnd()) {
			m_richEditLog.MoveWindow(rc.left, rc.top+ rc.Height()-34, rc.Width(), rc.Height() / 2-34);
		}
			// TODO: 在此处添加消息处理程序代码
	}
	void CDialogLog::OnDestroy()
	{
		akb::CMouseKey::loghWnd = NULL;
		CDialogEx::OnDestroy();

		// TODO: 在此处添加消息处理程序代码
	}
	LRESULT CDialogLog::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
	{
		// TODO: 在此添加专用代码和/或调用基类
		if (message == WM_LOG_LUA) {
			char *s;
			s = (char *)lParam;
			addrichLog("lua", (LPTSTR)s);
			free(s);
			return 0L;
		}
		return CDialogEx::WindowProc(message, wParam, lParam);
	}

	void CDialogLog::OnBnClickedBtnScroll()
	{
		bAutoScroll = !bAutoScroll;
		if (bAutoScroll)
			SetDlgItemText(IDC_BTN_SCROLL, "scroll");
		else
			SetDlgItemText(IDC_BTN_SCROLL, "stop");
	}
}




