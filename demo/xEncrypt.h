#ifndef DES_ENC_H
#define DES_ENC_H
#include	"windows.h"
#ifdef __cplusplus
extern "C" {
#endif
	BOOL xEncrypt(LPCTSTR lpszSource, LPCTSTR lpszDestination, LPCTSTR lpszPassword);
	BOOL xDecrypt(LPCTSTR lpszSource, LPCTSTR lpszDestination, LPCTSTR lpszPassword);
#ifdef __cplusplus
}
#endif
#endif