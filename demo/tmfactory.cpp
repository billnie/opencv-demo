
#include "DMMouseKey.h"

#include	"lua_dm.h"
#include "stdafx.h"
#include "tmfactory.h"

#include	"lfs.h"
void tmVirMachine::initMk(int inx) {
	if(mk == NULL)
		mk = new akb::CDMMouseKey();
	if (bstState == NULL) {
		bstState = new tmbststate();
	}
	mk->index = inx;
}
void tmVirMachine::initlua() {
	L = luaL_newstate();
	if (L) {
		/* 载入Lua基本库 */
		luaL_openlibs(L);
		lua_pushnumber(L, index);
		lua_setglobal(L, "numofindex");
		lua_register(L, "print", lua_print);
		lua_regFunction(L);
		luaopen_lfs(L);
	}
}
void tmVirMachine::release()
{
	if (L) { lua_close(L); L = NULL; }
}
int  tmVirMachine::initluaFile(TCHAR *filename) {
	int ret = -1;
	if (filename && L) {
		tmfactory *fac;
		TCHAR buf[255] = { 0 };
		fac = tmfactory::Instance();
		fac->getLuaPath(buf);
		_tcscat(buf, filename);
		ret = luaL_dofile(L, buf);
	}
	return ret;
}
int	tmVirMachine::initAccount(LPCTSTR name, LPCTSTR password) {
	int ret = -1;
	if (name && password && L) {
		lua_pushstring(L, name);
		lua_setglobal(L, "username");
		lua_pushstring(L, password);
		lua_setglobal(L, "password");
	}
	return ret;
}
int  tmVirMachine::callluaFunction(const TCHAR *function) {
	int ret = -1;
	if (function && L) {
		ret = lua_getglobal(L, function);
		if (ret > 0) {
			//参数从左到右压栈2
			ret = lua_pcall(L, 0, 1, 0);
		}
	}
	return ret;
}
int  tmVirMachine::callluavmFunction(const TCHAR *function, const char *fmt, ...) {
	int ret = -1;
	if (function && L) {
		va_list vl;//定义一个可变长参数指针，无初始值
		int narg;//可变长参数的数量
		int nres;//Lua函数返回值的数量
		va_start(vl, fmt);//让指针指向可变长参数的第一个参数首地址
		ret = lua_getglobal(L, function);//函数入栈
		//---参数入栈
		for (narg = 0; *fmt; narg++)//遍历参数
		{
			//检查栈中空间
			luaL_checkstack(L, 1, "too many arguments");
			switch (*fmt++)
			{
				//根据参数的约束符，按类型入栈,d-double i-int s-char*
			case 'd':
				lua_pushnumber(L, va_arg(vl, double));
				break;
			case 'i':
				lua_pushinteger(L, va_arg(vl, int));
				break;
			case 's':
				lua_pushstring(L, va_arg(vl, char*));
				break;
			case '>':
				//输入参数结束
				goto endargs;
			default:
				errors(L, "invalid option (%c)", *(fmt - 1));
			}
		}
		//---参数入栈
	endargs:
		nres = strlen(fmt);//期望的Lua函数返回值数量
		//调用Lua函数
		if (lua_pcall(L, narg, nres, 0) != 0)
		{
			errors(L, "error calling '%s':%s", function, lua_tostring(L, -1));
			goto End;
		}

		//调用之后的返回值检索
		//栈索引从栈顶往下依次是-1,-2,-3，所以第一个结果的栈索引是-nres
		nres = -nres;
		while (*fmt)
		{
			//依次得到返回值的类型描述符 *sig
			switch (*fmt++)
			{
			case 'd':
				if (!lua_isnumber(L, nres))
					errors(L, "wrong result type");
				*va_arg(vl, double*) = lua_tonumber(L, nres);
				break;
			case 'i':
				if (!lua_isnumber(L, nres))
					errors(L, "wrong result type");
				*va_arg(vl, int*) = lua_tointeger(L, nres);
				break;
			case 's':
				if (!lua_isstring(L, nres))
					errors(L, "wrong result type");
				*va_arg(vl, char**) = (char*)lua_tostring(L, nres);
				break;
			default:
				errors(L, "invalid option (%c)", *(fmt - 1));
			}
			//结果的参数索引＋1
			nres++;
		}
		ret = 1;
	End:
		va_end(vl);
	}
	return ret;
}
int tmVirMachine::process_Event() {
	int ret = -1;
	if (bstState) {
		bstState->proceTab_event(this);
	}
	return ret;
}
int  tmVirMachine::getLevel() {
	int ret = -1;
	callluaFunction("getlvl");
	lua_getglobal(L, "tmlevel");
	level = lua_tointeger(L, -1);
	return ret;
}
int  tmVirMachine::getLmotion() {
	int ret = -1;
	callluaFunction("getlotion");
	lua_getglobal(L, "tmlomtion");
	lmotion = lua_tointeger(L, -1);
	return ret;
}
int	 tmVirMachine::getlarrow() {
	int ret = -1;
	callluaFunction("getarrowcount");
	lua_getglobal(L, "tmarrowcount");
	larrow = lua_tointeger(L, -1);
	return ret;
}
tmfactory*tmfactory::g_fac = NULL;
tmfactory::tmfactory()
{

}
tmfactory::~tmfactory()
{
	
}

int tmfactory::getLuaPath(TCHAR *sz) {
	int ret = -1;
	TCHAR *cc;
	AppPath(sz);
#ifdef _DEBUG
	cc = _tcsrchr(sz, '\\');
	if (cc) {
		*cc = 0;
		_tcscat(sz, _T("\\demo"));
	}
	_tcscat(sz, "\\tm\\lua\\");
#else
	_tcscat(sz, "\\tm\\lua\\");
#endif
	ret = _tcslen(sz);
	return ret;
}
tmVirMachine * tmfactory::getMachine(int index) {
	tmVirMachine *mk = NULL;
	mk = mpMachine[index];
	if (mk == NULL) {
		mk = new tmVirMachine(index);
		mk->initluaFile("ocr.lua");
		mpMachine[index] = mk;
	}
	return mk;
}
int tmfactory::removeMachine(int index) {
	int ret = -1;
	tmVirMachine *mk = mpMachine[index];
	if (mk) {
		mpMachine.erase(index);
		delete mk;
	}
	return ret;
}