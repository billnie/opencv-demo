
#include "tmbststate.h"
#include "tmfactory.h"
using namespace tmx;

namespace tmx {

	template <class Event, class Fsm, class SourceState, class TargetState>
	bool Sm1_::Guard_Loading::operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const
	{
		//当前是否为启动中
		int ret;
		//e.mac->callluavmFunction("getlvl", ">i", &ret);
		return false;
	}
	
	template <class Event, class Fsm, class SourceState, class TargetState>
	bool Sm1_::Guard_Fighting::operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const
	{
		//当前是否为战斗
		int ret;
		//e.mac->callluavmFunction("getlvl", ">i", &ret);
		//e.mac->callluavmFunction("gaudfigting", ">i",&ret);
		//if (ret == 1) return true;
		return false;
	}
	template <class Event, class Fsm, class SourceState, class TargetState>
	bool Sm1_::Guard_Talking::operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const
	{
		//当前是否为对话，当前如果没有级别，那么不为对话
		int ret;
		//e.mac->callluavmFunction("getlvl", ">i", &ret);
		//if (ret < 1) return false;	//读不到级别
		//e.mac->callluavmFunction("gaudtalking", ">i", &ret);
		//if (ret == 1) return true;
		return false;
	}
	template <class Event, class Fsm, class SourceState, class TargetState>
	void Sm1_::ActionFighting::operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const
	{
		std::cout << "ActionFighting" << std::endl;
		f.evtstate = 1;
	}
	template <class Event, class Fsm, class SourceState, class TargetState>
	void Sm1_::ActionTalking::operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const
	{
		std::cout << "ActionTalking" << std::endl;
		f.evtstate = 1;
	}
	template <class Event, class Fsm, class SourceState, class TargetState>
	void Sm1_::ActionLoading::operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const
	{
		std::cout << "ActionLoading" << std::endl;
		f.evtstate = 1;
	}
}

tmbststate::tmbststate()
{
	sml.evtstate = 99;
}


tmbststate::~tmbststate()
{
}


int tmbststate::proceTab_event(tmVirMachine *mac) {
	int ret = -1;
	sml.process_event(tmx::Event3(mac));
	sml.current_state();
	return ret;
}

int tmbststate::startTabEvent()
{
	sml.start();
	return 0;
}
int tmbststate::stopTabEvent()
{
	sml.stop();
	sml.evtstate = 99;
	return 0;
}

