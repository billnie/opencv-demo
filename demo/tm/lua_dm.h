

#ifndef LUA_DM
#define LUA_DM
#include <lua.hpp>


//#include	"windows.h"
#ifdef __cplusplus
extern "C"
{
#endif

	int AppPath(LPTSTR path);
	int errors(lua_State *L, const char * fmt, ...);
	int call_va(lua_State *L, const char * func, const char *sig, ...);
	int lua_print(lua_State * luastate);
	int os_AppPath(lua_State* L);
	int os_sleep(lua_State* L);
	int dm_runqemu(lua_State* L);
	int dm_xbind(lua_State* L);
	int dm_moveto(lua_State* L);
	int dm_leftclick(lua_State* L);
	int dm_LeftDown(lua_State* L);
	int dm_LeftDoubleClick(lua_State* L);
	int dm_LeftUp(lua_State* L);
	int dm_KeyPressChar(lua_State* L);
	int dm_KeyPressStr(lua_State* L);
	int dm_ocr(lua_State* L);
	int dm_FindPic(lua_State* L);
	int dm_FindColorEx(lua_State* L);
	int dm_BindCount(lua_State* L);
	int dm_Path(lua_State* L);
	int dm_Vers(lua_State* L);
	int dm_OcrInFile(lua_State* L);
	
	int lua_regFunction(lua_State* L);

#ifdef __cplusplus
}
#endif
#endif

