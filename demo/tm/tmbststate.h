#pragma once
#include	"windows.h"
#include "tmfactory.h"
#include <iostream>
#include <boost/msm/back/state_machine.hpp>

#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/static_assert.hpp>

#include <io.h>
#include <fcntl.h> 

class tmVirMachine;
namespace tmx {
	namespace msm = boost::msm;
	namespace msmf = boost::msm::front;
	namespace mpl = boost::mpl;
	// Events
	struct Event3 {
		Event3(tmVirMachine *mac_) {
			mac = mac_;
		}
		tmVirMachine *mac;
	};
	// ----- State machine
	struct Sm1_ :msmf::state_machine_def<Sm1_>
	{
		struct StateUnknow_ :msmf::state_machine_def<StateUnknow_> {
			template <class Event, class Fsm>
			void on_entry(Event const&e, Fsm&f) const {
				std::cout << "StateFight_::on_entry()" << std::endl;
			}
			template <class Event, class Fsm>
			void on_exit(Event const&e, Fsm&f) const {
				std::cout << "StateFight_::on_exit()" << std::endl;
			}
		};
		struct StateLoding_ :msmf::state_machine_def<StateLoding_> {
				template <class Event, class Fsm>
				void on_entry(Event const&e, Fsm&f) const {
					std::cout << "StateFight_::on_entry()" << std::endl;
				}
				template <class Event, class Fsm>
				void on_exit(Event const&e, Fsm&f) const {
					std::cout << "StateFight_::on_exit()" << std::endl;
				}
			};
		struct StateFight_ :msmf::state_machine_def<StateFight_> {
			template <class Event, class Fsm>
			void on_entry(Event const&e, Fsm&f) const {
				std::cout << "StateFight_::on_entry()" << std::endl;
			}
			template <class Event, class Fsm>
			void on_exit(Event const&e, Fsm&f) const {
				std::cout << "StateFight_::on_exit()" << std::endl;
			}
		};
		struct StateTalk_ :msmf::state_machine_def<StateTalk_> {
			template <class Event, class Fsm>
			void on_entry(Event const&e, Fsm&f) const {
				std::cout << "State2_::on_entry()" << std::endl;
			}
			template <class Event, class Fsm>
			void on_exit(Event const&e, Fsm&f) const {
				std::cout << "State2_::on_exit()" << std::endl;
			}
		};
		// Set initial state
		typedef StateUnknow_ initial_state;

		struct Guard_Loading {	//启动到对话
			template <class Event, class Fsm, class SourceState, class TargetState>
			bool operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const;
		};
		struct Guard_Fighting {	//启动到对话
			template <class Event, class Fsm, class SourceState, class TargetState>
			bool operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const;
		};
		struct Guard_Talking {	//启动到对话
			template <class Event, class Fsm, class SourceState, class TargetState>
			bool operator()(Event const& e, Fsm& f, SourceState& s, TargetState&) const;
		};
		struct ActionLoading {
			template <class Event, class Fsm, class SourceState, class TargetState>
			void operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const;
		};
		struct ActionFighting {
			template <class Event, class Fsm, class SourceState, class TargetState>
			void operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const;
		};
		struct ActionTalking {
			template <class Event, class Fsm, class SourceState, class TargetState>
			void operator()(Event const& e, Fsm& f, SourceState&s, TargetState&t) const;
		};

		// Transition table
		struct transition_table :mpl::vector<
			//          Start    Event   Next  Action（满足条件后的动作） Guard(条件)
			msmf::Row < StateUnknow_, Event3, StateLoding_, ActionLoading, Guard_Loading >,
			msmf::Row < StateUnknow_, Event3, StateFight_, ActionFighting, Guard_Fighting >,
			msmf::Row < StateUnknow_, Event3, StateTalk_, ActionTalking, Guard_Talking >,
			msmf::Row < StateLoding_, Event3, StateFight_, ActionFighting, Guard_Fighting >,
			msmf::Row < StateLoding_, Event3, StateTalk_, ActionTalking, Guard_Talking >,
			msmf::Row < StateTalk_, Event3, StateFight_, ActionFighting, Guard_Fighting >,
			msmf::Row < StateFight_, Event3, StateTalk_, ActionTalking, Guard_Talking >
		> {};

		int evtstate;		//0为初始化，10为结束，1为要去切换
	};
	// back-end
	typedef msm::back::state_machine<Sm1_> Sm1;
}

class tmbststate
{
public:
	tmx::Sm1 sml;
	tmbststate();
	~tmbststate();

	int proceTab_event(tmVirMachine *mac);
	int startTabEvent();
	int stopTabEvent();
};

