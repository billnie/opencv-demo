
#include "stdafx.h"
#include <windows.h>
#include <tlhelp32.h>
#include "FindProc.h"
int FIND_PROC_BY_NAME(const char *szToFind, DWORD *processId)
// Created: 12/29/2000  (RK)
// Last modified: 6/16/2003  (RK)
// Please report any problems or bugs to kochhar@physiology.wisc.edu
// The latest version of this routine can be found at:
//     http://www.neurophys.wisc.edu/ravi/software/killproc/
// Check whether the process "szToFind" is currently running in memory
// This works for Win/95/98/ME and also Win/NT/2000/XP
// The process name is case-insensitive, i.e. "notepad.exe" and "NOTEPAD.EXE"
// will both work (for szToFind)
// Return codes are as follows:
//   0   = Process was not found
//   1   = Process was found
//   605 = Unable to search for process
//   606 = Unable to identify system type
//   607 = Unsupported OS
//   632 = Process name is invalid
// Change history:
//  3/10/2002   - Fixed memory leak in some cases (hSnapShot and
//                and hSnapShotm were not being closed sometimes)
//  6/13/2003   - Removed iFound (was not being used, as pointed out
//                by John Emmas)
{
	BOOL bResult, bResultm;
	DWORD aiPID[1000], iCb = 1000, iNumProc, iV2000 = 0;
	DWORD iCbneeded, i;
	char szName[MAX_PATH], szToFindUpper[MAX_PATH];
	HANDLE hProc, hSnapShot, hSnapShotm;
	OSVERSIONINFO osvi;
	HINSTANCE hInstLib;
	int iLen, iLenP, indx;
	HMODULE hMod;
	PROCESSENTRY32 procentry;
	MODULEENTRY32 modentry;

	// PSAPI Function Pointers.
	BOOL(WINAPI *lpfEnumProcesses)(DWORD *, DWORD cb, DWORD *);
	BOOL(WINAPI *lpfEnumProcessModules)(HANDLE, HMODULE *,
		DWORD, LPDWORD);
	DWORD(WINAPI *lpfGetModuleBaseName)(HANDLE, HMODULE,
		LPTSTR, DWORD);

	// ToolHelp Function Pointers.
	HANDLE(WINAPI *lpfCreateToolhelp32Snapshot)(DWORD, DWORD);
	BOOL(WINAPI *lpfProcess32First)(HANDLE, LPPROCESSENTRY32);
	BOOL(WINAPI *lpfProcess32Next)(HANDLE, LPPROCESSENTRY32);
	BOOL(WINAPI *lpfModule32First)(HANDLE, LPMODULEENTRY32);
	BOOL(WINAPI *lpfModule32Next)(HANDLE, LPMODULEENTRY32);

	// Transfer Process name into "szToFindUpper" and
	// convert it to upper case
	iLenP = strlen(szToFind);
	if (iLenP<1 || iLenP>MAX_PATH) return 632;
	for (indx = 0; indx<iLenP; indx++)
		szToFindUpper[indx] = toupper(szToFind[indx]);
	szToFindUpper[iLenP] = 0;

	// First check what version of Windows we're in
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	bResult = GetVersionEx(&osvi);
	if (!bResult || processId == NULL)     // Unable to identify system version
		return 606;
	*processId = 0;
	// At Present we only support Win/NT/2000 or Win/9x/ME
	if ((osvi.dwPlatformId != VER_PLATFORM_WIN32_NT) &&
		(osvi.dwPlatformId != VER_PLATFORM_WIN32_WINDOWS))
		return 607;

	if (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		// Win/NT or 2000 or XP

		// Load library and get the procedures explicitly. We do
		// this so that we don't have to worry about modules using
		// this code failing to load under Windows 95, because
		// it can't resolve references to the PSAPI.DLL.
		hInstLib = LoadLibraryA("PSAPI.DLL");
		if (hInstLib == NULL)
			return 605;

		// Get procedure addresses.
		lpfEnumProcesses = (BOOL(WINAPI *)(DWORD *, DWORD, DWORD*))
			GetProcAddress(hInstLib, "EnumProcesses");
		lpfEnumProcessModules = (BOOL(WINAPI *)(HANDLE, HMODULE *,
			DWORD, LPDWORD)) GetProcAddress(hInstLib,
				"EnumProcessModules");
		lpfGetModuleBaseName = (DWORD(WINAPI *)(HANDLE, HMODULE,
			LPTSTR, DWORD)) GetProcAddress(hInstLib,
				"GetModuleBaseNameA");

		if (lpfEnumProcesses == NULL ||
			lpfEnumProcessModules == NULL ||
			lpfGetModuleBaseName == NULL)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		bResult = lpfEnumProcesses(aiPID, iCb, &iCbneeded);
		if (!bResult)
		{
			// Unable to get process list, EnumProcesses failed
			FreeLibrary(hInstLib);
			return 605;
		}

		// How many processes are there?
		iNumProc = iCbneeded / sizeof(DWORD);

		// Get and match the name of each process
		for (i = 0; i<iNumProc; i++)
		{
			// Get the (module) name for this process

			strcpy(szName, "Unknown");
			// First, get a handle to the process
			hProc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE,
				aiPID[i]);
			// Now, get the process name
			if (hProc)
			{
				if (lpfEnumProcessModules(hProc, &hMod, sizeof(hMod), &iCbneeded))
				{
					iLen = lpfGetModuleBaseName(hProc, hMod, szName, MAX_PATH);
				}
			}
			CloseHandle(hProc);
			// Match regardless of lower or upper case
			if (strcmp(_strupr(szName), szToFindUpper) == 0)
			{
				// Process found
				*processId = aiPID[i];
				FreeLibrary(hInstLib);
				return 1;
			}
		}
	}

	if (osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
	{
		// Win/95 or 98 or ME

		hInstLib = LoadLibraryA("Kernel32.DLL");
		if (hInstLib == NULL)
			return FALSE;

		// Get procedure addresses.
		// We are linking to these functions of Kernel32
		// explicitly, because otherwise a module using
		// this code would fail to load under Windows NT,
		// which does not have the Toolhelp32
		// functions in the Kernel 32.
		lpfCreateToolhelp32Snapshot =
			(HANDLE(WINAPI *)(DWORD, DWORD))
			GetProcAddress(hInstLib,
				"CreateToolhelp32Snapshot");
		lpfProcess32First =
			(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
			GetProcAddress(hInstLib, "Process32First");
		lpfProcess32Next =
			(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
			GetProcAddress(hInstLib, "Process32Next");
		lpfModule32First =
			(BOOL(WINAPI *)(HANDLE, LPMODULEENTRY32))
			GetProcAddress(hInstLib, "Module32First");
		lpfModule32Next =
			(BOOL(WINAPI *)(HANDLE, LPMODULEENTRY32))
			GetProcAddress(hInstLib, "Module32Next");
		if (lpfProcess32Next == NULL ||
			lpfProcess32First == NULL ||
			lpfModule32Next == NULL ||
			lpfModule32First == NULL ||
			lpfCreateToolhelp32Snapshot == NULL)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		// The Process32.. and Module32.. routines return names in all uppercase

		// Get a handle to a Toolhelp snapshot of all the systems processes.

		hSnapShot = lpfCreateToolhelp32Snapshot(
			TH32CS_SNAPPROCESS, 0);
		if (hSnapShot == INVALID_HANDLE_VALUE)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		// Get the first process' information.
		procentry.dwSize = sizeof(PROCESSENTRY32);
		bResult = lpfProcess32First(hSnapShot, &procentry);

		// While there are processes, keep looping and checking.
		while (bResult)
		{
			// Get a handle to a Toolhelp snapshot of this process.
			hSnapShotm = lpfCreateToolhelp32Snapshot(
				TH32CS_SNAPMODULE, procentry.th32ProcessID);
			if (hSnapShotm == INVALID_HANDLE_VALUE)
			{
				CloseHandle(hSnapShot);
				FreeLibrary(hInstLib);
				return 605;
			}
			// Get the module list for this process
			modentry.dwSize = sizeof(MODULEENTRY32);
			bResultm = lpfModule32First(hSnapShotm, &modentry);

			// While there are modules, keep looping and checking
			while (bResultm)
			{
				if (strcmp(modentry.szModule, szToFindUpper) == 0)
				{
					// Process found
					CloseHandle(hSnapShotm);
					CloseHandle(hSnapShot);
					FreeLibrary(hInstLib);
					return 1;
				}
				else
				{  // Look for next modules for this process
					modentry.dwSize = sizeof(MODULEENTRY32);
					bResultm = lpfModule32Next(hSnapShotm, &modentry);
				}
			}

			//Keep looking
			CloseHandle(hSnapShotm);
			procentry.dwSize = sizeof(PROCESSENTRY32);
			bResult = lpfProcess32Next(hSnapShot, &procentry);
		}
		CloseHandle(hSnapShot);
	}
	FreeLibrary(hInstLib);
	return 0;
}
//根据进程ID获取窗口句柄
HWND GetHwndByPid(DWORD dwProcessID)
{
	//返回Z序顶部的窗口句柄
	HWND hWnd = ::GetTopWindow(0);
	TCHAR buf[MAX_PATH];
	while (hWnd)
	{
		DWORD pid = 0;
		//根据窗口句柄获取进程ID
		DWORD dwTheardId = ::GetWindowThreadProcessId(hWnd, &pid);
		if (dwTheardId != 0)
		{
			if (pid == dwProcessID)
			{
				GetWindowText(hWnd, buf, MAX_PATH);
				return hWnd;
			}
		}
		//返回z序中的前一个或后一个窗口的句柄
		hWnd = ::GetNextWindow(hWnd, GW_HWNDNEXT);
	}
	return hWnd;
}
BOOL CALLBACK FindWindowTitle(HWND hWnd, DWORD lParam, LPTSTR title, LPCTSTR className)
{
	DWORD dwPid = 0;
	HWND h;
	BOOL bret = false;
	h = GetNextWindow(hWnd, GW_HWNDNEXT);
	TCHAR buf[MAX_PATH];
	while (h != NULL) {
		GetClassName(h, buf, MAX_PATH);
		if (strcmp(buf, className) == 0) {
			GetWindowText(h, title, MAX_PATH);
			bret = true;
			break;
		}
		h = GetNextWindow(h, GW_HWNDNEXT);
	}
	return  bret;
}

HBITMAP capXXXWindow(LPTSTR className, LPTSTR titleName)
{
	HWND h;
	RECT r1;
	LPRECT lpRect;
	h = ::FindWindow(className, titleName);

	if (h == NULL) {
		return NULL;
	}
	//if (IsCoveredByOtherWindow(h))
	//	return NULL;
	::GetClientRect(h, &r1);
	::GetWindowRect(h, &r1);

	lpRect = &r1;

	HDC hScrDC, hMemDC;				// 屏幕和内存设备描述表
	HBITMAP hBitmap, hOldBitmap;	// 位图句柄
	int nX, nY, nX2, nY2;			// 选定区域坐标
	int nWidth, nHeight;			// 位图宽度和高度
	int xScrn, yScrn;				// 屏幕分辨率

	if (IsRectEmpty(lpRect))
		return NULL;

	//hScrDC = ::CreateDC(_T("DISPLAY"), NULL, NULL, NULL);		// 为屏幕创建设备描述表
	hScrDC = ::GetWindowDC(h);
	hMemDC = CreateCompatibleDC(hScrDC);				// 为屏幕设备描述表创建兼容的内存设备描述表

	nX = lpRect->left;
	nY = lpRect->top;
	nX2 = lpRect->right;
	nY2 = lpRect->bottom;

	xScrn = GetDeviceCaps(hScrDC, HORZRES);	// 获得屏幕水平分辨率
	yScrn = GetDeviceCaps(hScrDC, VERTRES);

	if (nX < 0)
		nX = 0;
	if (nY < 0)
		nY = 0;
	if (nX2 > xScrn)
		nX2 = xScrn;
	if (nY2 > yScrn)
		nY2 = yScrn;
	nWidth = nX2 - nX;
	nHeight = nY2 - nY;

	hBitmap = CreateCompatibleBitmap(hScrDC, nWidth, nHeight);		// 创建一个与屏幕设备描述表兼容的位图
	hOldBitmap = (HBITMAP)SelectObject(hMemDC, hBitmap);			// 把新位图选到内存设备描述表中
	BitBlt(hMemDC, 0, 0, nWidth, nHeight, hScrDC, 0, 0, SRCCOPY);	// 把屏幕设备描述表拷贝到内存设备描述表中

	hBitmap = (HBITMAP)SelectObject(hMemDC, hOldBitmap);			// 得到屏幕位图的句柄

	::ReleaseDC(h, hScrDC);
	DeleteDC(hMemDC);

	return hBitmap;
}