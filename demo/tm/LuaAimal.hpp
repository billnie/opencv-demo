//
//  LuaAimal.hpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#ifndef LuaAimal_hpp
#define LuaAimal_hpp
#include "lua.hpp"
#include <stdio.h>
class Animal;

class LuaAnimal
{
public:
    ~LuaAnimal(void);
    static void Register( lua_State *l );
private:
    static const char *className;
    static const luaL_Reg methods[];
    static const luaL_Reg methods_f[];
    
    static int create( lua_State *l );
    static int gc_animal( lua_State *l );
    static Animal *getAnimal( lua_State *l );
    
    static int sound( lua_State *l );
    static int setAge(lua_State *l);
    static int getAge(lua_State *l);
};
#endif /* LuaAimal_hpp */
