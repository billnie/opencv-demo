--c = CTest()
--print("c.Add(1, 2) ==> " .. c:Add(1, 2));
--d = CTest()
--print("d.Add(4, 5) ==> " .. d:Add(4, 5));
--print("d.Sub(104, 5) ==> " .. d:Sub(104, 5));
--require("Animal")
print( "test lua access C++ class" )

local function main()
--使用luaL_openlib( l, className, methods_f, 0 )注册，
--Animal是个table, 调用create方法
--local s = Animal.create("xx") --lua_gettop()=1 1:xx
local s = Animal:create("xx") --lua_gettop()=2 1:table, 2:xx, 相比.create多了一个table，指Animal本身
s:setAge(100)
s:sound()

--使用lua_register(l, className, LuaAnimal::create )注册
--Animal是个函数，直接调用方法

local a = Animal("ww")
--a:setAge(20)
--a:sound()
end

main()

print(Student)
--[[
1.打印Student地址
2.打印nil --> luaL_register(L, "Student", reg); 这段代码没有执行成功
]]

local p = Student.Create()
--[[
调用C++代码：
static int Create(lua_State *L) {
Student **s =  (Student**)lua_newuserdata(L, sizeof(Student*));
*s = new Student;

// 将userdata自定义数据与元表Meta_Student相关联
luaL_getmetatable(L, "Meta_Student");
lua_setmetatable(L, -2);

return 1;
}

@retrun p-> Student*
]]

p:SetAge(1200)     -- 调用方法1
p.SetAge(p, 1000)   -- 调用方法2
--[[
p:SetAge() p(userdata)没有注册SetAge方法, 就会寻找与p(userdata)相关联的元表Meta_Student有SetAge方法！并调用！

调用C++代码：
static int SetAge(lua_State *L) {
Student **s = (Student**)luaL_checkudata(L, 1, "Meta_Student");
luaL_argcheck(L, s != NULL, 1, "invalid user data");

int nAge = (int)lua_tonumber(L, -1);
(*s)->SetAge(nAge);

return 0;
}

]]


print(p:GetAge())
--[[
调用C++代码：
static int GetAge(lua_State *L) {
Student **s = (Student**)luaL_checkudata(L, 1, "Meta_Student");
luaL_argcheck(L, s != NULL, 1, "invalid user data");

lua_pushnumber( L, (lua_Number)((*s)->GetAge()) );

return 1;
}
]]
