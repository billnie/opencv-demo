--require "lfs"  --指定包名称
tmlevel = 0;		--角色级别
tmlomtion=0;		--红瓶数量
tmmapofcity = 0;	--当前是否在城市中的地图
tmarrowcount =0;	--弓箭数量
tmfighting =0;

local p = lfs.currentdir()  
local m_package_path = package.path  
package.path = string.format("%s;%s/tm/lua/?.lua;",  
    m_package_path, p)  

require("xcore")
require("xstart")
require("mv")

--判断当前是否有奖励的对话框，与人对话完，判断两遍
function xcheckdialog()  
	ret = xcheckdialoging();	--判断是否有人对话，及有弹出框对话
--	print("xcheckdialoging=",ret);
		sleep(600);
		ret = mapofcity();
		if ret == 1 then	--当前在城里
			findtalk();	
			if tmmapofcity ==0 then		--刚切换 过去的，停止自动
				ret = isfighting(300);						--当前没有自动战斗，则自动战斗
				if ret ==1 then  xautobar();	tmfighting=0;	end;		--自动战斗 
			end
			tmmapofcity = 1;
		else 							--当前在野外
			if tmfighting ==0 then   --上次在城里自动战斗
				ret = isfighting(400);						--当前没有自动战斗，则自动战斗
				if ret ==0 then  xautobar();	tmfighting=1;	end;		--自动战斗 
			else
				if isfighting(400)==0 then	--当前没有战斗，则去战斗
					xautobar();		tmfighting=1;			--自动战斗 
				end
			end;
			tmmapofcity = 0;
	end
	sleep(2000);
end

function lvlfight()	--查找对话的人
	local x=0;
	local k=0;
	print("lvlfight");
	findtalk();
end

function buyarrow() --买箭
	xbuyarrow(1000, 3);
end

function xlvl()--级别
	findtopredcolor();
	ret = ocr(14,12,50,38,"f2f2f2-202020",0.8,"nc.txt");
	print("lvl",ret);
	tmlevel = tonumber(ret);
	return ret;
end

function xlscrolls()--回城卷轴数量
	xautotask(1000);			--点左边自动任务
	ret = ocr(916,487,937,503,"e4af59-403222",0.7,"nc3.txt");
	print("scrolls",ret);
	return ret;
end

function xstartgame()	--开始游戏
    print(lfs.currentdir())
    print(AppPath());
--	KillProc();
	startgame(1);

end
function autobegin()
	local ii= 0;
	for i=1,10 do
		sleep(2000);
		getlvl();
		if tmlevel >0 then
			ret = mapofcity();
			if ret ==1 then			--当前不在城中
				xautobar();				--自动战斗 
			end
			ii = 1;
			break;
		end
	end
	print("init checing",ii);
	return ii;
end
