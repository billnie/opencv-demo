//
//  Utlis.hpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#ifndef Utlis_hpp
#define Utlis_hpp
#include "lua.hpp"
#include <stdio.h>

class Utlis
{
public:
    Utlis(void);
    ~Utlis(void);
    static void stackDump( lua_State *l );
};
#endif /* Utlis_hpp */
