// Simple FSM/HFSM class
// - rlyeh [2011..2015], zlib/libpng licensed.

// [ref]  http://en.wikipedia.org/wiki/Finite-state_machine
// [todo] GOAP? behavior trees?
// [todo] counters
// [note] common actions are 'init', 'quit', 'push', 'back' (integers)
//        - init and quit are called everytime a state is created or destroyed.
//        - push and back are called everytime a state is paused or resumed. Ie, when pushing and popping the stack tree.
// [note] on child states (tree of fsm's):
//        - actions are handled to the most inner active state in the decision tree
//        - unhandled actions are delegated to the parent state handler until handled or discarded by root state

#pragma once

#define FSM_VERSION "1.0.0" /* (2015/11/29) Code revisited to use fourcc integers (much faster); clean ups suggested by Chang Qian
#define FSM_VERSION "0.0.0" // (2014/02/15) Initial version */

#include <algorithm>
#include <deque>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
//模板函数：将string类型变量转换为常用的数值类型（此方法具有普遍适用性）
template <class Type>
Type stringToNum(const std::string& str)
{
    std::istringstream iss(str);
    Type num;
    iss >> num;
    return num;
}
namespace fsm
{
    template<typename T>
    inline std::string to_string( const T &t ) {
        std::stringstream ss;
        return ss << t ? ss.str() : std::string();
    }

    template<>
    inline std::string to_string( const std::string &t ) {
        return t;
    }

    typedef std::vector<std::string> args;
    typedef std::function< void( const fsm::args &args ) > call;

    struct state {
        int name;
        fsm::args args;

        state( const int &name = 'null' ) : name(name)
        {}

        state operator()() const {
            state self = *this;
            self.args = {};
            return self;
        }
        template<typename T0>
        state operator()( const T0 &t0 ) const {
            state self = *this;
            self.args = { fsm::to_string(t0) };
            return self;
        }
        template<typename T0, typename T1>
        state operator()( const T0 &t0, const T1 &t1 ) const {
            state self = *this;
            self.args = { fsm::to_string(t0), fsm::to_string(t1) };
            return self;
        }

        operator int () const {
            return name;
        }

        bool operator<( const state &other ) const {
            return name < other.name;
        }
        bool operator==( const state &other ) const {
            return name == other.name;
        }

        template<typename ostream>
        inline friend ostream &operator<<( ostream &out, const state &t ) {
            if( t.name >= 256 ) {
                out << char((t.name >> 24) & 0xff);
                out << char((t.name >> 16) & 0xff);
                out << char((t.name >>  8) & 0xff);
                out << char((t.name >>  0) & 0xff);
            } else {
                out << t.name;
            }
            out << "(";
            std::string sep;
            for(auto &arg : t.args ) {
                out << sep << arg;
                sep = ',';
            }
            out << ")";
            return out;
        }
    };

    typedef state trigger;

    struct transition {
        fsm::state previous, trigger, current;

        template<typename ostream>
        inline friend ostream &operator<<( ostream &out, const transition &t ) {
            out << t.previous << " -> " << t.trigger << " -> " << t.current;
            return out;
        }
    };

    class stack {
    public:

        stack( const fsm::state &start = 'null' ) : deque(1) {
            deque[0] = start;
            call( deque.back(), 'init' );
        }

        stack( int start ) : stack( fsm::state(start) ) 
        {}

        ~stack() {
            // ensure state destructors are called (w/ 'quit')
            while( size() ) {
                pop();
            }
        }

        // pause current state (w/ 'push') and create a new active child (w/ 'init')
        void push( const fsm::state &state ) {
            if( deque.size() && deque.back() == state ) {
                return;
            }
            // queue
            call( deque.back(), 'push' );
            deque.push_back( state );
            call( deque.back(), 'init' );
        }

        // terminate current state and return to parent (if any)
        void pop() {
            if( deque.size() ) {
                call( deque.back(), 'quit' );
                deque.pop_back();
            }
            if( deque.size() ) {
                call( deque.back(), 'back' );
            }
        }

        // set current active state
        void set( const fsm::state &state ) {
            if( deque.size() ) {
                replace( deque.back(), state );
            } else {
                push(state);
            }
        }

        // number of children (stack)
        size_t size() const {
            return deque.size();
        }

        // info
        // [] classic behaviour: "hello"[5] = undefined, "hello"[-1] = undefined
        // [] extended behaviour: "hello"[5] = h, "hello"[-1] = o, "hello"[-2] = l
        fsm::state get_state( signed pos = -1 ) const {
            signed size = (signed)(deque.size());
            return size ? *( deque.begin() + (pos >= 0 ? pos % size : size - 1 + ((pos+1) % size) ) ) : fsm::state();
        }
        fsm::transition get_log( signed pos = -1 ) const {
            signed size = (signed)(log.size());
            return size ? *( log.begin() + (pos >= 0 ? pos % size : size - 1 + ((pos+1) % size) ) ) : fsm::transition();
        }
        std::string get_trigger() const {
            std::stringstream ss;
            return ss << current_trigger, ss.str();
        }

        bool is_state( const fsm::state &state ) const {
            return deque.empty() ? false : ( deque.back() == state );
        }

        /* (idle)___(trigger)__/''(hold)''''(release)''\__
        bool is_idle()      const { return transition.previous == transition.current; }
        bool is_triggered() const { return transition.previous == transition.current; }
        bool is_hold()      const { return transition.previous == transition.current; }
        bool is_released()  const { return transition.previous == transition.current; } */

        // setup
        fsm::call &on( const fsm::state &from, const fsm::state &to ) {
            return callbacks[ bistate(from,to) ];
        }

        // generic call
        bool call( const fsm::state &from, const fsm::state &to ) const {
            std::map< bistate, fsm::call >::const_iterator found = callbacks.find(bistate(from,to));
            if( found != callbacks.end() ) {
                log.push_back( { from, current_trigger, to } );
                if( log.size() > 50 ) {
                    log.pop_front();
                }
                found->second( to.args );
                return true;
            }
            return false;
        }

        // user commands
        bool command( const fsm::state &trigger ) {
            size_t size = this->size();
            if( !size ) {
                return false;
            }
            current_trigger = fsm::state();
            std::deque< states::reverse_iterator > aborted;
            for( auto it = deque.rbegin(); it != deque.rend(); ++it ) {
                fsm::state &self = *it;
                if( !call(self,trigger) ) {
                    aborted.push_back(it);
                    continue;
                }
                for( auto it = aborted.begin(), end = aborted.end(); it != end; ++it ) {
                    call(**it, 'quit');
                    deque.erase(--(it->base()));
                }
                current_trigger = trigger;
                return true;
            }
            return false;
        }
        template<typename T>
        bool command( const fsm::state &trigger, const T &arg1 ) {
            return command( trigger(arg1) );
        }
        template<typename T, typename U>
        bool command( const fsm::state &trigger, const T &arg1, const U &arg2 ) {
            return command( trigger(arg1, arg2) );
        }

        // debug
        template<typename ostream>
        ostream &debug( ostream &out ) {
            int total = log.size();
            out << "status {" << std::endl;
            std::string sep = "\t";
            for( states::const_reverse_iterator it = deque.rbegin(), end = deque.rend(); it != end; ++it ) {
                out << sep << *it;
                sep = " -> ";
            }
            out << std::endl;
            out << "} log (" << total << " entries) {" << std::endl;
            for( int i = 0 ; i < total; ++i ) {
                out << "\t" << log[i] << std::endl;
            }
            out << "}" << std::endl;
            return out;
        }

        // aliases
        bool operator()( const fsm::state &trigger ) {
            return command( trigger );
        }
        template<typename T>
        bool operator()( const fsm::state &trigger, const T &arg1 ) {
            return command( trigger(arg1) );
        }
        template<typename T, typename U>
        bool operator()( const fsm::state &trigger, const T &arg1, const U &arg2 ) {
            return command( trigger(arg1, arg2) );
        }
        template<typename ostream>
        inline friend ostream &operator<<( ostream &out, const stack &t ) {
            return t.debug( out ), out;
        }

    protected:

        void replace( fsm::state &current, const fsm::state &next ) {
            call( current, 'quit' );
            current = next;
            call( current, 'init' );
        }

        typedef std::pair<int, int> bistate;
        std::map< bistate, fsm::call > callbacks;

        mutable std::deque< fsm::transition > log;
        std::deque< fsm::state > deque;
        fsm::state current_trigger;

        typedef std::deque< fsm::state > states;
    };
}


#include <iostream>

// custom states (gerunds) and actions (infinitives)

enum {
    bql_idle,
    
    bql_snap,
    bql_snaping,
    
    bql_record,
    bql_recording,
    
    bql_switch,
};

struct bql_player_fsm {
    
    // implementation conditions / guards

    
    // implementation actions

    
    // the core
    fsm::stack fsm;
    
    bql_player_fsm()
    {
        // define fsm transitions: on(state,trigger) -> do lambda
        fsm.on(bql_idle,bql_snap) = [&]( const fsm::args &args ) {

            fsm.set( bql_snap );
        };
        fsm.on(bql_idle,bql_record) = [&]( const fsm::args &args ) {

            fsm.set( bql_record );
        };
        fsm.on(bql_record,bql_recording) = [&]( const fsm::args &args ) {

            fsm.set( bql_recording );
        };
        
        fsm.on(bql_recording,bql_idle) = [&]( const fsm::args &args ) {

            fsm.set( bql_idle );
        };
        
        fsm.on(bql_snap,bql_snaping) = [&]( const fsm::args &args ) {

            fsm.set( bql_snaping );
        };
        fsm.on(bql_snaping,bql_idle) = [&]( const fsm::args &args ) {
            fsm.set( bql_idle );
        };
   
        // set initial fsm state
        fsm.set(bql_idle);
    }
    char*state_string(){
        switch(fsm.get_state()){
            case bql_idle:
                return "idle";
                break;
            case bql_snap:
            case bql_snaping:
                return "当前正在拍照";
                break;
            case bql_record:
            case bql_recording:
                return "当前正在录像!";
                break;
                
        }
        return "unknow state";
    }
};

// usage


enum {
    bql_network_none,
    
    bql_network_wwan,
    bql_network_wifi,
};
struct bql_network_fsm;
typedef void (*disconnect_wifi)(bql_network_fsm *fsm,void *classData);
struct bql_network_fsm {
    // implementation variables
    bool bconnectVRNVS;         //连接到vr wifi
    bool bconnectHDAV;          //连接到nightVision wifi
    bool bvrHeartBit;           //收到vr心跳
    bool bnightHeartBit;        //收到nightVision心跳
    bool bConnect5000;          //连接到5000端口成功了
    disconnect_wifi handleDisconnect;
    
    void *userObject;
    
    // implementation conditions / guards
    bool connectVRNVS() { return bconnectVRNVS; }
    
    // implementation actions
    void wifiname_check( const std::string &name ) {
        if(name.find("VRNVS") ==0)
        {
            bconnectVRNVS = true;
            bconnectHDAV = false;
        }
        else if(name.find("HDAV") ==0)
        {
            bconnectHDAV = true;
            bconnectVRNVS = false;
            bConnect5000 = false;
        }else{
            bconnectHDAV = false;
            bconnectVRNVS = false;
            bvrHeartBit = false;
            bnightHeartBit = false;
            bConnect5000 = false;
        }
    }
    void hearbit_check( const std::string &sport,const std::string &value ) {
        int port;
        int val;
        port= stringToNum<int>(sport);
        val = stringToNum<int>(value);
    ccx:
        if(bconnectVRNVS){
            if(port ==5000){
                if(val>0)
                {
                    bConnect5000 = true;
                    bvrHeartBit = true;
                }
                else
                    bvrHeartBit = false;
            }else if(port ==5001){
                if(val>0)
                    bnightHeartBit = true;
                else
                    bnightHeartBit = false;
            }
        }else if(val>0){
            bconnectVRNVS = true;
            goto ccx;
        }
    }
    //user function
    //能否进入vr
    bool canEnternVR(){
        int st;
        st = fsm.get_state();
        if( st ==bql_network_wifi){
            if(bconnectVRNVS /*&& (bvrHeartBit|| bConnect5000)*/)
                return true;
        }
        return false;
    }
    //能否进入夜视
    bool canEnterNightVision(){
        int st;
        st = fsm.get_state();
        if( st ==bql_network_wifi){
            if(bconnectVRNVS && bnightHeartBit)
                return true;
            else if(bconnectHDAV){
                return true;
            }
        }
        return false;
    }
    
    bool isC20Wifi(){
        if(bconnectVRNVS|| bconnectHDAV)
            return true;
        return false;
    }

    void leaveWifi(){
        bconnectVRNVS = false;
        bconnectHDAV = false;
        bvrHeartBit = false;
        bnightHeartBit = false;
        bConnect5000 = false;
    }
    // the core
    fsm::stack fsm;
    
    bql_network_fsm()
    {
        userObject = NULL;
        // define fsm transitions: on(state,trigger) -> do lambda
        fsm.on(bql_network_none,bql_network_wwan) = [&]( const fsm::args &args ) {
            
            fsm.set( bql_network_wwan );
        };
        fsm.on(bql_network_wwan,bql_network_none) = [&]( const fsm::args &args ) {
            
            fsm.set( bql_network_none );
        };
        fsm.on(bql_network_none,bql_network_wifi) = [&]( const fsm::args &args ) {
            if(args.size()==1){     //更新wifi name
                wifiname_check(args[0]);
                fsm.set( bql_network_wifi );
            }else if(args.size()==2){   //更新心跳等数据
                hearbit_check(args[0], args[1]);
            }
        };
        fsm.on(bql_network_wifi,bql_network_wifi) = [&]( const fsm::args &args ) {
            if(args.size()==2){   //更新心跳等数据
                hearbit_check(args[0], args[1]);
            }else if(args.size()==1){
                wifiname_check(args[0]);
            }
        };
        fsm.on(bql_network_wifi,bql_network_none) = [&]( const fsm::args &args ) {
            leaveWifi();
            if(userObject &&handleDisconnect){
                handleDisconnect(this, userObject);
            }
            fsm.set( bql_network_none );
        };
        
        fsm.on(bql_network_wifi,bql_network_wwan) = [&]( const fsm::args &args ) {
            leaveWifi();
            if(userObject &&handleDisconnect){
                handleDisconnect(this, userObject);
            }
            fsm.set( bql_network_wwan );
        };
        fsm.on(bql_network_wwan, bql_network_wifi) = [&]( const fsm::args &args ) {
            if(args.size()==1){     //更新wifi name
                wifiname_check(args[0]);
            }else if(args.size()==2){   //更新心跳等数据
                hearbit_check(args[0], args[1]);
            }
            fsm.set( bql_network_wifi );
        };
        // set initial fsm state
        fsm.set(bql_network_none);
    }
    char*state_string(){
        char str[255] = {0  };
        switch(fsm.get_state()){
            case bql_network_none:
                return "当前无网络";
                break;
            case bql_network_wwan:
                return "当前正在使用手机网络";
                break;
            case bql_network_wifi:
                sprintf(str, "当前连接到Wifi!, vrwifi = %d, hdavwifi = %d, vrhearbit = %d, nightbeat = %d",
                        bconnectVRNVS, bconnectHDAV, bvrHeartBit,bnightHeartBit );
                return str;
                return "当前连接到Wifi!";
                break;
                
        }
        return "unknow state";
    }
};

