

#ifndef FIND_PROC
#define FIND_PROC

#include	"windows.h"
#ifdef __cplusplus
extern "C"
{
#endif
	int FIND_PROC_BY_NAME(const char *name, DWORD *processId);
	HWND GetHwndByPid(DWORD dwProcessID);
	BOOL CALLBACK FindWindowTitle(HWND hWnd, DWORD lParam, LPTSTR title,  LPCTSTR className);
	HBITMAP capXXXWindow(LPTSTR className, LPTSTR titleName);
#ifdef __cplusplus
}
#endif
#endif

