//
//  Student.hpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#ifndef Student_hpp
#define Student_hpp
#include "lua.hpp"
#include <stdio.h>

class Student
{
public:
    Student();
    void SetAge(int nAge);
    int GetAge();
    
    int m_nAge;
};


#ifdef __cplusplus
extern "C" {
#endif
    int luaopen_student(lua_State *L);
#ifdef __cplusplus
}
#endif
#endif /* Student_hpp */
