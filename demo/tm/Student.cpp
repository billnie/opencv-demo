//
//  Student.cpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#include "Student.hpp"
#include "lua.hpp"
Student::Student()
{
    m_nAge = 0;
}

void Student::SetAge(int nAge)
{
    this->m_nAge = nAge;
}


int Student::GetAge()
{
    return this->m_nAge;
}

static int Create(lua_State *L) {
    Student **s =  (Student**)lua_newuserdata(L, sizeof(Student*));
    *s = new Student;
    
    // 将userdata自定义数据与元表Meta_Student相关联
    luaL_getmetatable(L, "Meta_Student");
    lua_setmetatable(L, -2);
    
    return 1;
}


static int SetAge(lua_State *L) {
    Student **s = (Student**)luaL_checkudata(L, 1, "Meta_Student");
    luaL_argcheck(L, s != NULL, 1, "invalid user data");
    
    int nAge = (int)lua_tonumber(L, -1);
    (*s)->SetAge(nAge);
    
    return 0;
}

static int GetAge(lua_State *L) {
    Student **s = (Student**)luaL_checkudata(L, 1, "Meta_Student");
    luaL_argcheck(L, s != NULL, 1, "invalid user data");
    
    lua_pushnumber( L, (lua_Number)((*s)->GetAge()) );
    
    return 1;
}

static int AutoDel(lua_State *L) {
    Student **s = (Student**)luaL_checkudata(L, 1, "Meta_Student");
    luaL_argcheck(L, s != NULL, 1, "invalid user data");
    
    delete (*s);
    
    return 0;
}

luaL_Reg reg[] =
{
    {"Create", Create },
    {NULL,NULL},
};
static  int luaopen_lstlib(lua_State* L)
{
    luaL_newlibtable(L,reg);
    luaL_setfuncs(L,reg,0);
    return 1;
}

int luaopen_student(lua_State *L)
{

    // 创建名字为“Meta_Student”元表
    luaL_newmetatable(L, "Meta_Student");
    
    //修改元表“Meta_Student”查找索引，把它指向“Meta_Student”自身
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    
    // GetAge方法
    lua_pushcfunction(L, GetAge);
    lua_setfield(L, -2, "GetAge");
    
    // SetAge方法
    lua_pushcfunction(L, SetAge);
    lua_setfield(L, -2, "SetAge");
    
    // 自动删除，如果表里有__gc,Lua的垃圾回收机制会调用它。
    lua_pushcfunction(L, AutoDel);
    lua_setfield(L,-2,"__gc");
    
    // 注册reg中的方法到Lua中（“Student”相当于类名）,reg中的方法相当于Student成员函数
//    luaL_requiref(L,LFS_LIBNAME,luaopen_lfslib,1);
    luaL_requiref(L, "Student", luaopen_lstlib,1);
    
    return 1;
}
