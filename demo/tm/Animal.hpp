//
//  Animal.hpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#ifndef Animal_hpp
#define Animal_hpp

#include <stdio.h>
class Animal
{
public:
    Animal( const char *name );
    void setAge( int age );
    int getAge();
    void sound();
    ~Animal(void);
    
private:
    const char *name;
    int age;
};
#endif /* Animal_hpp */
