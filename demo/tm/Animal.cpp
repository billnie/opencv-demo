//
//  Animal.cpp
//  luacxx
//
//  Created by zzx on 2017/10/19.
//  Copyright © 2017年 com.nxj.rac. All rights reserved.
//

#include "Animal.hpp"
Animal::Animal( const char* name ):age(0)
{
    this->name = name;
}

Animal::~Animal(void)
{
    printf( "Animal destructor." );
}

void Animal::setAge( int age )
{
    this->age = age;
}

int Animal::getAge()
{
    return this->age;
}

void Animal::sound()
{
    printf("--Animal-- name: %s, age:%d\n", this->name, this->age );
}
