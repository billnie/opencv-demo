
//http://blog.csdn.net/sidyhe/article/details/45286067
#include	"color.h"
//解析大漠生成的数据
void DT(const char * strOutputString, ...);
/**
判断某个点是否在矩形中
 @param a <#a description#>
 @param b <#b description#>
 @param left <#left description#>
 @param top <#top description#>
 @param right <#right description#>
 @param bottom <#bottom description#>
 @return <#return value description#>
 */
inline int isinrect(int a,int b,int left, int top, int right, int bottom){
    if(a>left && a< right && b>top && b< bottom) return 1;
    else return 0;
}

inline int isrectinrect(int a,int b,int c, int d,int left, int top, int right, int bottom){
    if(isinrect(left, top, a,b,c,d)
       ||isinrect(left, bottom, a,b,c,d)
       ||isinrect(right, top, a,b,c,d)
       ||isinrect(right, bottom, a,b,c,d))
        return 1;
    else return 0;
}
int dm_binaryParse(int x, const char *str, int *xm, int **xa, int **xb) {
	int ret = 0;
	int i;
	if (x > 0 && _tcslen(str) > 0 && xm && xa && xb) {
		char sa[32], sb[32], sc[128];
		const char *sz, *sd, *s1, *s2;
		int minx, miny, maxx, maxy, mm;
		sz = strchr((const char*)str, '|');
		if (sz) {
			sd = sz + 1; s1 = str; s2 = sd;
			int j;
			minx = 0; miny = 0; maxx = 0; maxy = 0;
			*xa = (int*)malloc(sizeof(int)*x);
			*xb = (int*)malloc(sizeof(int)*x);
			int *qa, *qb;
			qa = *xa;	qb = *xb;
			for (i = 0; i < x; i++) {
				//取第一个数
				j = 0;
				while (*s1 != ','&& s1<sz &&j<8) {
					sa[j++] = *s1; s1++;
				}
				sa[j] = '\0'; j = 0;
				mm = atoi(sa);	qa[i] = mm;
				if (i == 0) minx = mm;
				if (mm < minx) minx = mm;		//取最小值
				if (mm > maxx)  maxx = mm;
				while (*s2 != ','&& s2 != '\0'&&j<8) {
					sb[j++] = *s2; s2++;
				}
				sb[j] = '\0';
				mm = atoi(sb);	qb[i] = mm;
				if (i == 0)  miny = mm;
				if (mm < miny) miny = mm;		//取最小值
				if (mm > maxy)  maxy = mm;
				//sprintf(sc, _T("%d (%s,%s)"), i, sa, sb);
				//DT(sc);
				if (s1 == sz || s2 == '\0') break;
				s1++; s2++;
			}
			if (i >= x - 1) {
				ret = 1;
				j = 0;
				xm[j++] = minx;
				xm[j++] = miny;
				xm[j++] = maxx;
				xm[j++] = maxy;
				xm[j++] = maxx - minx;
				xm[j++] = maxy - miny;
			}
		}
	}
	return ret;
}
int dm_pixelCrop(int x,  int *xm, int *xa, int *xb,int left, int top, int right, int bottom){
	int ret = -1;
    if(x>0 && xm && xa &&xb && left>=0 && right>left && top >=0 && bottom > top){
		int i, j,k, minx, miny, maxx, maxy;
		if(isrectinrect(xm[0], xm[1], xm[2], xm[3], left, top, right, bottom)){
			k = 0;
			for(i=0;i< x;i++){
                if(isinrect(xa[i],xb[i], left, top, right, bottom)){
                    continue;
                }
                if(i!=k){
                    xa[k] = xa[i]; xb[k]= xb[i]; k++;
                }
            }
            //
            minx = xa[0]; miny = xb[0];
			maxx = minx;  maxy = miny;
			for (i = 1; i < k; i++) {
				if (minx > xa[i]) minx = xa[i];
				else if (xa[i] > maxx) maxx = xa[i];
				if (miny > xb[i]) miny = xb[i];
				else if (xb[i] > maxy) maxy = xb[i];
			}
            xm[0] = minx;
			xm[1] = miny;
			xm[2] = maxx;
            xm[3] = maxy;
            xm[4] = maxx-minx;
            xm[5] = maxy-miny;
            ret = k;
        }else ret = x;
    }
    return ret;
}
//将索引生成点阵数据，便于运算, xm,左上角，右下角宽高度，xa,xb为x,y坐标
int index2matrix(int num, int *xm, int *xa, int *xb, unsigned char**data) {
	int ret = 0;
	if (num > 0 && xm && xa && xb && data ) {
		int w, h,i;
		unsigned char *xx;
		w = xm[4] + 1;
		h = xm[5] + 1;
		*data = 0;
		if (w > 2 && h > 2) {
			ret = w*h;
			*data = malloc(ret);
			xx = *data;
			memset(xx, 0, ret);
			for (i = 0; i < num; i++) {
				xx[i*w + h] = 255;
			}
		}
	}
	return ret;
}
//点阵转索引
int matrix2index(int w, int h, unsigned char *data, int **xa, int **xb) {
	int ret = 0;
	if (w > 1 && h > 1 && data && xa && xb) {
		int num, i, m, k;
		int *da, *db;
		num = w*h;	m = 0;
		for (i = 0; i < num; i++) {
			if (data[i] > 0) m++;
		}
		*xa = (int*)malloc(m);
		*xb = (int*)malloc(m);
		k = 0; da = *xa; db = *xb;
		for (i = 0; i < num; i++) {
			if (data[i] > 0) {
				da[k] = i% w;
				db[k] = i/w;
				k++;
			}
		}
	}
	return ret;
}

int dm_histogramDist(int x, const char *str,int *a,int *b, int *c, int *d){
    int ret = 0;
    int *xa, *xb, *ha, *hb,dx,dy,di,dj,mx,my,tt,i;
    int xm[10] = { 0 };  xa=0; xb = 0; ha = 0; hb = 0;
    tt = dm_binaryParse(x, str, xm, &xa, &xb);
    if (tt >0) {
//		DT("ret=%d,%s",x,str);
        dm_histogram(x, xm, xa,xb,&ha,&hb);
        //直方图中x的分布很集中，也就是说x分布中大部分分布数值为0
        tt = 0;
        for(i=0; i < xm[4]+1; i++){
            if(ha[i] == 0) tt++;
        }
        *a = xm[4] + 1 -tt;
        tt = 0;
        for(i=0; i < xm[5] + 1; i++){
            if(hb[i] == 0) tt++;
        }
        *b = xm[5] + 1 -tt;
		tt = 0;
		for (i = 0; i < xm[4] + 1; i++) {
			tt += ha[i]; if (tt >xm[4] / 2) break;
		}
		*c = xm[0] +i;
		tt = 0;
		for (i = 0; i < xm[5] + 1; i++) {
			tt += hb[i]; if (tt >xm[5] / 2) break;
		}
		*d = xm[1] + i;
        if(xa) free(xa);
        if(xb) free(xb);
        if(ha) free(ha);
        if(hb) free(hb);
    }
    return ret;
}
int dm_histogram(int x, int *xm, int *xa, int *xb, int **ha, int **hb){
    int ret = 0;
    if(x>0 && xm && xa && xb && ha &&hb){
        int i,j = 0;
        int dx,dy, *ax,*ay;
        //找xa重心，和xb重心
        dx = xm[4] + 1; dy = xm[5] + 1;
        ax = (int*)malloc(sizeof(int)*dx);
        memset(ax, 0, sizeof(int)*dx);
        ay = (int*)malloc(sizeof(int)*dy);
        memset(ay, 0, sizeof(int)*dy);
        for (i = 0; i <x; i++) {
            ax[xa[i] - xm[0]]++;
        }
        for (i = 0; i <x; i++) {
            ay[xb[i] - xm[1]]++;
        }
		*ha = ax; *hb = ay;
        ret = 1;
    }
    return ret;
}
