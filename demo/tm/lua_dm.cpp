

#include	"dm.h"
#include "stdafx.h"

#include	"lua_dm.h"
#include "DMMouseKey.h"
#include "tmfactory.h"

#include "FindProc.h"
#include	"bitmap_image.hpp"
#include	"color.h"
void DT(const char * strOutputString, ...);
using namespace akb;
extern HWND GetSubWindowClass(HWND hWnd, LPCTSTR cls);
extern "C" CDMMouseKey* get_MouseKey(lua_State* L);

int genbmpfilename(char *buf, char *name, char *sub) {
	int ret = -1;
	if (buf && name) {
#ifdef _DEBUG
		strcpy(buf, "D:\\testgame\\tm\\bm\\");

#else
		strcpy(buf, ".\\tmp\\");
#endif
		if(sub ==NULL||strlen(sub)==0) strcat(buf, name);
		else {
			char *s; s = strchr(name, '.');
			char ss[128] = { 0 };
			strcpy(ss, name);	ss[s - name] = '\0';
			strcat(buf, ss);
			strcat(buf, sub);
			strcat(buf,s);
		}
	}
	return ret;
}

int KillProcess(LPCSTR pszClassName, LPCSTR
	pszWindowTitle)
{
	HANDLE hProcessHandle;
	ULONG nProcessID;
	HWND TheWindow;
	BOOL bret;
	TheWindow = ::FindWindow(pszClassName, pszWindowTitle);
	::GetWindowThreadProcessId(TheWindow, &nProcessID);
	hProcessHandle = ::OpenProcess(PROCESS_TERMINATE, FALSE,
		nProcessID);
	bret = TerminateProcess(hProcessHandle, 0);
	CloseHandle(hProcessHandle);
	return bret;
}
int AppPath(LPTSTR path) {
	int ret = -1;
	TCHAR ss[MAX_PATH], *sz;
//	TCHAR ss[MAX_PATH];
	GetModuleFileName(NULL, ss, MAX_PATH);
	sz =_tcsrchr(ss, '\\');
	if (sz) *sz = 0;
	_tcscpy(path, ss);
	return ret;
}
int RunApp(LPCTSTR path, LPTSTR cmdline, LPCTSTR apppath) {
	int ret = 0;

	PROCESS_INFORMATION ProcessInfo;
	STARTUPINFO StartupInfo; //入口参数
	ZeroMemory(&ProcessInfo, sizeof(ProcessInfo));
	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof(StartupInfo);

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));

	if (CreateProcess(path, cmdline,
		NULL, NULL, FALSE, 0, NULL, apppath, &StartupInfo, &ProcessInfo)) {
		//WaitForSingleObject(ProcessInfo.hProcess, 10);
		//CloseHandle(ProcessInfo.hThread);
		//CloseHandle(ProcessInfo.hProcess);
		ret = 1;
	}
	return ret;
}
int os_AppPath(lua_State* L) {
	CString strCurPath;
	GetModuleFileName(NULL, strCurPath.GetBuffer(MAX_PATH), MAX_PATH);
	int pos = strCurPath.ReverseFind(_T('\\'));
	strCurPath = strCurPath.Left(pos);
	lua_pushstring(L, strCurPath.LockBuffer());
	return 1;
}
#include <stdarg.h>
int errors(lua_State *L, const char * fmt, ...) {
	return 0;
}
int call_va(lua_State *L, const char * func, const char *sig, ...)
{
	va_list vl;//定义一个可变长参数指针，无初始值
	int narg;//可变长参数的数量
	int nres;//Lua函数返回值的数量
	int ret,rx;
	rx = 0;
	va_start(vl, sig);//让指针指向可变长参数的第一个参数首地址
	ret = lua_getglobal(L, func);//函数入栈
	//---参数入栈
	for (narg = 0; *sig; narg++)//遍历参数
	{
		//检查栈中空间
		luaL_checkstack(L, 1, "too many arguments");
		switch (*sig++)
		{
			//根据参数的约束符，按类型入栈,d-double i-int s-char*
		case 'd':
			lua_pushnumber(L, va_arg(vl, double));
			break;
		case 'i':
			lua_pushinteger(L, va_arg(vl, int));
			break;
		case 's':
			lua_pushstring(L, va_arg(vl, char*));
			break;
		case '>':
			//输入参数结束
			goto endargs;
		default:
			errors(L,"invalid option (%c)",*(sig-1));
		}
	}
	//---参数入栈
endargs:
	nres = strlen(sig);//期望的Lua函数返回值数量

	//调用Lua函数
	if (lua_pcall(L, narg, nres, 0) != 0)
	{
		errors(L,"error calling '%s':%s",func,lua_tostring(L,-1));
		goto End;
	}

	//调用之后的返回值检索
	//栈索引从栈顶往下依次是-1,-2,-3，所以第一个结果的栈索引是-nres
	nres = -nres;
	while (*sig)
	{
		//依次得到返回值的类型描述符 *sig
		switch (*sig++)
		{
		case 'd':
			if (!lua_isnumber(L, nres))
				errors(L, "wrong result type");
			*va_arg(vl, double*) = lua_tonumber(L, nres);
			break;
		case 'i':
			if (!lua_isnumber(L, nres))
				errors(L, "wrong result type");
			*va_arg(vl, int*) = lua_tointeger(L, nres);
			break;
		case 's':
			if (!lua_isstring(L, nres))
				errors(L, "wrong result type");
			*va_arg(vl, char**) = (char*)lua_tostring(L, nres);
			break;
		default:
			errors(L,"invalid option (%c)",*(sig-1));
		}
		//结果的参数索引＋1
		nres++;
	}
	rx = 1;
End:
	va_end(vl);
	return rx;
}
int lua_print(lua_State * luastate)
{
	int nargs = lua_gettop(luastate);
	std::string t;
	for (int i = 1; i <= nargs; i++)
	{
		if (lua_istable(luastate, i))
			t += "table";
		else if (lua_isnone(luastate, i))
			t += "none";
		else if (lua_isnil(luastate, i))
			t += "nil";
		else if (lua_isboolean(luastate, i))
		{
			if (lua_toboolean(luastate, i) != 0)
				t += "true";
			else
				t += "false";
		}
		else if (lua_isfunction(luastate, i))
			t += "function";
		else if (lua_islightuserdata(luastate, i))
			t += "lightuserdata";
		else if (lua_isthread(luastate, i))
			t += "thread";
		else
		{
			const char * str = lua_tostring(luastate, i);
			if (str)
				t += lua_tostring(luastate, i);
			else
				t += lua_typename(luastate, lua_type(luastate, i));
		}
		if (i != nargs)
			t += ", ";// t += "\t";
	}

#ifdef ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, "cocos2d-x debug info", "%s", t.c_str());
#else
	t += "\r\n";
	DT("%s", t.c_str());
	char *str = strdup(t.c_str());
	if (::IsWindow(akb::CMouseKey::loghWnd)) {
		PostMessage(akb::CMouseKey::loghWnd, WM_LOG_LUA, 1, (LPARAM)str);
	}
#endif
	return 0;
}
extern "C" int os_sleep(lua_State* L) {
	int x;
	double fval;
	int argc = lua_gettop(L);
	if (argc >= 1) {
		x = luaL_checkinteger(L, 1);
		Sleep(x);
	}
	return 1;
}
extern "C" int dm_runqemu(lua_State* L) {
	int x, y;
	int argc = lua_gettop(L);
	x = 0;
	if (argc >= 1) {
		TCHAR buf2[MAX_PATH];
		x = luaL_checkinteger(L, 1);
		//先判断是否存在，存在，则不去启动
		CDMMouseKey *mk = get_MouseKey(L);
		HWND h;
		bool bRun = false;
		DWORD dw;
		HWND hwnd;
		bRun = true;
		y = FIND_PROC_BY_NAME("dnplayer.exe",&dw);
		if (y == 1) {	//找到进程了
			hwnd = GetHwndByPid(dw);
			if (FindWindowTitle(hwnd, dw,  buf2,_T("LDPlayerMainFrame"))) {
				TCHAR ss[32];
				sprintf(ss, _T("-%d"), x);
				if (_tcsstr(buf2, ss)) {		//存在模拟器的
					x = 2;	bRun = false;
				}
				else {
				}
			}
		}
		
		if (bRun) {
			TCHAR buf[MAX_PATH] = { 0 };
			TCHAR buf3[MAX_PATH] = { 0 };
			::GetPrivateProfileString("Settings",
				"dofus",
				"",
				buf,
				MAX_PATH, ".\\Settings.ini");
			if (_tcslen(buf) > 4) {
				sprintf(buf2, _T("%s\\dnconsole.exe"), buf);
				sprintf(buf2, _T("%s\\dnconsole.exe launch --index %d"), buf, x);
				sprintf(buf3, _T("launch --index %d"), x);
				//			x = mk->dm.RunApp(buf2, 0);
				RunApp(NULL, buf2, buf);
				x = 1;
			}
			else x = 0;
		}
	}
	else x = 0;
	lua_pushnumber(L, x);
	return 1;
}

//解析大漠生成数据，得到矩形左上角及右下角及宽高数据
extern "C" int uitls_Binary(lua_State* L)
{
	int x, tt;
	const char *str;
	int argc = lua_gettop(L);
	tt = 1;
	if (argc >= 2) {
		int *xa, *xb;
		int xm[10] = { 0 };
		x = luaL_checkinteger(L, 1);
		str = luaL_checkstring(L, 2);
		xa = NULL; xb = NULL;
		tt = dm_binaryParse(x, str, xm, &xa, &xb);
		if(tt >0){
			tt = 7; int j = 0;
//			DT("end %d (%d,%d) (%d,%d)=(%d,%d)", x,xm[0], xm[1], xm[2], xm[3], xm[4], xm[6]);
			lua_pushnumber(L, tt);
			lua_pushnumber(L, xm[j++]);	//左上角
			lua_pushnumber(L, xm[j++]);
			lua_pushnumber(L, xm[j++]);	//右下角
			lua_pushnumber(L, xm[j++]);
			lua_pushnumber(L, xm[j++]);	//宽高
			lua_pushnumber(L, xm[j++]);
		}
		if (xa) free(xa);
		if (xb) free(xb);
		xa = NULL; xb = NULL;
	}
	if(tt==1) {
		lua_pushnumber(L, 0);
	}
	return tt;
}
extern "C" int uitls_Center(lua_State* L)
{
	int x, tt,i;
	const char *str;
	int argc = lua_gettop(L);
	tt = 1;
	if (argc >= 2) {
		int *xa, *xb, *ha, *hb,dx,dy,di,dj,mx,my;
		int xm[10] = { 0 };
		x = luaL_checkinteger(L, 1);
		str = luaL_checkstring(L, 2);
		xa = NULL; xb = NULL;
		tt = dm_binaryParse(x, str, xm, &xa, &xb);
		if (tt >0) {
			tt = 7; int j = 0;
			//找xa重心，和xb重心
			dx = xm[4] + 1; dy = xm[5] + 1;
			ha = (int*)malloc(sizeof(int)*dx);
			memset(ha, 0, sizeof(int)*dx);
			hb = (int*)malloc(sizeof(int)*dy);
			memset(hb, 0, sizeof(int)*dy);
			for (i = 0; i <x; i++) {
				ha[xa[i] - xm[0]]++;
			}
			for (i = 0; i <x; i++) {
				hb[xb[i] - xm[1]]++;
			}
			mx = ha[0]; di = 0;
			for (i = 1; i < dx; i++) {
				if (ha[i] > mx) {
					mx = ha[i];  di = i;
				}
			}
			my = hb[0]; dj = 0;
			for (i = 1; i < dy; i++) {
				if (hb[i] > my) {
					my = hb[i];  dj = i;
				}
			}
			tt = 2;
			lua_pushnumber(L, di+xm[0]);
			lua_pushnumber(L, dj + xm[1]);
			free(ha); free(hb);
		}
		if (xa) free(xa);
		if (xb) free(xb);
		xa = NULL; xb = NULL;
	}
	if (tt == 1) {
		lua_pushnumber(L, 0);
	}
	return tt;
}
extern "C" int uitls_ColorHist(lua_State* L)
{
    int x,i,tt;
    const char *str;
    int argc = lua_gettop(L);
    tt = 1;
    if (argc >= 2) {
        int a,b,c,d;
        x = luaL_checkinteger(L, 1);
        str = luaL_checkstring(L, 2);
		if (str) {
			DT("ret=%d,%s", x, str);
		}
        dm_histogramDist(x, str,&a,&b,&c,&d);
        tt = 4;
        lua_pushnumber(L, a);
        lua_pushnumber(L, b);
		lua_pushnumber(L, c);
		lua_pushnumber(L, d);
    }
    if (tt == 1) {
        lua_pushnumber(L, 0);
    }
    return tt;
}
extern "C" int uitls_CropRect(lua_State* L)
{
	int x, i, tt;
	const char *str;
	int argc = lua_gettop(L);
	tt = 1;
	if (argc >= 6) {
		int a, b;
		int *xa, *xb, i, cnt, dx, dy;
		int left, top, right, bottom;
		int xm[10] = { 0 };
		x = luaL_checkinteger(L, 1);
		str = luaL_checkstring(L, 2);
		left = luaL_checkinteger(L, 3);
		top = luaL_checkinteger(L, 4);
		right = luaL_checkinteger(L, 5);
		bottom = luaL_checkinteger(L, 6);
		//解析，算法去掉其中区域的
		xa = NULL; xb = NULL;
		tt = dm_binaryParse(x, str, xm, &xa, &xb);
		if (tt > 0) {
			tt = dm_pixelCrop(x, xm, xa, xb, left, top, right, bottom);
			lua_pushnumber(L, tt);	a = tt; tt = 0;
			lua_pushnumber(L, xm[tt++]);
			lua_pushnumber(L, xm[tt++]);
			lua_pushnumber(L, xm[tt++]);
			lua_pushnumber(L, xm[tt++]);
			lua_pushnumber(L, xm[tt++]);
			lua_pushnumber(L, xm[tt++]);
			tt = 7;
			if (argc == 7) {	//保存文件
				str = luaL_checkstring(L, 7);
				if (str) {
					char buf[255] = { 0 };
					genbmpfilename(buf, (char*)str, "_crop");
					bitmap_image bm(xm[4] + 1, xm[5] + 1);
					bm.clear();
					rgb_t  color = { 255, 255, 255 };
					for (int i = 0; i <= a; i++) {
						bm.set_pixel(xa[i] - xm[0], xb[i] - xm[1], color);
					}
					bm.save_image(buf);
				}
			}
		}
	}
	if (tt == 1) {
		lua_pushnumber(L, 0);
	}
	return tt;
}
//将大漠数据生成到位图数据
extern "C" int uitls_BitmapFile(lua_State* L)
{
	int x, tt;
	const char *str;
	const char *filename;
	int argc = lua_gettop(L);
	tt = 1;
	if (argc >= 3) {
		int *xa, *xb,i,cnt,dx,dy;
		int xm[10] = { 0 };
		char buf[255] = { 0 };
		x = luaL_checkinteger(L, 1);
		str = luaL_checkstring(L, 2);
		filename = luaL_checkstring(L, 3);
		if (argc == 5) {
			dx = luaL_checkinteger(L, 4);
			dy = luaL_checkinteger(L, 5);
		}
		xa = NULL; xb = NULL;
		tt = dm_binaryParse(x, str, xm, &xa, &xb);
		if (tt > 0) {
			//存为位图，将识别的区域存即可，不用存整个
			bitmap_image bm(xm[4]+1, xm[5]+1);
			bm.clear();
			rgb_t  color = {255, 255, 255};
			for (i = 0; i < x; i++) {
				bm.set_pixel(xa[i]-xm[0], xb[i]-xm[1], color);
			}
			genbmpfilename(buf, (char*)filename, "");
			bm.save_image(buf);
			if (argc == 5  ) {
				genbmpfilename(buf, (char*)filename, "_");
				rgb_t  color2 = {255, 0, 0}; 
				bm.set_pixel(dx - xm[0], dy - xm[1], color2);
				bm.save_image(buf);
			}
		}
		if (xa) free(xa);
		if (xb) free(xb);
		xa = NULL; xb = NULL;
	}
	if (tt == 1) {
		lua_pushnumber(L, 0);
	}
	return tt;
}
extern "C" CDMMouseKey* get_MouseKey(lua_State* L) {
	CDMMouseKey *mk = NULL;
	int num;
	lua_getglobal(L, "numofindex");
	num = lua_tointeger(L, -1);
	if (1) {
		tmfactory *fac; fac = tmfactory::Instance();
		tmVirMachine *mac = fac->getMachine(num);
		if (mac->vmState == tmVirMachine::tmRunning)
		{
			mk = (CDMMouseKey *)mac->mk;
			if (mk && mk->IsBind()) return mk;
		}
		else mk = (CDMMouseKey*)CDMMouseKey::Instance();
	}
	return mk;
}

extern "C" int dm_xbind(lua_State* L) {
	int x, y;
	int argc = lua_gettop(L);
	x = 0;
	if (argc >= 1) {
		TCHAR buf2[MAX_PATH];
		HWND h;
		x = luaL_checkinteger(L, 1);
		CDMMouseKey *mk = get_MouseKey(L);

		h = ::FindWindow(_T("LDPlayerMainFrame"), NULL);
		if (h != NULL) {
			GetWindowText(h, buf2, MAX_PATH);
			TCHAR ss[32];
			sprintf(ss, _T("-%d"), x);
			if (_tcsstr(buf2, ss)) {		//存在模拟器的
				h = GetSubWindowClass(h, _T("RenderWindow"));
				if (mk->IsValiedHWnd() == 0 ) {
					if (h == mk->hWnd) x = 2;		//已经绑定过了
					else x = mk->BindWindow((long)h, "gdi2", "windows", "windows", 0);
				}
				else x = 0;
			}
		}
		else x = 0;
	}
	lua_pushnumber(L, x);
	return 1;
}
extern "C" int dm_moveto(lua_State* L) {
	int x, y;
	int argc = lua_gettop(L);
	if (argc >= 2) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk)mk->dm.MoveTo(x, y);
	}
	return 1;
}
extern "C" int dm_leftclick(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if(mk)mk->dm.LeftClick();
	}
	return 1;
}
extern "C" int dm_WheelDown(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk)mk->dm.WheelDown();
	}
	return 1;
}
extern "C" int dm_WheelUp(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		mk->dm.WheelUp();
	}
	return 1;
}
extern "C" int dm_LeftDown(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk)mk->dm.LeftDown();
	}
	return 1;
}
extern "C" int dm_LeftDoubleClick(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk)mk->dm.LeftDoubleClick();
	}
	return 1;
}
extern "C" int dm_LeftUp(lua_State* L) {
	int argc = lua_gettop(L);
	if (argc == 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk)mk->dm.LeftUp();
	}
	return 1;
}
extern "C" int dm_KeyPressChar(lua_State* L)
{
	const char *str;
	int argc = lua_gettop(L);
	if (argc >= 1) {
		str = luaL_checkstring(L, 1);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			mk->dm.KeyPressChar(str);
			lua_pushstring(L, "1");
		}
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_KeyPressChars(lua_State* L)
{
	const char *str;
	int x,len,i;
	int argc = lua_gettop(L);
	if (argc >= 2) {
		str = luaL_checkstring(L, 1);
		x = luaL_checkinteger(L, 2);
		CDMMouseKey *mk = get_MouseKey(L);
		if (str &&mk) {
			len = strlen(str);
			char buf[8] = { 0 };
			for (i = 0; i < len; i++) {
				buf[0] = str[i];
				if (str[i] == '@') {
					mk->dm.KeyDown(16);
					Sleep(20);
//					mk->dm.KeyPress(50);
					mk->dm.KeyDownChar("2");
					Sleep(10);
					mk->dm.KeyUpChar("2");
					Sleep(20);
					mk->dm.KeyUp(16);
//					mk->dm.KeyPress(64);
				}
				else mk->dm.KeyPressChar(buf);
				Sleep(x);
			}
		}
		lua_pushstring(L, "1");
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_KeyPressStr(lua_State* L)
{
	int x;
	const char *str;
	int argc = lua_gettop(L);
	if (argc >= 2) {
		x = luaL_checkinteger(L, 2);
		str = luaL_checkstring(L, 1);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			x = mk->dm.KeyPressStr(str, x);
			lua_pushinteger(L, x);
		}
	}
	else {
		lua_pushinteger(L, 0);
	}
	return 1;
}
extern "C" int dm_Vers(lua_State* L)
{
	int argc = lua_gettop(L);
	if (argc >= 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			CString str = mk->dm.Ver();
			lua_pushstring(L, str.LockBuffer());
		}
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_Path(lua_State* L)
{
	int argc = lua_gettop(L);
	if (argc >= 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			CString str = mk->dm.GetBasePath();
			lua_pushstring(L, str.LockBuffer());
		}
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_BindCount(lua_State* L)
{
	int argc = lua_gettop(L);
	if (argc >= 0) {
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			long count = mk->dm.GetDmCount();
			lua_pushinteger(L, count);
		}
	}
	else {
		lua_pushinteger(L, 0);
	}
	return 1;
}
extern "C" int dm_ocr(lua_State* L)
{
	int x, y, x2, y2;
	const char *color;
	const char *dict;
	CString s;
	double fval;
	int argc = lua_gettop(L);
	if (argc >= 6) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		color = luaL_checkstring(L, 5);
		fval = luaL_checknumber(L, 6);
		dict = luaL_checkstring(L, 7);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			mk->dm.SetDict(0, dict);
			//第二个参数是大小
			if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
				s = mk->dm.Ocr(x, y, x + x2, y + y2, color, fval);
			}
			else s = mk->dm.Ocr(x, y, x2, y2, color, fval);
			lua_pushstring(L, s.LockBuffer());
		}
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_FindPic(lua_State* L)
{
	int x, y, x2, y2, dir, ret;
	const char *color;
	const char *picname;
	CString s;
	double fval;
	int argc = lua_gettop(L);
	VARIANT va, vb;
	if (argc >= 8) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		picname = luaL_checkstring(L, 5);
		color = luaL_checkstring(L, 6);
		fval = luaL_checknumber(L, 7);
		dir = luaL_checkinteger(L, 8);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			//第二个参数是大小
			if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
				ret = mk->dm.FindPic(x, y, x + x2, y + y2, picname, color, fval, dir, &va, &vb);
			}
			else ret = mk->dm.FindPic(x, y, x2, y2, picname, color, fval, dir, &va, &vb);
			lua_pushnumber(L, ret);
			lua_pushnumber(L, va.intVal);
			lua_pushnumber(L, vb.intVal);
			return 3;
		}
	}
	else {
		lua_pushnumber(L, -1);
	}
	return 1;
}
extern "C" int dm_GetResultCount(lua_State* L)
{
	int x;
	const char *str;
	int argc = lua_gettop(L);
	if (argc >= 1) {
		str = luaL_checkstring(L, 1);
		CDMMouseKey *mk = get_MouseKey(L);
		if (mk) {
			x = mk->dm.GetResultCount(str);
			lua_pushnumber(L, 1);
		}
	}
	else {
		lua_pushnumber(L, 0);
	}
	return 1;
}
int dm_FindColorEx(lua_State* L)
{
	int x, y, x2, y2, dir, ret;
	const char *color;
	CString s;
	double fval;
	int argc = lua_gettop(L);
	VARIANT va, vb;
	if (argc >= 7) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		color = luaL_checkstring(L, 5);
		fval = luaL_checknumber(L, 6);
		dir = luaL_checkinteger(L, 7);
		CDMMouseKey *mk = get_MouseKey(L);

		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			s = mk->dm.FindColorEx(x, y, x + x2, y + y2, color, fval, dir);
		}
		else s = mk->dm.FindColorEx(x, y, x2, y2, color, fval, dir);
		x = mk->dm.GetResultCount(s.LockBuffer());
		VARIANT va, vb;
		if(x >0)
			mk->dm.GetResultPos(s.LockBuffer(), 0, &va, &vb);
		lua_pushnumber(L, x);
		lua_pushstring(L, s.LockBuffer());
	}
	else {
		lua_pushnumber(L, 0);
		lua_pushstring(L, "");
	}
	return 2;
}
int dm_GetColorNum(lua_State* L)
{
	int x, y, x2, y2, dir, ret;
	const char *color;
	CString s;
	double fval;
	int argc = lua_gettop(L);
	VARIANT va, vb;
	if (argc >= 6) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		color = luaL_checkstring(L, 5);
		fval = luaL_checknumber(L, 6);
		CDMMouseKey *mk = get_MouseKey(L);

		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			x = mk->dm.GetColorNum(x, y, x + x2, y + y2, color, fval);
		}
		else x = mk->dm.GetColorNum(x, y, x2, y2, color, fval);
		
		lua_pushnumber(L, x);
	}
	else {
		lua_pushnumber(L, 0);
	}
	return 1;
}
int dm_IsDisplayDead(lua_State* L)
{
	int x, y, x2, y2, dir, ret;
	int argc = lua_gettop(L);
	if (argc >= 5) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		dir = luaL_checkinteger(L, 5);
		CDMMouseKey *mk = get_MouseKey(L);
		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			x = mk->dm.IsDisplayDead(x, y, x + x2, y + y2,dir);
		}
		else x = mk->dm.IsDisplayDead(x, y, x2, y2,dir);
		lua_pushnumber(L, x);
	}else {
		lua_pushnumber(L, 0);
	}
	return 1;
}
extern "C" int dm_OcrInFile(lua_State* L)
{
	int x, y, x2, y2, dir, ret;
	const char *color;
	const char *picname;
	CString s;
	double fval;
	int argc = lua_gettop(L);
	VARIANT va, vb;
	if (argc >= 7) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		picname = luaL_checkstring(L, 5);
		color = luaL_checkstring(L, 6);
		fval = luaL_checknumber(L, 7);
		CDMMouseKey *mk = get_MouseKey(L);
		CString str;
		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			str = mk->dm.OcrInFile(x, y, x + x2, y + y2, picname, color, fval);
		}
		else str = mk->dm.OcrInFile(x, y, x2, y2, picname, color, fval);
		lua_pushstring(L, str.LockBuffer());
		return 1;
	}
	else {
		lua_pushstring(L, "");
	}
	return 1;
}
extern "C" int dm_Capture(lua_State* L)
{
	int x, y, x2, y2, dir;
	const char *picname;
	int argc = lua_gettop(L);
	if (argc >= 5) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		picname = luaL_checkstring(L, 5);
		CDMMouseKey *mk = get_MouseKey(L);
		char buf[255] = { 0 };
#ifdef _DEBUG
		strcpy(buf, "D:\\testgame\\tm\\bm\\");
		strcat(buf, picname);
#else
		strcpy(buf, picname);
#endif
		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			dir = mk->dm.Capture(x, y, x + x2, y + y2, buf);
		}
		else dir = mk->dm.Capture(x, y, x2, y2, buf);
		lua_pushinteger(L, dir);
		return 1;
	}
	else {
		lua_pushinteger(L, 0);
	}
	return 1;
}
extern "C" int dm_CaptureGif(lua_State* L)
{
	int x, y, x2, y2, dir, delay, dur;
	const char *picname;
	int argc = lua_gettop(L);
	if (argc >= 7) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		picname = luaL_checkstring(L, 5);
		delay = luaL_checkinteger(L, 6);
		dur = luaL_checkinteger(L, 7);
		CDMMouseKey *mk = get_MouseKey(L);
		char buf[255] = { 0 };
#ifdef _DEBUG
		strcpy(buf, "D:\\testgame\\tm\\bm\\");
		strcat(buf, picname);
#else
		strcpy(buf, picname);
#endif
		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			dir = mk->dm.CaptureGif(x, y, x + x2, y + y2, buf, delay, dur);
		}
		else dir = mk->dm.CaptureGif(x, y, x2, y2, buf,delay, dur);
		lua_pushinteger(L, dir);
		return 1;
	}
	else {
		lua_pushinteger(L, 0);
	}
	return 1;
}
extern "C" int dm_CaptureJpg(lua_State* L)
{
	int x, y, x2, y2, dir;
	const char *picname;
	float quality;
	int argc = lua_gettop(L);
	if (argc >= 6) {
		x = luaL_checkinteger(L, 1);
		y = luaL_checkinteger(L, 2);
		x2 = luaL_checkinteger(L, 3);
		y2 = luaL_checkinteger(L, 4);
		picname = luaL_checkstring(L, 5);
		quality = luaL_checknumber(L, 6);
		CDMMouseKey *mk = get_MouseKey(L);
		char buf[255] = { 0 };
#ifdef _DEBUG
		strcpy(buf, "D:\\testgame\\tm\\bm\\");
		strcat(buf, picname);
#else
		strcpy(buf, picname);
#endif
		//第二个参数是大小
		if (x2 < x || y2 < y || x2 - x < 4 || y2 - y < 4) {
			dir = mk->dm.CaptureJpg(x, y, x + x2, y + y2, buf,quality);
		}
		else dir = mk->dm.CaptureJpg(x, y, x2, y2, buf,quality);
		lua_pushinteger(L, dir);
		return 1;
	}
	else {
		lua_pushinteger(L, 0);
	}
	return 1;
}
int dm_KillProc(lua_State* L) {
	KillProcess(_T("LDPlayerMainFrame"), NULL);
	return 1;
}
int lua_regFunction(lua_State* L)
{
	lua_register(L, "xbind", dm_xbind);
	
	lua_register(L, "ocr", dm_ocr);
	lua_register(L, "FindPic", dm_FindPic);
	lua_register(L, "FindColorEx", dm_FindColorEx);
	lua_register(L, "GetColorNum", dm_GetColorNum);
	lua_register(L, "MoveTo", dm_moveto);
	lua_register(L, "WheelUp", dm_WheelUp);
	lua_register(L, "WheelDown", dm_WheelDown);
	lua_register(L, "LeftClick", dm_leftclick);
	lua_register(L, "LeftDown", dm_LeftDown);
	lua_register(L, "LeftUp", dm_LeftUp);
	lua_register(L, "LeftDoubleClick", dm_LeftDoubleClick);
	lua_register(L, "KeyPressChar", dm_KeyPressChar);
	lua_register(L, "KeyPressChars", dm_KeyPressChars);
	lua_register(L, "KeyPressStr", dm_KeyPressStr);
	lua_register(L, "GetResultCount", dm_GetResultCount);
	lua_register(L, "bindCount", dm_BindCount);
	lua_register(L, "IsDisplayDead", dm_IsDisplayDead);
	lua_register(L, "bdPath", dm_Path);
	lua_register(L, "bdVers", dm_Vers);

	lua_register(L, "OcrInFile", dm_OcrInFile);
    
    lua_register(L, "qmeu", dm_runqemu);

	lua_register(L, "Capture", dm_Capture);
	lua_register(L, "CaptureGif", dm_CaptureGif);
	lua_register(L, "CaptureJpg", dm_CaptureJpg);
    
	lua_register(L, "CropRect", uitls_CropRect);	//找颜色，去掉某个巨区域后的颜色

    lua_register(L, "ColorCenter", uitls_Center);
    lua_register(L, "ColorHist", uitls_ColorHist);  //返回直方图的情况，x,y轴集中度
    lua_register(L, "Binary", uitls_Binary);
    lua_register(L, "BitmapFile", uitls_BitmapFile);
    lua_register(L, "KillProc", dm_KillProc);
    lua_register(L, "sleep", os_sleep);
    lua_register(L, "AppPath", os_AppPath);
	return 0;
}
