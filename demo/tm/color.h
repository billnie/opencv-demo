/*

*/

/* Define 'chdir' for systems that do not implement it */
#ifndef COLOR_H
#define COLOR_H

#ifdef __cplusplus
extern "C" {
#endif
    
/**
  
 @param x 
 @param xm <#xm description#>
 @param xa <#xa description#>
 @param xb <#xb description#>
 @param left <#left description#>
 @param top <#top description#>
 @param right <#right description#>
 @param bottom <#bottom description#>
 @return <#return value description#>
 */
int dm_pixelCrop(int x,  int *xm, int *xa, int *xb,int left, int top, int right, int bottom);
int dm_binaryParse(int x, const char *str, int *xm, int **xa, int **xb);
    //返回x轴分布和y轴健在的情况,x为点数量，a,b为x轴非空数量，及y轴非空数量
    int dm_histogramDist(int x, const char *str,int *a,int *b,int *c, int *d);
    //求二值图的直方图, x为数据数量，xm为数据描述，xa,xb为数据，ha,hb为生成的直方数据
    int dm_histogram(int x, int *xm, int *xa, int *xb, int **ha, int **hb);
#ifdef __cplusplus
}
#endif

#endif
