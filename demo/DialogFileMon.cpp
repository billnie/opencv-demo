// DialogFileMon.cpp : 实现文件
//

#include "stdafx.h"
#include "demo.h"
#include "DialogFileMon.h"
#include "afxdialogex.h"
#include "BrowseFolder.h"
namespace aui {

	// CDialogFileMon 对话框

	IMPLEMENT_DYNAMIC(CDialogFileMon, CDialogEx)

		CDialogFileMon::CDialogFileMon(CWnd* pParent /*=NULL*/)
		: CDialogEx(IDD_DLG_MONFILE, pParent)
	{

	}

	CDialogFileMon::~CDialogFileMon()
	{

	}

	void CDialogFileMon::DoDataExchange(CDataExchange* pDX)
	{
		CDialogEx::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_RICHEDIT21, m_richEdit);
	}


	BEGIN_MESSAGE_MAP(CDialogFileMon, CDialogEx)
		ON_WM_DESTROY()
		ON_BN_CLICKED(IDC_BTN_CLEAR, &CDialogFileMon::OnBnClickedBtnClear)
		ON_BN_CLICKED(IDC_BTN_START, &CDialogFileMon::OnBnClickedBtnStart)
		ON_BN_CLICKED(IDC_BTN_DIR, &CDialogFileMon::OnSelectDir)
		ON_WM_SIZE()
	END_MESSAGE_MAP()


	BOOL CDialogFileMon::OnInitDialog()
	{
		CDialogEx::OnInitDialog();
	
		m_bAddNew0 = (::GetPrivateProfileInt("Selections", "m_bAddNew", 0, ".\\Settings.ini") == 0 ? false : true);
		m_bDel0 = (::GetPrivateProfileInt("Selections", "m_bDel", 0, ".\\Settings.ini") == 0 ? false : true);
		m_bRename0 = (::GetPrivateProfileInt("Selections", "m_bRename", 0, ".\\Settings.ini") == 0 ? false : true);
		m_bModify0 = (::GetPrivateProfileInt("Selections", "m_bModify", 0, ".\\Settings.ini") == 0 ? false : true);
		m_bOther0 = (::GetPrivateProfileInt("Selections", "m_bOther", 0, ".\\Settings.ini") == 0 ? false : true);


		/*------------------------------------------------------*/
		char buf[MAX_PATH];
		::GetPrivateProfileString("Settings",
			"监视目录",
			"",
			buf,
			MAX_PATH, ".\\Settings.ini");
		strcpy((LPTSTR)(LPCTSTR)m_strWatchedDir, buf);
		/*------------------------------------------------------*/
		m_hThread = NULL;
		hDir = INVALID_HANDLE_VALUE;
		m_i = 0;
		SetDlgItemText(IDC_STA_DIR, m_strWatchedDir);
		return TRUE;  // return TRUE unless you set the focus to a control
					  // 异常: OCX 属性页应返回 FALSE
	}


	void CDialogFileMon::OnDestroy()
	{
		CDialogEx::OnDestroy();
	}
	int CDialogFileMon::addrichLog(LPTSTR title, LPTSTR message)
	{
		m_richEdit.ReplaceSel(_T("\r\n"));
		SYSTEMTIME sTime;
		GetLocalTime(&sTime);
		TCHAR chBuf[100];
		//wsprintf(chBuf,_T("%u/%u/%u %u:%u:%u:%u %d\r\n"),sTime.wYear, sTime.wMonth, sTime.wDay,sTime.wHour, sTime.wMinute, sTime.wSecond,
		//	sTime.wMilliseconds,sTime.wDayOfWeek);
		wsprintf(chBuf, _T("%02u:%02u:%02u-->"), sTime.wHour, sTime.wMinute, sTime.wSecond,
			sTime.wMilliseconds);
		m_richEdit.ReplaceSel((LPCTSTR)chBuf);
		if (title && strlen(title) > 0)
		{
			m_richEdit.ReplaceSel((LPCTSTR)title);
			m_richEdit.ReplaceSel((LPCTSTR)_T(" :"));
		}
		if (message && strlen(message) > 0)
			m_richEdit.ReplaceSel((LPCTSTR)message);
		m_richEdit.PostMessage(WM_VSCROLL, SB_BOTTOM, 0);
		return 0;
	}
	BOOL CDialogFileMon::StartWatch(CString path)
	{
		m_strWatchedDir = path;

		DWORD ThreadId;  //创建一个新线程用于监视
		m_hThread = ::CreateThread(NULL, 0, ThreadProc, this, 0, &ThreadId);

		return NULL != m_hThread;
	}


	void CDialogFileMon::OnSelectDir()
	{
		CBrowseFolder Dlg;
		if (Dlg.DoModal(this, NULL) == IDOK)
		{
			m_strWatchedDir = Dlg.GetDirPath();
			::WritePrivateProfileString("Settings",
				"监视目录",
				m_strWatchedDir,
				".\\Settings.ini");
			SetDlgItemText(IDC_STA_DIR, m_strWatchedDir);

		}
	}
	/*----------------------线程函数------------------------------------*/
	DWORD WINAPI CDialogFileMon::ThreadProc(LPVOID lParam)  //线程函数
	{
		CDialogFileMon* obj = (CDialogFileMon*)lParam;  //参数转化

		obj->hDir = CreateFile(            //打开目录，得到目录的句柄
			obj->m_strWatchedDir,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_BACKUP_SEMANTICS,
			NULL
			);
		if (obj->hDir == INVALID_HANDLE_VALUE)
			return false;
		string mess;
		char buf[(sizeof(FILE_NOTIFY_INFORMATION) + MAX_PATH) * 2];
		FILE_NOTIFY_INFORMATION* pNotify = (FILE_NOTIFY_INFORMATION*)buf;
		DWORD dwBytesReturned;
		while (true)
		{
			if (::ReadDirectoryChangesW(obj->hDir,
				pNotify,
				sizeof(buf),
				true,
				FILE_NOTIFY_CHANGE_FILE_NAME |
				FILE_NOTIFY_CHANGE_DIR_NAME |
				FILE_NOTIFY_CHANGE_ATTRIBUTES |
				FILE_NOTIFY_CHANGE_SIZE |
				FILE_NOTIFY_CHANGE_LAST_WRITE |
				FILE_NOTIFY_CHANGE_LAST_ACCESS |
				FILE_NOTIFY_CHANGE_CREATION |
				FILE_NOTIFY_CHANGE_SECURITY,
				&dwBytesReturned,
				NULL,
				NULL))
			{
				char tmp[MAX_PATH], str1[MAX_PATH], str2[MAX_PATH];
				memset(tmp, 0, sizeof(tmp)); //ZeroMemory()
				WideCharToMultiByte(CP_ACP, 0, pNotify->FileName, pNotify->FileNameLength / 2, tmp, 99, NULL, NULL);
				strcpy(str1, tmp);

				if (pNotify->NextEntryOffset != 0)
				{
					PFILE_NOTIFY_INFORMATION p = (PFILE_NOTIFY_INFORMATION)((char*)pNotify + pNotify->NextEntryOffset);
					memset(tmp, 0, sizeof(tmp));
					WideCharToMultiByte(CP_ACP, 0, p->FileName, p->FileNameLength / 2, tmp, 99, NULL, NULL);
					strcpy(str2, tmp);
				}
				switch (pNotify->Action)
				{
				case FILE_ACTION_ADDED:
					if (obj->m_bAddNew0)
					{
						mess = "新文件";
						//CTime tt = CTime::GetCurrentTime();
						//CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "添加了新文件");
						//obj->m_list.SetItemText(0, 3, str1);
						//obj->m_list.SetItemText(0, 1, strTT);
						//		PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;

				case FILE_ACTION_REMOVED:
					if (obj->m_bDel0)
					{
						mess = "删文件";
						//CTime tt = CTime::GetCurrentTime();
						//CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "删除了文件");
						//obj->m_list.SetItemText(0, 3, str1);
						//obj->m_list.SetItemText(0, 1, strTT);
						//		PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;

				case FILE_ACTION_RENAMED_NEW_NAME:
					if (obj->m_bRename0)
					{
						mess = "重命名文件";
						CTime tt = CTime::GetCurrentTime();
						CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "重命名了文件");
						//strcat(str1, "->");
						//obj->m_list.SetItemText(0, 3, strcat(str1, str2));
						//obj->m_list.SetItemText(0, 1, strTT);
						//			PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;

				case FILE_ACTION_RENAMED_OLD_NAME:
					if (obj->m_bRename0)
					{
						mess = "重命名文件";
						//CTime tt = CTime::GetCurrentTime();
						//CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "重命名了文件");
						//strcat(str1, " 改名为 ");
						//obj->m_list.SetItemText(0, 3, strcat(str1, str2));
						//obj->m_list.SetItemText(0, 1, strTT);
						//			PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;

				case FILE_ACTION_MODIFIED:
					if (obj->m_bModify0)
					{
						mess = "修改文件";
						//CTime tt = CTime::GetCurrentTime();
						//CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "修改了文件");
						//obj->m_list.SetItemText(0, 3, str1);
						//obj->m_list.SetItemText(0, 1, strTT);
						//		PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;

				default:
					if (obj->m_bOther0)
					{
						mess = "未知";
						//CTime tt = CTime::GetCurrentTime();
						//CString strTT;
						//strTT.Format("%d:%d:%d", tt.GetHour(), tt.GetMinute(), tt.GetSecond());
						//obj->m_list.InsertItem(0, obj->m_szi);
						//obj->m_list.SetItemText(0, 2, "未知变化");
						//obj->m_list.SetItemText(0, 3, "");
						//obj->m_list.SetItemText(0, 1, strTT);

						//		PlaySound(MAKEINTRESOURCE(IDR_WAVE1),AfxGetResourceHandle(),SND_RESOURCE|SND_PURGE|SND_NODEFAULT); 
					}
					break;
				}
				obj->addrichLog((LPTSTR)mess.c_str(), str1);
	//			obj->m_i++;
	//			itoa(obj->m_i, obj->m_szi, 10);
				//AfxBeginThread(SoundThread,0);			
			}
			else
				break;
		}
		return 0;
	}
}

void aui::CDialogFileMon::OnBnClickedBtnStart()
{
	CString str;
	GetDlgItemText(IDC_BTN_START, str);
	if (str.Compare(_T("start") )== 0) {
		this->StartWatch(m_strWatchedDir);
		SetDlgItemText(IDC_BTN_START, "stop");
	}
	else {
		if (m_hThread != NULL)
		{
			::TerminateThread(m_hThread, 0);
			m_hThread = NULL;
		}
		if (hDir != INVALID_HANDLE_VALUE)
		{
			CloseHandle(hDir);
			hDir = INVALID_HANDLE_VALUE;
		}
		SetDlgItemText(IDC_BTN_START, "start");
	}
}

void aui::CDialogFileMon::OnBnClickedBtnClear()
{
	m_richEdit.SetFocus();
	m_richEdit.SetSel(0, -1);
	m_richEdit.ReplaceSel("");
	m_richEdit.Clear();
}


void aui::CDialogFileMon::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	CRect r, rc, rd, r1;
	GetClientRect(rc);

	if (m_richEdit.GetSafeHwnd()) {
		m_richEdit.MoveWindow(rc.left, rc.top + 30, rc.Width(), rc.Height() -35);
	}
}