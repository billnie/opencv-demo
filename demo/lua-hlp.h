#ifndef LUA_HLP_H
#define LUA_HLP_H
#include	"windows.h"


#ifdef __cplusplus
extern "C" {
#endif


	int execlua2(int v1, int v2, LPCTSTR func, LPCTSTR file, long *lret);
	int execlua3(LPCTSTR s1, int v2, LPCTSTR func, LPCTSTR file, long *lret);
	//返回串的
	int execlua4(int v1, int v2, LPCTSTR func, LPCTSTR file, LPTSTR str);
	int execlua5(LPCTSTR s1, int v1, int v2, LPCTSTR func, LPCTSTR file, long *lret);
	//返回识别字体的函数
	int execluaocr(LPCTSTR func, LPCTSTR file, LPTSTR str);

	//解析参数
	int parseocrstring(LPCTSTR str, int *x, int *y, int *w, int *h, LPTSTR color, float *val);

#ifdef __cplusplus
}
#endif
#endif