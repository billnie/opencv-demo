// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACEBTN_468B4BA8032C_INCLUDED
#define _INC_CFACEBTN_468B4BA8032C_INCLUDED

#include "CFaceObj.h"



class CFaceDeal;

class CImage;

//��ť�����ࡣ
//##ModelId=4729DEB10338
class CFaceBtn 
: public CFaceObj
{
protected:
	//##ModelId=4729DEB10343
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd* pWnd);

	//##ModelId=4729DEB10356
	virtual bool OnDeActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB10360
	virtual bool OnActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB10374
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB1037F
	virtual bool OnLButtonDown(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB1038A
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB1039C
	virtual bool OnMouseMove(UINT nFlag, CPoint* pt, CWnd * lpWnd);

public:
	//##ModelId=4729DEB103A7
	void SetCheck(int nCheck, CWnd *pWnd);

	//##ModelId=4729DEB103B1
	int GetCheck();

	//##ModelId=4729DEB103BB
	virtual void Draw(CDC* pDC, CImage* pImag, CRect * pRect = NULL);



	//##ModelId=4729DEB103C5
	virtual BOOL PtInMe(CPoint * pt);

	//##ModelId=4729DEB103CF
	virtual ~CFaceBtn();

	//##ModelId=4729DEB103D1
	CFaceBtn();



};

#endif /* _INC_CFACEBTN_468B4BA8032C_INCLUDED */
