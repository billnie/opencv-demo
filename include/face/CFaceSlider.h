// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACESLIDER_468B4BE501A5_INCLUDED
#define _INC_CFACESLIDER_468B4BE501A5_INCLUDED

#include "CFaceObj.h"



class CFaceDeal;

class CImage;

//滑动条处理类
//##ModelId=4729DEAF00AA
class CFaceSlider 
: public CFaceObj
{
	friend class CFaceDeal;
public:
	//##ModelId=4729DEAF00BF
	int SetPos(int nPos, CWnd * pWnd, int nPos2);
	//##ModelId=4729DEAF00CC
	int GetPos();


	//##ModelId=4729DEAF00D3
	virtual void Draw(CDC* pDC, CImage* pImag, CRect * pRect = NULL);

	//##ModelId=4695B9F301E4
	//滑块在外面的滑动条
	//##ModelId=4729DEAF00F1
	void DrawOuter(CDC *pDC, CImage *pImag, CRect *pRect = NULL);

	//##ModelId=4729DEAF010F
	void DrawVector(CDC *pDC, CImage *pImag, CRect *pRect = NULL);

	//##ModelId=4729DEAF019B
	virtual BOOL PtInMe(CPoint * pt);

	//##ModelId=4729DEAF01C3
	BOOL PtInMeOuter(CPoint *pt);
	//##ModelId=4729DEAF01E1
	virtual ~CFaceSlider();

	//##ModelId=4729DEAF01FF
	CFaceSlider();
protected:


	//##ModelId=4729DEAF0213
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd* pWnd);

	//##ModelId=4729DEAF0228
	virtual bool OnDeActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEAF023C
	virtual bool OnActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEAF0247
	virtual bool OnSpecialMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEAF0252
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF0263
	virtual bool OnLButtonDown(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF0277
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF0282
	virtual bool OnMouseMove(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF028E
	bool bHr;				//水平还是垂直滑块条

	//##ModelId=4729DEAF029F
	bool bFirst;
	//##ModelId=4729DEAF02A9
	bool bOut;				//滑块在外面的滑动条。
	//##ModelId=4729DEAF02B3
	int m_iPosition;		//滑块位置
	//##ModelId=4729DEAF02B4
	int m_iPosition2;		//滑块位置（这个是对应于特殊应用的，如时光平移的。
	//##ModelId=4729DEAF02BD
	CSize m_szRgn;			//滑动条的位置范围
	//##ModelId=4729DEAF02C7
	CRect m_RectFill;		
	//##ModelId=4729DEAF02C8
	CRect m_RectNonFill;
	//##ModelId=4729DEAF02D1
	CRect m_RectSolider;
	//##ModelId=4729DEAF02DB
	int m_iedge;			//滑动条的边缘保留位置

	
	//##ModelId=4729DEAF02DC
	CRect m_RectNewSolider;
};

#endif /* _INC_CFACESLIDER_468B4BE501A5_INCLUDED */
