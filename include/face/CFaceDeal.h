// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACEDEAL_468B4E190232_INCLUDED
#define _INC_CFACEDEAL_468B4E190232_INCLUDED

#include "Image.h"

#include "CFaceBgnd.h"

#include "XMLParse.h"

#include "CFaceObj.h"

#include "CFaceRgn.h"
#include <AFXCMN.H>
class CFaceObj;

class CXMLParse;

class CFaceSlider;
class CFaceBtn;
class CFaceText;

class CFaceLBox;

class CFaceTimer;
//皮肤管理类，作为与拥有皮肤的窗口对象交互。
//
//需要对控件进行操作时，可以向主皮肤拥有窗口发送消息，SendMess
//age()，消息到过 FaceMessage()后，会分发到各个控件的。
//
//控件如果要向外面发送消息，主要是向主窗口发送消息。如控件左键
//按下，等消息，通知主程序进行处理。
//##ModelId=4729DEB002FB
class CFaceDeal 
{
protected:
	//读取定时器按钮信息
	//##ModelId=472B38C20185
	bool ReadTimer(CXMLParse *pXml);

	//主窗口发过来的滑动条消息，处理将会在控件中搜索与之名称相
	//对应的控件，然后进行消息处理。
	//##ModelId=4729DEB00319
	long FaceSliderMessage(WPARAM wParam, LPARAM lParam);

	//主窗口发过来的按钮消息，处理将会在控件中搜索与之名称相对
	//应的控件，然后进行消息处理。
	//##ModelId=4729DEB00323
	long FaceBtnMessage(WPARAM wParam, LPARAM lParam);

	//##ModelId=473F22F9020C
	long FaceTextMessage(WPARAM wParam, LPARAM lParam);
public:
	//主窗口发过来的列表框消息，处理将会在控件中搜索与之名称相
	//对应的控件，然后进行消息处理。
	//##ModelId=4729DEB0032D
	long FaceLBOXMessage(WPARAM wParam, LPARAM lParam);


private:
	//是否抓住鼠标
	//##ModelId=4729DEB00337
	bool bCapMouse;
	//是否按下鼠标左键
	//##ModelId=4729DEB00338
	bool bLDown;

	//##ModelId=4729DEB0034B
	CFaceObj* m_curFaceObj;

	//##ModelId=4729DEB00356
	CToolTipCtrl m_ToolTip;

	//##ModelId=4729DEB00360
	CWnd * m_lpWnd;


public:
	//读取文件显示控件
	//##ModelId=472BC2E00197
	void ReadText(CXMLParse* pXml);

	//窗口拥有者发过来的消息，这个函数将会将消息分发给FaceXXXM
	//essage
	//##ModelId=4729DEB00369
	long FaceMessage(WPARAM wParam, LPARAM lParam);


	//##ModelId=4729DEB00373
	long SetSliderPos(int nPos, LPCTSTR sname);

	//##ModelId=4729DEB0037E
	long SetSliderRange(int nMin, int nMax, LPCTSTR sname);

	//从窗口所拥有的控件中查找与名称相同的对象，并且返回对象指
	//针。
	//##ModelId=4729DEB00389
	CFaceObj * FindObj(LPCTSTR sname);

	//##ModelId=4729DEB0039B
	void SetButtonStatus(int nstatus, LPCTSTR sname);

	//查找落在某个点的对象
	//##ModelId=4729DEB003A5
	CFaceRgn * FindRgn(CPoint* pt);

	//从配置文件读取创建列表框控件。
	//##ModelId=4729DEB003AF
	bool ReadLBox(CXMLParse* pXml);

	//从配置文件中读取创建滑动条控件。
	//##ModelId=4729DEB003B9
	bool ReadSliders(CXMLParse * pXml);

	//##ModelId=4729DEB003C3
	bool PreTranslateMessage(MSG * pMsg);

	//##ModelId=4729DEB003D7
	void SetToolTipText(LPCTSTR sText);

	//##ModelId=4729DEB003E1
	void CreateTips(CWnd * pWnd);

	//##ModelId=4729DEB1000D
	CRect m_rctbgnd;

	//计算控件在新的
	//##ModelId=4729DEB10017
	void CalcObjRgn(int init = 1);

	//从配置文件中读取按钮控件。
	//##ModelId=4729DEB1002B
	bool ReadObjs(CXMLParse* pXml);

	//##ModelId=4729DEB1003F
	void CalcRgn(LPRECT  pRect);

	//绘制窗口图片，刷新窗口时调用这个方法。
	//##ModelId=4729DEB1005D
	void Draw(CDC * pDC, CRect * rect);

	//##ModelId=4729DEB1007B
	virtual ~CFaceDeal();

	//##ModelId=4729DEB10099
	CFaceDeal();

	//##ModelId=4729DEB100B7
	void Release();

	//从配置文件中读取区域信息。
	//##ModelId=4729DEB100D5
	bool ReadRgn(CXMLParse * pXml);

	//从文件中加载窗口中的控件配置文件，从而加载图片，创建控件
	//区域等。
	//##ModelId=4729DEB100FD
	bool LoadSkins(LPTSTR fname, LPTSTR fpath);

	//##ModelId=4729DEB1016D
	CFaceBgnd m_facebgnd;


	//##ModelId=4729DEB10181
	std::vector<CFaceObj *>m_FaceObjs;

	//##ModelId=4729DEB10196
	std::vector<CFaceRgn *>m_Rgns;

	//查找落在某个点的控件对象。
	//##ModelId=4729DEB1019E
	CFaceObj*  FindObj(CPoint* pt);

	//主窗口发过来的左键按下消息。
	//##ModelId=4729DEB101E4
	long DealLbDown(UINT uFlags, CPoint *pt);

	//主窗口发过来的左键弹起消息。
	//##ModelId=4729DEB1023E
	long DealLbUp(UINT uFlags, CPoint *pt);

	//主窗口发过来的右键按下消息。
	//##ModelId=4729DEB10284
	long DealRbDown();

	//主窗口发过来的鼠标移动消息。
	//##ModelId=4729DEB102B6
	long DealMouseMove(UINT uFlag, CPoint *pt);

	//##ModelId=4729DEB102F2
	bool LoadFile(LPTSTR fname);
	//##ModelId=4729DEB1031A
	BOOL SaveBitmapToFile(HBITMAP hBitmap, LPCTSTR lpFileName ) ;
};

#endif /* _INC_CFACEDEAL_468B4E190232_INCLUDED */
