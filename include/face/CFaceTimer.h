// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACEBTN_468B4BA8042C_INCLUDED
#define _INC_CFACEBTN_468B4BA8042C_INCLUDED

#include "CFaceObj.h"
#include <afxcmn.h>

#define MAX_TIMER 10

//时钟类型，分为时钟和计时器
//##ModelId=4729E99B0083

//时钟图片参数。
//##ModelId=472B36800206
typedef struct _tagBMPTimer
{
	int nWidth;		//单张图片宽度
	int nHeight;	//单张图片高度
	int nEmWidth;	//分时中间宽度
}BMPTimer;
class CFaceDeal;

class CImage;

//时中显示类。在这个类内自建一个定时器，自动更新时钟，也可以在
//刷新时更新时钟，也可以用来记录录像时间。
//外部可以启动时钟，关闭时钟，设置时钟图片。
//外部也可以将时钟类型设置为记录录像时间。
//##ModelId=4729DEAE0398
class CFaceTimer 
: public CFaceObj
{	
	friend class CFaceDeal;

private:
	//定时器类型
	//##ModelId=4729E90B0054
	TIMETYPE m_emType;
	//定时器状态
	//##ModelId=473F281401DB
	TIMERSTATUS m_emStatue;
	//时钟id
	//##ModelId=4729E3620263
	DWORD m_dwTimer;

	//图片文件名
	//##ModelId=4729E19A028F
	LPCTSTR m_strfname;

	//录像时钟
	//##ModelId=4729E1B000BA
	CTime m_Timer;

	//CTimeSpan
	//##ModelId=4729E1FC0114
	CTimeSpan m_timespan;

	//图片列表
	//##ModelId=4729E2C3008E
	CImageList m_imglist;

	//窗口指针
	//##ModelId=472B368002E4
	CWnd * m_pWnd;

	//时钟图片参数
	//##ModelId=472B36800384
	BMPTimer m_tParam;

	//当前有多少个定时器　
	//##ModelId=472B3680038D
	static int m_nUse;
	//最大定时器个数
	//##ModelId=472B368100E5
	static DWORD m_nNum[MAX_TIMER];
	//定时器静态指针
	//##ModelId=472B3681016A
	static CFaceTimer *m_obj[MAX_TIMER];
protected:
	//停止时钟，只需将时钟清0就可以了，然后设置时钟类型
	//##ModelId=473F273202B1
	int TimerStop();

	//##ModelId=4729E81B00B3
	static void CALLBACK TimerProc(HWND hwnd,UINT uMsg,UINT idEvent,DWORD dwTime);	
	
	//启动定时器
	//##ModelId=4729DFEC03E6
	DWORD SetTimer(int ntime = 1000);

	//关闭定时器。
	//##ModelId=4729E0AD034D
	void KillTimer();

	//显示时钟组合成的图片。
	//##ModelId=4729E0EC00EB
	void ShowTimer(CDC  *pDC, CPoint  *t);

	//##ModelId=4729E1340120
	bool SetTimerPic(LPCTSTR fname);

	//##ModelId=4729DEAE03AC
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd* pWnd);


	//##ModelId=4729DEAE03B7
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAE03C2
	virtual bool OnLButtonDown(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAE03CD
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAE03D7
	virtual bool OnMouseMove(UINT nFlag, CPoint* pt, CWnd * lpWnd);

public:
	//暂停时钟
	//##ModelId=472BB621000F
	int Pause();

	//继续计时
	//##ModelId=472BB66F0314
	int Play();

	//返回计时器状态，当前是暂停还是计时状态。
	//##ModelId=472BB6B40100
	int GetTimerStatus();

	//设置时钟类型
	//##ModelId=4729E85A038E
	void SetType(TIMETYPE emType);


	//##ModelId=4729DEAF0000
	virtual void Draw(CDC* pDC, CImage* pImag, CRect * pRect = NULL);



	//##ModelId=4729DEAF000A
	virtual BOOL PtInMe(CPoint * pt);

	//##ModelId=4729DEAF000D
	virtual ~CFaceTimer();

	//##ModelId=4729DEAF0015
	CFaceTimer();

};

#endif /* _INC_CFACEBTN_468B4BA8032C_INCLUDED */
