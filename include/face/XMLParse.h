#pragma once

#import "msxml.dll" 

//##ModelId=4729DEAE0052
class CXMLParse
{
public:
	//##ModelId=4729DEAE0077
	CXMLParse();
	//##ModelId=4729DEAE008B
	virtual ~CXMLParse();

public:
	//##ModelId=4729DEAE009F
	BOOL SetFieldValue(CString strField, CString strValue);
	//##ModelId=4729DEAE00C7
	CString GetFieldValue(CString strField);
	//##ModelId=4729DEAE00D1
	void SaveFile();						//Save to xml file
	//##ModelId=4729DEAE00DB
	BOOL OpenFile(CString &strXMLFile, CString strRootName, CString strSessionName = "");			//Open xml file
	//##ModelId=4729DEAE0103
	void SetSessionName(CString strName) {m_strSessionName = strName;}
	//##ModelId=4729DEAE010D
	BOOL Initialize();
	//##ModelId=4729DEAE0185
	void ClearAll();
	//##ModelId=4729DEAE018F
	BOOL CreateXMLFile(CString &strXML, CString &strFileName);
	//##ModelId=4729DEAE0199
	BOOL AddNewSession();
	//##ModelId=4729DEAE019A
	BOOL DeleteSession();
	//##ModelId=4729DEAE01A3
	BOOL AddNewNote(CString strNote);
	//##ModelId=4729DEAE01A5
	BOOL FindPointedSession(CString strNodeName, CString strValue);
	//##ModelId=4729DEAE01B7
	int  GetSessionNumber();
	//##ModelId=473F22F80138
	CString GetSessionVal();
	//##ModelId=473F22F80143
	bool SetSessionVal(CString strVal );
	//##ModelId=4729DEAE01B8
	BOOL GoToPointedSession(int nIndex);

private:
	//##ModelId=4729DEAE01BA
	CString m_strFileName;
	//##ModelId=4729DEAE0244
	CString m_strRootName;
	//##ModelId=4729DEAE024E
	CString m_strSessionName;
	//##ModelId=4729DEAE0276
	MSXML::IXMLDOMDocumentPtr m_pXMLDoc;
	//##ModelId=4729DEAE028B
	MSXML::IXMLDOMNodePtr m_pXMLCurNode;
};
