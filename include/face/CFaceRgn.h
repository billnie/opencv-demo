// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACERGN_468B4B88000F_INCLUDED
#define _INC_CFACERGN_468B4B88000F_INCLUDED



#include "CFaceObj.h"


class CXMLParse;

class CFaceDeal;

//皮肤区域管理
//##ModelId=4729DEAF02EF
class CFaceRgn 
{

	friend class CFaceDeal;
public:
	//窗口改变后，重新计算区域所在矩形大小。
	//不同类型区域计算方法不同
	//##ModelId=4729DEAF02F9
	void CalcRgn(CRect * pRect);

	//判断某点是否落在这个区域
	//##ModelId=4729DEAF02FB
	BOOL IsInRgn(CPoint * pt  );

	//判断该点是否落在某个控件上。如果是在，返回控件对像，否则
	//返回NULL
	//##ModelId=4729DEAF0304
	CFaceObj * IsFaceObj(CPoint* pt);

	//##ModelId=4729DEAF0317
	void CalcObjRgn(int init = 1);
	
	//##ModelId=4729DEAF0321
	void CalcFillRgn(CRect *pRect);
protected:
	//##ModelId=4729DEAF0323
	CRect m_RectNew;

	//##ModelId=4729DEAF032B
	CRect m_Rect;

	//##ModelId=4729DEAF0335
	TCHAR m_sRgname[32];

	//##ModelId=4729DEAF0354
	std::vector<CFaceObj *> m_Objs;
};

#endif /* _INC_CFACERGN_468B4B88000F_INCLUDED */
