// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACETEXT_468B4BD303C8_INCLUDED
#define _INC_CFACETEXT_468B4BD303C8_INCLUDED

#include "CFaceObj.h"


class CFaceDeal;
class MTimer;
class CImage;
#include "MTimer.h"
//文本控件，分为静态文件，闪烁文件，移动文件等多种形式。
//##ModelId=472BBBC20101
typedef enum _tagFACETEXTTYPE
{
	STATICTEXT = 0,		//静态文本
	FLASHTEXT,			//闪烁文件
	MOVETEXT,			//移动文本
}FACETEXTTYPE;
class CXMLParse;

//##ModelId=4729DEAF001E
class CFaceText 
: public CFaceObj
{
	friend class CFaceDeal;
private:
	//要显示的文件
	//##ModelId=472BBF7B03BE
	TCHAR m_strText[MAX_PATH];

	//文本类型
	//##ModelId=472BBC060037
	FACETEXTTYPE m_emTexttype;

protected:
	//##ModelId=472CB1000379
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd* pWnd);

	//##ModelId=472BBFAB00A6
	bool SetText(LPCTSTR str, DWORD dwlen, DWORD * dwret, CWnd *pwnd = NULL);

	//返回当前显示的文本
	//##ModelId=472BBFB803A8
	bool GetText(LPTSTR str, DWORD* dwret);

	//##ModelId=472BBD3F0177
	MTimer m_Timer;
	//##ModelId=472C9E0402F9
	COLORREF m_textColor;	//文字颜色

	//##ModelId=4729DEAF0029
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF0035
	virtual bool OnLButtonDown(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF0049
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEAF005A
	virtual bool OnMouseMove(UINT nFlag, CPoint* pt, CWnd * lpWnd);

public:

	//##ModelId=4729DEAF0071
	virtual void Draw(CDC* pDC, CImage* pImag, CRect* pRect = NULL);



	//##ModelId=4729DEAF007B
	virtual BOOL PtInMe(CPoint * pt);

	//##ModelId=4729DEAF0083
	virtual ~CFaceText();

	//##ModelId=4729DEAF0085
	CFaceText();

};

#endif /* _INC_CFACETEXT_468B4BD303C8_INCLUDED */
