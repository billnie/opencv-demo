// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACEBGND_468C8AFA029F_INCLUDED
#define _INC_CFACEBGND_468C8AFA029F_INCLUDED

//#include "CFaceObj.h"
#include	<vector>
#include "Image.h"

//##ModelId=4729DEB103D9
typedef struct _tagBgnd
{
	CImage *m_img;
	int		m_status;
	TCHAR stname[64];
}Bgnd, *LPBgnd;
class CXMLParse;

//皮肤背景图管理
//##ModelId=4729DEB20023
class CFaceBgnd 

{
public:
	//根据名称选择背景图片对应的CImage对象。
	//##ModelId=4729DEB2002D
	CImage * GetFaceBgnd(LPTSTR sBgndname);

	//获取缺少的背景图片对应的CImage 对像指针。
	//##ModelId=4729DEB2002F
	bool GetDefaultImg(CImage * *  pImag);

	//获取背景图片的大小矩形。
	//##ModelId=4729DEB20037
	void GetFaceRgn(CRect * pRect);

	//设置背景图片的路径。
	//##ModelId=4729DEB20039
	void SetResPath(LPTSTR spath);

	//从配置文件中读取背景图片到CImage对象数组中。
	//##ModelId=4729DEB20042
	bool ReadBgnd(CXMLParse* pXml);

	//##ModelId=4729DEB20057
	std::vector<Bgnd> m_imgs;

	//##ModelId=4729DEB20060
	virtual ~CFaceBgnd();

	//##ModelId=4729DEB20069
	CFaceBgnd();
private:
	//##ModelId=4729DEB2006B
	CImage * m_pDefaultImage;

	//##ModelId=4729DEB20073
	TCHAR sPath[128];
};

#endif /* _INC_CFACEBGND_468C8AFA029F_INCLUDED */
