// MTimer.h: interface for the MTimer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MTimer_H__60C5F543_3594_4AB3_BFAE_E221C2F01C4B__INCLUDED_)
#define AFX_MTimer_H__60C5F543_3594_4AB3_BFAE_E221C2F01C4B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
class MTimer ;
//##ModelId=472BB91F02FF
typedef CMap<UINT,UINT,MTimer*,MTimer*>		MTimerMap ;
//##ModelId=472BB91F031D
typedef LRESULT (WINAPI *TimerProcPtr)(LPVOID, LPVOID , UINT, WPARAM, LPARAM);

//窗口无关计时器类
//##ModelId=472BB91F0345
class MTimer  
{
public:
	//##ModelId=472BB91F0359
	BOOL IsActive();
	//##ModelId=472BB91F035A
	void SetUnActive();
	//##ModelId=472BB91F036D
	void SetTimerProc(TimerProcPtr pProc);
	//##ModelId=472BB91F0378
	static void CALLBACK TimerProc(HWND hwnd,UINT uMsg,UINT idEvent,DWORD dwTime);	
	//##ModelId=472BB91F038C
	void ResetMTimer();
	//##ModelId=472BB91F0395
	void KillMTimer();
	//##ModelId=472BB91F0396
	bool SetMTimer(UINT nElapse, int n);
	//##ModelId=472BB91F03A1
	MTimer();

	//##ModelId=472BB91F03A2
	virtual ~MTimer();
	//##ModelId=472BB91F03A4
	static LPVOID pVoid;	
	//##ModelId=472BB9200030
	int nType;		//1为硬盘空间监控，2为文件录像时间长度监控，3为录像文件长度监控
private:
	//##ModelId=472BB920003A
	UINT	m_nTimerID ;	//计时器ID
	//##ModelId=472BB9200045
	static  MTimerMap  m_mapTimer;	//计时器ID和相应对象映射
	//##ModelId=472BB920004E
	UINT	m_nElapse ;		//计时器间隔
protected:
	//##ModelId=472BB9200062
	TimerProcPtr	m_pProc;	//计时器触发过程指针
	//##ModelId=472BB920006C
	BOOL			m_bActive;	//是否在触发标志

};

#endif // !defined(AFX_MTimer_H__60C5F543_3594_4AB3_BFAE_E221C2F01C4B__INCLUDED_)
