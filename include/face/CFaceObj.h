// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACEOBJ_468B4B9F02EE_INCLUDED
#define _INC_CFACEOBJ_468B4B9F02EE_INCLUDED

class CFaceDeal;

class CImage;


#include "Image.h"
#include	"faceconst.h"
#include "CFaceBgnd.h"

//控件基础类。
//##ModelId=4729DEAF0372
class CFaceObj 
{
private:
	//##ModelId=472C9F2D01D4
	DWORD m_dwStyle;


	friend class CFaceDeal;
	friend class CFaceRgn;
public:

	//发送消息给主窗口处理
	//##ModelId=4729DEAF037C
	virtual long SendMessage(UINT msg, WPARAM wParam, LPARAM lParam);

	//##ModelId=4729DEAF0391
	CImage *m_lpCurImg;

	//##ModelId=4729DEAF039B
	CFaceBgnd *m_lpBgnd;

	//##ModelId=4729DEAF039F
	virtual BOOL PtInMe(CPoint * pt);

	
	//##ModelId=4729DEAF03A5
	virtual void Draw(CDC* pDC, CImage *pImag, CRect *pRect = NULL);

	//##ModelId=4729DEAF03B0
	virtual ~CFaceObj();

	//##ModelId=4729DEAF03B8
	CFaceObj();

protected:
	//显示与否控件属性
	//##ModelId=472CA1D501F9
	bool ShowObje(bool bShow = true);

	//是否禁用控件。
	//##ModelId=472CA1E60316
	bool EnableObj(bool bEnable = true);

	//设置控件风格
	//##ModelId=472C9F6400E2
	DWORD SetObjStyle(DWORD dwStyle);

	//取得控件属性
	//##ModelId=472C9FA6006F
	DWORD GetObjStyle();

	//##ModelId=472C9FE20148
	bool IsVisible();

	//控件是否被禁用
	//##ModelId=472CA0280134
	bool IsObjEnable();

	//##ModelId=4729DEAF03B9
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd * pWnd);

	//##ModelId=4729DEAF03CC
	int IsEnable();

	//改变控件的状，重新绘制界面相应部分的图片。
	//##ModelId=4729DEAF03CD
	void EnableObj(CWnd *pWnd);

	//改变控件的状，重新绘制界面相应部分的图片。
	//##ModelId=4729DEAF03D7
	void DisableObj(CWnd *pWnd);

	//##ModelId=4729DEAF03E0
	virtual bool OnActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB00003
	virtual bool OnDeActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB00016
	virtual bool OnSpecialMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB0002A
	virtual bool OnLButtonDown(UINT nFlag, CPoint * pt, CWnd *lpWnd);

	//##ModelId=4729DEB00036
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd *lpWnd);

	//##ModelId=4729DEB00048
	virtual bool OnMouseMove(UINT nFlag, CPoint * pt, CWnd *lpWnd);

	//##ModelId=4729DEB00054
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint * pt, CWnd *lpWnd);

	//控件区域大小，是指原始图片中的大小。
	//##ModelId=4729DEB00066
	CRect m_Rect;
	//经过变换后，控件的区域大小
	//##ModelId=4729DEB00070
	CRect m_RectNew;
	//Check   101
	//UnCheck 100
	//Enable  1
	//Disable 2
	//##ModelId=4729DEB0007A
	int m_iStatus;
	//控件名称，这个在检索控件名称时用，所以一个窗口中不能有两
	//个名称相同的控件。
	//##ModelId=4729DEB00084
	TCHAR m_sObjname[64];
	//提示信息
	//##ModelId=4729DEB0008E
	TCHAR m_sTipInfo[64];

	//##ModelId=4729DEB00098
	CPoint m_PointRgn;

	//##ModelId=4729DEB00099
	TCHAR m_sObjType[64];
};

#endif /* _INC_CFACEOBJ_468B4B9F02EE_INCLUDED */
