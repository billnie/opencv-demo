//faceconst.h

#pragma once

#include <string>
#include <vector>


//##ModelId=4729DEAE032A
typedef std::basic_string<TCHAR> tstring;

//按钮消息
//##ModelId=4729DEAE0335
typedef struct _tagButtonMsg
{
	TCHAR sName[128];	//消息按钮名称
	int nFlag;			//消息类型，是按下，还是释放按钮
	void *pParam;		//消息要传递的参数
	DWORD dwReverse;
	int nReverse[3];
}ButtonMsg, *LPButtonMsg;

//滑动条消息
//##ModelId=4729DEAE0353
typedef struct _tagSliderMsg
{
	TCHAR sName[128];	//消息对象名称
	int nFlag;			//消息类型
	int nCur;
	int nMin;
	int nMax;
	void *pParam;		//消息要传递的参数
	DWORD dwReverse;
	int nReverse[3];
}SliderMsg, *LPSliderMsg;

//列表框消息
//##ModelId=4729DEAE0371
typedef struct _tagFaceLBoxMsg
{
	TCHAR sName[128];	//消息对象名称
	int nFlag;			//消息类型
	int nCur;			//当前选择项
	void *pParam;		//消息要传递的参数
	DWORD dwReverse;
	int nReverse[3];
}FaceLBoxMsg , *LPFaceLBoxMsg;

//文本消息
//##ModelId=473F22F80201
typedef struct _tagTextMsg
{
	TCHAR sName[128];	//消息对像名称
	int nFlag;
	void *pParam;
	int nlen;	//字符串长度
	DWORD dwReverse;
	int nReverse[3];	
}TextMsg, *LPTextMsg;
//皮肤中不同控件消息分类
//##ModelId=472C9E040259
typedef enum _tagFACETYPEMESSAGE
{
	MESS_BTN =1,		//按钮消息
	MESS_SLD,			//滑动条消息
	MESS_LBOX,			//列表框消息
	MESS_TEXT,			//文件控件消息
	MESS_TIMER			//定时器消息
}FACETYPEMESSAGE;

//////////////////////////////////////////////////////////////
//定时器消息
#define TIMER_NEW	1		//新建定时器
#define TIMER_PAUSE	2		//暂停定时器，时钟数据不变
#define TIMER_STOP	3		//停止定时器，时钟数据变为0

#define TIMER_KILL	4		//删除定时器　
#define TIMER_TYPE	5		//设置定时器类型，分为时钟和计时器
#define TIMER_CON	6		//继续计时
typedef enum _tagTIMERTYPE
{

	TIMER = 0,

	RECTIMER
}TIMETYPE;

//时钟状态，分为暂停状态，此时，时钟不会向前走的。停止状态，将
//计数值清0，不会向走的。运行时正常走
//##ModelId=473F24C902E6
typedef enum _tagTIMERSTATUS
{
	TIMEPAUSE = 0,
	TIMESTOP ,
	TIMERUN
}TIMERSTATUS;
//////////////////////////////////////////////////////


#define WM_MSGFACEBTN	WM_USER+20			//发送来自皮肤传过来的消息
#define WM_MSGFACESLIDER	WM_USER + 21	//发送来自滑动条的消息
#define WM_MSGFACELBOX		WM_USER + 22	//发送来自lbox的消息
#define	WM_MSGFACE		WM_USER+2			//接受消息

#define WM_MSG_VIEW		WM_USER + 19		//处理来自视图的消息
