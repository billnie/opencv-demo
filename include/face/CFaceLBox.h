// Copyright (C) 1991 - 1999 Rational Software Corporation

#if defined (_MSC_VER) && (_MSC_VER >= 1000)
#pragma once
#endif
#ifndef _INC_CFACELBOX_4695BA2B002E_INCLUDED
#define _INC_CFACELBOX_4695BA2B002E_INCLUDED

#include "CFaceObj.h"
#include "CFaceSlider.h"
#include <afxcmn.h>
#include <vector>
class CImage;
//##ModelId=4729DEB000AC
typedef struct _tagLBoxParam
{
	int m_MaxRows;		//列表框最大能拥有的条数
	int m_Rows;			//当前拥有的条数
	int m_SelRows;		//当前选择的行
	int m_CurRows;		//当前热点行
	int m_iRowbegin;	//列表框显示的第一条记录的行数
	int m_Height;		//高度
}LBoxParam, *PLBoxParam;
//##ModelId=4729DEB000CB
typedef struct _tagLBoxData
{
	void *pData;
}LBoxData;
//##ModelId=4729DEB000DE
typedef struct _tagMyData
{
	tstring m_ts;
	int		m_flag;
}MyData;
//##ModelId=4729DEB00106
class CFaceLBox 
: public CFaceObj
{
protected:

	//##ModelId=4729DEB00111
	virtual long OnPaint(CWnd *pWnd);
	//##ModelId=4729DEB0016B
	virtual long FaceMessage(WPARAM wParam, LPARAM lParam, CWnd* pWnd);

	//清除列表框中的所有项
	//##ModelId=4729DEB0017E
	void ResentContent(CWnd *pWnd);

	//返回当前鼠标所落在的列表框行
	//##ModelId=4729DEB00180
	int GetRows(CPoint * pt);
	//返回列表框项数量
	//##ModelId=4729DEB00189
	int GetCount();

	//取得当前所选择的项
	//##ModelId=4729DEB00192
	int GetCurSel();

	//设置当前选择项
	//##ModelId=4729DEB0019C
	int SetCurSel(int item);

	//添加一个列表框项
	//##ModelId=4729DEB0019E
	int AddString(LPCTSTR str, CWnd *pWnd, int flag );

	//删除一个列表项
	//##ModelId=4729DEB001B3
	int DeleteString(LPCTSTR str);

	//删除一个列表项
	//##ModelId=4729DEB001BB
	int DeleteItem(int item);

	//##ModelId=4729DEB001C5
	virtual bool OnActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB001D0
	virtual bool OnDeActiveMouseMove(UINT nFlag, CPoint* pt, CWnd* lpWnd);

	//##ModelId=4729DEB001E2
	virtual bool OnLButtonDbClck(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB001F6
	virtual bool OnLButtonDown(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB0020A
	virtual bool OnLButtonUp(UINT nFlag, CPoint* pt, CWnd * lpWnd);

	//##ModelId=4729DEB00216
	virtual bool OnMouseMove(UINT nFlag, CPoint* pt, CWnd * lpWnd);

public:
	//##ModelId=4729DEB00234
	CFaceSlider theCFaceSlider;

	//##ModelId=4729DEB0023D
	virtual BOOL PtInMe(CPoint* pt);

	//##ModelId=4729DEB00251
	virtual void Draw(CDC* pDC, CImage* pImag, CRect* pRect = NULL);

	//##ModelId=4729DEB00265
	virtual ~CFaceLBox();

	//##ModelId=4729DEB00279
	CFaceLBox();

private:

	//##ModelId=4729DEB00284
	LBoxParam m_LBParam;
	//##ModelId=4729DEB00299
	std::vector<MyData> m_LBData;
	//##ModelId=4729DEB002AC
	CImageList m_listimg;
};

#endif /* _INC_CFACELBOX_4695BA2B002E_INCLUDED */
