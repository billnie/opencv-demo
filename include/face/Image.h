// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGE_H__479399D1_7903_49F0_AE9D_746F2F460FB1__INCLUDED_)
#define AFX_IMAGE_H__479399D1_7903_49F0_AE9D_746F2F460FB1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//#include <afxpriv.h>
#include <OCIDL.H>

//#include "t.h"
// CImage is a helper class ,which displays bmp,jpg,gif(only one page) and so on.
// since we call ole to do actual drawings,it's a simple class.

//##ModelId=4729DEAE029E
class CImage  
{
public:
	//##ModelId=4729DEAE02A8
	bool SaveToBmpFile(LPTSTR  strBmpFileName);
	//##ModelId=4729DEAE02BC
	LPTSTR GetFileName();
	//##ModelId=4729DEAE02BD
	bool IsOpen();
	//##ModelId=4729DEAE02BE
	void Close();
	//返回图片的宽高。
	//##ModelId=4729DEAE02C6
	CSize GetSize();
	//绘制图片，在指定的点，和图片上指定点，绘制一定宽和高的图
	//片。
	//##ModelId=4729DEAE02C7
	void Draw(CDC * pDC,CPoint ptTopLeft,CSize size,CPoint ptImage = CPoint(-1,-1),CSize szImage= CSize(0,0));
	//从文件中加载图片。
	//##ModelId=4729DEAE02DA
	BOOL OpenFile(LPCTSTR lpszPathName);
	//##ModelId=4729DEAE02E4
	CImage();
	//##ModelId=4729DEAE02EE
	virtual ~CImage();
private:
	//##ModelId=4729DEAE02F9
	SIZE m_sizeInPix;
	//##ModelId=4729DEAE0305
	SIZE m_sizeInHiMetric;
	//##ModelId=4729DEAE0317
	LPPICTURE m_pPicture;
	//##ModelId=4729DEAE031B
	TCHAR m_strPicFileName[MAX_PATH];

};

#endif // !defined(AFX_IMAGE_H__479399D1_7903_49F0_AE9D_746F2F460FB1__INCLUDED_)
