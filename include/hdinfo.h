

#pragma once
#include "reg.h"

void GetBiosSN();
void GetMacID();
void CPUID();

int GetAppPath(LPTSTR s);

//取网卡的mac地址,返回值为＜1时，取失败，>0时为取的个数, 如果有多个，中间有;分开
int GetMacAddr(char *szAddr);


//与注册文件相关的操作
//产生注册所需的硬件信息文件
int GerHdSerialFile(LPCTSTR fname, HdInfo *info);
//判断是否存在文件在当前模块所有在目录下
int HasHdSerialFile(LPCTSTR fname, HdInfo *info);
//与注册文件相关的操作
//产生注册所需的硬件信息文件
int GerHdSerialFile(LPCTSTR fname);


//与序列号相关的操作函数
int GerSerFile(LPCTSTR s, SerInfo *info);
int HasSerFile(LPTSTR s, SerInfo *info);



//与磁盘相关的操作函数

//取得逻辑磁盘列表
int GetLogicDisk(LPTSTR s);
//取得逻辑磁盘的序列号
int GetLogicDiskSerial(LPTSTR s);
//取得硬盘序列号
int GetDiskSerial(BYTE *pbyte, int n);
bool GetXXXLogicDiskSerial(LPCTSTR s, LPTSTR serial);
bool GetXXXLogicDiskSerial(LPCTSTR s, DWORD *serial);

//扇区相关的操作
int ReadWriteSector(int secnum, int num, BYTE *pbyte, bool bread = true);
int GerSect(int secnum, SectorInfo *sec);	//生成扇区中加密内容
int HasSect(int secnum, SectorInfo *sec);	//取得扇区中的加密信息

//与字符串相关的函数
bool WCharToMByte(LPCWSTR lpcwszStr, LPSTR lpszStr, DWORD *dwSize);
bool MByteToWChar(LPCSTR lpcszStr, LPWSTR lpwszStr, DWORD *dwSize);
char * TtoA(LPTSTR s);
TCHAR * AtoT(char *);
//从一个字节串中查找某个字节串
int FindBYTE(BYTE *s, int len, BYTE *beg, BYTE *end, BYTE *bstring);



//guid产生
TCHAR* GenGuid();
