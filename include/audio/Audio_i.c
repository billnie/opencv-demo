/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Nov 16 10:43:35 2007
 */
/* Compiler settings for G:\work\调试信息工具\共享软件相关\Audio\Audio.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IAudioDeal = {0xEAF91137,0x9F3F,0x4FE2,{0x9A,0xA9,0x61,0xD4,0x09,0x27,0x17,0xDD}};


const IID LIBID_AUDIOLib = {0xC2AF3E5A,0x78B9,0x4B90,{0xAE,0xED,0x67,0x11,0x6F,0x6D,0xB8,0x78}};


const CLSID CLSID_AudioDeal = {0x4833A3A8,0xF4BF,0x42B4,{0x8E,0xFD,0x9D,0xBD,0x0B,0x13,0xA3,0x93}};


#ifdef __cplusplus
}
#endif

