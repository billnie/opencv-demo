/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Nov 16 10:43:35 2007
 */
/* Compiler settings for G:\work\调试信息工具\共享软件相关\Audio\Audio.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __Audio_h__
#define __Audio_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IAudioDeal_FWD_DEFINED__
#define __IAudioDeal_FWD_DEFINED__
typedef interface IAudioDeal IAudioDeal;
#endif 	/* __IAudioDeal_FWD_DEFINED__ */


#ifndef __AudioDeal_FWD_DEFINED__
#define __AudioDeal_FWD_DEFINED__

#ifdef __cplusplus
typedef class AudioDeal AudioDeal;
#else
typedef struct AudioDeal AudioDeal;
#endif /* __cplusplus */

#endif 	/* __AudioDeal_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IAudioDeal_INTERFACE_DEFINED__
#define __IAudioDeal_INTERFACE_DEFINED__

/* interface IAudioDeal */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAudioDeal;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAF91137-9F3F-4FE2-9AA9-61D4092717DD")
    IAudioDeal : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EnumDev( 
            DWORD nType,
            DWORD pdev) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EnumSubDev( 
            DWORD nType,
            TCHAR __RPC_FAR *sDevname,
            DWORD pdevarr) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SelectDev( 
            DWORD nType,
            TCHAR __RPC_FAR *sdev) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SelectSubDev( 
            DWORD nType,
            TCHAR __RPC_FAR *s,
            TCHAR __RPC_FAR *sub) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetVolument( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD dwvol) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetVolument2( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwleft,
            DWORD __RPC_FAR *dwrgh) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetVolument( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLin,
            DWORD __RPC_FAR *dwvol) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetVolument2( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwl,
            DWORD __RPC_FAR *dwr) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMute( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD dwmute) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMute( 
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwret) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAudioDealVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IAudioDeal __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IAudioDeal __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IAudioDeal __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EnumDev )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            DWORD pdev);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EnumSubDev )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            TCHAR __RPC_FAR *sDevname,
            DWORD pdevarr);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectDev )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            TCHAR __RPC_FAR *sdev);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectSubDev )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            TCHAR __RPC_FAR *s,
            TCHAR __RPC_FAR *sub);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetVolument )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD dwvol);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetVolument2 )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwleft,
            DWORD __RPC_FAR *dwrgh);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetVolument )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLin,
            DWORD __RPC_FAR *dwvol);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetVolument2 )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwl,
            DWORD __RPC_FAR *dwr);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetMute )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD dwmute);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMute )( 
            IAudioDeal __RPC_FAR * This,
            DWORD nType,
            LPCTSTR sDev,
            LPCTSTR sLine,
            DWORD __RPC_FAR *dwret);
        
        END_INTERFACE
    } IAudioDealVtbl;

    interface IAudioDeal
    {
        CONST_VTBL struct IAudioDealVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAudioDeal_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAudioDeal_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAudioDeal_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAudioDeal_EnumDev(This,nType,pdev)	\
    (This)->lpVtbl -> EnumDev(This,nType,pdev)

#define IAudioDeal_EnumSubDev(This,nType,sDevname,pdevarr)	\
    (This)->lpVtbl -> EnumSubDev(This,nType,sDevname,pdevarr)

#define IAudioDeal_SelectDev(This,nType,sdev)	\
    (This)->lpVtbl -> SelectDev(This,nType,sdev)

#define IAudioDeal_SelectSubDev(This,nType,s,sub)	\
    (This)->lpVtbl -> SelectSubDev(This,nType,s,sub)

#define IAudioDeal_SetVolument(This,nType,sDev,sLine,dwvol)	\
    (This)->lpVtbl -> SetVolument(This,nType,sDev,sLine,dwvol)

#define IAudioDeal_SetVolument2(This,nType,sDev,sLine,dwleft,dwrgh)	\
    (This)->lpVtbl -> SetVolument2(This,nType,sDev,sLine,dwleft,dwrgh)

#define IAudioDeal_GetVolument(This,nType,sDev,sLin,dwvol)	\
    (This)->lpVtbl -> GetVolument(This,nType,sDev,sLin,dwvol)

#define IAudioDeal_GetVolument2(This,nType,sDev,sLine,dwl,dwr)	\
    (This)->lpVtbl -> GetVolument2(This,nType,sDev,sLine,dwl,dwr)

#define IAudioDeal_SetMute(This,nType,sDev,sLine,dwmute)	\
    (This)->lpVtbl -> SetMute(This,nType,sDev,sLine,dwmute)

#define IAudioDeal_GetMute(This,nType,sDev,sLine,dwret)	\
    (This)->lpVtbl -> GetMute(This,nType,sDev,sLine,dwret)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_EnumDev_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    DWORD pdev);


void __RPC_STUB IAudioDeal_EnumDev_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_EnumSubDev_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    TCHAR __RPC_FAR *sDevname,
    DWORD pdevarr);


void __RPC_STUB IAudioDeal_EnumSubDev_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_SelectDev_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    TCHAR __RPC_FAR *sdev);


void __RPC_STUB IAudioDeal_SelectDev_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_SelectSubDev_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    TCHAR __RPC_FAR *s,
    TCHAR __RPC_FAR *sub);


void __RPC_STUB IAudioDeal_SelectSubDev_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_SetVolument_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLine,
    DWORD dwvol);


void __RPC_STUB IAudioDeal_SetVolument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_SetVolument2_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLine,
    DWORD __RPC_FAR *dwleft,
    DWORD __RPC_FAR *dwrgh);


void __RPC_STUB IAudioDeal_SetVolument2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_GetVolument_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLin,
    DWORD __RPC_FAR *dwvol);


void __RPC_STUB IAudioDeal_GetVolument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_GetVolument2_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLine,
    DWORD __RPC_FAR *dwl,
    DWORD __RPC_FAR *dwr);


void __RPC_STUB IAudioDeal_GetVolument2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_SetMute_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLine,
    DWORD dwmute);


void __RPC_STUB IAudioDeal_SetMute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAudioDeal_GetMute_Proxy( 
    IAudioDeal __RPC_FAR * This,
    DWORD nType,
    LPCTSTR sDev,
    LPCTSTR sLine,
    DWORD __RPC_FAR *dwret);


void __RPC_STUB IAudioDeal_GetMute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAudioDeal_INTERFACE_DEFINED__ */



#ifndef __AUDIOLib_LIBRARY_DEFINED__
#define __AUDIOLib_LIBRARY_DEFINED__

/* library AUDIOLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_AUDIOLib;

EXTERN_C const CLSID CLSID_AudioDeal;

#ifdef __cplusplus

class DECLSPEC_UUID("4833A3A8-F4BF-42B4-8EFD-9DBD0B13A393")
AudioDeal;
#endif
#endif /* __AUDIOLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
