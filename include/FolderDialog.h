///////////////////////////////////////////////////////////////////////////////
// FolderDialog.h : header file
//
//     If this class(es) works well, it's written by zouzan on 2004-04-10
//	    else god created it!
///////////////////////////////////////////////////////////////////////////////

#if !defined(__FOLDERDIALOG_H__)
#define __FOLDERDIALOG_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/////////////////////////////////////////////////////////////////////////////
#ifndef BIF_NEWDIALOGSTYLE
#define BIF_NEWDIALOGSTYLE   0x0040
#endif

/////////////////////////////////////////////////////////////////////////////
// CFolderDialog 
//##ModelId=47E46ACB00FD
class CFolderDialog
{
// Construction
public:
	//##ModelId=47E46ACB0107
	CFolderDialog();
	//##ModelId=47E46ACB0111
	virtual ~CFolderDialog();

// Attributes
public:
	// Title of browse window if you set
	//##ModelId=47E46ACB011B
	CString	m_strTitle; 
	
	//flags used by BROWSEINFO.ulFlags;
	//##ModelId=47E46ACB011C
	UINT	m_uFlag;

	//is editbox readonly if use BIF_EDITBOX
	//##ModelId=47E46ACB0125
	BOOL	m_bEditReadonly;

	// is NewFolder button visible, only used when BIF_NEWDIALOGSTYLE usable.
	//##ModelId=47E46ACB0126
	BOOL	m_bShowCreateButton;

	//a special folder to set root dir, eg: CSIDL_PERSONAL. CSIDL_DESKTOP=0 is default.
	//##ModelId=47E46ACB012F
	int		m_nCSIDLRoot;

	//The format string used to query creating new folder. It must contain '%1', which will replaced by new folder's name��
	//##ModelId=47E46ACB0130
	CString m_strQueryCreateFormat;

// Operations
public:
	//##ModelId=47E46ACB0139
	BOOL Browse(HWND hwndOwner, UINT nDisplay, LPCTSTR lpszInitPath=NULL);
	//##ModelId=47E46ACB0143
	BOOL Browse(HWND hwndOwner, LPCTSTR lpszDisplay, LPCTSTR lpszInitPath=NULL);
	//##ModelId=47E46ACB014D
	CString	GetPathName(){return m_strSelectPath;}

// Overrides
protected:
	//##ModelId=47E46ACB014E
	virtual BOOL DoBrowse();
	// overrides to add more size items.
	//##ModelId=47E46ACB0157
	virtual void OnAddSizeItems(HWND hDlg);	
	// helper for OnAddSizeItems
	//##ModelId=47E46ACB0161
	void AddSizeItem(HWND hDlg, UINT nID, UINT mode);

protected:
	//##ModelId=47E46ACB016C
	HWND	m_hOwner;	// Parent of browse dialog
	//##ModelId=47E46ACB016D
	CString	m_strDisplay;// Text shown above tree control
	//##ModelId=47E46ACB0175
	CString	m_strInitPath;

	//##ModelId=47E46ACB017F
	CString	m_strSelectPath;// result path
	
	//##ModelId=47E46ACB0194
	BFFCALLBACK	m_pBrowseCallbackProc;//browse callback proc.
	//##ModelId=47E46ACB019E
	WNDPROC		m_pNewWndProc;// browse windowproc.
	//////////////////////////////////////////
	//
	//##ModelId=47E46ACB022A
	enum SizeMode
	{
		//##ModelId=47E46ACB0234
		SIZE_RIGHT	= 0x01,
		//##ModelId=47E46ACB0235
		SIZE_BOTTOM	= 0x02,
		//##ModelId=47E46ACB023E
		MOVE_RIGHT	= 0x04,
		//##ModelId=47E46ACB023F
		MOVE_BOTTOM = 0x08
	};
	//##ModelId=47E46ACB0248
	struct CSizeItem
	{
		//##ModelId=47E46ACB0253
		HWND	hCtrl;
		//##ModelId=47E46ACB0254
		UINT	mode;// combination of SizeMode
		//##ModelId=47E46ACB025C
		CRect	rect;
	};
	//##ModelId=47E46ACB0266
	class CSizeItemList : public CPtrList
	{
	public:
		//##ModelId=47E46ACB0270
		CSizeItemList(){};
		//##ModelId=47E46ACB0271
		~CSizeItemList()
		{
			POSITION pos = GetHeadPosition();
			while(pos){
				delete (CSizeItem *)GetNext(pos);
			}
			RemoveAll();
		}
	public:
		//##ModelId=47E46ACB0272
		CSize m_sizeDlg;//Current size of Client.
	};

	// Map HWND to his m_SizeItemList
	//##ModelId=47E46ACB027A
	class CSizeItemListMap 
	{
	public:
		//##ModelId=47E46ACB0284
		CSizeItemListMap(){};
		//##ModelId=47E46ACB0285
		~CSizeItemListMap(){};

		//##ModelId=47E46ACB0286
		void SetAt(HWND hDlg, CSizeItemList *pList){
			void *value;
			if(! m_map.Lookup((void *)hDlg, value)){
				m_map.SetAt((void *)hDlg, (void*)pList);
			}
		}
		//##ModelId=47E46ACB028F
		BOOL Lookup(HWND hDlg, CSizeItemList *&pList){
			return m_map.Lookup((void *)hDlg, (void *&)pList);
		}
		//##ModelId=47E46ACB0292
		void RemoveAt(HWND hDlg){
			m_map.SetAt((void*)hDlg, NULL);
		}

	protected:
		//##ModelId=47E46ACB029A
		CMapPtrToPtr m_map;
	};

	//##ModelId=47E46ACB01A0
	CSizeItemList	m_SizeItemList;
	/////////////////////////////////////////

protected:
	//##ModelId=47E46ACB01A8
	BOOL CheckSelection(LPCTSTR lpszPath);
	//##ModelId=47E46ACB01B2
	static BOOL		IsNewUIUsable();// check BIF_NEWDIALOGSTYLE usable
	//##ModelId=47E46ACB01B4
	static void		SaveLastRect(HWND hDlg);// save position to registry
	//##ModelId=47E46ACB01BE
	static BOOL		LoadLastRect(HWND hDlg);// load last position from registry.
	//##ModelId=47E46ACB01C7
	static int		CALLBACK BrowseCallbackProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lData);
	//##ModelId=47E46ACB01DA
	static LRESULT	CALLBACK NewWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	//##ModelId=47E46ACB01E6
	static WNDPROC			s_pOldWndProc;
	//##ModelId=47E46ACB01EE
	static const CSize		s_sizeMin;// minimize size of browse dialog
	//##ModelId=47E46ACB0203
	static CSizeItemListMap s_mapSizeItemList;
};

namespace CFolderDialog_Helper
{
	BOOL	IsDirectoryExist(LPCTSTR lpszDir);
	BOOL	CreateAllDirectory(LPCTSTR lpszDir);
	DWORD	GetDllVersion(LPCTSTR lpszDllName);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__FOLDERDIALOG_H__)
