////////////////////////////////////////////////////////////////////////////////////
// DSHelper.cpp

#include "stdafx.h"
#include <dshowasf.h>
#include "DSHelper.h"
#include <stdio.h>
#include <wmsysprf.h>
#define  MAX_DEVICE_NAME_LEN 255

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)		if( p ) { (p)->Release(); (p) = NULL;}
#endif //SAFE_RELEASE


// Tear down everything downstream of a given filter, remove downdtream from graph if set
void DS_NukeDownstream(IFilterGraph *pGraph, IBaseFilter *pFilter, BOOL bRemoveFilter/*=TRUE*/) 
{
	if (pGraph && pFilter)
	{
		IEnumPins * pinEnum = 0;
		if (SUCCEEDED(pFilter->EnumPins(&pinEnum)))
		{
			pinEnum->Reset();
			IPin * pin = 0;
			ULONG cFetched = 0;
			bool pass = true;
			while (pass && S_OK == (pinEnum->Next(1, &pin, &cFetched)))
			{
				if (pin && cFetched)
				{
					IPin * connectedPin = 0;
					pin->ConnectedTo(&connectedPin);
					if(connectedPin) 
					{
						PIN_INFO pininfo;
						if (SUCCEEDED(connectedPin->QueryPinInfo(&pininfo)))
						{
							if(pininfo.dir == PINDIR_INPUT) 
							{
								DS_NukeDownstream(pGraph, pininfo.pFilter, bRemoveFilter);
								pGraph->Disconnect(connectedPin);
								pGraph->Disconnect(pin);

								if(bRemoveFilter)
									pGraph->RemoveFilter(pininfo.pFilter);
							}
							pininfo.pFilter->Release();
						}
						connectedPin->Release();
					}
					pin->Release();
				}
				else
				{
					pass = false;
				}
			}
			pinEnum->Release();
		}
	}
}

// Tear down everything upstream of a given filter, remove upstream from graph if set
void DS_NukeUpstream(IFilterGraph * pGraph, IBaseFilter * pFilter, BOOL bRemoveFilter /*=TRUE*/) 
{
	if (pGraph && pFilter)
	{
		IEnumPins * pinEnum = 0;
		if (SUCCEEDED(pFilter->EnumPins(&pinEnum)))
		{
			pinEnum->Reset();
			IPin * pin = 0;
			ULONG cFetched = 0;
			bool pass = true;
			while (pass && S_OK == (pinEnum->Next(1, &pin, &cFetched)))
			{
				if (pin && cFetched)
				{
					IPin * connectedPin = 0;
					pin->ConnectedTo(&connectedPin);
					if(connectedPin) 
					{
						PIN_INFO pininfo;
						if (SUCCEEDED(connectedPin->QueryPinInfo(&pininfo)))
						{
							if(pininfo.dir == PINDIR_OUTPUT) 
							{
								DS_NukeUpstream(pGraph, pininfo.pFilter, bRemoveFilter);
								pGraph->Disconnect(connectedPin);
								pGraph->Disconnect(pin);

								if(bRemoveFilter)
									pGraph->RemoveFilter(pininfo.pFilter);
							}
							pininfo.pFilter->Release();
						}
						connectedPin->Release();
					}
					pin->Release();
				}
				else
				{
					pass = false;
				}
			}
			pinEnum->Release();
		}
	}
}

BOOL DS_AddFilter(IFilterGraph *pGraph, const GUID *pGUID, LPCTSTR lpszName, IBaseFilter **ppFilter)
{
	ASSERT(pGraph && pGUID);

	IBaseFilter *pFilter = NULL;
	HRESULT hr;
	hr = CoCreateInstance(*pGUID, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&pFilter);
	if(FAILED(hr))
		return FALSE;

	USES_CONVERSION;
	if(FAILED(pGraph->AddFilter(pFilter, T2W( (LPTSTR)lpszName) )))
	{
		SAFE_RELEASE(pFilter);
		return FALSE;
	}

	if(ppFilter)
		*ppFilter = pFilter;
	else
		SAFE_RELEASE(pFilter);

	return TRUE;
}

BOOL DS_AddFilter(IFilterGraph *pGraph, const GUID *clsidDeviceClass, LPCTSTR szDevName,
				  LPCTSTR szAlias,IBaseFilter **ppFilter)
{
	ASSERT(pGraph && clsidDeviceClass);
	
	IBaseFilter *pFilter = NULL;

	if(szAlias == NULL)
		return NULL;
	CString strDev(szDevName);
//	CComPtr <IBaseFilter>		pTmpFilter;
	CComPtr	<IEnumMoniker>		iEmMoniker;
	CComPtr	<ICreateDevEnum>	iCreateDevEnum;
	ULONG cFetched;
	
	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, 
		NULL, CLSCTX_INPROC_SERVER,	IID_ICreateDevEnum, 
		(void**)&iCreateDevEnum);
	
	if(hr == NOERROR)
	{
		hr = iCreateDevEnum->CreateClassEnumerator
			(*clsidDeviceClass, &iEmMoniker, 0);
		if(hr == NOERROR) 
		{
			iEmMoniker->Reset();
			IMoniker *iMoniker = NULL;
			while(hr = iEmMoniker->Next(1, &iMoniker, &cFetched), hr == S_OK)
			{
				CComPtr<IPropertyBag>  iBag;
				char szTmpName[MAX_DEVICE_NAME_LEN];
				hr = iMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&iBag);
				if(SUCCEEDED(hr))
				{
					VARIANT var;
					var.vt = VT_BSTR;
					hr = iBag->Read(L"FriendlyName", &var, NULL);
					if (hr == NOERROR)
					{
						WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, szTmpName, MAX_DEVICE_NAME_LEN, NULL, NULL);
						SysFreeString(var.bstrVal);
						size_t nDevNameSize = strlen(szTmpName);
						if (strDev.IsEmpty())
						{;}
						
// 						tolower(szTmpName);
// 						tolower(szDevName);
						else if(memcmp(szTmpName,szDevName,nDevNameSize) != 0)
						{
							iMoniker->Release();
							iMoniker = NULL;							
							continue;
						}
						hr = iMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pFilter);
						ASSERT(SUCCEEDED(hr));
						
						iMoniker->Release();
						iMoniker = NULL;
						break;
						
					}
				}
				iMoniker->Release();
				iMoniker = NULL;
			}
		}
	}

	USES_CONVERSION;
	if(FAILED(pGraph->AddFilter(pFilter, T2W( (LPTSTR)szAlias) )))
	{
		SAFE_RELEASE(pFilter);
		return FALSE;
	}
	
	if(ppFilter)
		*ppFilter = pFilter;
	else
		SAFE_RELEASE(pFilter);
	
	return TRUE;
}
BOOL	DS_CreateFilterByName(const GUID *clsidDeviceClass, LPCTSTR szDevName,
				  IBaseFilter **ppFilter)
{
	ASSERT(clsidDeviceClass);
	
	IBaseFilter *pFilter = NULL;

	CString strDev(szDevName);
//	CComPtr <IBaseFilter>		pTmpFilter;
	CComPtr	<IEnumMoniker>		iEmMoniker;
	CComPtr	<ICreateDevEnum>	iCreateDevEnum;
	ULONG cFetched;
	
	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, 
		NULL, CLSCTX_INPROC_SERVER,	IID_ICreateDevEnum, 
		(void**)&iCreateDevEnum);
	
	if(hr == NOERROR)
	{
		hr = iCreateDevEnum->CreateClassEnumerator
			(*clsidDeviceClass, &iEmMoniker, 0);
		if(hr == NOERROR) 
		{
			iEmMoniker->Reset();
			IMoniker *iMoniker = NULL;
			while(hr = iEmMoniker->Next(1, &iMoniker, &cFetched), hr == S_OK)
			{
				CComPtr<IPropertyBag>  iBag;
				char szTmpName[MAX_DEVICE_NAME_LEN];
				hr = iMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&iBag);
				if(SUCCEEDED(hr))
				{
					VARIANT var;
					var.vt = VT_BSTR;
					hr = iBag->Read(L"FriendlyName", &var, NULL);
					if (hr == NOERROR)
					{
						WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, szTmpName, MAX_DEVICE_NAME_LEN, NULL, NULL);
						SysFreeString(var.bstrVal);
						size_t nDevNameSize = strlen(szTmpName);
						if (strDev.IsEmpty())
						{;}
						else if(memcmp(szTmpName,szDevName,nDevNameSize) != 0)
						{
							iMoniker->Release();
							iMoniker = NULL;							
							continue;
						}
						hr = iMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pFilter);
						ASSERT(SUCCEEDED(hr));
						
						iMoniker->Release();
						iMoniker = NULL;
						break;
						
					}
				}
				iMoniker->Release();
				iMoniker = NULL;
			}
		}
	}

	
	if(ppFilter)
		*ppFilter = pFilter;
	else
		SAFE_RELEASE(pFilter);
	
	return TRUE;
}
BOOL DS_FindFilterByName(IFilterGraph *pGraph, LPCTSTR lpszFilterName, IBaseFilter **ppFilter)
{
	if(pGraph)
	{
		USES_CONVERSION;
		if(SUCCEEDED(pGraph->FindFilterByName(T2W((LPTSTR)lpszFilterName), ppFilter)))
			return TRUE;
	}
	return FALSE;
}


// An error code is returned in m_dwErrorCode member variable
// if the function failed.
BOOL DS_FindPin(IPin** ppPin, IBaseFilter* pFilter, LPCTSTR pszName, PIN_DIRECTION Dir)
{
	ULONG n;
	PIN_DIRECTION PinDir;
	BOOL bFound = FALSE;
    CComPtr<IEnumPins> enumPin = NULL;
    IPin *pPin = NULL;

	USES_CONVERSION;

	if (SUCCEEDED(pFilter->EnumPins(&enumPin))) 
	{
		enumPin->Reset();

		while (!bFound && (S_OK == enumPin->Next(1, &pPin, &n))) 
		{
			if (S_OK == pPin->QueryDirection(&PinDir)) 
			{
				if (PinDir == Dir) 
				{
					if(pszName == NULL)
						bFound = TRUE;
					else
					{
						PIN_INFO pi;
						if (SUCCEEDED(pPin->QueryPinInfo(&pi)))
						{
							if (_tcscmp(pszName, W2T(pi.achName)) == 0)
								bFound = TRUE;

							pi.pFilter->Release();
						}
					}
				}
			}

			if (! bFound)
				pPin->Release();
		}
	}

	if (bFound)
		*ppPin = pPin;

	return bFound;
}

// An error code is returned in m_dwErrorCode member variable
// if the function failed.
BOOL DS_FindPin(IPin** ppPin, IBaseFilter* pFilter, GUID MajorType, PIN_DIRECTION Dir)
{
	IEnumMediaTypes *pEnumMedia;
	AM_MEDIA_TYPE* pMediaTypes;
    IPin *pPin = NULL;
    IEnumPins *pins = NULL;
	PIN_DIRECTION PinDir;
	BOOL bFound = FALSE;
	ULONG n;
	HRESULT hr;

	if (SUCCEEDED(pFilter->EnumPins(&pins))) 
	{
		hr = pins->Reset();

		while (!bFound && (S_OK == pins->Next(1, &pPin, &n))) 
		{
			if (S_OK == pPin->QueryDirection(&PinDir)) 
			{
				if (PinDir == Dir) 
				{
					if(MajorType == GUID_NULL)
						bFound = TRUE;
					else
					{
						if (S_OK == pPin->EnumMediaTypes(&pEnumMedia))
						{
							if (S_OK == pEnumMedia->Next(1, &pMediaTypes, &n))
							{
								if (pMediaTypes->majortype == MajorType)
								{
									bFound = TRUE;
								}

								DeleteMediaType(pMediaTypes);
							}

							pEnumMedia->Release();
						}
					}
				}
			}

			if (! bFound)
			{
				pPin->Release();
			}
		}

		pins->Release();
	}

	if (bFound)
		*ppPin = pPin;

	return bFound;
}

BOOL	DS_FindConnectedPin (OUT IPin **ppPin, 
							 IN	 IBaseFilter *pFilter,
							 IN  GUID majorType,
							 PIN_DIRECTION Dir)
{
	CComPtr<IEnumPins> enumPin;
	IPin *pPin = NULL;
	ULONG n =1;
	BOOL bFound = FALSE;

	if(SUCCEEDED(pFilter->EnumPins(&enumPin)))
	{
		enumPin->Reset();
		while(! bFound && enumPin->Next(1, &pPin, &n)==S_OK)
		{
			PIN_DIRECTION PinDir;
			if(SUCCEEDED(pPin->QueryDirection(&PinDir)))
			{
				if(PinDir == Dir)
				{
					AM_MEDIA_TYPE mt;
					ZeroMemory(&mt, sizeof(mt));

					if(SUCCEEDED(pPin->ConnectionMediaType(&mt)))
					{
						if(majorType==GUID_NULL	|| mt.majortype == majorType)
						{
							bFound = TRUE;
						}
					}
				}
			}
			
			if(! bFound)
				pPin->Release();
		}
	}
	if(bFound)
		*ppPin = pPin;

	return bFound;
}

BOOL	DS_ConnectFilter(IN	 IBaseFilter *pUpFilter,
						 IN	 LPCTSTR lpszUpPinName,
						 IN	 IBaseFilter *pDownFilter,
						 IN	 LPCTSTR lpszDownPinName,
						 OUT IPin	**ppUpPin,
						 OUT IPin	**ppDownPin)
{
	IPin *pUpPin = NULL;
	IPin *pDownPin = NULL;

	if(! DS_FindPin(&pUpPin, pUpFilter, lpszUpPinName, PINDIR_OUTPUT))
		return FALSE;

	if(! DS_FindPin(&pDownPin, pDownFilter, lpszDownPinName, PINDIR_INPUT))
	{
		SAFE_RELEASE(pUpPin);
		return FALSE;
	}

	HRESULT hr;
	if(FAILED(hr=pUpPin->Connect(pDownPin, NULL)))
	{
		SAFE_RELEASE(pUpPin);
		SAFE_RELEASE(pDownPin);
		return FALSE;
	}

	if(ppUpPin != NULL)
		*ppUpPin = pUpPin;
	else
		SAFE_RELEASE(pUpPin);

	if(ppDownPin != NULL)
		*ppDownPin = pDownPin;
	else
		SAFE_RELEASE(pDownPin);

	return TRUE;
}

BOOL	DS_ConnectFilter(IN	 IBaseFilter *pUpFilter,
						 IN	 GUID typeUpPin,
						 IN	 IBaseFilter *pDownFilter,
						 IN	 GUID typeDownPin,
						 OUT IPin	**ppUpPin ,
						 OUT IPin	**ppDownPin)
{
	IPin *pUpPin = NULL;
	IPin *pDownPin = NULL;

	if(! DS_FindPin(&pUpPin, pUpFilter, typeUpPin, PINDIR_OUTPUT))
		return FALSE;

	if(! DS_FindPin(&pDownPin, pDownFilter, typeDownPin, PINDIR_INPUT))
	{
		SAFE_RELEASE(pUpPin);
		return FALSE;
	}

	if(FAILED(pUpPin->Connect(pDownPin, NULL)))
	{
		SAFE_RELEASE(pUpPin);
		SAFE_RELEASE(pDownPin);
		return FALSE;
	}

	if(ppUpPin != NULL)
		*ppUpPin = pUpPin;
	else
		SAFE_RELEASE(pUpPin);

	if(ppDownPin != NULL)
		*ppDownPin = pDownPin;
	else
		SAFE_RELEASE(pDownPin);

	return TRUE;
}

BOOL	DS_ConnectToFilter(	IN	 IPin	*pUpPin,
							IN	 IBaseFilter *pDownFilter,
							IN	 LPCTSTR lpszDownPinName,
							OUT IPin	**ppDownPin)
{
	IPin *pDownPin = NULL;

	if(! DS_FindPin(&pDownPin, pDownFilter, lpszDownPinName, PINDIR_INPUT))
	{
		return FALSE;
	}

	if(FAILED(pUpPin->Connect(pDownPin, NULL)))
	{
		SAFE_RELEASE(pDownPin);
		return FALSE;
	}

	if(ppDownPin != NULL)
		*ppDownPin = pDownPin;
	else
		SAFE_RELEASE(pDownPin);

	return TRUE;
}

BOOL	DS_ConnectToFilter(	IN	 IPin	*pUpPin,
							IN	 IBaseFilter *pDownFilter,
							IN	 GUID typeDownPin,
							OUT IPin	**ppDownPin)
{
	IPin *pDownPin = NULL;

	if(! DS_FindPin(&pDownPin, pDownFilter, typeDownPin, PINDIR_INPUT))
	{
		return FALSE;
	}

	if(FAILED(pUpPin->Connect(pDownPin, NULL)))
	{
		SAFE_RELEASE(pDownPin);
		return FALSE;
	}

	if(ppDownPin != NULL)
		*ppDownPin = pDownPin;
	else
		SAFE_RELEASE(pDownPin);

	return TRUE;
}


BOOL DS_GetDXVersion(LONGLONG *pllVer)
{
	LONGLONG llVer;
    HKEY  hKey=0;
    DWORD type=REG_BINARY, size=sizeof(LONGLONG);
	
    // Open the appropriate registry key
    LONG result = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
		TEXT("Software\\Microsoft\\DirectX"),
		0, KEY_READ, &hKey );

    if( ERROR_SUCCESS != result )
        return FALSE;
	
    result = RegQueryValueEx( hKey, TEXT("InstalledVersion"), NULL,
		&type, (BYTE*)&llVer, &size );
	
	RegCloseKey( hKey );

	if( ERROR_SUCCESS == result )
    {
		*pllVer = llVer;
		return TRUE;
    }
	
    return FALSE;	
}


////////////////////////////////////////////////////////////////////////////////////

// Adds a DirectShow filter graph to the Running Object Table,
// allowing GraphEdit to "spy" on a remote filter graph.
BOOL DS_AddGraphToRot(IUnknown *pUnkGraph, DWORD *pdwRegister) 
{
    CComPtr <IMoniker>              pMoniker;
    CComPtr <IRunningObjectTable>   pROT;
    WCHAR wsz[128];

    if (FAILED(GetRunningObjectTable(0, &pROT)))
        return FALSE;

    wsprintfW(wsz, L"FilterGraph %08x pid %08x\0", (DWORD_PTR) pUnkGraph, 
             GetCurrentProcessId());

    if (SUCCEEDED(CreateItemMoniker(L"!", wsz, &pMoniker))) 
	{
        if(SUCCEEDED(pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, pUnkGraph, 
                            pMoniker, pdwRegister)))
		{
			return TRUE;
		}
	}
        
    return FALSE;
}

// Removes a filter graph from the Running Object Table
void DS_RemoveGraphFromRot(const DWORD dwRegister )
{
    CComPtr <IRunningObjectTable> pROT;

    if (SUCCEEDED(GetRunningObjectTable(0, &pROT))) 
        pROT->Revoke(dwRegister);
}


BOOL DS_IsOverlayUsable()
{
	//返回值，result=1为可以，=2为初始化失败，=3为创建Overlay表面失败，
	int result=1;
	BOOL bflag=FALSE;//bflag为TRUE时代表初始化失败，
	
#define INIT_DIRECTDRAW_STRUCT(x) (ZeroMemory(&x, sizeof(x)), x.dwSize=sizeof(x))
	//初始化
	HRESULT         ddrval;
    LPDIRECTDRAW    lpDD1;
	
	LPDIRECTDRAW2       g_lpdd = NULL;
	LPDIRECTDRAWSURFACE g_lpddsPrimary = NULL;
	LPDIRECTDRAWSURFACE g_lpddsOverlay = NULL;
	
	DDSURFACEDESC   ddsd;
	
    ddrval = DirectDrawCreate( NULL, &lpDD1, NULL );
    if( FAILED(ddrval))
		bflag=TRUE;
	
    ddrval = lpDD1->QueryInterface(IID_IDirectDraw2, (void **)&g_lpdd);
    if( FAILED(ddrval))
		bflag=TRUE;
    
    lpDD1->Release();
	
    if (!g_lpdd) 
		result=2;
    // For NORMAL cooperative level we no longer need to provide an HWND.
    ddrval = g_lpdd->SetCooperativeLevel(NULL, DDSCL_NORMAL);
    if( FAILED(ddrval))
		bflag=TRUE;
	
	
    INIT_DIRECTDRAW_STRUCT(ddsd);
    ddsd.dwFlags = DDSD_CAPS;
    ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
    ddrval = g_lpdd->CreateSurface(&ddsd, &g_lpddsPrimary, NULL );
	if( FAILED(ddrval))
		bflag=TRUE;
	
	
	if(bflag)
	{
		if (g_lpddsPrimary)
		{
			g_lpddsPrimary->Release();
			g_lpddsPrimary=NULL;
		}
		if (g_lpdd)
		{
			g_lpdd->Release();
			g_lpdd=NULL;
		}
		result=2;
	}
	else
		result=1;
	
	//创建Overlay表面
	DDSURFACEDESC   ddsdOverlay;
    int             i;
	
	
	DDPIXELFORMAT g_ddpfOverlayFormats[] = 
	{  
		{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 16,  0x7C00, 0x03e0, 0x001F, 0},      // 16-bit RGB 5:5:5
		{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 16,  0xF800, 0x07e0, 0x001F, 0},   // 16-bit RGB 5:6:5
		{sizeof(DDPIXELFORMAT), DDPF_FOURCC,MAKEFOURCC('U','Y','V','Y'),0,0,0,0,0}, // UYVY
		{sizeof(DDPIXELFORMAT), DDPF_FOURCC,MAKEFOURCC('Y','U','Y','2'),0,0,0,0,0}
	};  // YUY2
	
#define NUM_OVERLAY_FORMATS (sizeof(g_ddpfOverlayFormats) / sizeof(g_ddpfOverlayFormats[0]))
	
	
    if (!g_lpdd || !g_lpddsPrimary)
        result=3;
    else
	{
		// It's currently not possible to query for pixel formats supported by the
		// overlay hardware (though GetFourCCCodes() usually provides a partial 
		// list).  Instead you need to call CreateSurface() to try a variety of  
		// formats till one works.  
		INIT_DIRECTDRAW_STRUCT(ddsdOverlay);
		ddsdOverlay.ddsCaps.dwCaps=DDSCAPS_OVERLAY | DDSCAPS_FLIP | DDSCAPS_COMPLEX | DDSCAPS_VIDEOMEMORY;
		ddsdOverlay.dwFlags= DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_BACKBUFFERCOUNT| DDSD_PIXELFORMAT;
		ddsdOverlay.dwWidth=320;
		ddsdOverlay.dwHeight=240;
		ddsdOverlay.dwBackBufferCount=2;
		
		// Try to create an overlay surface using one of the pixel formats in our
		// global list.
		i=0;
		do 
		{
			ddsdOverlay.ddpfPixelFormat=g_ddpfOverlayFormats[i];
			// Try to create the overlay surface
			ddrval = g_lpdd->CreateSurface(&ddsdOverlay, &g_lpddsOverlay, NULL);
		} while( FAILED(ddrval) && (++i < NUM_OVERLAY_FORMATS) );
		
		// If we failed to create an overlay surface, let's try again with a single
		// (non-flippable) buffer.
		if(FAILED(ddrval))
		{
			// Just in case we're short on video memory or the hardware doesn't like flippable
			// overlay surfaces, let's make another pass using a single buffer.
			ddsdOverlay.dwBackBufferCount=0;
			ddsdOverlay.ddsCaps.dwCaps=DDSCAPS_OVERLAY | DDSCAPS_VIDEOMEMORY;
			ddsdOverlay.dwFlags= DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_PIXELFORMAT;
			// Try to create the overlay surface
			ddrval = g_lpdd->CreateSurface(&ddsdOverlay, &g_lpddsOverlay, NULL);
			i=0;
			do 
			{
				ddsdOverlay.ddpfPixelFormat=g_ddpfOverlayFormats[i];
				ddrval = g_lpdd->CreateSurface(&ddsdOverlay, &g_lpddsOverlay, NULL);
			} while( FAILED(ddrval) && (++i < NUM_OVERLAY_FORMATS) );
			
			// We just couldn't create an overlay surface.  Let's exit.
			if (FAILED(ddrval))
				result=3;
		}
		
	}
    
	//显示Overlay表面
	
	
	//........
	
	//DestroyOverlay表面
	if (g_lpddsOverlay)
	{
        // Use UpdateOverlay() with the DDOVER_HIDE flag to remove an overlay 
        // from the display.
        g_lpddsOverlay->UpdateOverlay(NULL, g_lpddsPrimary, NULL, DDOVER_HIDE, NULL);
		g_lpddsOverlay->Release();
		g_lpddsOverlay=NULL;
	}
	
	//FreeDirectDraw
	if (g_lpddsPrimary)
	{
		g_lpddsPrimary->Release();
		g_lpddsPrimary=NULL;
	}
	
    if (g_lpdd)
	{
		g_lpdd->Release();
		g_lpdd=NULL;
	}
	
	return (result==1);
}
BOOL DS_LookUpFilter(const GUID *clsidDeviceClass, VectorString &vec)
{
	ASSERT(clsidDeviceClass);
	
	IBaseFilter *pFilter = NULL;
	

	//	CComPtr <IBaseFilter>		pTmpFilter;
	CComPtr	<IEnumMoniker>		iEmMoniker;
	CComPtr	<ICreateDevEnum>	iCreateDevEnum;
	ULONG cFetched;
	CComPtr<IMoniker>iMoniker;	

	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, 
		NULL, CLSCTX_INPROC_SERVER,	IID_ICreateDevEnum, 
		(void**)&iCreateDevEnum);
	
	if(hr == NOERROR)
	{
		hr = iCreateDevEnum->CreateClassEnumerator
			(*clsidDeviceClass, &iEmMoniker, 0);
		if(hr == NOERROR) 
		{
			iEmMoniker->Reset();

			while(hr = iEmMoniker->Next(1, &iMoniker, &cFetched), hr == S_OK)
			{
				CComPtr<IPropertyBag>  iBag;
				char szTmpName[MAX_DEVICE_NAME_LEN];
				CString s;
				hr = iMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&iBag);
				if(SUCCEEDED(hr))
				{
					VARIANT var;
					var.vt = VT_BSTR;
					hr = iBag->Read(L"FriendlyName", &var, NULL);
					if (hr == NOERROR)
					{
						WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, szTmpName, MAX_DEVICE_NAME_LEN, NULL, NULL);
						SysFreeString(var.bstrVal);
						s = szTmpName;
						vec.push_back(s.LockBuffer());
				
					}
				}
				iMoniker.Release();
			}
		}
	}
	
	return TRUE;
}
BOOL DS_EnumFilterPin(IBaseFilter *pFilter, PIN_DIRECTION pinDir, VectorString &vec)
{
	HRESULT hr;
	ULONG cbret;
	CComPtr<IPin>m_p;
	CComPtr<IEnumPins>m_pin;
	PIN_INFO pInfo;

	if(pFilter == NULL)
		return FALSE;

	hr = pFilter->EnumPins(&m_pin);
	if(FAILED(hr))
		return FALSE;
	while(m_pin->Next(1, &m_p, &cbret) == S_OK)
	{
		hr = m_p->QueryPinInfo(&pInfo);
		if(FAILED(hr))
			return FALSE;
		USES_CONVERSION;
		if(pInfo.dir == pinDir)
			vec.push_back(W2A(pInfo.achName));
		m_p.Release();
	}
	return TRUE;
}
BOOL	DS_FilterDesignPin(IBaseFilter *pFilter, PIN_DIRECTION pinDir, LPCTSTR szPinName)
{
	HRESULT hr;
	ULONG cbret;
	CComPtr<IPin>m_p;
	CComPtr<IEnumPins>m_pin;
	PIN_INFO pInfo;

	if(pFilter == NULL)
		return FALSE;

	hr = pFilter->EnumPins(&m_pin);
	if(FAILED(hr))
		return FALSE;
	while(m_pin->Next(1, &m_p, &cbret) == S_OK)
	{
		hr = m_p->QueryPinInfo(&pInfo);
		if(FAILED(hr))
			return FALSE;
		USES_CONVERSION;
		if(pInfo.dir == pinDir)
		{
			CComPtr<IAMAudioInputMixer>m_mix;
			if(m_p->QueryInterface(IID_IAMAudioInputMixer, (void **)&m_mix) == S_OK)
			{
				if(_tcscmp(W2A(pInfo.achName), szPinName) == 0)
				{

				 BOOL bLoudness=0, bMono=0;
				double dblBass=0, dblBassRange=0, dblMixLevel=0, 
						dblPan=0, dblTreble=0, dblTrebleRange=0;
					hr = m_mix->get_Bass(&dblBass);
					hr = m_mix->get_BassRange(&dblBassRange);
					hr = m_mix->get_Loudness(&bLoudness);
					hr = m_mix->get_MixLevel(&dblMixLevel);
					hr = m_mix->get_Mono(&bMono);
					hr = m_mix->get_Pan(&dblPan);
					hr = m_mix->get_Treble(&dblTreble);
					hr = m_mix->get_TrebleRange(&dblTrebleRange);
					hr = m_mix->put_Enable(TRUE);

				}
				else
				{
					m_mix->put_Enable(FALSE);
				}
			}
		}
		m_p.Release();
	}
	return TRUE;
}

//取得音频采集输出口的媒体类型
BOOL	DS_EnumAudioCapMedType(const GUID *clsidDeviceClass, LPCTSTR sDevName, LPCTSTR sPinName, VectorString &vec)
{
	CComPtr<IBaseFilter>pCapFilter;
	CComPtr<IPin>m_pin;
	CComPtr<IAMStreamConfig>m_pincfg;
	//创建filter
	if(!DS_CreateFilterByName(clsidDeviceClass, sDevName, &pCapFilter))
		return FALSE;
	//查找输出Pin脚
	if(!DS_FindPin(&m_pin, pCapFilter, sPinName, PINDIR_OUTPUT))
		return FALSE;

	//
	return DS_EnumPinMedType(m_pin, vec);
}
//取得Pin口的媒体类型
BOOL	DS_EnumPinMedType(IPin *pPin, VectorString &)
{
	//
	CComPtr<IAMStreamConfig>m_pincfg;
	HRESULT hr;

	AUDIO_STREAM_CONFIG_CAPS  pAudioconfig;
	AM_MEDIA_TYPE *pmt;
	WAVEFORMATEX *wavfmt;

	int ncnt, nsize;

	if(pPin == NULL)
		return FALSE;
	//找出输出Pin脚的IAMStreamConfig接口
	hr = pPin->QueryInterface(IID_IAMStreamConfig, (void **)&m_pincfg);
	if(hr != S_OK)
		return FALSE;
	if(m_pincfg->GetNumberOfCapabilities(&ncnt, &nsize)!= S_OK)
		return FALSE;
	//找到当前和系统所有的媒体类型
	for(int i=0; i< ncnt; i++)
	{
		m_pincfg->GetStreamCaps(i, &pmt, reinterpret_cast<BYTE*>(&pAudioconfig));
		wavfmt = reinterpret_cast<WAVEFORMATEX*>(pmt->pbFormat);
		MyFreeMediaType(pmt);
		DeleteMediaType(pmt);
	}
	return TRUE;
}
//取得Pin口的媒体类型
BOOL	DS_EnumPinEMedType(IPin *pPin, VectorWavFormat &vec)
{
	//
	CComPtr<IEnumMediaTypes>m_enumpin;
	HRESULT hr;

	AM_MEDIA_TYPE *pmt;
	WAVEFORMATEX *fmt;
	ULONG pucFted;

	if(pPin == NULL)
		return FALSE;
	//找出输出Pin脚的IAMStreamConfig接口
	hr = pPin->EnumMediaTypes(&m_enumpin);
	if(hr != S_OK)
		return FALSE;

	//找到当前和系统所有的媒体类型

	while(m_enumpin->Next(1, &pmt, &pucFted) == S_OK)
	{
		fmt = reinterpret_cast<WAVEFORMATEX*>(pmt->pbFormat);
		vec.push_back(*fmt);
		MyFreeMediaType(pmt);
		DeleteMediaType(pmt);
	}

	return TRUE;
}
//取得某个filter某个脚的所有媒体类型
BOOL	DS_EnumFilterMedType(
						 IBaseFilter *pFilter, 
						 LPCTSTR szPinName, 
						 PIN_DIRECTION pinDir,
						 VectorWavFormat &vec)
{
	CComPtr<IPin>pPin;
	if(pFilter == NULL)
		return FALSE;

	if(!DS_FindPin(&pPin, pFilter, szPinName, pinDir))
		return FALSE;
	return DS_EnumPinEMedType(pPin, vec);
}
BOOL	DS_EnumFilterMedType(
						   IBaseFilter *pFilter, 
						   LPCTSTR szPinName, 
						   PIN_DIRECTION pinDir,
						   LPSTR sname)
{
	CComPtr<IPin>pPin;
	if(pFilter == NULL)
		return FALSE;

	if(!DS_FindPin(&pPin, pFilter, szPinName, pinDir))
		return FALSE;
	return TRUE;
}
void	MyFreeMediaType(IN AM_MEDIA_TYPE *mt)
{
    if (mt->cbFormat != 0)
    {
        CoTaskMemFree((PVOID)mt->pbFormat);
        mt->cbFormat = 0;
        mt->pbFormat = NULL;
    }
    if (mt->pUnk != NULL)
    {
        // Unecessary because pUnk should not be used, but safest.
        mt->pUnk->Release();
        mt->pUnk = NULL;
    }
}
//取得某个filter所有某个脚的媒类型
BOOL	DS_GetFilterCurMedType(
						  IBaseFilter *pFilter, 
						  LPCTSTR szPinName, 
						  PIN_DIRECTION pinDir,
						  WAVEFORMATEX *wavfmt)
{
	CComPtr<IPin>pPin;
	AM_MEDIA_TYPE pmt;
//	WAVEFORMATEX *wavfmt;

	if(pFilter == NULL)
		return FALSE;

	if(!DS_FindPin(&pPin, pFilter, szPinName, pinDir))
		return FALSE;
	
	if(pPin->ConnectionMediaType(&pmt) != S_OK)
		return FALSE;

//	wavfmt = reinterpret_cast<WAVEFORMATEX*>(pmt.pbFormat);
	memcpy(wavfmt, pmt.pbFormat, sizeof(WAVEFORMATEX));
	MyFreeMediaType(&pmt);
//	DeleteMediaType(&pmt);

	return TRUE;
}
//设置filter某个pin脚的媒体类型
BOOL	DS_SetFilterCurMedType(
						  IBaseFilter *pFilter,
						  LPCTSTR szPinName, 
						  PIN_DIRECTION pinDir,
						  WAVEFORMATEX* wavfmt)
{
	BOOL bRet = TRUE;
	CComPtr<IPin>pPin;
	CComPtr<IAMStreamConfig>pConfig;
	AM_MEDIA_TYPE *pmt;

	HRESULT hr;

	if(pFilter == NULL)
		return FALSE;

	if(!DS_FindPin(&pPin, pFilter, szPinName, pinDir))
		return FALSE;
	
	//查询IAMStreamConfig接口
	hr = pPin->QueryInterface(IID_IAMStreamConfig, (void **)&pConfig);
	if(hr != S_OK)
		return FALSE;
	hr = pConfig->GetFormat(&pmt);

	//将新的格式送给当前媒体类型结构中
	memcpy(pmt->pbFormat, wavfmt, sizeof(WAVEFORMATEX));


	//在这里判断如果cbSize>0则要进行比较相等才会去进一步进行
		//查询与之相等的媒体类型
	if(wavfmt->cbSize>0)
	{
		LPWAVEFORMATEX pfmt;
		if(DS_FindFilterMedTye(pPin, wavfmt, &pfmt))
		{
			memcpy(pmt->pbFormat, pfmt, sizeof(WAVEFORMATEX)+wavfmt->cbSize);
		}
		else
		{
			MyFreeMediaType(pmt);
			DeleteMediaType(pmt);

			return FALSE;
		}
	}
	if(pConfig->SetFormat(pmt) == S_OK)
		bRet = TRUE;
	else
		bRet = FALSE;

	MyFreeMediaType(pmt);
	DeleteMediaType(pmt);

	return bRet;
}
BOOL	DS_FindFilterMedTye( IPin *pPin, 
							WAVEFORMATEX *wavfmt, 
							LPWAVEFORMATEX *nwavfmt )
{
	CComPtr<IEnumMediaTypes>m_enumpin;
	HRESULT hr;

	LPWAVEFORMATEX fmt;

	AM_MEDIA_TYPE *pmt;
	ULONG pucFted;

	if(pPin == NULL)
		return FALSE;
	//找出输出Pin脚的IAMStreamConfig接口
	hr = pPin->EnumMediaTypes(&m_enumpin);
	if(hr != S_OK)
		return FALSE;

	//找到当前和系统所有的媒体类型
	while(m_enumpin->Next(1, &pmt, &pucFted) == S_OK)
	{
		fmt = reinterpret_cast<WAVEFORMATEX*>(pmt->pbFormat);
		//比较是否相等
		if(memcmp(fmt, wavfmt, sizeof(WAVEFORMATEX)) == 0)
		{
			LPWAVEFORMATEX pfmt = (LPWAVEFORMATEX)malloc(sizeof(WAVEFORMATEX) + fmt->cbSize);
			memcpy(pfmt, fmt, sizeof(WAVEFORMATEX) + fmt->cbSize);
			*nwavfmt = pfmt;
			MyFreeMediaType(pmt);
			DeleteMediaType(pmt);
			return TRUE;
		}
		MyFreeMediaType(pmt);
		DeleteMediaType(pmt);
	}

	return FALSE;
}
BOOL	DS_SetFileName(
						  IBaseFilter *pFilter, 
						  LPADData pAd
						  )
{

	CTime t = CTime::GetCurrentTime();
	CString s, s2;
	switch(pAd->audiotype)
	{
	case 0:
		s2 = ".wav";
		break;
	case 1:
		s2 = ".mp3";
		break;
	case 2:
		s2 = ".wma";
		break;
	}

	s = t.Format("%y%m%d%H%M%S");
	s += s2;
	s2 = s;
	s = pAd->szDirectory;
	s += _T("\\");
	s += s2;
	CComPtr<IFileSinkFilter>pFile;

	USES_CONVERSION;
	if(pFilter->QueryInterface(IID_IFileSinkFilter, (void **)&pFile) == S_OK)
	{
		if(pFile->SetFileName(T2OLE(s.LockBuffer()), NULL) == S_OK)
		{
			strcpy(pAd->szFile, s.LockBuffer());
			return TRUE;
		}
	}	
// 	else
// 	{
// 		CComPtr<IFileSinkFilter>pF;
// 		pFilter->Quer
// 	}
	return FALSE;
}

bool	DS_LoadWmaProfile(	IN IBaseFilter *pFilter, 
							IN LPCTSTR szfname)
{
	WIN32_FIND_DATA fd;
	if(::FindFirstFile(szfname, &fd) == INVALID_HANDLE_VALUE)
	{
		return false;	
	}
	WCHAR *buf = NULL;
	HANDLE hFile;
	DWORD dwret;
	DWORD dwread;
	hFile = ::CreateFile(szfname, GENERIC_READ, FILE_SHARE_READ , NULL,
		OPEN_EXISTING, 0, NULL);
	BY_HANDLE_FILE_INFORMATION finf;
	GetFileInformationByHandle(hFile, &finf);
	
	dwret = finf.nFileSizeLow;
	buf = new WCHAR[dwret/2 ];
//	buf[dwret/2] = L'0';
/*	buf[dwret+1] = 0;*/
	ReadFile(hFile, buf, dwret, &dwread, NULL);
	CloseHandle(hFile);
	IWMProfileManager* pProfileMgr = NULL;
	IWMProfile*        pProfile    = NULL;
	CComPtr<IConfigAsfWriter> pcfg;
	HRESULT hr = S_OK;

	// Create a profile manager.
	hr = WMCreateProfileManager(&pProfileMgr);

	// Retrieve the data for the general-purpose broadband video profile.
	hr = pProfileMgr->LoadProfileByData(
		(WCHAR *)buf, &pProfile);

	hr = pFilter->QueryInterface(IID_IConfigAsfWriter, (void **)&pcfg);
	hr = pcfg->ConfigureFilterUsingProfile(pProfile);

	delete []buf;
	// Clean up.
	pProfile->Release();
	pProfile = NULL;
	pProfileMgr->Release();
	pProfileMgr = NULL;
	if(FAILED(hr))
		return false;
	return true;
}


BOOL	DS_Confingwma(		
							IN IBaseFilter *pFilter)
{
	bool bret;
	HRESULT hr;
	CComPtr<IConfigAsfWriter> pcfg;
	CComPtr<IWMProfile >profile;
	CComPtr<IWMStreamConfig>pstream;
	CComPtr<IWMMediaProps >mediaprop;
	WMT_VERSION ver;
	WM_MEDIA_TYPE wmtype;
	GUID guid;
	int i;
	DWORD dwret,dwrate;
	
	DS_LoadProfile(&profile);
// 	DS_CreateProfile(&profile);

	hr = pFilter->QueryInterface(IID_IConfigAsfWriter, (void **)&pcfg);
	hr = pcfg->ConfigureFilterUsingProfile(profile);
	return true;

	hr = pcfg->GetCurrentProfile(&profile);
	hr = profile->GetVersion(&ver);
	hr = profile->GetStreamCount(&dwret);
	for( i = 1; i<= dwret; i++)
	{
		hr = profile->GetStreamByNumber(i, &pstream);

		pstream->GetStreamType(&guid);
		if(guid == WMMEDIATYPE_Audio)
		{
			ASSERT(0);	
		}
		if(guid == WMMEDIATYPE_Video)
		{
			ASSERT(0);
		}
		hr = pstream->GetBitrate(&dwrate);
	}

	hr = pFilter->QueryInterface(IID_IWMMediaProps , (void **)&mediaprop);
	mediaprop->GetMediaType(NULL, &dwret);
	mediaprop->GetMediaType(&wmtype, &dwret);
	mediaprop->GetType(&guid);
	return bret;
}

void	DS_CreateProfile(IWMProfile **pProfiles)
{
	IWMProfileManager*       pProfileMgr = NULL;
	IWMProfile*              pProfileTmp = NULL;
 	IWMProfile3*             pProfile    = NULL;
	IWMStreamPrioritization* pPriority   = NULL;

	WM_STREAM_PRIORITY_RECORD StreamArray[3];
	HRESULT hr = S_OK;
	IWMStreamConfig *pStream;
	// Initialize COM.

	// Create a profile manager object.
	hr = WMCreateProfileManager(&pProfileMgr);

	// Create an empty profile.
	hr = pProfileMgr->CreateEmptyProfile(WMT_VER_7_0, &pProfileTmp);

	// Get the IWMProfile3 for the new profile, then release the old one.
	hr = pProfileTmp->QueryInterface(IID_IWMProfile3, (void **)&pProfile);
	pProfileTmp->Release();
	pProfileTmp = NULL;

	// Give the new profile a name and description.
	hr = pProfile->SetName(L"Prioritization_Example");
	hr = pProfile->SetDescription(L"Only for use with example code.");

	// Create the first stream.
	hr = pProfile->CreateNewStream(WMMEDIATYPE_Audio, &pStream);

	// TODO: configure the stream as needed for the scenario.

	// Set the stream number.
	hr = pStream->SetStreamNumber(1);

	// Give the stream a name for clarity.
	hr = pStream->SetStreamName(L"Lecturer_Audio");

	// Include the new stream in the profile.
	hr = pProfile->AddStream(pStream);

	// Release the stream configuration interface.
	pStream->Release();
	pStream = NULL;
	pProfileMgr->Release();
	pProfileMgr = NULL;
	
	*pProfiles = pProfile;
}

void	DS_LoadProfile(IWMProfile **pProfiles)
{
	IWMProfileManager* pProfileMgr = NULL;
	IWMProfile*        pProfile    = NULL;

	HRESULT hr = S_OK;

	// Initialize COM.
	hr = CoInitialize(NULL);

	// Create a profile manager.
	hr = WMCreateProfileManager(&pProfileMgr);

	// Retrieve the data for the general-purpose broadband video profile.
	hr = pProfileMgr->LoadProfileByID(WMProfile_V70_56DialUpStereo, &pProfile);

	// TODO: Perform whatever customizations are needed. For details about
	// editing profiles, see Using Custom Profiles.

	// Clean up.
// 	pProfile->Release();
// 	pProfile = NULL;
	pProfileMgr->Release();
	pProfileMgr = NULL;
	*pProfiles = pProfile;
}

bool	DS_CreateFilter(IN const GUID *pGUID,
						OUT IBaseFilter **ppFilter )
{
	ASSERT( pGUID);

	IBaseFilter *pFilter = NULL;
	HRESULT hr;
	hr = CoCreateInstance(*pGUID, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&pFilter);
	if(FAILED(hr))
		return FALSE;

	if(ppFilter)
		*ppFilter = pFilter;
	else
		SAFE_RELEASE(pFilter);

	return TRUE;
}