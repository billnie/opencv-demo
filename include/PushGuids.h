//------------------------------------------------------------------------------
// File: PushGuids.h
//
// Desc: DirectShow sample code - GUID definitions for PushSource filter set
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#pragma once

#ifndef __PUSHGUIDS_DEFINED
#define __PUSHGUIDS_DEFINED

// {9A80E195-3BBA-4821-B18B-21BB496F80F8}
DEFINE_GUID(CLSID_ScreenBmp, 
0xb88551e6, 0xb35, 0x4e37, 0x92, 0xe1, 0xf3, 0xac, 0x40, 0x35, 0x88, 0xa9);

// {CB83D662-33A5-4dbf-830C-25E52627C1C3}
DEFINE_GUID(CLSID_ScreenBmpSet, 
0x6679ba97, 0x4bf0, 0x4289, 0x88, 0x9c, 0x4b, 0x94, 0xb7, 0xa3, 0x92, 0x22);

// {4EA6930A-2C8A-4ae6-A561-56E4B5044437}
DEFINE_GUID(CLSID_ScreenCap, 
0x1c0a3105, 0x15f9, 0x4a75, 0xb5, 0x92, 0xd9, 0x80, 0xbd, 0xa9, 0x43, 0x6b);

// {CD941008-953A-4a1d-AE3A-1965FE2E600E}
DEFINE_GUID(CLSID_ScreenCapSetup, 
0xcd941008, 0x953a, 0x4a1d, 0xae, 0x3a, 0x19, 0x65, 0xfe, 0x2e, 0x60, 0xe);

#endif
