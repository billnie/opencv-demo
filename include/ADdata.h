// addata.h: interface for the CValidFunction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VALIDFUNCTION_H__F23994E6_9038_47E3_8715_9E907ADEC71E__INCLUDED_)
#define AFX_VALIDFUNCTION_H__F23994E6_9038_47E3_8715_9E907ADEC71E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MSG_PLAYBACK 1
#define MSG_MOVEPLAYWND 2
#define WM_AUDIO_CAP WM_USER+1212	//视频窗口要处理的消息
#define	WM_PL_WND		//播放文件列表窗口要处理的消息

//回放要处理的消息
#define WM_AD_MSG	0	//与录音有关的操作-----------大类
#define WM_AD_RECORD	1	//录音
#define WM_AD_PAUSE	2	//暂停
#define WM_AD_STOP	3	//停止
#define	WM_AD_CONTIN 4	//继续录音

//视频窗口要处理的消息
#define WM_MCM_MSG	0	//大类
#define WM_QUERY_ENAUDIO 0	//查询压缩时的参数
#define WM_QUERY_XMLPARSE	1	//查询xml处理类
#define WM_QUERY_MP3IDV1	2	//为录像文件写id信息
#define	WM_QUERY_ADDATA		3	//查询录音数据结构
#define	WM_QUERY_RECVEC		4	//查询录音类型数组


//播放文件列表窗口要处理的消息
#define	WM_GET_PROP 0		//大类
#define	WM_GET_ADDATA	0

typedef struct _tagADData
{
	int audiotype;	//媒体类型为未设置，0为wav, 1为mp3
	int curstatus;	//当前状态0为未设置，1为录音，2为暂停，3为停止。
	TCHAR szDirectory[255];	//工作目录
	TCHAR szFile[255];
	TCHAR szRecDev[255];
	TCHAR szRecDevPin[255];
	WAVEFORMATEX wav;
}ADData, *LPADData;


#endif // !defined(AFX_VALIDFUNCTION_H__F23994E6_9038_47E3_8716_9E907ADEC71E__INCLUDED_)
