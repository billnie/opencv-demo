// DialogBarEx.cpp : implementation file
//

#include "stdafx.h"

#include "DialogBarEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogBarEx
/*
Class written by Sharad Kelkar drssk@ad1.vsnl.net.in
This is freeware without any kind of restriction on usage
and distribution.
*/


#define WM_INITDIALOGBAR WM_USER + 1

BEGIN_MESSAGE_MAP(CDialogBarEx,  CDialogBar)
	//{{AFX_MSG_MAP(CDialogBarEx)
	ON_WM_CREATE()
	ON_MESSAGE(WM_INITDIALOGBAR , InitDialogBarHandler )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//##ModelId=468B5EFE01F5
CDialogBarEx::CDialogBarEx()
{
}

//##ModelId=468B5EFE01F8
CDialogBarEx::~CDialogBarEx()
{
}

// Note from Sharad Kelkar
// We have manualy added entry ON_MESSAGE(WM_INITDIALOGBAR , InitDialogBarHandler)
// as there is not automatic help from Class Wizard



/////////////////////////////////////////////////////////////////////////////
// CDialogBarEx message handlers

//##ModelId=468B5EFE0204
int CDialogBarEx::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
		if (CDialogBar::OnCreate(lpCreateStruct) == -1)
		return -1;
 	
	// TODO: Add your specialized creation code here
	// --------- 
	// We post WM_INITDIALOGBAR message here to dialog bar
	PostMessage(WM_INITDIALOGBAR , 0 , 0 );
	return 0;
}


//##ModelId=468B5EFE01FA
long CDialogBarEx::InitDialogBarHandler(WPARAM wParam, LPARAM lParam)
{
	UpdateData(FALSE);
	OnInitDialogBar() ;
	return 0;
}

// Notes from Sharad Kelkar
// OnInitDialogBar is Just empty function it is
// expected to be overriden from derived class

//##ModelId=468B5EFE01F6
void CDialogBarEx::OnInitDialogBar()
{
// TODO
// Add your custom initialization code here.

}
