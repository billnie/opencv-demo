


#pragma once

#ifdef __cplusplus

extern "C" {
#endif


// {51EDC64F-01A2-4383-89E8-A51057878A0D}
DEFINE_GUID(IID_IScreenCap, 
0xa54de107, 0x2909, 0x4d5b, 0x8f, 0x1d, 0xc9, 0x9c, 0x7a, 0x46, 0xc9, 0x4e);

//DT is a macro to help debug app by dump useful info, 
//it works for debug and release version.
//usage:
//DT(_T("work ok in line %d"),m_nLine);

//define following line to enable DT
DECLARE_INTERFACE_(IScreenCap, IUnknown)
{



    STDMETHOD(put_bShowText) (THIS_
    				  bool bShow       /* [in] */	// the media type selected
				 ) PURE;

    STDMETHOD(get_bShowText) (THIS_
    				  bool *bShow       /* [in] */	// the media type selected
				 ) PURE; 
	STDMETHOD(get_ShowText)	(THIS_
					LPTSTR , DWORD dw, DWORD *dwret)PURE;
	STDMETHOD(put_ShowText)	(THIS_
					LPCTSTR , DWORD dw)PURE;
	//与鼠标大小相关的的接口
	STDMETHOD (put_ShowMouse) (bool bShow) = 0;	//设置显示鼠标与否
	STDMETHOD (get_ShowMouse) (bool *bShow) = 0;	//取得当前是否显示鼠标
	STDMETHOD (put_MouseSize) (POINT nSize) = 0;		//设置鼠标大小
	STDMETHOD (get_MouseSize) (POINT *nSize) = 0;		//取得鼠标大小

	//与捕捉窗口大小相关的接口
	STDMETHOD (get_WindowSize) (RECT *r) = 0;			//设置窗口大小
	STDMETHOD (put_WindowSize) (RECT r) = 0;			//取得窗口大小
	STDMETHOD (get_MiniSize) (POINT *pt) = 0;			//最小可以设置多大的窗口
};

#ifdef __cplusplus
}
#endif