//
//  paybox.h
//  PayBox
//
//  Created by billnie on 3/8/13.
//  Copyright (c) 2013 rosschen. All rights reserved.
//

#ifndef ePayBox_posenc_h
#define ePayBox_posenc_h

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
    char *md5Str(const char*str);
	unsigned char* str2hex(const char *str, int* len);
	char* hextStr(unsigned char* c, int len);
	char *hex2bstr(unsigned char* c, int len);
	char * skipblank(char *buf);
	unsigned char asc2hex(unsigned char b);
	char * decodeCode(const char *keyStr, int *len);
	char * decodeByPrivteKey(const char *priKey, const char *mess, int *len);
	char * decodeByPubKey(const char *pubKey, const unsigned char *mess, int *len)  ;
	 //base64的方式的加密
	int encodeByPubKey(const char *pubKey, const char *mess, char **encBody);
	 //bio的方式的加密
	int encodeByBioPubKey(const char *pubKey, const char *mess, char **encBody);
	    
	int encodeByBIOPriKey(const char *priKey, const unsigned char * mess,int data_len, char **encBody)   ;
	    
	//key检�?int checkRSA(const unsigned char *key, int data_len, int type);

	int rsa_Sign(const char *priKey, const unsigned char *mess, int data_len, int nid, char **sigBody);
	int very_Sign(const char *pubKey, const unsigned char *mess, int data_len, int nid, char **sigBody);
	char* base64_encode(const char* data, int data_len);
	char *base64_decode(const char* data, int data_len, int *retlen);
	    
	int sha(const unsigned char *data, int data_len, unsigned char **encBody);
    
    int seedRand(int len, unsigned char **randBody);
//对串进行md5,并且返回�?char *md5Str(const char*str);
#ifdef __cplusplus
}
#endif
#endif
