

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Tue Jan 08 16:13:50 2008
 */
/* Compiler settings for .\IniDeal.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __IniDeal_h__
#define __IniDeal_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IIniFile_FWD_DEFINED__
#define __IIniFile_FWD_DEFINED__
typedef interface IIniFile IIniFile;
#endif 	/* __IIniFile_FWD_DEFINED__ */


#ifndef __IniFile_FWD_DEFINED__
#define __IniFile_FWD_DEFINED__

#ifdef __cplusplus
typedef class IniFile IniFile;
#else
typedef struct IniFile IniFile;
#endif /* __cplusplus */

#endif 	/* __IniFile_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IIniFile_INTERFACE_DEFINED__
#define __IIniFile_INTERFACE_DEFINED__

/* interface IIniFile */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IIniFile;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D2EF1542-A1CA-4755-89E3-4EBF0A939B36")
    IIniFile : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetFile( 
            BSTR str) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStringValue( 
            BSTR section,
            BSTR key,
            BSTR value,
            DWORD len,
            DWORD *rlen) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStringValue( 
            BSTR section,
            BSTR key,
            BSTR value) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Save( 
            BSTR fname) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetIntValue( 
            BSTR section,
            BSTR key,
            ULONG *value) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetIntValue( 
            BSTR section,
            BSTR key,
            LONG value) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EreaseSection( 
            BSTR s) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EreaseKey( 
            BSTR section,
            BSTR key) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IIniFileVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IIniFile * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IIniFile * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IIniFile * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetFile )( 
            IIniFile * This,
            BSTR str);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetStringValue )( 
            IIniFile * This,
            BSTR section,
            BSTR key,
            BSTR value,
            DWORD len,
            DWORD *rlen);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetStringValue )( 
            IIniFile * This,
            BSTR section,
            BSTR key,
            BSTR value);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IIniFile * This,
            BSTR fname);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetIntValue )( 
            IIniFile * This,
            BSTR section,
            BSTR key,
            ULONG *value);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetIntValue )( 
            IIniFile * This,
            BSTR section,
            BSTR key,
            LONG value);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EreaseSection )( 
            IIniFile * This,
            BSTR s);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EreaseKey )( 
            IIniFile * This,
            BSTR section,
            BSTR key);
        
        END_INTERFACE
    } IIniFileVtbl;

    interface IIniFile
    {
        CONST_VTBL struct IIniFileVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IIniFile_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IIniFile_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IIniFile_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IIniFile_SetFile(This,str)	\
    (This)->lpVtbl -> SetFile(This,str)

#define IIniFile_GetStringValue(This,section,key,value,len,rlen)	\
    (This)->lpVtbl -> GetStringValue(This,section,key,value,len,rlen)

#define IIniFile_SetStringValue(This,section,key,value)	\
    (This)->lpVtbl -> SetStringValue(This,section,key,value)

#define IIniFile_Save(This,fname)	\
    (This)->lpVtbl -> Save(This,fname)

#define IIniFile_GetIntValue(This,section,key,value)	\
    (This)->lpVtbl -> GetIntValue(This,section,key,value)

#define IIniFile_SetIntValue(This,section,key,value)	\
    (This)->lpVtbl -> SetIntValue(This,section,key,value)

#define IIniFile_EreaseSection(This,s)	\
    (This)->lpVtbl -> EreaseSection(This,s)

#define IIniFile_EreaseKey(This,section,key)	\
    (This)->lpVtbl -> EreaseKey(This,section,key)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_SetFile_Proxy( 
    IIniFile * This,
    BSTR str);


void __RPC_STUB IIniFile_SetFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_GetStringValue_Proxy( 
    IIniFile * This,
    BSTR section,
    BSTR key,
    BSTR value,
    DWORD len,
    DWORD *rlen);


void __RPC_STUB IIniFile_GetStringValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_SetStringValue_Proxy( 
    IIniFile * This,
    BSTR section,
    BSTR key,
    BSTR value);


void __RPC_STUB IIniFile_SetStringValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_Save_Proxy( 
    IIniFile * This,
    BSTR fname);


void __RPC_STUB IIniFile_Save_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_GetIntValue_Proxy( 
    IIniFile * This,
    BSTR section,
    BSTR key,
    ULONG *value);


void __RPC_STUB IIniFile_GetIntValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_SetIntValue_Proxy( 
    IIniFile * This,
    BSTR section,
    BSTR key,
    LONG value);


void __RPC_STUB IIniFile_SetIntValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_EreaseSection_Proxy( 
    IIniFile * This,
    BSTR s);


void __RPC_STUB IIniFile_EreaseSection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IIniFile_EreaseKey_Proxy( 
    IIniFile * This,
    BSTR section,
    BSTR key);


void __RPC_STUB IIniFile_EreaseKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IIniFile_INTERFACE_DEFINED__ */



#ifndef __INIDEALLib_LIBRARY_DEFINED__
#define __INIDEALLib_LIBRARY_DEFINED__

/* library INIDEALLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_INIDEALLib;

EXTERN_C const CLSID CLSID_IniFile;

#ifdef __cplusplus

class DECLSPEC_UUID("711FF3E7-B518-4189-A1CD-06BA5F80F288")
IniFile;
#endif
#endif /* __INIDEALLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


