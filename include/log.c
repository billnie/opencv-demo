/**
Copyright (C) 2012 - Frank de Brabander

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include <stdio.h>
#include <string.h>
#include	<stdarg.h>
#ifdef ANDROIDDEV
#include <jni.h>
#elif WIN32
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <windows.h> 
//#import <Foundation/Foundation.h>
#endif
#include "log.h"

int logs(LogPriority prio, const char * tag, const char *fmt, ...)
{


#ifdef ANDROIDDEV    
    char pszDest[1024] = {0};
    int DestLen = 1024;
    va_list args;
    
    va_start(args, fmt);
    vsnprintf(pszDest,DestLen, fmt, args);
    va_end(args);
    if(prio > LOGVERBOSE)
    {
        __android_log_print((int)prio, tag, pszDest);
    }
#elif WIN32
	char buf[4096] = {0};
	char *p;
	
	va_list args;
	p = buf;

	va_start(args, fmt);
	p += _vsnprintf(p, sizeof buf - 1, fmt, args);
	va_end(args);
	if(prio > LOGVERBOSE)
	{
		OutputDebugString(buf);
		//logfile(logfilename, buf);
		//logfile(logfilename, "\r\n");
	}

#else
    char pszDest[1024] = {0};
    int DestLen = 1024;
    va_list args;
    
    va_start(args, fmt);
    vsnprintf(pszDest,DestLen, fmt, args);
    va_end(args);
    if(prio > LOGVERBOSE){
        NSLog([NSString stringWithFormat:@"%s", pszDest]);
    }
#endif


	return 0;
}

void printuint8array(LogPriority prio,const char *tag, uint8_t *audio, int len)
{
	if(audio && len >0)
	{
		char sz[8] = {0};
		char buf[1024] = {0};
		int i ;
		for(i = 0; i < len; i++)
		{
			sprintf(sz, "%02x,", audio[i]);
			strcat(buf, sz);
			if(i > 256)
				break;
		}
		logs(prio, tag, buf);
	}
}

