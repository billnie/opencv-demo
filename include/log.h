/* -*- mode:c++; indent-tabs-mode: nil -*- */
/* Copyright (c) 2007, Technische Universitaet Berlin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright 
 *   notice, this list of conditions and the following disclaimer in the 
 *   documentation and/or other materials provided with the distribution.
 * - Neither the name of the Technische Universitaet Berlin nor the names 
 *   of its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * This CRC-16 function produces a 16-bit running CRC that adheres to the
 * ITU-T CRC standard.
 *
 * The ITU-T polynomial is: G_16(x) = x^16 + x^12 + x^5 + 1
 * @param crc Running CRC value
 * @param b Byte to "add" to the CRC
 * @return New CRC value
 *
 * @author Andreas Koepke <koepke@tkn.tu-berlin.de> 
 * @author Paul Curtis (pointed out this implementation, which is believed to be public domain)
 */
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif
typedef enum LogPriority{
    LOGUNKNOWN=0,
    LOGDEFAULT,/*onlyforSetMinPriority()*/
    LOGVERBOSE,
    LOGDEBUG,
    LOGINFO,
    LOGWARN,
    LOGERROR,
    LOGFATAL,
    LOGSILENT,/*onlyforSetMinPriority();mustbelast*/
}LogPriority;

int logs(LogPriority prio, const char * tag, const char *fmt, ...);

void printuint8array(LogPriority prio, const char *tag, uint8_t *audio, int len);
#define xprintf(format, ...) logs(LOGVERBOSE,"xx", format, __VA_ARGS__)
#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif
