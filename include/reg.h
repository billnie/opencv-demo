
#pragma once
//收集硬件信息
#define HDMES _T("HD:")	//硬盘序列号
#define	LDMES _T("LD:")	//逻辑盘序列号
#define	MACMES _T("MAC:") //MAC地址信息
#define	MATMES	_("MAT:") //主板序列号信息
#define	ENDMES _T("@@#")	//一个硬件信息写完
//用户序列号信息
#define USEMES _T("USER:")	//用户名
#define SNMES _T("SN:")		//序列号
#define FHUA	_T("FHUA:")	//密文，由用户名和密钥产的密文
#define METHOD _T("METHOD:")	//密码产生的方式
//用户使用限制信息存在扇区中
#define	TNMES	_T("TN:")		//剩下使用的次数都是4个byte字节
#define	TDMES	_T("TD:")		//剩下使用的天数都是4个byte字节
#define	LTMES	_T("LT:")		//最后一天登录日期，如果两个日期不同，则算一天，防止修改日期作蔽

//收集注册信息用的结构
typedef struct _tagHdInfo
{
	TCHAR sdisk[128];				//硬盘序列号
	TCHAR sdiskserial[MAX_PATH];	//逻辑分区序列号
	TCHAR smac[64];					//mac地址
	TCHAR smaboard[32];				//主板序列号
}HdInfo;

//注册信息结构
typedef struct _tagSerInfo
{
	TCHAR suser[MAX_PATH];		//用户名
	TCHAR ssn[MAX_PATH];		//序列号
	TCHAR sgeruser[MAX_PATH];	//用户加密后的密文
	TCHAR smorth[MAX_PATH];		//加密方法
}SerInfo;

//扇区所有的结构信息
typedef struct _tagSectorInfo
{
	int m_revday;			//剩下多少天
	int m_revtime;			//剩下多少次可试用
	TCHAR ssn[MAX_PATH];
	BYTE m_type;			//方式，是剩下多少天，还是剩下多少次
	TCHAR slastday[16];		//最后登录日期
}SectorInfo;