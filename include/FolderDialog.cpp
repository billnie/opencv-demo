// FolderDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FolderDialog.h"
#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
#pragma comment (lib, "shlwapi.lib")
using namespace CFolderDialog_Helper;
#define PACKVERSION(major,minor) MAKELONG(minor,major)

/////////////////////////////////////////////////////////////////////////////
// CFolderDialog
//##ModelId=47E46ACB01E6
WNDPROC			CFolderDialog::s_pOldWndProc(NULL);
//##ModelId=47E46ACB01EE
const CSize		CFolderDialog::s_sizeMin(280, 280);
//##ModelId=47E46ACB0203
CFolderDialog::CSizeItemListMap CFolderDialog::s_mapSizeItemList;

//
//##ModelId=47E46ACB0107
CFolderDialog::CFolderDialog()
{
	m_hOwner = NULL;

	m_uFlag = BIF_RETURNFSANCESTORS 
			| BIF_DONTGOBELOWDOMAIN 
			| BIF_RETURNONLYFSDIRS 
			| BIF_EDITBOX | BIF_VALIDATE
		//	| BIF_STATUSTEXT
				; 

	if(CFolderDialog::IsNewUIUsable())
		m_uFlag |= BIF_NEWDIALOGSTYLE;

	m_bEditReadonly = TRUE;
	m_bShowCreateButton = TRUE;
	m_nCSIDLRoot = 0;//=CSIDL_DESKTOP;
	m_strQueryCreateFormat = _T("Directory: \n\n '%1' \n\n not exists!  Do you want to create it?  ");

	m_pBrowseCallbackProc	= CFolderDialog::BrowseCallbackProc;
	m_pNewWndProc			= CFolderDialog::NewWndProc;

	::OleInitialize(NULL);
}

//##ModelId=47E46ACB0111
CFolderDialog::~CFolderDialog()
{
	::OleUninitialize();
}

/////////////////////////////////////////////////////////////////////////////
// CFolderDialog methods
//##ModelId=47E46ACB0143
BOOL CFolderDialog::Browse(HWND hwndOwner, LPCTSTR lpszDisplay, LPCTSTR lpszInitPath/*=NULL*/)
{
	m_hOwner = hwndOwner;
	m_strDisplay = lpszDisplay;
	m_strInitPath = lpszInitPath;

	return DoBrowse();
}

//##ModelId=47E46ACB0139
BOOL CFolderDialog::Browse(HWND hwndOwner, UINT nDisplay, LPCTSTR lpszInitPath/*=NULL*/)
{
	m_hOwner = hwndOwner;
	m_strDisplay.LoadString(nDisplay);
	m_strInitPath = lpszInitPath;

	return DoBrowse();
}

//##ModelId=47E46ACB014E
BOOL CFolderDialog::DoBrowse()
{
	LPMALLOC	 pMalloc; 
	LPITEMIDLIST pidlRoot(NULL);
	LPITEMIDLIST pidlResult(NULL);

	if (SHGetMalloc (&pMalloc)!= NOERROR) 
		return FALSE ; 

	if(m_nCSIDLRoot)
		::SHGetSpecialFolderLocation(m_hOwner, m_nCSIDLRoot, &pidlRoot);
	
	BROWSEINFO bi;
	ZeroMemory (&bi, sizeof(BROWSEINFO)); 
	bi.hwndOwner	= m_hOwner; 
	bi.lpszTitle	= m_strDisplay;
	bi.ulFlags		= m_uFlag;
	bi.lpfn			= m_pBrowseCallbackProc; //回调函数地址 
	bi.lParam		= (LPARAM)this;
	bi.pidlRoot		= pidlRoot;
	bi.pszDisplayName = NULL;//Address of a buffer to receive the display name of the folder selected by the user.

	// clear path
	this->m_strSelectPath.Empty();

	pidlResult = ::SHBrowseForFolder(&bi);
	if(pidlResult)
	{
		::SHGetPathFromIDList(pidlResult, m_strSelectPath.GetBuffer(MAX_PATH));
		m_strSelectPath.ReleaseBuffer();

		pMalloc ->Free(pidlResult); 
	}
	if(pidlRoot)
		pMalloc->Free(pidlRoot);
	pMalloc ->Release(); 

	// m_strSelectPath may be set on BrowseCallbackProc-BFFM_VALIDATEFAILED.
	return  CheckSelection(m_strSelectPath);
}

/////////////////////////////////////////////////////////////////////////////
// CFolderDialog message handlers
//##ModelId=47E46ACB01A8
BOOL CFolderDialog::CheckSelection(LPCTSTR lpszPath)
{
	if( (m_uFlag & BIF_BROWSEINCLUDEFILES))
		return ::PathFileExists(lpszPath);
	else
		return (::PathFileExists(lpszPath) && ::PathIsDirectory(lpszPath));
}


//##ModelId=47E46ACB01C7
int CALLBACK CFolderDialog::BrowseCallbackProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lData)
{
	const UINT nIDEdit = 0x3744;

	TCHAR szPath[MAX_PATH] = {0};
	CFolderDialog *pDlg = (CFolderDialog *)lData;

	switch(uMsg)
	{
	case BFFM_INITIALIZED:
		//////////////////////////////////////////////////////////////////////
		if(! (pDlg->m_uFlag & BIF_NEWDIALOGSTYLE))
		{
			// Make sizable
			CWnd::FromHandle(hWnd)->ModifyStyle(0, WS_THICKFRAME);
			::AppendMenu(::GetSystemMenu(hWnd, FALSE), MF_BYCOMMAND, SC_SIZE, _T("&Size"));

			// replace new wndproc.
			s_pOldWndProc = (WNDPROC)::SetWindowLong(hWnd, GWL_WNDPROC, (LONG)pDlg->m_pNewWndProc);

			// prepare for resize
			pDlg->OnAddSizeItems(hWnd);

			// Load last rect
			CFolderDialog::LoadLastRect(hWnd);
		}

		// set window text
		if(! pDlg->m_strTitle.IsEmpty())
			::SetWindowText(hWnd, pDlg->m_strTitle);

		// set EDITBOX readonly attribute
		if(pDlg->m_uFlag & BIF_EDITBOX)
			((CEdit *)CWnd::FromHandle(::GetDlgItem(hWnd, nIDEdit)))->SetReadOnly(pDlg->m_bEditReadonly);

		// set NewFolder button visible
		if(! pDlg->m_bShowCreateButton)
			::ShowWindow(::GetDlgItem(hWnd, 0x3746), FALSE);

		////////////////////////////////////////////////////////////////////////////////////
		// set init path
		if(pDlg->CheckSelection(pDlg->m_strInitPath))
		{
			::SendMessage( hWnd, BFFM_SETSELECTION, TRUE, (LPARAM)(LPCTSTR)pDlg->m_strInitPath);
			::SetDlgItemText(hWnd, nIDEdit, pDlg->m_strInitPath);
		}

		break;

	case BFFM_SELCHANGED:
		::SHGetPathFromIDList((LPCITEMIDLIST)lParam, szPath);
		::SendMessage( hWnd, BFFM_SETSTATUSTEXT, 0, (LPARAM)szPath);
		::SetDlgItemText(hWnd, nIDEdit, szPath);

		if(! pDlg->CheckSelection(szPath))
			::SendMessage( hWnd, BFFM_ENABLEOK, 0, FALSE);
		break;

	case BFFM_VALIDATEFAILED:

		// path is somecase illegal, eg. "c:\\windows", "abcde"  etc.
		::GetFullPathName((LPCTSTR)lParam, MAX_PATH, szPath, NULL);

		if(_tcsicmp(szPath, (LPCTSTR)lParam) != 0)
		{
			return BrowseCallbackProc(hWnd, BFFM_VALIDATEFAILED, (LPARAM)szPath, lData);
		}

		if(! pDlg->CheckSelection(szPath))
		{
			CString strMsg;
			if(! (pDlg->m_uFlag & BIF_BROWSEINCLUDEFILES))
			{
				strMsg.FormatMessage(pDlg->m_strQueryCreateFormat, szPath);
				if(IDYES != ::MessageBox(hWnd, strMsg, _T(""), MB_YESNO|MB_ICONQUESTION))
					return 1;

				if(! CreateAllDirectory(szPath))
				{
					strMsg.FormatMessage(AFX_IDP_FILE_BAD_PATH, szPath);// "%1 中包含无效的路径。"
					::MessageBox(hWnd, strMsg, _T(""), MB_ICONHAND);
					return 1;
				}
			}
			else
			{
				strMsg.FormatMessage(AFX_IDP_FILE_NOT_FOUND, szPath);// "没有找到 %1。"
				::MessageBox(hWnd, strMsg, _T(""), MB_ICONHAND);
				return 1;
			}
		}

		// notify dialog selection changed.
		::SendMessage(hWnd, BFFM_SETSELECTION, TRUE, (LPARAM)(LPCTSTR)szPath);
		pDlg->m_strSelectPath = szPath;

		break;
	}
	return 0;
}


//##ModelId=47E46ACB01DA
LRESULT CALLBACK CFolderDialog::NewWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_SIZE:
		if(wParam != SIZE_MINIMIZED)
		{
			CSizeItemList *pList;
			if( s_mapSizeItemList.Lookup(hWnd, pList))
			{
				int dx = LOWORD(lParam) - pList->m_sizeDlg.cx;
				int dy = HIWORD(lParam) - pList->m_sizeDlg.cy;

				pList->m_sizeDlg.cx = LOWORD(lParam);
				pList->m_sizeDlg.cy = HIWORD(lParam);
		
				POSITION pos = pList->GetHeadPosition();
				while(pos)
				{
					CSizeItem *pItem = (CSizeItem *)pList->GetNext(pos);
					CWnd *pCtrl = CWnd::FromHandle(pItem->hCtrl);

					if(::IsWindow(pItem->hCtrl))
					{
						if(pItem->mode & SIZE_RIGHT)
							pItem->rect.right += dx;
						if(pItem->mode & SIZE_BOTTOM)
							pItem->rect.bottom += dy;
						if(pItem->mode & MOVE_RIGHT)
							pItem->rect.OffsetRect(dx, 0);
						if(pItem->mode & MOVE_BOTTOM)
							pItem->rect.OffsetRect(0, dy);

						CWnd::FromHandle(pItem->hCtrl)->MoveWindow(pItem->rect);
					}
				}
				::InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;
	case WM_GETMINMAXINFO:
		{
			MINMAXINFO *lpMMI = ( MINMAXINFO FAR* )lParam;
			lpMMI->ptMinTrackSize.x = s_sizeMin.cx;
			lpMMI->ptMinTrackSize.y = s_sizeMin.cy;
		}
		break;
	case WM_COMMAND:
		if( HIWORD(wParam)==EN_CHANGE && LOWORD(wParam)==0x3744)// Editbox enchange
		{
			::SendMessage(hWnd, BFFM_ENABLEOK, 0, ::GetWindowTextLength((HWND)lParam)>0);
		}
		return (*s_pOldWndProc)(hWnd, uMsg, wParam, lParam);

	case WM_ERASEBKGND:
		{
			// exclude tree rect
			CRect rc;
			::GetWindowRect(::GetDlgItem(hWnd, 0x3741), &rc);
			CWnd::FromHandle(hWnd)->ScreenToClient(&rc);
			::ExcludeClipRect((HDC)wParam, rc.left, rc.top, rc.right, rc.bottom);
		}
		return (*s_pOldWndProc)(hWnd, uMsg, wParam, lParam);

	case WM_DESTROY:
		s_mapSizeItemList.RemoveAt(hWnd);
		CFolderDialog::SaveLastRect(hWnd);
		return (*s_pOldWndProc)(hWnd, uMsg, wParam, lParam);

	default:
		return (*s_pOldWndProc)(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

//##ModelId=47E46ACB0157
void CFolderDialog::OnAddSizeItems(HWND hDlg)
{
	const UINT nIDDisplay = 0x3742;// prompt text
	const UINT nIDPath	  = 0x3743;// the static control to show you selected path
	const UINT nIDEidt	  = 0x3744;// Edit box
	const UINT nIDTree	  = 0x3741;// Tree ctrl
	const UINT nIDOK	  = IDOK;
	const UINT nIDCancel  = IDCANCEL;

	AddSizeItem(hDlg, nIDDisplay,	SIZE_RIGHT);
	AddSizeItem(hDlg, nIDPath,		SIZE_RIGHT);
	AddSizeItem(hDlg, nIDEidt,		SIZE_RIGHT);
	AddSizeItem(hDlg, nIDTree,		SIZE_RIGHT|SIZE_BOTTOM);
	AddSizeItem(hDlg, nIDOK,		MOVE_BOTTOM|MOVE_RIGHT);
	AddSizeItem(hDlg, nIDCancel,	MOVE_BOTTOM|MOVE_RIGHT);

	CRect rcDlg;
	::GetClientRect(hDlg, &rcDlg);
	m_SizeItemList.m_sizeDlg = rcDlg.Size();

	s_mapSizeItemList.SetAt(hDlg, &m_SizeItemList);
}

//##ModelId=47E46ACB0161
void CFolderDialog::AddSizeItem(HWND hDlg, UINT nID, UINT mode)
{
	HWND hCtrl = ::GetDlgItem(hDlg, nID);
	if(! hCtrl)
		return;

	// get control's rect
	CRect rc;
	::GetWindowRect(hCtrl, &rc);
	CWnd::FromHandle(hDlg)->ScreenToClient(&rc);

	POSITION pos = m_SizeItemList.GetHeadPosition();
	while(pos)
	{
		CSizeItem *pItem = (CSizeItem *)m_SizeItemList.GetNext(pos);
		if(pItem->hCtrl == hCtrl)
		{
			pItem->mode = mode;
			pItem->rect = rc;
			return;
		}
	}

	CSizeItem *pItem = new CSizeItem;
	pItem->hCtrl = hCtrl;
	pItem->mode = mode;
	pItem->rect = rc;

	m_SizeItemList.AddTail((void *)pItem);
}

//##ModelId=47E46ACB01B2
BOOL CFolderDialog::IsNewUIUsable()
{
	return GetDllVersion(TEXT("Shlwapi.dll")) >= PACKVERSION(5, 0);
}

//##ModelId=47E46ACB01B4
void CFolderDialog::SaveLastRect(HWND hDlg)
{
	WINDOWPLACEMENT wp;
	if(::GetWindowPlacement(hDlg, &wp))
	{
		HKEY hKey;
		LONG lRet = RegCreateKeyEx(HKEY_LOCAL_MACHINE,   // Handle of an open key
			_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell"),		// Address of subkey name
			(DWORD) 0,       // Reserved value
			NULL,              // Address of class name
			(DWORD) 0,         // Special options flags
			KEY_WRITE,    // Desired security access
			NULL,              // Key security descriptor
			&hKey,				// Opened handle buffer
			NULL);             // What really happened
		if(lRet == ERROR_SUCCESS)
		{
			::RegSetValueEx(hKey, 
				_T("BrowseFolderRect"), 
				0, 
				REG_BINARY, 
				(CONST BYTE *)&wp.rcNormalPosition,
				sizeof(RECT));

			::RegCloseKey(hKey);
		}
	}
}

//##ModelId=47E46ACB01BE
BOOL CFolderDialog::LoadLastRect(HWND hDlg)
{
	CRect rc;
	BOOL bResult = FALSE;

	HKEY hKey;
	LONG lRet = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell"),	
					(DWORD) 0, 
					KEY_READ,     
					&hKey);    
	if(lRet == ERROR_SUCCESS)
	{
		DWORD dwType = REG_BINARY;
		DWORD dwSize = sizeof(RECT);

		lRet = ::RegQueryValueEx(hKey, 
					_T("BrowseFolderRect"), 
					0, 
					&dwType, 
					(LPBYTE)&rc,
					&dwSize);
		if(lRet == ERROR_SUCCESS)
		{
			if(dwType==REG_BINARY && dwSize==sizeof(RECT))
				bResult = TRUE;
		}

		::RegCloseKey(hKey);
	}

	if(bResult)
	{
		CWnd::FromHandle(hDlg)->MoveWindow(&rc);
	}

	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
namespace CFolderDialog_Helper
{
	BOOL IsDirectoryExist(LPCTSTR lpszDir)
	{
		if(lpszDir==NULL || _tcslen(lpszDir)==0)
			return FALSE;

		DWORD dwRet = ::GetFileAttributes(lpszDir);
		return dwRet!=-1 && (dwRet & FILE_ATTRIBUTE_DIRECTORY);
	}

	BOOL CreateAllDirectory(LPCTSTR lpszDir)
	{
		if(lpszDir==NULL || _tcslen(lpszDir)==0)
			return FALSE;
		
		// base case . . .if directory exists
		if(IsDirectoryExist(lpszDir))
			return TRUE;

		CString strDir(lpszDir);
		if(strDir.Right(1) == '\\')
			strDir = strDir.Left(strDir.GetLength()-1); 

		 // recursive call, one less directory
		int nFound = strDir.ReverseFind('\\');
		CString strSubDir = strDir.Left(nFound);

		if(! CreateAllDirectory(strSubDir) )
			return FALSE;

		return ::CreateDirectory(strDir,NULL); 
	}

	DWORD GetDllVersion(LPCTSTR lpszDllName)
	{
		HINSTANCE hinstDll;
		DWORD dwVersion = 0;

		hinstDll = LoadLibrary(lpszDllName);
		
		if(hinstDll)
		{
			DLLGETVERSIONPROC pDllGetVersion;

			pDllGetVersion = (DLLGETVERSIONPROC) GetProcAddress(hinstDll, "DllGetVersion");
			if(pDllGetVersion)
			{
				DLLVERSIONINFO dvi;
				HRESULT hr;

				ZeroMemory(&dvi, sizeof(dvi));
				dvi.cbSize = sizeof(dvi);

				hr = (*pDllGetVersion)(&dvi);

				if(SUCCEEDED(hr))
				{
					dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
				}
			}
        
			FreeLibrary(hinstDll);
		}
		return dwVersion;
	}
};
