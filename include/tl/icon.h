

#ifndef _TL_ICON_
#define	_TL_ICON_

//从文件中加载icon
HICON LoadIconFromFile(LPTSTR szName, UINT ntype = IMAGE_ICON);

//清理掉icon句柄
LONG  DestroyIcons(HICON icon);


//开始监视驱动安装，szDrv为要监控的驱动名称，目前为不为空就可以，是针对全部驱动进行操作
LONG SetupMontor(LPSTR szDrv);

//停止监视驱动安装，　szXX为要停止监控的驱动名称，目前是针对全部驱动进行操作
LONG KillMontor(LPTSTR szXX = NULL);

//驱动签名操作提示
LONG SetupTip(DWORD dwOperation);

#endif

