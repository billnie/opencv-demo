
#pragma once


//tip封装处理

//szName 为提示的bmp文件路径
//szInfo 为提示信息
//szTip  设计为标题，			暂时未使用
//szguid 为设计的tip guid使用，	暂时未使用
//dwAlway 为该线程实例句柄
//dwTimeout	汽泡显示的时长　0为不会自动关闭，但鼠标点击时会关，最长时长为10s即　10000
//			1s即为1000　2s即为2000...
long TipInfos(LPTSTR szName, LPTSTR szguid,
			 LPTSTR szTip, LPTSTR szInfo
			 , DWORD dwAlway, DWORD dwTimeout  );