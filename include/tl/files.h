

#pragma once

//取可执行程序路径
void GetPaths(HINSTANCE hins, LPTSTR );

//
void GetInsName(HINSTANCE hins, LPTSTR);

//判断文件是否存在
bool IsFileExist(LPTSTR s);

bool CreateGuids(LPTSTR sGuid);

//打开一个文件
//exam OpenOneFile(s, _T("notepad.exe")); s, 为文件名
bool OpenOneFile(LPCTSTR szFile, LPCTSTR szExe);

//取文件最后修改时间
bool GetLastWdTime(LPCTSTR sName, FILETIME *ft);

//取文件大小
DWORD GetFileSizes(LPTSTR s, DWORD &dwLow);
