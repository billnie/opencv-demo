

#pragma once
#include <wtypes.h>
#include <tchar.h>
//判断是否有文件存在
bool IsFileExist(LPCTSTR fname);

//创建目录
BOOL SfxCreatePath(LPTSTR szPath,int nStartPos = 2);
//判断文件目录是否存在
BOOL SfxIsDirectory(LPCTSTR szPath);

//注册组件
bool Register(LPCTSTR fname);
//注销组件
bool UnRegister(LPCTSTR fname);