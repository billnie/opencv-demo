﻿//
//  paybox.c
//  PayBox
//
//  Created by billnie on 3/8/13.
//  Copyright (c) 2013 rosschen. All rights reserved.
//

#include <stdio.h>
#include <sys/stat.h>


#include <stdio.h>
//#include <netinet/in.h>
#include <fcntl.h>
//#include <strings.h>
#include <stdlib.h>

#include <openssl/buffer.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/rand.h>
#include<openssl/sha.h>

#include <openssl/md5.h>
#define  LOGTAG    "posenc"
#ifndef MD5_DIGEST_LENGHT
#define MD5_DIGEST_LENGHT 16
#endif
#ifdef ANDROIDDEV
#include <assert.h>
#include <jni.h>
 #include <android/log.h>

#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGWF
#elif WIN32
#pragma comment(lib, "legacy_stdio_definitions.lib")
#if _MSC_VER>=1900
#include "stdio.h" 
_ACRTIMP_ALT FILE* __cdecl __acrt_iob_func(unsigned);
#ifdef __cplusplus 
extern "C"
#endif 
FILE* __cdecl __iob_func(unsigned i) {
	return __acrt_iob_func(i);
}
#endif /* _MSC_VER>=1900 */

#ifdef _DEBUG
//#pragma comment(lib,"..\\lib\openssl\\libeay32d.lib")	
//#pragma comment(lib,"..\\lib\\openssl\\ssleay32d.lib")
#else
//#pragma comment(lib,"..\\lib\openssl\\libeay32.lib")	
//#pragma comment(lib, "..\\lib\openssl\\ssleay32.lib")
#endif
#endif
//#include <sys/mman.h>
#include <fcntl.h>
//#include <unistd.h>



//#include "FrameState.h"
#include	"log.h"
#include "posenc.h"
#ifndef MD5_DIGEST_LENGHT
#define MD5_DIGEST_LENGHT 16
#endif
const char base[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

static char find_pos(char ch);

/* */
char *base64_encode(const char* data, int data_len)
{
    //int data_len = strlen(data);
    int prepare = 0;
    int ret_len;
    int temp = 0;
    char *ret = NULL;
    char *f = NULL;
    int tmp = 0;
    char changed[4];
    int i = 0;
    ret_len = data_len / 3;
    temp = data_len % 3;
    if (temp > 0)
    {
        ret_len += 1;
    }
    ret_len = ret_len*4 + 1;
    ret = (char *)malloc(ret_len);
    
    if ( ret == NULL)
    {
        printf("No enough memory.\n");
        exit(0);
    }
    memset(ret, 0, ret_len);
    f = ret;
    while (tmp < data_len)
    {
        temp = 0;
        prepare = 0;
        memset(changed, '\0', 4);
        while (temp < 3)
        {
            //printf("tmp = %d\n", tmp);
            if (tmp >= data_len)
            {
                break;
            }
            prepare = ((prepare << 8) | (data[tmp] & 0xFF));
            tmp++;
            temp++;
        }
        prepare = (prepare<<((3-temp)*8));
        //printf("before for : temp = %d, prepare = %d\n", temp, prepare);
        for (i = 0; i < 4 ;i++ )
        {
            if (temp < i)
            {
                changed[i] = 0x40;
            }
            else
            {
                changed[i] = (prepare>>((3-i)*6)) & 0x3F;
            }
            *f = base[changed[i]];
            //printf("%.2X", changed[i]);
            f++;
        }
    }
    *f = '\0';
    
    return ret;
}
/* */
static char find_pos(char ch)
{
    char *ptr = (char*)strrchr(base, ch);//the last position (the only) in base[]
    return (ptr - base);
}
/* */
char *base64_decode(const char *data, int data_len, int *retlen)
{
    int ret_len = (data_len / 4) * 3;
    int equal_count = 0;
    char *ret = NULL;
    char *f = NULL;
    int tmp = 0;
    int temp = 0;
    char need[32] ={0};
    int prepare = 0;
    int i = 0;
    int cnt = 0;
    
    if (*(data + data_len - 1) == '=')
    {
        equal_count += 1;
    }
    if (*(data + data_len - 2) == '=')
    {
        equal_count += 1;
    }
    if (*(data + data_len - 3) == '=')
    {//seems impossible
        equal_count += 1;
    }
    switch (equal_count)
    {
        case 0:
            ret_len += 4;//3 + 1 [1 for NULL]
            break;
        case 1:
            ret_len += 4;//Ceil((6*3)/8)+1
            break;
        case 2:
            ret_len += 3;//Ceil((6*2)/8)+1
            break;
        case 3:
            ret_len += 2;//Ceil((6*1)/8)+1
            break;
    }
    ret = (char *)malloc(ret_len);
    if (ret == NULL)
    {
        printf("No enough memory.\n");
        exit(0);
    }
    memset(ret, 0, ret_len);
    f = ret;
    while (tmp < (data_len - equal_count))
    {
        temp = 0;
        prepare = 0;
        memset(need, 0, 4);
        while (temp < 4)
        {
            if (tmp >= (data_len - equal_count))
            {
                break;
            }
            prepare = (prepare << 6) | (find_pos(data[tmp]));
            temp++;
            tmp++;
        }
        prepare = prepare << ((4-temp) * 6);
        for (i=0; i<3 ;i++ )
        {
            if (i == temp)
            {
                break;
            }
            *f = (char)((prepare>>((2-i)*8)) & 0xFF);
            f++;
            cnt ++;
        }
    } 
    *f = '\0';
    *retlen = cnt;
    return ret; 
}
unsigned char asc2hex(unsigned char b)
{
	unsigned char x = 0;
	if (b >= 0x30 && b <= 0x39) {
		x = b - 0x30;
	}
	else if(b>= 'a' && b <= 'f'){
		x = b - 'a' + 10;
	}
	else if(b >= 'A' && b<='F')
	{
		x = b - 'A' + 10;
	}
	return x;
}

unsigned char* str2hex(const char *str, int* len)
{
	unsigned char *buf = NULL;
	const char *pa =NULL;
	if (str  )
    {
		int cnt;
		int nlen;
        int i;
		unsigned char x;
		cnt = strlen(str);
		nlen = cnt * 2 + 4;
		buf = (unsigned char*)malloc(nlen);
		memset(buf, 0, nlen);
		pa = str;
		
		for (i = 0; i < cnt; i+=2)
        {
			x = asc2hex(pa[i]);
			buf[i/2] = (x <<4) | asc2hex(pa[i+1]);
		}
		*len = cnt/2;
	}
	return buf;
}
char * skipblank(char *buf)
{
	char *s, *ss;
	s = buf;
	ss = buf;
	if(buf)
	{
        
		while(*buf)
		{
			while(*buf ==' ')
				buf++;
			*s = *buf;
			s++;
			buf ++;
		}
		*s = '\0';
	}
	return ss;
}
int strtokey(RSA**r, char *s, int len)
{
	if (s && len > 10)
	{
		const unsigned char *pp;
		pp =(const unsigned char*)s;
		*r = d2i_RSA_PUBKEY(r, &pp, len);
		if (*r) {
			return 1;
		}
	}
	return 0;
}
int evpkey(const char *s, int len, char **out)
{
    int tlen = 0;
   	if (s!=NULL && len > 10)
	{
        int i, k;
        int bl;
        EVP_ENCODE_CTX ctx;
        BUF_MEM *dataB = NULL;
     
        *out = NULL;

		dataB = BUF_MEM_new()  ;

        BUF_MEM_grow(dataB, len+32);
        memcpy(dataB->data, s, len);

        bl = len;

        EVP_DecodeInit(&ctx);
        i = EVP_DecodeUpdate(&ctx, (unsigned char*)dataB->data, &bl,
                             (unsigned char*)dataB->data, bl);
        if (i < 0) {

        }
        i = EVP_DecodeFinal(&ctx, (unsigned char*)dataB->data, &k);
        bl +=k;
        tlen = bl;

        if (bl > 0) {
            *out = (char*)malloc(tlen);
            memcpy(*out, dataB->data, tlen);
        }

        OPENSSL_free(dataB);
	}
	return tlen;
}
char * decodeCode(const char *strKey, int *len)
{
    //鍔犲叆鎹㈣绗?    char *keyStr;
    char strline[68] = {0};
    int i;
    int cnt;
    int tlen;
	char *keyStr;
    tlen = strlen(strKey);
    keyStr = (char *)malloc(tlen+64);
    
    memset(keyStr, 0, tlen+16);
    
    for (i=0; i < tlen; i+=64) {
        if (tlen -i < 64) {
            cnt = tlen-i;
        }
        else
            cnt = 64;
        memcpy(strline, strKey+i, cnt);
        strline[cnt] = '\0';
        strcat(keyStr, strline);
        strcat(keyStr, "\n");
    }
    tlen = strlen(keyStr);
    
    *len = tlen;
    return keyStr;
}
char *hex2bstr(unsigned char* c, int len)
{
	int  i;
	unsigned char * str;
//	char buf[2048] = {0};
	char strTemp[8] = {0};

	str = (unsigned char*)malloc(len *3 + 4);
	memset(str, 0, len*3 + 4);

	for (i =0; i < len; i++) {
		if(i>0 )
			sprintf((char*)&str[i*3-1] , " %02x", c[i]);
		else
			sprintf((char*)&str[i*2] , "%02x", c[i]);
	}
	return (char*)str;
}
char* hextStr(unsigned char* c, int len)
{
	//
	int  i;
	unsigned char * str;

	char strTemp[8] = {0};

	str = (unsigned char*)malloc(len *2 + 4);
	memset(str, 0, len*2 + 4);

	for (i =0; i < len; i++) {
        sprintf((char*)&str[i*2] , "%02x", c[i]);
	}

    //sprintf(buf, "hextStr, %s", str	);
    //logs(LOGINFO, LOGTAG, buf);

	return (char*)str;
}
int PrivarykeyFromBIO(RSA**r, const unsigned char*byte, int len  )
{
    char buf[255] = {0};
    if (byte && len > 10 ) {
        BIO *mem;
        BUF_MEM *biobuf = NULL;
		RSA *rsa;

        mem = BIO_new_mem_buf((void*)byte, len);
        
        
        BIO_get_mem_ptr(mem, &biobuf);
        
        rsa = d2i_RSAPrivateKey_bio(mem, NULL);
        *r = rsa;
        sprintf(buf, "PrivarykeyFromBIO, ret = 0x%08x len = %d ",rsa, len);
        logs(LOGVERBOSE, LOGTAG, buf);
        BIO_free(mem);
        
        if(rsa)
            return 1;
    }
    
    return 0;
}

int PublickeyFromBIO(RSA**r, const unsigned char*byte, int len  )
{
    if (byte && len > 10 ) {
        BIO *mem;
        RSA *rsa;
        BUF_MEM *biobuf = NULL;

        mem = BIO_new_mem_buf((void*)byte, len);
        
        
        BIO_get_mem_ptr(mem, &biobuf);
        
        rsa = d2i_RSAPublicKey_bio(mem, NULL);
        
        *r = rsa;
        
        BIO_free(mem);
        
        if(rsa)
            return 1;
    }
    
    return 0;
}
int checkRSA(const unsigned char *key, int data_len, int type)
{
    int ret = 0;
    if (key && data_len > 10) {
        RSA *rsa =NULL;
        
        if (type == 0) {    //鍏墢
            ret = PublickeyFromBIO(&rsa, key, data_len);
        }
        else if(type == 1)  //绉侀挜
        {
            ret = PrivarykeyFromBIO(&rsa,key,  data_len);
        }
        if(rsa )
        {
            ret = RSA_size(rsa);
            RSA_free(rsa);
        }
    }
    return ret;
}
int encodeByBioPubKey(const char *pubKey, const char *mes, char **encBody)
{
    int ret = 0;
    if (pubKey && mes) {
		RSA *rsa_=NULL;
		int len;
        unsigned char *s;
        
		s = str2hex(pubKey, &len);
		if (s) {
			char *buf;
			PublickeyFromBIO(&rsa_, s, len );
			
			len ++;
			free(s);
            
			

			len = strlen(mes);
			
			buf = (char*)malloc(1024*4);
            memset(buf, 0, 1024*4);
            
			if (rsa_) {
				len = RSA_public_encrypt(len, (const unsigned char *)mes, (unsigned char*)buf, rsa_,RSA_PKCS1_PADDING);
				if (len >= 0) {
					//杞崲涓轰覆
                    //杞崲涓轰覆
                    *encBody = hextStr((unsigned char*)buf, len);
                    ret = len*2;
                    
                    sprintf(buf, "encodeByPubKey, RSA_public_encrypt ret = %d", len	);
                    logs(LOGVERBOSE, LOGTAG, buf);
                }
				RSA_free(rsa_);
			}
            free(buf);
		}
	}
    return ret;
}
int encodeByPubKey(const char *pubKey, const char *mess, char **encBody)
{
	int len;
	int ret = 0;
	char *s=NULL;
	char *outStr = NULL;
    char buf[1280] = {0};
	if (pubKey!=NULL && mess!=NULL && encBody !=NULL) {
		RSA *rsa_=NULL;
		len = strlen(pubKey);

		if (pubKey) {
			//Âú®Ê≠§Ë¶ÅÊ†ºÂºèÂåñ‰∏Ä‰∏?            len = evpkey(pubKey, len, &s);

            sprintf(buf, "encodeByPubKey,mess = %s evpkey ret = %d", mess, len	);
            logs(LOGVERBOSE, LOGTAG, buf);
            
            if(len > 0)
            {
				char *buf;
    			len = strtokey(&rsa_, s, len);

    			free(s);
                
    			
  
    			len = strlen(mess);

    			buf = (char*)malloc(1024*4);
    			memset(buf, 0, 1024*4);
                
    			if (rsa_) {
    				len = RSA_public_encrypt(len, (const unsigned char*)mess, (unsigned char*)buf, rsa_,RSA_PKCS1_PADDING);

    				if (len >= 0) {
    					//ËΩ¨Êç¢‰∏∫‰∏≤
    					*encBody = hextStr((unsigned char*)buf, len);
    					ret = len*2;

        	            sprintf(buf, "encodeByPubKey, RSA_public_encrypt ret = %d", len	);
        	            logs(LOGVERBOSE, LOGTAG, buf);
    				}
    				RSA_free(rsa_);
    			}
                free(buf);
            }
		}
	}
	return ret;
}
int encodeByBIOPriKey(const char *priKey, const unsigned char * mess, int data_len, char **encBody)
{
    int ret = 0;
    if (priKey && mess && encBody && data_len > 0) {
        int tlen;
        char buf[1024] = {0};
        
        unsigned char *DecryptBuff = NULL;
        int clength;
        unsigned char *ct ;
        char *keystr = NULL;
        RSA *rs = NULL;
        
        //ct = str2hex(mess, &clength);
        
        keystr = (char*)str2hex(priKey, &tlen);
        
        sprintf(buf, "encodeByBIOPriKey, ret = %d %d, mes = %s", data_len	, tlen, mess);
        logs(LOGVERBOSE, LOGTAG, buf);
        
        //tlen = 610;
        if(PrivarykeyFromBIO(&rs,(const unsigned char*) keystr, tlen)==1)
        {
            logs(LOGVERBOSE, LOGTAG, "begin encodeByBIOPriKey");
            tlen = RSA_size(rs);
            DecryptBuff = (unsigned char*)malloc(tlen);
            memset(DecryptBuff, 0, tlen);
            
            logs(LOGVERBOSE, LOGTAG, "begin encodeByBIOPriKey");
            ret = RSA_private_encrypt(data_len, mess, DecryptBuff, rs,
                                      RSA_PKCS1_PADDING);
            
            //result = hextStr(DecryptBuff, clength);
            
            sprintf(buf, "encodeByBIOPriKey, ret = %d %s", data_len	, DecryptBuff);
            logs(LOGVERBOSE, LOGTAG, buf);
            *encBody =(char*) DecryptBuff;

            //ret = tlen;
            RSA_free(rs);
        }
        //free(ct);
        free(keystr);
        
        

    }
    return ret;
}
char * decodeByPrivteKey(const char *priKey, const char *mess, int *slen)
{
    int tlen;
    int ret;
    char buf[1024] = {0};

	unsigned char *DecryptBuff = NULL;
	int clength;
	unsigned char *ct ;
    char *keystr = NULL;
    RSA *rs = NULL;
 
    ct = str2hex(mess, &clength);
    
    keystr = (char*)str2hex(priKey, &tlen);
    
    sprintf(buf, "decodeByPrivteKey, ret = %d %d, mes = %s", clength	, tlen, mess);
    logs(LOGVERBOSE, LOGTAG, buf);

    //tlen = 610;
    if(PrivarykeyFromBIO(&rs, (const unsigned char*)keystr, tlen)==1)
    {
        logs(LOGVERBOSE, LOGTAG, "begin RSA_private_decrypt");
        tlen = RSA_size(rs);
        DecryptBuff = (unsigned char *)malloc(tlen);
        memset(DecryptBuff, 0, tlen);
        
        logs(LOGVERBOSE, LOGTAG, "begin RSA_private_decrypt");
        ret = RSA_private_decrypt(clength, ct, DecryptBuff, rs,RSA_PKCS1_PADDING);
        
        //result = hextStr(DecryptBuff, clength);
        
        sprintf(buf, "decodeByPrivteKey, ret = %d %s", clength	, DecryptBuff);
        logs(LOGVERBOSE, LOGTAG, buf);
        
        //free(DecryptBuff);
        *slen = ret;
        RSA_free(rs);
    }
    free(ct);
    free(keystr);
    
    return (char*)DecryptBuff;
}
char * decodeByPubKey(const char *pubKey, const unsigned char *mess, int *slen)
{
    int tlen;
    int ret =0;
    char buf[1024] = {0};
    
	unsigned char *DecryptBuff = NULL;
	int clength;
	unsigned char *ct ;
    char *keystr = NULL;
    RSA *rs = NULL;
    
    //ct = str2hex(mess, &clength);
    
    keystr = (char*)str2hex(pubKey, &tlen);
    
    //sprintf(buf, "decodeByPrivteKey, ret = %d %d, mes = %s", clength	, tlen, mess);
    //logs(LOGVERBOSE, LOGTAG, buf);
    
    //tlen = 610;
    if(PublickeyFromBIO(&rs, (unsigned char *)keystr, tlen)==1)
    {
        logs(LOGVERBOSE, LOGTAG, "begin RSA_private_decrypt");
        tlen = RSA_size(rs);
        DecryptBuff =(unsigned char *) malloc(tlen);
        memset(DecryptBuff, 0, tlen);
        
        logs(LOGVERBOSE, LOGTAG, "begin RSA_private_decrypt");
        ret = RSA_public_decrypt(*slen, mess, DecryptBuff, rs,RSA_PKCS1_PADDING);
        
        //result = hextStr(DecryptBuff, clength);
        
        //sprintf(buf, "decodeByPrivteKey, ret = %d %s", clength	, DecryptBuff);
        //logs(LOGVERBOSE, LOGTAG, buf);
        
        //free(DecryptBuff);
        *slen = ret;
        RSA_free(rs);
    }
    //free(ct);
    free(keystr);
    
    return (char*)DecryptBuff;
 
}
int rsa_Sign(const char *priKey, const unsigned char *mess, int data_len, int nid, char **sigBody)
{
    int tlen;
    int ret = 0;
    char buf[1024] = {0};
    
	unsigned char *DecryptBuff = NULL;
	int clength;
	unsigned char *ct ;
    char *keystr = NULL;
    RSA *rs = NULL;
    unsigned char md[64+1];

    ct = (unsigned char *)str2hex((const char*)mess, &clength);
    
    keystr = (char*)str2hex((const char*)priKey, &tlen);
    
    sprintf(buf, "rsa_Sign, ret = %d %d, mes = %s", clength	, tlen, mess);
    logs(LOGVERBOSE, LOGTAG, buf);
    
    
    memset(md, 0, 64 +1);
    if (nid == NID_md5) {
        ret = EVP_Digest((const void *)ct, clength, (unsigned char *)md, (unsigned int *)&clength, EVP_md5(), NULL);
    }
    else if(nid == NID_sha1)
    {
        ret = EVP_Digest((const void *)ct, clength, (unsigned char *)md, (unsigned int *)&clength, EVP_sha1(), NULL);
    }
    if(ret != 0)
    {
        //tlen = 610;
        if(PrivarykeyFromBIO(&rs, (unsigned char*)keystr, tlen)==1)
        {
            logs(LOGVERBOSE, LOGTAG, "begin rsa_Sign");
            tlen = RSA_size(rs);
            DecryptBuff =(unsigned char*) malloc(tlen);
            memset(DecryptBuff, 0, tlen);
            tlen = 16;
            logs(LOGVERBOSE, LOGTAG, "begin rsa_Sign");
            ret = RSA_sign(nid, (const unsigned char *)md, clength, (unsigned char*)DecryptBuff, (unsigned int *)&tlen, rs);

            sprintf(buf, "rsa_Sign, ret = %d %s", clength	, DecryptBuff);
            logs(LOGVERBOSE, LOGTAG, buf);

            *sigBody =(char*) DecryptBuff;
            RSA_free(rs);
        }
    }
    free(ct);
    free(keystr);
    
    return tlen;
}
int very_Sign(const char *pubKey, const unsigned char *mess, int data_len, int nid, char **sigBody)
{
    int tlen;
    int ret;
    char buf[1024] = {0};
    
	unsigned char *DecryptBuff = NULL;
	int clength=0;
	unsigned char *ct =NULL;
    char *keystr = NULL;
    RSA *rs = NULL;
    
    //ct = str2hex(mess, &clength);
    
    keystr = (char*)str2hex(pubKey, &tlen);
    
    sprintf(buf, "very_Sign, ret = %d %d, mes = %s", clength	, tlen, mess);
    logs(LOGVERBOSE, LOGTAG, buf);
    
    //tlen = 610;
    if(PublickeyFromBIO(&rs, (unsigned char*)keystr, tlen)==1)
    {
        logs(LOGVERBOSE, LOGTAG, "begin very_Sign");
        tlen = RSA_size(rs);
        DecryptBuff = (unsigned char*)malloc(tlen);
        memset(DecryptBuff, 0, tlen);
        
        logs(LOGVERBOSE, LOGTAG, "begin very_Sign");
        ret = RSA_verify(nid, mess,data_len ,DecryptBuff, tlen,rs );
        
        //result = hextStr(DecryptBuff, clength);
        
        sprintf(buf, "very_Sign, ret = %d %s", clength	, DecryptBuff);
        logs(LOGVERBOSE, LOGTAG, buf);
        
        //free(DecryptBuff);
        *sigBody = (char*)DecryptBuff;
        RSA_free(rs);
    }
    free(ct);
    free(keystr);
    
    return tlen;
}

char *md5Str(const char*str)
{
    if(str !=NULL)
    {
        unsigned char md[MD5_DIGEST_LENGHT+1];
        memset(md, 0, MD5_DIGEST_LENGHT +1);
        if(EVP_Digest(str, strlen(str), md, NULL, EVP_md5(), NULL) != 0)
        {
            return hextStr(md, MD5_DIGEST_LENGHT);
        }
    }
    return NULL;
}

int sha(const unsigned char *data, int data_len, unsigned char **encBody)
{
    int ret = 0;
    if  (data && data_len > 0 && encBody!=NULL)
    {
        //int tlen;
        unsigned char * t1, *t2, *t3;
		SHA_CTX ctx;
        //t1 = (unsigned char *) malloc(data_len);
        *encBody = (unsigned char *) malloc(data_len);
        memset(*encBody, 0 , data_len);
        //t3 = SHA1(data,data_len,t1);
        
        ret = SHA1_Init(&ctx);
        ret = SHA1_Update(&ctx, data, data_len);
        ret = SHA1_Final(*encBody,&ctx);
        if (ret ==1)
            ret = 20;
    }
    return ret;
}

int seedRand(int data_len, unsigned char **randBody)
{
    int ret = 0;
    if  ((data_len% 8) == 0 && data_len > 0 && randBody!=NULL)
    {
        *randBody = (unsigned char *) malloc(data_len>>3);
        
        ret = RAND_pseudo_bytes(*randBody,  data_len>>3);
        if (ret ==1)
            ret = data_len>>3;
    }
    return ret;
}

