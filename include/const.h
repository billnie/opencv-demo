
//const.h
#ifndef _CONST_DEFINE_H
#define _CONST_DEFINE_H

#include <string>
#include <vector>
using namespace std;

#define	MAX_FILTER_NAME 90
typedef struct _tagAudioRecData
{
	WAVEFORMATEX wav;
	int formatid;			//格式索引号
	LCID dwlangu;			//格式语言
	TCHAR szFilterName[MAX_FILTER_NAME];
}AudioRecData, *LPAudioRecData;

typedef std::basic_string<TCHAR> TString;
typedef std::vector<TString>  VectorString;

typedef std::vector<WAVEFORMATEX>VectorWavFormat;

typedef std::vector<AudioRecData>VectorAudioRecData;
#endif 