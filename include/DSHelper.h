///////////////////////////////////////////////////////////////////////////////////
// DSHelper.h

#if !defined(__DSHELPER_H__)
#define __DSHELPER_H__

//#include <streams.h>
#include <dshowasf.h>
#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

//
// Tear down everything downstream of a given filter, remove downdtream from graph if set
//
void	DS_NukeDownstream(IN IFilterGraph *pGraph, 
						  IN IBaseFilter *pFilter,
						  IN BOOL bRemoveFilter = TRUE); 
//
// Tear down everything upstream of a given filter, remove upstream from graph if set
//
void	DS_NukeUpstream(IN IFilterGraph * pGraph, 
						IN IBaseFilter * pFilter, 
						IN BOOL bRemoveFilter = TRUE); 


BOOL	DS_FindPin(OUT IPin** ppPin, 
				   IN  IBaseFilter* pFilter, 
				   IN  GUID MajorType, 
				   IN  PIN_DIRECTION Dir);
//从指定filter中查找某个输出脚
BOOL	DS_FindPin(OUT IPin** ppPin, 
				   IN  IBaseFilter* pFilter, 
				   IN  LPCTSTR pszName,
				   IN  PIN_DIRECTION Dir); 

BOOL	DS_FindConnectedPin (OUT IPin **ppPin, 
							 IN	 IBaseFilter *pFilter,
							 IN  GUID majorType,
							 PIN_DIRECTION PinDir);

BOOL	DS_ConnectFilter(IN	 IBaseFilter *pUpFilter,
						 IN	 LPCTSTR lpszUpPinName,
						 IN	 IBaseFilter *pDownFilter,
						 IN	 LPCTSTR pDownPinName,
						 OUT IPin	**ppUpPin = NULL,
						 OUT IPin	**ppDownPin = NULL);

BOOL	DS_ConnectFilter(IN	 IBaseFilter *pUpFilter,
						 IN	 GUID typeUpPin,
						 IN	 IBaseFilter *pDownFilter,
						 IN	 GUID typeDownPin,
						 OUT IPin	**ppUpPin = NULL,
						 OUT IPin	**ppDownPin = NULL);

BOOL	DS_ConnectToFilter(	IN	 IPin	*pUpPin,
							IN	 IBaseFilter *pDownFilter,
							IN	 LPCTSTR pDownPinName,
							OUT IPin	**ppDownPin = NULL);

BOOL	DS_ConnectToFilter(	IN	 IPin	*pUpPin,
							IN	 IBaseFilter *pDownFilter,
							IN	 GUID typeDownPin,
							OUT IPin	**ppDownPin = NULL);


BOOL	DS_AddFilter(IN  IFilterGraph *pGraph, 
					 IN  const GUID *pGUID, 
					 IN  LPCTSTR lpszName,
					 OUT IBaseFilter **ppFilter = NULL);
bool	DS_CreateFilter(IN const GUID *pGUID,
						OUT IBaseFilter **ppFilter );
BOOL DS_AddFilter(IFilterGraph *pGraph, const GUID *clsidDeviceClass, LPCTSTR szDevName,
				  LPCTSTR szAlias,IBaseFilter **ppFilter);

BOOL	DS_FindFilterByName(IN  IFilterGraph *pGraph, 
							IN  LPCTSTR lpszFilterName,
							OUT IBaseFilter **ppFilter);

//通过设备名创建某类设备中某个设备
BOOL	DS_CreateFilterByName(
							  IN const GUID *clsidDeviceClass, 
							  IN LPCTSTR szDevName,
							  OUT IBaseFilter **ppFilter);
BOOL	DS_GetDXVersion(OUT LONGLONG *pllVer);

BOOL	DS_IsOverlayUsable();

//搜索某类如压缩类，采集类等的filter的集合
BOOL	DS_LookUpFilter(
						IN const GUID *clsidDeviceClass, 
						OUT VectorString &);
//搜索filter所有的pin脚
BOOL	DS_EnumFilterPin(
						 IN IBaseFilter *pFilter, 
						 IN PIN_DIRECTION pinDir, 
						 OUT VectorString &r);

//取得音频采集输出口的所有媒体类型
BOOL	DS_EnumAudioCapMedType(
						  IN const GUID *clsidDeviceClass, 
						  IN LPCTSTR sDevName, 
						  IN LPCTSTR strPinName, 
						  OUT VectorString &);
//取得Pin口的所有的媒体类型
BOOL	DS_EnumPinMedType(
						  IN IPin *, 
						  OUT VectorString &);
//取得Pin口的所有的媒体类型通过枚举脚取得
BOOL	DS_EnumPinEMedType(
						  IN IPin *, 
						  OUT VectorWavFormat &);
//取得filter某个pin脚的所有媒体类型
BOOL	DS_EnumFilterMedType(
						  IN IBaseFilter *pFilter, 
						  IN LPCTSTR szPinName, 
						  IN PIN_DIRECTION pinDir,
						  OUT VectorWavFormat &);
//取得filter所有某个脚的媒类型
BOOL	DS_GetFilterCurMedType(
						  IN IBaseFilter *pFilter, 
						  IN LPCTSTR szPinName, 
						  IN PIN_DIRECTION pinDir,
						  OUT WAVEFORMATEX* wavfmt);
//设置filter某个pin脚的媒体类型
BOOL	DS_SetFilterCurMedType(
						  IN IBaseFilter *pFilter,
						  IN LPCTSTR szPinName, 
						  IN PIN_DIRECTION pinDir,
						  IN WAVEFORMATEX* wavfmt);
BOOL	DS_FindFilterMedTye(
						  IN IPin *pPin,
						  IN WAVEFORMATEX *wavfmt,
						  OUT LPWAVEFORMATEX *nwavfmt
						  );
//选取某个输入脚，作为源pin
BOOL	DS_FilterDesignPin(
						  IN IBaseFilter *pFilter, 
						  IN PIN_DIRECTION pinDir, 
						  IN LPCTSTR szPinName);
BOOL	DS_SetFileName(
						  IN IBaseFilter *pFilter, 
						  IN LPADData nType
						  );
BOOL	DS_Confingwma(		
							IN IBaseFilter *pFilter);
bool	DS_LoadWmaProfile(	IN IBaseFilter *pFilter, 
							IN LPCTSTR szfname);
//媒体类型结构清理
void	MyFreeMediaType(IN AM_MEDIA_TYPE* mt);
// Debug helpers
//
BOOL	DS_AddGraphToRot(IN  IUnknown *pUnkGraph, 
						 OUT DWORD *pdwRegister); 
void	DS_RemoveGraphFromRot(IN const DWORD dwRegister);

void	DS_CreateProfile(IWMProfile **pProfile);
void	DS_LoadProfile(IWMProfile **pProfile);
#endif