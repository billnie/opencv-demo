// GtDeal.cpp: implementation of the CGtDeal class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "stdio.h"
#include "tchar.h"
#include "dt.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BOOL DebugTrace(TCHAR *lpszFormat,...)
{
	static HWND hwnd = ::FindWindow(_T("Fhua-DbgView"),	_T("DbgView"));
	if(!IsWindow(hwnd))
		hwnd = ::FindWindow(NULL, _T("DbgView"));
	if(hwnd)
	{
		static TCHAR szMsg[512];
		va_list argList;
		va_start(argList, lpszFormat);
		try
		{
			_vstprintf(szMsg,lpszFormat, argList);
		}
		catch(...)
		{
			lstrcpy(szMsg ,_T("DebugHelper:Invalid string format!"));
		}
		va_end(argList);
		DWORD dwId = GetCurrentProcessId();
		::SendMessage(hwnd,WM_SETTEXT,dwId,(LPARAM)(LPCTSTR)szMsg);
	}
	return TRUE;
}